Imports System.Data.SqlClient

Public Class DALDataException
    Inherits Exception

    Private Const cClassName As String = "DALDataException"
    Private Const cClassVersion As String = "1.0"
    Private Const cClassRelease As String = "a"
    Private Const cClassDescrip As String = "Exception class for use in the DAL Library."

    Private mErrorDate As Date
    Private mErrorTime As Date
    Private mDisplayTitle As String
    Private mDisplaySuggestion As String
    Private mExceptionType As String
    Private mExceptionModule As String
    Private mExceptionFunction As String
    Private mExceptionMisc As String
    Private mExceptionIdentifier As String
    Private mExceptionCode As Integer

    Private Sub Initialize()
        Me.mErrorDate = Today
        Me.mErrorTime = Now
        Me.mDisplayTitle = ""
        Me.mDisplaySuggestion = ""
        Me.mExceptionType = ""
        Me.mExceptionFunction = ""
        Me.mExceptionModule = ""
        Me.mExceptionMisc = ""
        Me.mExceptionIdentifier = ""
        Me.mExceptionCode = 0
    End Sub
    Public Sub New()
        MyBase.New("Data Exception - No Specific Message Passed")
        Initialize()
        ErrorDate = Today
        ErrorTime = Now
        Title = "DAL Library Data Exception"
        Suggestion = "A DAL Library Data Exception has been thrown.  No suggested resolution."
        ExceptionType = "Unspecified"
        GenerateId()
    End Sub
    Public Sub New(ByVal messageIn As String)
        MyBase.New(messageIn)
        Initialize()
        ErrorDate = Today
        ErrorTime = Now
        Title = "DAL Library Data Exception"
        Suggestion = "A DAL Library Data Exception has been thrown.  No suggested resolution."
        ExceptionType = "Unspecified"
        GenerateId()
    End Sub
    Public Sub New(ByVal messageIn As String, ByVal codeIn As Integer)
        MyBase.New(messageIn)
        Initialize()
        Me.mExceptionCode = codeIn
        ErrorDate = Today
        ErrorTime = Now
        Title = "DAL Library Data Exception"
        Suggestion = "A DAL Library Data Exception has been thrown.  No suggested resolution."
        ExceptionType = "Unspecified"
        GenerateId()
    End Sub
    Public Sub New(ByVal messageIn As String, ByVal errorDateIn As Date, ByVal errorTimeIn As Date)
        MyBase.New(messageIn)
        Initialize()
        Me.ErrorDate = errorDateIn
        Me.ErrorTime = errorTimeIn
        Title = "DAL Library Data Exception"
        Suggestion = "A DAL Library Data Exception has been thrown.  No suggested resolution."
        ExceptionType = "Unspecified"
        GenerateId()
    End Sub
    Public Sub New(ByVal messageIn As String, ByVal errorDateIn As Date, ByVal errorTimeIn As Date, ByVal titleIn As String, ByVal suggestionIn As String, ByVal excTypeIn As String)
        MyBase.New(messageIn)
        Initialize()
        Me.ErrorDate = errorDateIn
        Me.ErrorTime = errorTimeIn
        Title = titleIn
        Suggestion = suggestionIn
        ExceptionType = excTypeIn
        GenerateId()
    End Sub
    Public Sub New(ByVal messageIn As String, ByVal errorDateIn As Date, ByVal errorTimeIn As Date, ByVal titleIn As String, ByVal suggestionIn As String, ByVal excTypeIn As String, ByVal codeIn As Integer)
        MyBase.New(messageIn)
        Initialize()
        Me.ErrorDate = errorDateIn
        Me.ErrorTime = errorTimeIn
        Me.ExceptionCode = codeIn
        Title = titleIn
        Suggestion = suggestionIn
        ExceptionType = excTypeIn
        GenerateId()
    End Sub

    Public Property ErrorDate() As Date
        Get
            Return mErrorDate.ToShortDateString
        End Get
        Set(ByVal Value As Date)
            mErrorDate = Value
        End Set
    End Property
    Public Property ErrorTime() As Date
        Get
            Return mErrorTime.ToShortTimeString
        End Get
        Set(ByVal Value As Date)
            mErrorTime = Value
        End Set
    End Property
    Public Property ExceptionCode() As Integer
        Get
            Return Me.mExceptionCode
        End Get
        Set(ByVal Value As Integer)
            Me.mExceptionCode = Value
        End Set
    End Property
    Public Property Title() As String
        Get
            Return mDisplayTitle
        End Get
        Set(ByVal Value As String)
            mDisplayTitle = Value
        End Set
    End Property
    Public Property Suggestion() As String
        Get
            Return mDisplaySuggestion
        End Get
        Set(ByVal Value As String)
            mDisplaySuggestion = Value
        End Set
    End Property
    Public Property ExceptionType() As String
        Get
            Return mExceptionType
        End Get
        Set(ByVal Value As String)
            mExceptionType = Value
        End Set
    End Property
    Public Property ExeptionModule() As String
        Get
            Return mExceptionModule
        End Get
        Set(ByVal Value As String)
            mExceptionModule = Value
        End Set
    End Property
    Public Property ExceptionFunction() As String
        Get
            Return mExceptionFunction
        End Get
        Set(ByVal Value As String)
            mExceptionFunction = Value
        End Set
    End Property
    Public Property ExceptionMiscInfo() As String
        Get
            Return mExceptionMisc
        End Get
        Set(ByVal Value As String)
            mExceptionMisc = Value
        End Set
    End Property
    Public ReadOnly Property ExceptionId() As String
        Get
            Return mExceptionIdentifier
        End Get
    End Property
    Public Function LogException() As Boolean
        'Dim nucDataExcDb As New DALDataExceptionDb
        'Return nucDataExcDb.InsertException(Me)
        Dim apppath As String
        apppath = Environment.CurrentDirectory
        Dim oWrite As System.IO.StreamWriter

        If Not IO.File.Exists(apppath & "\log\results.txt") Then
            System.IO.File.CreateText(apppath & "\log\results.txt")
        End If
        oWrite = New System.IO.StreamWriter(apppath & "\log\results.txt", True)
        oWrite.WriteLine(Me.Message)
        oWrite.Flush()
        oWrite.Close()
        Return True
    End Function
    Private Sub GenerateId()
        Dim randomNumber As Integer
        Randomize() ' Initialize random-number generator.
        randomNumber = CInt(Int((1000 * Rnd()) + 1)) ' Generate random value between 1 and 1000.
        Me.mExceptionIdentifier = randomNumber.ToString & Today.ToShortDateString & Now.ToShortTimeString
    End Sub
#Region "Class Information"
    Public ReadOnly Property ClassName() As String
        Get
            Return cClassName
        End Get
    End Property
    Public ReadOnly Property ClassVersion() As String
        Get
            Return cClassVersion
        End Get
    End Property
    Public ReadOnly Property ClassRelease() As String
        Get
            Return cClassRelease
        End Get
    End Property
    Public ReadOnly Property ClassDescription() As String
        Get
            Return cClassDescrip
        End Get
    End Property
#End Region
End Class
