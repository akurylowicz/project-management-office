Imports System.Data

Public Class DALDataManager
    Private Const cClassName As String = "DALDataManager"
    Private Const cClassVersion As String = "1.0"
    Private Const cClassRelease As String = "a"
    Private Const cClassDescrip As String = "Database class for use in the DAL library."
    Private Const cDefaultDbName As String = "common"
    Private Const cDefaultDbProdServer As String = "ntsvrnomi"
    Private Const cDefaultDbDevServer As String = "ntsvrdev"

    Private mDatabaseId As Integer
    Private mDatabaseName As String
    Private mLocalServer As String
    Private mDevServer As String
    Private mProdServer As String
    Private mDatabaseLevel As Integer
    Private mAssocId As Integer
    Private mUserId As String
    Private Shared mIsInstantiated As Boolean
    Private Shared mDatabase As DALDataManager

    Private Const mStdUser As String = "pseudoSA"
    Private Const mStdPassword As String = "n56!71L"

    Private mDirectSetServer As String
    Private mDirectSetUser As String
    Private mDirectSetPassword As String

    Private mDirectSetConnection As String


    Public Enum DatabaseLevel
        Local = 1
        Development = 2
        Production = 3
        DirectSet = 4
    End Enum
    Public Enum DatabaseError
        dbNotInstantiated = 9001
    End Enum

    Private Sub New()
        Me.mDatabaseId = 0
        Me.mDatabaseName = ""
        Me.mLocalServer = ""
        Me.mDevServer = ""
        Me.mProdServer = ""
        Me.mAssocId = Nothing
        Me.mUserId = Nothing

        ' If no database level is passed, default it to the production value.
        Me.mDatabaseLevel = DALDataManager.DatabaseLevel.Production

    End Sub
    Private Sub New(ByVal dbLevel As Integer, ByVal assocId As Integer, ByVal userId As String, Optional ByVal localDbName As String = "")
        Me.mDatabaseId = 0
        Me.mDatabaseName = ""
        Me.mLocalServer = ""
        Me.mDevServer = ""
        Me.mProdServer = ""

        If localDbName > "" Then
            Me.mLocalServer = localDbName
        End If

        If dbLevel = 1 Or dbLevel = 2 Or dbLevel = 3 Then
            Me.mDatabaseLevel = dbLevel
        Else
            Dim exc As New DALDataException("Invalid Database Level passed to DALDataManager.  Level passed was: " & dbLevel & ". ")
            exc.LogException()
            Throw exc
        End If

        If IsNumeric(assocId) And assocId > 0 Then
            Me.mAssocId = assocId
        Else
            Dim exc As New DALDataException("Invalid Assoc Id passed to DALDataManager.  Must be numeric.  Cannot be Zero.  Assoc Id passed was: " & assocId & ". ")
            exc.LogException()
            Throw exc
        End If

        If userId > "" Then
            Me.mUserId = userId
        Else
            Dim exc As New DALDataException("Invalid User Id passed to DALDataManager.  Must not be blank.  User Id passed was: " & userId & ". ")
            exc.LogException()
            Throw exc
        End If
    End Sub
    Private Sub New(ByVal dbLevel As Integer, ByVal userId As String, Optional ByVal localDbName As String = "")
        Me.mDatabaseId = 0
        Me.mDatabaseName = ""
        Me.mLocalServer = ""
        Me.mDevServer = ""
        Me.mProdServer = ""

        If localDbName > "" Then
            Me.mLocalServer = localDbName
        End If

        If dbLevel = 1 Or dbLevel = 2 Or dbLevel = 3 Then
            Me.mDatabaseLevel = dbLevel
        Else
            Dim exc As New DALDataException("Invalid Database Level passed to DALDataManager.  Level passed was: " & dbLevel & ". ")
            exc.LogException()
            Throw exc
        End If

        Me.mAssocId = Nothing

        If userId > "" Then
            Me.mUserId = userId
        Else
            Dim exc As New DALDataException("Invalid User Id passed to DALDataManager.  Must not be blank.  User Id passed was: " & userId & ". ")
            exc.LogException()
            Throw exc
        End If
    End Sub
    Private Sub New(ByVal serverName As String, ByVal userId As String, ByVal userPassword As String)
        Me.mDatabaseId = 0
        Me.mDatabaseName = ""
        Me.mLocalServer = ""
        Me.mDevServer = ""
        Me.mProdServer = ""

        Me.mDatabaseLevel = DALDataManager.DatabaseLevel.DirectSet
        Me.mDirectSetServer = serverName
        Me.mDirectSetUser = userId
        Me.mDirectSetPassword = userPassword

    End Sub
    Private Sub New(ByVal connStr As String)
        Me.mDirectSetConnection = connStr
    End Sub
    Public Shared Function GetDatabaseObject() As DALDataManager
        If mIsInstantiated Then
            Return mDatabase
        Else
            Dim exc As New DALDataException("The Databse object was not instantiated prior to being used", DatabaseError.dbNotInstantiated)
            exc.LogException()
            Throw exc
        End If
    End Function
    Public ReadOnly Property Level() As Integer
        Get
            Return mDatabaseLevel
        End Get
    End Property
    Public ReadOnly Property AssocId() As Integer
        Get
            If Me.mAssocId = 0 Then
                GetAssocId()
            End If
            Return mAssocId
        End Get
    End Property
    Public ReadOnly Property UserId() As String
        Get
            If Me.mUserId = "" Then
                GetAssocId()
            End If
            Return mUserId
        End Get
    End Property
    Public ReadOnly Property DevServer() As String
        Get
            Return mDevServer
        End Get
    End Property
    Public ReadOnly Property ProdServer() As String
        Get
            Return Me.mProdServer
        End Get
    End Property
    Public ReadOnly Property LocalServer() As String
        Get
            Return Me.mLocalServer
        End Get
    End Property
    Public ReadOnly Property DirectServer() As String
        Get
            Return Me.mDirectSetServer
        End Get
    End Property
    Public ReadOnly Property CurrentDatabaseServer() As String
        Get
            Dim server As String = ""
            Select Case Me.mDatabaseLevel
                Case DatabaseLevel.Local
                    server = mLocalServer

                Case DatabaseLevel.Development
                    server = Me.mDevServer

                Case DatabaseLevel.Production
                    server = Me.mProdServer

                Case DatabaseLevel.DirectSet
                    server = Me.mDirectSetServer
            End Select
            Return server
        End Get
    End Property
    Public ReadOnly Property StandardDbUser() As String
        Get
            Return DALDataManager.mStdUser
        End Get
    End Property
    Public ReadOnly Property StandardDbPassword() As String
        Get
            Return DALDataManager.mStdPassword
        End Get
    End Property
    Public ReadOnly Property DirectSetDbUser() As String
        Get
            Return Me.mDirectSetUser
        End Get
    End Property
    Public ReadOnly Property DirectSetDbPassword() As String
        Get
            Return Me.mDirectSetPassword
        End Get
    End Property

    Public Shared Sub CreateDatabaseObject()
        mDatabase = New DALDataManager
        mIsInstantiated = True
    End Sub

    Public Shared Sub CreateLocalDatabaseObject(ByVal assocIdIn As Integer, ByVal userIdIn As String, ByVal localDbNameIn As String)
        mDatabase = New DALDataManager(DatabaseLevel.Local, assocIdIn, userIdIn, localDbNameIn)
        mIsInstantiated = True
    End Sub
    Public Shared Sub CreateLocalDatabaseObject(ByVal assocIdIn As Integer, ByVal localDbNameIn As String)
        mDatabase = New DALDataManager(DatabaseLevel.Local, assocIdIn, localDbNameIn)
        mIsInstantiated = True
    End Sub
    Public Shared Sub CreateLocalDatabaseObject(ByVal userIdIn As String, ByVal localDbNameIn As String)
        mDatabase = New DALDataManager(DatabaseLevel.Local, userIdIn, localDbNameIn)
        mIsInstantiated = True
    End Sub

    Public Shared Sub CreateDatabaseObject(ByVal dbLevelIn As Integer, ByVal assocIdIn As Integer, ByVal userIdIn As String)
        mDatabase = New DALDataManager(dbLevelIn, assocIdIn, userIdIn)
        mIsInstantiated = True
    End Sub
    Public Shared Sub CreateDatabaseObject(ByVal dbLevelIn As Integer, ByVal assocIdIn As Integer)
        mDatabase = New DALDataManager(dbLevelIn, assocIdIn)
        mIsInstantiated = True
    End Sub
    Public Shared Sub CreateDatabaseObject(ByVal dbLevel2 As Integer, ByVal userId2 As String)
        mDatabase = New DALDataManager(dbLevel2, userId2)
        mIsInstantiated = True
    End Sub
    Public Shared Sub CreateDatabaseObject(ByVal serverIn As String, ByVal userIn As String, ByVal passwordIn As String)
        mDatabase = New DALDataManager(serverIn, userIn, passwordIn)
        mIsInstantiated = True
    End Sub
    Public Shared Sub CreateDatabaseObject(ByVal connStringIn As String)
        mDatabase = New DALDataManager(connStringIn)
        mIsInstantiated = True
    End Sub

    Public Function DbConnection() As SqlClient.SqlConnection
        Dim tmpConn As SqlClient.SqlConnection
        Me.mDatabaseName = cDefaultDbName
        Me.mDevServer = cDefaultDbDevServer
        Me.mProdServer = cDefaultDbProdServer
        tmpConn = GetDefaultConnection()
        Return tmpConn
    End Function
    Public Function DbConnection(ByVal dbId As Integer) As SqlClient.SqlConnection
        Me.mDatabaseId = dbId
        Me.mDatabaseName = ""
        Dim tmpConn As SqlClient.SqlConnection
        GetDatabaseInfo()
        tmpConn = BuildConnection()
        Return tmpConn
    End Function
    Public Function DbConnection(ByVal dbName As String) As SqlClient.SqlConnection
        Me.mDatabaseName = dbName
        Me.mDatabaseId = 0
        Dim tmpConn As SqlClient.SqlConnection
        GetDatabaseInfo()
        tmpConn = BuildConnection()
        Return tmpConn
    End Function
    Public Function DbDirectSetConnection() As SqlClient.SqlConnection
        Me.mDatabaseName = ""
        Me.mDatabaseId = 0
        Dim tmpConn As SqlClient.SqlConnection
        tmpConn = GetConnection()
        Return tmpConn
    End Function
    Private Sub GetDatabaseInfo()
        If Me.mDatabaseId > 0 Then
            LoadDatabaseInfoById()
        Else
            If Me.mDatabaseName > "" Then
                LoadDatabaseInfoByName()
            End If
        End If
    End Sub
    Private Sub LoadDatabaseInfoById()
        Dim ds As New DataSet
        Dim cnCommon As SqlClient.SqlConnection = GetConnection()
        Dim cmdGetDb As New SqlClient.SqlDataAdapter("spSelectDatabaseById", cnCommon)
        cmdGetDb.SelectCommand.CommandType = CommandType.StoredProcedure
        cmdGetDb.SelectCommand.Parameters.Add(New SqlClient.SqlParameter("@databaseId", mDatabaseId))

        Try
            cmdGetDb.Fill(ds)

            If ds.Tables(0).Rows.Count > 0 Then
                Me.mDatabaseName = ds.Tables(0).Rows(0).Item("databaseName")
                Me.mDevServer = ds.Tables(0).Rows(0).Item("databaseDevServer")
                Me.mProdServer = ds.Tables(0).Rows(0).Item("databaseProdServer")
            Else
                Dim exc As New DALDataException("Database not found in LoadDatabaseInfo ById.  Database id in is: " & Me.mDatabaseId & ".")
                exc.LogException()
                Throw exc
                Throw exc
            End If

        Catch xSql As SqlClient.SqlException
            Dim exc As New DALDataException("Unable to read Database in LoadDatabaseInfoById() in DALLib.DALDataManager. Database Id in is " _
            & Me.mDatabaseId & ". Sql Error in is " & xSql.Number & " - " & xSql.Message, Today.ToShortDateString, Now.ToShortTimeString)
            exc.LogException()
            Throw (exc)

        Finally
            cmdGetDb = Nothing
        End Try
    End Sub
    Private Sub LoadDatabaseInfoByName()
        Dim ds As New DataSet
        Dim cnCommon As SqlClient.SqlConnection = GetConnection()
        Dim cmdGetDb As New SqlClient.SqlDataAdapter("spSelectDatabaseByName", cnCommon)
        cmdGetDb.SelectCommand.CommandType = CommandType.StoredProcedure
        cmdGetDb.SelectCommand.Parameters.Add(New SqlClient.SqlParameter("@databaseName", mDatabaseName))

        Try
            cmdGetDb.Fill(ds)

            If ds.Tables(0).Rows.Count > 0 Then
                Me.mDatabaseId = ds.Tables(0).Rows(0).Item("databaseId")
                Me.mDatabaseName = ds.Tables(0).Rows(0).Item("databaseName")
                Me.mDevServer = ds.Tables(0).Rows(0).Item("databaseDevServer")
                Me.mProdServer = ds.Tables(0).Rows(0).Item("databaseProdServer")
            Else
                Dim exc As New DALDataException("Database not found in LoadDatabaseInfoByName.  Database Name in is: " & Me.mDatabaseName & ".")
                exc.LogException()
                Throw exc
            End If

        Catch xSql As SqlClient.SqlException
            Dim exc As New DALDataException("Unable to read Database in LoadDatabaseInfoById() in DALLib.DALDataManager. Database Id in is " _
            & Me.mDatabaseId & ". Sql Error in is " & xSql.Number & " - " & xSql.Message, Today.ToShortDateString, Now.ToShortTimeString)
            exc.LogException()
            Throw (exc)

        Finally
            cmdGetDb = Nothing
        End Try

    End Sub
    Private Function GetDefaultConnection() As SqlClient.SqlConnection
        Dim conn As New SqlClient.SqlConnection
        Dim connString As String
        Dim connDB As String = "common"
        Dim connServer As String
        Dim connUser As String
        Dim connPassword As String

        Select Case Me.mDatabaseLevel
            Case DatabaseLevel.Local
                connServer = Me.mLocalServer
                connUser = DALDataManager.mStdUser
                connPassword = DALDataManager.mStdPassword

            Case DatabaseLevel.Development
                connServer = Me.mDevServer
                connUser = DALDataManager.mStdUser
                connPassword = DALDataManager.mStdPassword

            Case DatabaseLevel.Production
                connServer = Me.mProdServer
                connUser = DALDataManager.mStdUser
                connPassword = DALDataManager.mStdPassword

            Case DatabaseLevel.DirectSet
                connServer = Me.mDirectSetServer
                connUser = Me.mDirectSetUser
                connPassword = Me.mDirectSetPassword

            Case Else
                Dim exc As New DALDataException("Massive System Error.  Invalid DBSource in DALDataManager Object in DALLib.  Contact Information Systems team.")
                exc.LogException()
                Throw exc

        End Select

        connString = "Initial Catalog=" & connDB
        connString += ";data source=" & connServer
        connString += ";User Id=" & connUser & ";PWD=" & connPassword

        conn.ConnectionString = connString

        Return conn

    End Function
    Private Function GetConnection() As SqlClient.SqlConnection
        Dim conn As New SqlClient.SqlConnection
        Dim connString As String
        Dim connDB As String = "common"
        Dim connServer As String
        Dim connUser As String
        Dim connPassword As String

        If Me.mDirectSetConnection > "" Then
            conn.ConnectionString = Me.mDirectSetConnection
        Else
            Select Case Me.mDatabaseLevel
                Case DatabaseLevel.Local
                    connServer = mLocalServer
                    connUser = DALDataManager.mStdUser
                    connPassword = DALDataManager.mStdPassword

                Case DatabaseLevel.Development
                    connServer = "ntsvrdev"
                    connUser = DALDataManager.mStdUser
                    connPassword = DALDataManager.mStdPassword

                Case DatabaseLevel.Production
                    connServer = "ntsvrnomi"
                    connUser = DALDataManager.mStdUser
                    connPassword = DALDataManager.mStdPassword

                Case DatabaseLevel.DirectSet
                    connServer = Me.mDirectSetServer
                    connUser = Me.mDirectSetUser
                    connPassword = Me.mDirectSetPassword

                Case Else
                    Dim exc As New DALDataException("Massive System Error.  Invalid DBSource in DALDataManager Object in DALLib.  Contact Information Systems team.")
                    exc.LogException()
                    Throw exc

            End Select

            connString = "Initial Catalog=" & connDB
            connString += ";data source=" & connServer
            connString += ";User Id=" & connUser & ";PWD=" & connPassword

            conn.ConnectionString = connString
        End If
        Return conn

    End Function
    Private Function BuildConnection() As SqlClient.SqlConnection
        Dim conn As New SqlClient.SqlConnection
        Dim connString As String
        Dim connServer As String
        Dim connDB As String = Me.mDatabaseName
        Dim connUser As String = DALDataManager.mStdUser
        Dim connPassword As String = DALDataManager.mStdPassword

        If Me.mDirectSetConnection > "" Then
            conn.ConnectionString = Me.mDirectSetConnection
        Else
            Select Case Me.mDatabaseLevel
                Case DatabaseLevel.Local
                    connServer = Me.mLocalServer

                Case DatabaseLevel.Development
                    connServer = Me.mDevServer

                Case DatabaseLevel.Production
                    connServer = Me.mProdServer

                Case DatabaseLevel.DirectSet
                    connServer = Me.mDirectSetServer
                    connUser = Me.mDirectSetUser
                    connPassword = Me.mDirectSetPassword

                Case Else
                    Dim exc As New DALDataException("Massive System Error.  Invalid DBSource in DALDataManager Object in DALLib.  Contact Information Systems team.")
                    exc.LogException()
                    Throw exc

            End Select

            connString = "Initial Catalog=" & connDB
            connString += ";data source=" & connServer
            connString += ";User Id=" & connUser & ";PWD=" & connPassword

            conn.ConnectionString = connString
        End If
        Return conn

    End Function
    Public Function GetDatabaseName() As String
        Dim ds As New DataSet
        Dim dbName As String = ""
        Dim cnCommon As SqlClient.SqlConnection = GetConnection()
        Dim cmdGetDb As New SqlClient.SqlDataAdapter("spSelectDatabaseById", cnCommon)
        cmdGetDb.SelectCommand.CommandType = CommandType.StoredProcedure
        cmdGetDb.SelectCommand.Parameters.Add(New SqlClient.SqlParameter("@databaseId", mDatabaseId))

        Try
            cmdGetDb.Fill(ds)

            If ds.Tables(0).Rows.Count > 0 Then
                dbName = ds.Tables(0).Rows(0).Item("databaseName")
            Else
                Dim exc As New DALDataException("Database not found in LoadDatabaseInfo ById.  Database id in is: " & Me.mDatabaseId & ".")
                exc.LogException()
                Throw exc
            End If

        Catch xSql As SqlClient.SqlException
            Dim exc As New DALDataException("Unable to read Database in LoadDatabaseInfoById() in DALLib.DALDataManager. Database Id in is " _
            & Me.mDatabaseId & ". Sql Error in is " & xSql.Number & " - " & xSql.Message, Today.ToShortDateString, Now.ToShortTimeString)
            exc.LogException()
            Throw (exc)

        Finally
            cmdGetDb = Nothing
        End Try
        Return dbName
    End Function
    Public Function GetDatabaseName(ByVal dbIdIn As Integer) As String
        Dim ds As New DataSet
        Dim dbName As String = ""
        Dim cnCommon As SqlClient.SqlConnection = GetConnection()
        Dim cmdGetDb As New SqlClient.SqlDataAdapter("spSelectDatabaseById", cnCommon)
        cmdGetDb.SelectCommand.CommandType = CommandType.StoredProcedure
        cmdGetDb.SelectCommand.Parameters.Add(New SqlClient.SqlParameter("@databaseId", dbIdIn))

        Try
            cmdGetDb.Fill(ds)

            If ds.Tables(0).Rows.Count > 0 Then
                dbName = ds.Tables(0).Rows(0).Item("databaseName")
            Else
                Dim exc As New DALDataException("Database not found in LoadDatabaseInfo ById.  Database id in is: " & Me.mDatabaseId & ".")
                exc.LogException()
                Throw exc
            End If

        Catch xSql As SqlClient.SqlException
            Dim exc As New DALDataException("Unable to read Database in LoadDatabaseInfoById() in DALLib.DALDataManager. Database Id in is " _
            & Me.mDatabaseId & ". Sql Error in is " & xSql.Number & " - " & xSql.Message, Today.ToShortDateString, Now.ToShortTimeString)
            exc.LogException()
            Throw (exc)

        Finally
            cmdGetDb = Nothing
        End Try
        Return dbName
    End Function
    Public Function GetDatabaseId() As Integer
        Dim ds As New DataSet
        Dim dbId As Integer = 0
        Dim cnCommon As SqlClient.SqlConnection = GetConnection()
        Dim cmdGetDb As New SqlClient.SqlDataAdapter("spSelectDatabaseByName", cnCommon)
        cmdGetDb.SelectCommand.CommandType = CommandType.StoredProcedure
        cmdGetDb.SelectCommand.Parameters.Add(New SqlClient.SqlParameter("@databaseName", mDatabaseName))

        Try
            cmdGetDb.Fill(ds)

            If ds.Tables(0).Rows.Count > 0 Then
                dbId = ds.Tables(0).Rows(0).Item("databaseId")
            Else
                Dim exc As New DALDataException("Database not found in LoadDatabaseInfo ById.  Database id in is: " & Me.mDatabaseId & ".")
                exc.LogException()
                Throw exc
            End If

        Catch xSql As SqlClient.SqlException
            Dim exc As New DALDataException("Unable to read Database in LoadDatabaseInfoById() in DALLib.DALDataManager. Database Id in is " _
            & Me.mDatabaseId & ". Sql Error in is " & xSql.Number & " - " & xSql.Message, Today.ToShortDateString, Now.ToShortTimeString)
            exc.LogException()
            Throw (exc)

        Finally
            cmdGetDb = Nothing
        End Try

        Return dbId

    End Function
    Public Function GetDatabaseId(ByVal dbNameIn As String) As Integer
        Dim ds As New DataSet
        Dim dbId As Integer = 0
        Dim cnCommon As SqlClient.SqlConnection = GetConnection()
        Dim cmdGetDb As New SqlClient.SqlDataAdapter("spSelectDatabaseByName", cnCommon)
        cmdGetDb.SelectCommand.CommandType = CommandType.StoredProcedure
        cmdGetDb.SelectCommand.Parameters.Add(New SqlClient.SqlParameter("@databaseName", dbNameIn))

        Try
            cmdGetDb.Fill(ds)

            If ds.Tables(0).Rows.Count > 0 Then
                dbId = ds.Tables(0).Rows(0).Item("databaseId")
            Else
                Dim exc As New DALDataException("Database not found in LoadDatabaseInfo ById.  Database name in is: " & dbNameIn & ".")
                exc.LogException()
                Throw exc
            End If

        Catch xSql As SqlClient.SqlException
            Dim exc As New DALDataException("Unable to read Database in LoadDatabaseInfoById() in DALLib.DALDataManager. Database Id in is " _
            & Me.mDatabaseId & ". Sql Error in is " & xSql.Number & " - " & xSql.Message, Today.ToShortDateString, Now.ToShortTimeString)
            exc.LogException()
            Throw (exc)

        Finally
            cmdGetDb = Nothing
        End Try

        Return dbId

    End Function
    Private Function GetAssocId() As Integer
        If Me.mUserId > "" Then
            Dim ds As New DataSet
            Dim cnCommon As SqlClient.SqlConnection = GetConnection()
            Dim cmdGetDb As New SqlClient.SqlDataAdapter("spSelectUserById", cnCommon)
            cmdGetDb.SelectCommand.CommandType = CommandType.StoredProcedure
            cmdGetDb.SelectCommand.Parameters.Add(New SqlClient.SqlParameter("@UserId", Me.mUserId))

            Try
                cmdGetDb.Fill(ds)

                If ds.Tables(0).Rows.Count > 0 Then
                    Me.mAssocId = ds.Tables(0).Rows(0).Item("associateID")
                Else
                    Dim exc As New DALDataException("Database not found in GetAssocId in DatabaseManager.  User id in is: " & Me.mUserId & ".")
                    exc.LogException()
                    Throw exc
                End If

            Catch xSql As SqlClient.SqlException
                Dim exc As New DALDataException("Unable to read Database in GetAssocId() in DALLib.DALDataManager. User Id in is " _
                & Me.mUserId & ". Sql Error in is " & xSql.Number & " - " & xSql.Message, Today.ToShortDateString, Now.ToShortTimeString)
                exc.LogException()
                Throw (exc)

            Finally
                cmdGetDb = Nothing
            End Try

        End If
    End Function
    Public Sub SetLocalServer(ByVal serverIn As String)
        Me.mLocalServer = serverIn
    End Sub
    Public Function DirectSetConnection() As Boolean
        If Me.mDirectSetConnection > "" Then
            Return True
        Else
            Return False
        End If
    End Function
#Region "Class Information"
    Public ReadOnly Property ClassName() As String
        Get
            Return cClassName
        End Get
    End Property
    Public ReadOnly Property ClassVersion() As String
        Get
            Return cClassRelease
        End Get
    End Property
    Public ReadOnly Property ClassRelease() As String
        Get
            Return cClassVersion
        End Get
    End Property
    Public ReadOnly Property ClassDescription() As String
        Get
            Return cClassDescrip
        End Get
    End Property
#End Region
End Class

