Imports System
Imports System.Configuration
Imports System.Data
Imports System.Data.SqlClient

Friend Class DALDataExceptionDb
  Private objDb As DALDataManager
  Private cnCommon As SqlConnection

  Public Sub New()
    Try
      objDb = DALDataManager.GetDatabaseObject

    Catch exc As DALDataException
      exc.LogException()
      Throw exc

    Catch otherExc As Exception
      Throw otherExc
    End Try

    cnCommon = objDb.DbConnection("common")
    objDb = Nothing
  End Sub
  Public Function InsertException(ByVal exceptionIn As DALDataException) As Boolean
    Dim retCode As Boolean = False
    Dim cmdError As New SqlCommand("spInsertException", cnCommon)

    ' Mark the Command as a SPROC
    cmdError.CommandType = CommandType.StoredProcedure

    ' Add Parameters to SPROC
    cmdError.Parameters.Add(New SqlParameter("@exceptionDate", exceptionIn.ErrorDate))
    cmdError.Parameters.Add(New SqlParameter("@exceptionTime", exceptionIn.ErrorTime))
    cmdError.Parameters.Add(New SqlParameter("@exceptionType", exceptionIn.ExceptionType))
    cmdError.Parameters.Add(New SqlParameter("@exceptionModule", exceptionIn.ExeptionModule))
    cmdError.Parameters.Add(New SqlParameter("@exceptionFunction", exceptionIn.ExceptionFunction))
    cmdError.Parameters.Add(New SqlParameter("@exceptionCode", exceptionIn.ExceptionCode))
    cmdError.Parameters.Add(New SqlParameter("@exceptionIdentifier", exceptionIn.ExceptionId))
    cmdError.Parameters.Add(New SqlParameter("@exceptionMisc", exceptionIn.ExceptionMiscInfo))
    cmdError.Parameters.Add(New SqlParameter("@displayTitle", exceptionIn.Title))
    cmdError.Parameters.Add(New SqlParameter("@displaySuggestion", exceptionIn.Suggestion))
    cmdError.Parameters.Add(New SqlParameter("@exceptionMessage", exceptionIn.Message))

    If exceptionIn.StackTrace Is Nothing Then
      cmdError.Parameters.Add(New SqlParameter("@exceptionTrace", ""))
    Else
      cmdError.Parameters.Add(New SqlParameter("@exceptionTrace", exceptionIn.StackTrace))
    End If

    If exceptionIn.Source Is Nothing Then
      cmdError.Parameters.Add(New SqlParameter("@exceptionSource", ""))
    Else
      cmdError.Parameters.Add(New SqlParameter("@exceptionSource", exceptionIn.Source))
    End If

    Try
      cnCommon.Open()
      If cmdError.ExecuteNonQuery() > 0 Then
        retCode = True
      Else
        retCode = False
        Dim errorString As String
        errorString = "Insert of exception failed in InsertException(exceptionIn)"
        Dim theException As New DALDataException(errorString)
        theException.LogException()
        Throw theException
      End If

    Catch xSql As SqlClient.SqlException
      Dim exc As New DALDataException("Unable to insert record in InsertException() in NucDataExceptionDb. " _
      & ". Sql Error in is " & xSql.Number & " - " & xSql.Message, Today.ToShortDateString, Now.ToShortTimeString)
      exc.LogException()
      Throw exc

    Finally
      cnCommon.Close()
      cmdError = Nothing
    End Try

    Return retCode

  End Function
  Protected Overrides Sub Finalize()
    MyBase.Finalize()
    Me.cnCommon = Nothing
  End Sub

End Class
