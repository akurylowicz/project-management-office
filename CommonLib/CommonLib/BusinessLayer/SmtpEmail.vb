Imports System.net.mail
' JDV July 10, 2012 - Add a Host Name instead of the IP Address...
Public Class SmtpEmail
  Private Const cClassName As String = "SmtpEmail"
  Private Const cClassAuthor As String = "John Vancil"
  Private Const cClassVersion As String = "1.0"

  Private mToField As String = ""
  Private mFromField As String = ""
  Private mCcField As String = ""
  Private mBccField As String = ""
  Private mSubject As String = ""
  Private mBody As String = ""
  Private mSmtpIpAddr As String = ""
  Private mAttachment As Object
  Private mAttachmentPath As String
  Private mBodyAsHtml As Boolean = False
  Private mAttachmentIncluded As Boolean = False
  Private mMultAttachments As SmtpEmailAttachmentCollection

    Private mHostName As String

  Public Sub New()
    Initialize()
  End Sub
  Public Sub New(ByVal smtpAddressIn As String)
    Initialize()
    Me.mSmtpIpAddr = smtpAddressIn
  End Sub
  Private Sub Initialize()
    'Me.mSmtpIpAddr = "172.16.2.20"
        'Me.mSmtpIpAddr = "192.168.50.26"
        Me.mSmtpIpAddr = "192.168.50.43"
        Me.mHostName = "ost-emailrelay.ostusa.com"
    Me.mMultAttachments = New SmtpEmailAttachmentCollection
  End Sub
  Public Property ToField() As String
    Get
      Return Me.mToField
    End Get
    Set(ByVal Value As String)
      Me.mToField = Value
    End Set
  End Property
  Public Property FromField() As String
    Get
      Return Me.mFromField
    End Get
    Set(ByVal Value As String)
      Me.mFromField = Value
    End Set
  End Property
  Public Property CcField() As String
    Get
      Return Me.mCcField
    End Get
    Set(ByVal Value As String)
      Me.mCcField = Value
    End Set
  End Property
  Public Property BccField() As String
    Get
      Return Me.mBccField
    End Get
    Set(ByVal Value As String)
      Me.mBccField = Value
    End Set
  End Property
  Public Property Body() As String
    Get
      Return Me.mBody
    End Get
    Set(ByVal Value As String)
      Me.mBody = Value
    End Set
  End Property
  Public Property BodyIsHtml() As Boolean
    Get
      Return Me.mBodyAsHtml
    End Get
    Set(ByVal Value As Boolean)
      Me.mBodyAsHtml = Value
    End Set
  End Property
  Public Property Subject() As String
    Get
      Return Me.mSubject
    End Get
    Set(ByVal Value As String)
      Me.mSubject = Value
    End Set
  End Property
  Public Property SmtpServerAddress() As String
    Get
      Return Me.mSmtpIpAddr
    End Get
    Set(ByVal Value As String)
      Me.mSmtpIpAddr = Value
    End Set
  End Property
  Public Property Attachment() As Object
    Get
      Return Me.mAttachment
    End Get
    Set(ByVal Value As Object)
      Me.mAttachment = Value
      Me.mAttachmentPath = ""
      Me.mAttachmentIncluded = True
    End Set
  End Property
  Public Property AttachmentPath() As Object
    Get
      Return Me.mAttachmentPath
    End Get
    Set(ByVal Value As Object)
      Me.mAttachmentPath = Value
      Me.mAttachment = Nothing
      Me.mAttachmentIncluded = True
    End Set
  End Property
  Public ReadOnly Property AttachmentCollection() As SmtpEmailAttachmentCollection
    Get
      If Me.mMultAttachments Is Nothing Then
        Me.mMultAttachments = New SmtpEmailAttachmentCollection
      End If
      Return Me.mMultAttachments
    End Get
  End Property
  Public ReadOnly Property HasAttachment() As Boolean
    Get
      Return Me.mAttachmentIncluded
    End Get
  End Property
  Public Function Validate() As UtilValidationList
    Dim valList As New UtilValidationList
    If Me.mToField = "" Then
      Dim val As New UtilValidate
      val.ErrorDescription = "To Email cannot be blank."
      val.Process = Me.ClassName
      val.SuggestedSolution = "Enter a valid Email Address in the field."
      val.ErrorCode = 10001
      valList.Add(val)
      val = Nothing
    End If
    If Not IsValidEmail(Me.mToField) Then
      Dim val As New UtilValidate
      val.ErrorDescription = "To Email must be a valid email."
      val.Process = Me.ClassName
      val.SuggestedSolution = "Enter a valid Email Address in the form of joepublic@publiccorp.com in the field."
      val.ErrorCode = 10002
      valList.Add(val)
      val = Nothing
    End If
    If Me.mFromField = "" Then
      Dim val As New UtilValidate
      val.ErrorDescription = "From Email cannot be blank."
      val.Process = Me.ClassName
      val.SuggestedSolution = "Enter a valid Email Address in the field."
      val.ErrorCode = 10001
      valList.Add(val)
      val = Nothing
    End If
    If Not IsValidEmail(Me.mFromField) Then
      Dim val As New UtilValidate
      val.ErrorDescription = "From Email must be a valid email."
      val.Process = Me.ClassName
      val.SuggestedSolution = "Enter a valid Email Address in the form of joepublic@publiccorp.com in the field."
      val.ErrorCode = 10002
      valList.Add(val)
      val = Nothing
    End If
    If Me.mSubject = "" Then
      Dim val As New UtilValidate
      val.ErrorDescription = "Subject line cannot be blank."
      val.Process = Me.ClassName
      val.SuggestedSolution = "Enter a Subject in the field."
      val.ErrorCode = 10002
      valList.Add(val)
      val = Nothing
    End If
    Return valList
  End Function
  Public Function Send() As Boolean
    Dim sent As Boolean = True

    If Not Validate.Count = 0 Then
      sent = False
      Return sent
    End If

    Try
      Dim email As New System.Net.Mail.MailMessage

      ' Adding to try to avoid SPAM filters
      email.BodyEncoding = System.Text.Encoding.GetEncoding("utf-8")

      'email.Fie.Add("http://schemas.microsoft.com/cdo/configuration/smtpconnectiontimeout", 60)

      email.To.Add(Me.ToField)
      email.From = New System.Net.Mail.MailAddress(Me.FromField)
      email.Subject = Me.Subject

      If Me.mCcField > "" Then
        email.CC.Add(Me.mCcField)
      End If

      If Me.mBccField > "" Then
        email.Bcc.Add(Me.mBccField)
      End If

      If Me.mBodyAsHtml Then
        email.IsBodyHtml = True
      Else
        email.IsBodyHtml = False
      End If

      If Me.mBody > "" Then
        email.Body = Me.mBody
      End If

      If Me.mAttachmentIncluded Then
        If Not Me.mAttachment Is Nothing Then
          Dim mailAttach As New Attachment(Me.Attachment)
          email.Attachments.Add(mailAttach)
        Else
          If Me.mAttachmentPath > "" Then
            Dim mailAttach As New Attachment(Me.mAttachmentPath & "\" & Me.Attachment)
            email.Attachments.Add(mailAttach)
          End If
        End If
      End If

      If Me.mMultAttachments.Count > 0 Then
        For Each attach As Attachment In Me.mMultAttachments
          email.Attachments.Add(attach)
        Next
      End If

            'Dim client As New System.Net.Mail.SmtpClient(Me.mSmtpIpAddr)
            Dim client As New System.Net.Mail.SmtpClient(Me.mHostName)
            client.Send(email)

    Catch ex As Exception
      Throw ex
    End Try

    Return sent
  End Function
  ReadOnly Property ClassAuthor() As String
    Get
      Return cClassAuthor
    End Get
  End Property
  ReadOnly Property ClassName() As String
    Get
      Return cClassName
    End Get
  End Property
  ReadOnly Property ClassVersion() As String
    Get
      Return cClassVersion
    End Get
  End Property
End Class
