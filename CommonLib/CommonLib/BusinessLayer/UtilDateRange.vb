Public Class UtilDateRange
  Private mStartDate As Date
  Private mEndDate As Date
  Private mStartTime As String
  Private mEndTime As String
  Private mMinDate As Date

  Private Const cClassName As String = "UtilDateRange"

  Public Sub New()
    Initialize()
  End Sub
  Public Sub New(ByVal startDateIn As Date, ByVal endDateIn As Date)
    Initialize()
    StartDate = startDateIn
    EndDate = endDateIn
  End Sub
  Public Sub New(ByVal startDateIn As Date, ByVal startTimeIn As String, ByVal endDateIn As Date, ByVal endTimeIn As String)
    Initialize()
    If startTimeIn > "" Then
      StartDate = CDate(startDateIn.ToShortDateString & " " & startTimeIn)
      EndDate = CDate(endDateIn.ToShortDateString & " " & endTimeIn)
    Else
      StartDate = startDateIn
      EndDate = endDateIn
    End If
  End Sub
  Private Sub Initialize()
    Me.mStartDate = Nothing
    Me.mStartTime = ""
    Me.mEndDate = Nothing
    Me.mEndTime = ""
    Me.mMinDate = CDate("1/1/1700")
  End Sub
  Public Property MinimumDate()
    Get
      Return Me.mMinDate
    End Get
    Set(ByVal Value)
      Me.mMinDate = Value
    End Set
  End Property
  Public Property StartDate() As Object
    Get
      If IsDate(Me.mStartDate) Then
        If Me.mStartDate > Me.mMinDate Then
          Return mStartDate
        Else
          Return Nothing
        End If
      Else
        Return Nothing
      End If
    End Get
    Set(ByVal Value As Object)
      If IsDate(CDate(Value)) Then
        mStartDate = CDate(Value)
      Else
        Dim exc As New LogException("The value passed into the StartDate property on the DateRange object must be a valid date.")
        exc.ExeptionModule = cClassName
        exc.LogException()
        Throw exc
      End If
    End Set
  End Property
  ''' <summary>Gets or Sets the Ending Date Value for the object.</summary>
  Public Property EndDate() As Object
    Get
      If IsDate(Me.mEndDate) Then
        If Me.mEndDate > Me.mMinDate Then
          Return mEndDate
        Else
          Return Nothing
        End If
      Else
        Return Nothing
      End If
    End Get
    Set(ByVal Value As Object)
      If IsDate(CDate(Value)) Then
        mEndDate = CDate(Value)
      Else
        Dim exc As New LogException("The value passed into the StartDate property on the DateRange object must be a valid date.")
        exc.ExeptionModule = cClassName
        exc.LogException()
        Throw exc
      End If
    End Set
  End Property
  Public Property StartTime() As String
    Get
      Return mStartTime
    End Get
    Set(ByVal Value As String)
      If IsDate(Value) Then
        mStartTime = CDate(Value).ToShortTimeString
      Else
        mStartTime = Value
      End If
    End Set
  End Property
  Public Property EndTime() As String
    Get
      Return mEndTime
    End Get
    Set(ByVal Value As String)
      If IsDate(Value) Then
        mEndTime = CDate(Value).ToShortTimeString
      Else
        mEndTime = Value
      End If
    End Set
  End Property
  ''' <summary>Checks the passed object and determines if it is a valid date or 
  ''' not.</summary>
  ''' <returns>Boolean - True if date is valid; False if it is not.</returns>
  ''' <param name="dateIn">Date - The date to check</param>
  Public Function CheckValidDate(ByVal dateIn As Date) As Boolean
    Dim retCode As Boolean = False
    If IsDate(dateIn) Then
      If dateIn > Me.mMinDate Then
        retCode = True
      End If
    End If
    Return retCode
  End Function
  ''' <summary>
  ''' Elapsed seconds between the Starting Date / Time and Ending Date / Time
  ''' combination.
  ''' </summary>
  ''' <returns>Double - represents the elapsed seconds.</returns>
  Public Function ElapsedSeconds() As Double
    If CheckValidDate(mStartDate) And CheckValidDate(mEndDate) Then
      Dim tmpStartDate As DateTime = CDate(Me.mStartDate & " " & Me.mStartTime)
      Dim tmpEndDate As DateTime = CDate(Me.mEndDate & " " & Me.mEndTime)
      ElapsedSeconds = DateDiff(DateInterval.Second, tmpStartDate, tmpEndDate)
    Else
      ElapsedSeconds = 0
    End If
  End Function
  ''' <summary>
  ''' Elapsed minutes between the Starting Date / Time and Ending Date / Time
  ''' combination.
  ''' </summary>
  ''' <returns>Double - represents the elapsed minutes.</returns>
  Public Function ElapsedMinutes() As Double
    If CheckValidDate(mStartDate) Then
      If CheckValidDate(mEndDate) Then
        Dim tmpStartDate As DateTime = CDate(Me.mStartDate & " " & Me.mStartTime)
        Dim tmpEndDate As DateTime = CDate(Me.mEndDate & " " & Me.mEndTime)
        ElapsedMinutes = DateDiff(DateInterval.Minute, tmpStartDate, tmpEndDate)
      Else
        Dim tmpStartDate As DateTime = CDate(Me.mStartDate & " " & Me.mStartTime)
        Dim tmpEndDate As DateTime = Now
        ElapsedMinutes = DateDiff(DateInterval.Minute, tmpStartDate, tmpEndDate)
      End If
    Else
      ElapsedMinutes = 0
    End If
  End Function
  ''' <summary>
  ''' Elapsed hours between the Starting Date / Time and Ending Date / Time
  ''' combination.
  ''' </summary>
  ''' <returns>Double - represents the elapsed hours.</returns>
  Public Function ElapsedHours() As Double
    If CheckValidDate(mStartDate) Then
      If CheckValidDate(mEndDate) Then
        Dim tmpStartDate As DateTime = CDate(Me.mStartDate & " " & Me.mStartTime)
        Dim tmpEndDate As DateTime = CDate(Me.mEndDate & " " & Me.mEndTime)
        ElapsedHours = DateDiff(DateInterval.Hour, tmpStartDate, tmpEndDate)
      Else
        Dim tmpStartDate As DateTime = CDate(Me.mStartDate & " " & Me.mStartTime)
        Dim tmpEndDate As DateTime = Now
        ElapsedHours = DateDiff(DateInterval.Hour, tmpStartDate, tmpEndDate)
      End If
    Else
      ElapsedHours = 0
    End If

  End Function
  ''' <summary>
  ''' Elapsed days between the Starting Date / Time and Ending Date / Time
  ''' combination.
  ''' </summary>
  ''' <returns>Double - represents the elapsed days.</returns>
  Public Function ElapsedDays() As Double
    If CheckValidDate(mStartDate) Then
      If CheckValidDate(mEndDate) Then
        Dim tmpStartDate As DateTime = CDate(Me.mStartDate & " " & Me.mStartTime)
        Dim tmpEndDate As DateTime = CDate(Me.mEndDate & " " & Me.mEndTime)
        ElapsedDays = DateDiff(DateInterval.Day, tmpStartDate, tmpEndDate)
      Else
        Dim tmpStartDate As DateTime = CDate(Me.mStartDate & " " & Me.mStartTime)
        Dim tmpEndDate As DateTime = Now
        ElapsedDays = DateDiff(DateInterval.Day, tmpStartDate, tmpEndDate)
      End If
    Else
      ElapsedDays = 0
    End If
  End Function
  ''' <summary>
  ''' Elapsed Week days between the Starting Date / Time and Ending Date / Time
  ''' combination.
  ''' </summary>
  ''' <returns>Double - represents the elapsed week days.</returns>
  Public Function ElapsedWeekDays() As Double
    If CheckValidDate(mStartDate) Then
      If CheckValidDate(mEndDate) Then
        Dim tmpStartDate As DateTime = CDate(Me.mStartDate & " " & Me.mStartTime)
        Dim tmpEndDate As DateTime = CDate(Me.mEndDate & " " & Me.mEndTime)
        ElapsedWeekDays = DateDiff(DateInterval.Weekday, tmpStartDate, tmpEndDate)
      Else
        Dim tmpStartDate As DateTime = CDate(Me.mStartDate & " " & Me.mStartTime)
        Dim tmpEndDate As DateTime = Now
        ElapsedWeekDays = DateDiff(DateInterval.Weekday, tmpStartDate, tmpEndDate)
      End If
    Else
      ElapsedWeekDays = 0
    End If
  End Function

  ''' <summary>
  ''' Elapsed weeks between the Starting Date / Time and Ending Date / Time
  ''' combination.
  ''' </summary>
  ''' <returns>Double - represents the elapsed weeks.</returns>
  Public Function ElapsedWeeks() As Double
    If CheckValidDate(mStartDate) Then
      If CheckValidDate(mEndDate) Then
        Dim tmpStartDate As DateTime = CDate(Me.mStartDate & " " & Me.mStartTime)
        Dim tmpEndDate As DateTime = CDate(Me.mEndDate & " " & Me.mEndTime)
        ElapsedWeeks = DateDiff(DateInterval.WeekOfYear, tmpStartDate, tmpEndDate)
      Else
        Dim tmpStartDate As DateTime = CDate(Me.mStartDate & " " & Me.mStartTime)
        Dim tmpEndDate As DateTime = Now
        ElapsedWeeks = DateDiff(DateInterval.WeekOfYear, tmpStartDate, tmpEndDate)
      End If
    Else
      ElapsedWeeks = 0
    End If
  End Function
  ''' <summary>
  ''' Elapsed months between the Starting Date / Time and Ending Date / Time
  ''' combination.
  ''' </summary>
  ''' <returns>Double - represents the elapsed months.</returns>
  Public Function ElapsedMonths() As Double
    If CheckValidDate(mStartDate) Then
      If CheckValidDate(mEndDate) Then
        Dim tmpStartDate As DateTime = CDate(Me.mStartDate & " " & Me.mStartTime)
        Dim tmpEndDate As DateTime = CDate(Me.mEndDate & " " & Me.mEndTime)
        ElapsedMonths = DateDiff(DateInterval.Month, tmpStartDate, tmpEndDate)
      Else
        Dim tmpStartDate As DateTime = CDate(Me.mStartDate & " " & Me.mStartTime)
        Dim tmpEndDate As DateTime = Now
        ElapsedMonths = DateDiff(DateInterval.Month, tmpStartDate, tmpEndDate)
      End If
    Else
      ElapsedMonths = 0
    End If
  End Function
  ''' <summary>
  ''' Elapsed quarters between the Starting Date / Time and Ending Date / Time
  ''' combination.
  ''' </summary>
  ''' <returns>Double - represents the elapsed quarters.</returns>
  Public Function ElapsedQuarters() As Double
    If CheckValidDate(mStartDate) Then
      If CheckValidDate(mEndDate) Then
        Dim tmpStartDate As DateTime = CDate(Me.mStartDate & " " & Me.mStartTime)
        Dim tmpEndDate As DateTime = CDate(Me.mEndDate & " " & Me.mEndTime)
        ElapsedQuarters = DateDiff(DateInterval.Quarter, tmpStartDate, tmpEndDate)
      Else
        Dim tmpStartDate As DateTime = CDate(Me.mStartDate & " " & Me.mStartTime)
        Dim tmpEndDate As DateTime = Now
        ElapsedQuarters = DateDiff(DateInterval.Quarter, tmpStartDate, tmpEndDate)
      End If
    Else
      ElapsedQuarters = 0
    End If
  End Function

  ''' <summary>
  ''' Elapsed years between the Starting Date / Time and Ending Date / Time
  ''' combination.
  ''' </summary>
  ''' <returns>Double - represents the elapsed years.</returns>
  Public Function ElapsedYears() As Double
    If CheckValidDate(mStartDate) Then
      If CheckValidDate(mEndDate) Then
        Dim tmpStartDate As DateTime = CDate(Me.mStartDate & " " & Me.mStartTime)
        Dim tmpEndDate As DateTime = CDate(Me.mEndDate & " " & Me.mEndTime)
        ElapsedYears = DateDiff(DateInterval.Year, tmpStartDate, tmpEndDate)
      Else
        Dim tmpStartDate As DateTime = CDate(Me.mStartDate & " " & Me.mStartTime)
        Dim tmpEndDate As DateTime = Now
        ElapsedYears = DateDiff(DateInterval.Year, tmpStartDate, tmpEndDate)
      End If
    Else
      ElapsedYears = 0
    End If
  End Function
  Public ReadOnly Property ElapsedTime() As String
    Get
      Dim strDays As String = ""
      Dim strHours As String = ""
      Dim retStr As String = ""

      If Me.ElapsedDays > 0 Then
        If Me.ElapsedHours > 0 Then
          strDays = CInt(Me.ElapsedHours / 24)
          strHours = Me.ElapsedHours - (strDays * 24)
        End If
      Else
        strDays = ""
        strHours = Me.ElapsedHours
        If strHours > 0 Then
          If strHours > 24 Then
            strDays = CInt(Me.ElapsedHours / 24)
            strHours = Me.ElapsedHours - (strDays * 24)
          End If
        Else
          retStr = "Less than 1 hour"
        End If
      End If

      If strDays > "" Then
        If strDays = "1" Then
          retStr = retStr & strDays & " day; "
        Else
          retStr = retStr & strDays & " days; "
        End If
      End If

      If strHours = "1" Then
        retStr = retStr & strHours & " hour"
      Else
        If strHours = "0" Then
          retStr = retStr
        Else
          retStr = retStr & strHours & " hours"
        End If
      End If

      Return retStr
    End Get
  End Property
  Public ReadOnly Property WeekStartDate() As Date
    Get
      If IsValidDate(Me.mStartDate) Then
        Select Case CDate(Me.mStartDate).DayOfWeek
          Case DayOfWeek.Sunday
            Return Me.mStartDate

          Case DayOfWeek.Monday
            Return DateAdd(DateInterval.Day, -1, Me.mStartDate)

          Case DayOfWeek.Tuesday
            Return DateAdd(DateInterval.Day, -2, Me.mStartDate)

          Case DayOfWeek.Wednesday
            Return DateAdd(DateInterval.Day, -3, Me.mStartDate)

          Case DayOfWeek.Thursday
            Return DateAdd(DateInterval.Day, -4, Me.mStartDate)

          Case DayOfWeek.Friday
            Return DateAdd(DateInterval.Day, -5, Me.mStartDate)

          Case DayOfWeek.Saturday
            Return DateAdd(DateInterval.Day, -6, Me.mStartDate)

        End Select
      Else
        Throw New Exception("Cannot get the WeekStartDate without a valid Start Date. CommonLib.UtilDateRange.WeekStartDate()")
      End If
    End Get
  End Property
  Public ReadOnly Property WeekEndDate() As Date
    Get
      If IsValidDate(Me.mEndDate) Then
        Select Case CDate(Me.mEndDate).DayOfWeek
          Case DayOfWeek.Saturday
            Return Me.mEndDate

          Case DayOfWeek.Friday
            Return DateAdd(DateInterval.Day, +1, Me.mEndDate)

          Case DayOfWeek.Thursday
            Return DateAdd(DateInterval.Day, +2, Me.mEndDate)

          Case DayOfWeek.Wednesday
            Return DateAdd(DateInterval.Day, +3, Me.mEndDate)

          Case DayOfWeek.Tuesday
            Return DateAdd(DateInterval.Day, +4, Me.mEndDate)

          Case DayOfWeek.Monday
            Return DateAdd(DateInterval.Day, +5, Me.mEndDate)

          Case DayOfWeek.Sunday
            Return DateAdd(DateInterval.Day, +6, Me.mEndDate)

        End Select
      Else
        Throw New Exception("Cannot get the WeekEndDate without a valid End Date. CommonLib.UtilDateRange.WeekEndDate()")
      End If
    End Get
  End Property
End Class

