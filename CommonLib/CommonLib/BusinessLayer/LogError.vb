Public Class LogError
  Dim mErrCode As Integer
  Dim mErrField As String
  Dim mErrMessage As String
  Dim mErrSuggestion As String
  Dim mErrDate As Date

  Public Sub New()
    Initialize()
  End Sub
  Public Sub New(ByVal errCodeIn As Integer, ByVal errFieldIn As String, ByVal errMsgIn As String, ByVal errSuggestionIn As String)
    mErrCode = errCodeIn
    mErrField = errFieldIn
    mErrMessage = errMsgIn
    mErrSuggestion = errSuggestionIn
    mErrDate = Now
  End Sub
  Public Sub Initialize()
    mErrCode = 0
    mErrField = ""
    mErrMessage = ""
    mErrSuggestion = ""
    mErrDate = Now
  End Sub
  Public Property ErrorCode() As String
    Get
      Return mErrCode
    End Get
    Set(ByVal Value As String)
      mErrCode = Value
    End Set
  End Property
  Public Property ErrorField() As String
    Get
      Return mErrField
    End Get
    Set(ByVal Value As String)
      mErrField = Value
    End Set
  End Property
  Public Property ErrorMessage() As String
    Get
      Return mErrMessage
    End Get
    Set(ByVal Value As String)
      mErrMessage = Value
    End Set
  End Property
  Public Property ErrorSuggestion() As String
    Get
      Return mErrSuggestion
    End Get
    Set(ByVal Value As String)
      mErrSuggestion = Value
    End Set
  End Property

  Public ReadOnly Property FormattedErrorString() As String
    Get
      Dim retString As String
      retString = "Error Code: " & mErrCode & vbCrLf
      retString = retString & "Error Field: " & mErrField & vbCrLf
      retString = retString & "Error Message: " & mErrMessage & vbCrLf
      retString = retString & "Error Suggestion: " & mErrSuggestion
      Return retString
    End Get
  End Property
  Public ReadOnly Property HtmlFormattedErrorString() As String
    Get
      Dim retString As String
      retString = "Error Code: " & mErrCode & "<BR>"
      retString = retString & "Error Field: " & mErrField & "<BR>"
      retString = retString & "Error Message: " & mErrMessage & "<BR>"
      retString = retString & "Error Suggestion: " & mErrSuggestion & "<BR>"
      Return retString
    End Get
  End Property
  Public ReadOnly Property ErrorString() As String
    Get
      Dim retString As String
      retString = "Error Code: " & mErrCode
      retString = retString & " Error Field: " & mErrField
      retString = retString & " Error Message: " & mErrMessage
      retString = retString & " Error Suggestion: " & mErrSuggestion
      Return retString
    End Get
  End Property
  Public Shadows ReadOnly Property ToString() As String
    Get
      Dim retString As String
      retString = "Error Code: " & mErrCode
      retString = retString & " Error Field: " & mErrField
      retString = retString & " Error Message: " & mErrMessage
      retString = retString & " Error Suggestion: " & mErrSuggestion
      Return retString
    End Get
  End Property

  Public Function LogError() As Boolean
    Throw New Exception("Need to create the Dtaabase Insertion Routines for LogError.LogError")
    'Return excDb.InsertError(Me)
  End Function
End Class
