Public Class UtilValidationList
  Inherits System.Collections.CollectionBase

  Public Sub Add(ByVal aValidation As UtilValidate)
    List.Add(aValidation)
  End Sub

  Public Sub Remove(ByVal index As Integer)
    If index > Count - 1 Or index < 0 Then
      Dim qacException As New Exception("The index passed to the Remove method does not exist in the Collection")
      Throw qacException
    Else
      List.RemoveAt(index)
    End If
  End Sub
  Public ReadOnly Property Item(ByVal index As Integer) As UtilValidate
    Get
      If index > Count - 1 Or index < 0 Then
        Dim qacException As New Exception("The index passed to the Item method does not exist in the Collection")
        Throw qacException
      Else
        Return CType(List.Item(index), UtilValidate)
      End If
    End Get
  End Property

End Class

