Public Class LogException
  Inherits Exception

  Private Const cClassName As String = "LogException"
  Private Const cClassVersion As String = "1.0"
  Private Const cClassRelease As String = "a"
  Private Const cClassDescrip As String = "Exception class for use in internal systems."
  Private mErrorDate As Date
  Private mErrorTime As Date
  Private mDisplayTitle As String
  Private mDisplaySuggestion As String
  Private mExceptionType As String
  Private mExceptionModule As String
  Private mExceptionFunction As String
  Private mExceptionMisc As String
  Private mExceptionIdentifier As String
  Private mExceptionCode As Integer

  Private mSystemName As String
  Private mSystemVersion As String
  Private mUserId As Integer
  Private mUserName As String


#Region "Class Information"
  Public ReadOnly Property ClassName() As String
    Get
      Return cClassName
    End Get
  End Property
  Public ReadOnly Property ClassVersion() As String
    Get
      Return cClassRelease
    End Get
  End Property
  Public ReadOnly Property ClassRelease() As String
    Get
      Return cClassVersion
    End Get
  End Property
  Public ReadOnly Property ClassDescription() As String
    Get
      Return cClassDescrip
    End Get
  End Property
#End Region

  Public Sub New()
    MyBase.New("Data Exception - No Specific Message Passed")
    Initialize()
    ErrorDate = Today
    ErrorTime = Now
    Title = "NucCommon Library Exception"
    Suggestion = "A Common Library Exception has been thrown.  No suggested resolution."
    ExceptionType = "Unspecified"
    GenerateId()
  End Sub
  Public Sub New(ByVal messageIn As String)
    MyBase.New(messageIn)
    Initialize()
    ErrorDate = Today
    ErrorTime = Now
    Title = "NucCommon Library Exception"
    Suggestion = "A  Common Library Exception has been thrown.  No suggested resolution."
    ExceptionType = "Unspecified"
    GenerateId()
  End Sub
  Public Sub New(ByVal messageIn As String, ByVal codeIn As Integer)
    MyBase.New(messageIn)
    Initialize()
    Me.mExceptionCode = codeIn
    ErrorDate = Today
    ErrorTime = Now
    Title = "NucCommon Library Exception"
    Suggestion = "A  Common Library Exception has been thrown.  No suggested resolution."
    ExceptionType = "Unspecified"
    GenerateId()
  End Sub
  Public Sub New(ByVal messageIn As String, ByVal errorDateIn As Date, ByVal errorTimeIn As Date)
    MyBase.New(messageIn)
    Initialize()
    Me.ErrorDate = errorDateIn
    Me.ErrorTime = errorTimeIn
    Title = "NucCommon Library Exception"
    Suggestion = "A Common Library Exception has been thrown.  No suggested resolution."
    ExceptionType = "Unspecified"
    GenerateId()
  End Sub
  Public Sub New(ByVal messageIn As String, ByVal errorDateIn As Date, ByVal errorTimeIn As Date, ByVal titleIn As String, ByVal suggestionIn As String, ByVal excTypeIn As String)
    MyBase.New(messageIn)
    Initialize()
    Me.ErrorDate = errorDateIn
    Me.ErrorTime = errorTimeIn
    Title = titleIn
    Suggestion = suggestionIn
    ExceptionType = excTypeIn
    GenerateId()
  End Sub
  Public Sub New(ByVal messageIn As String, ByVal errorDateIn As Date, ByVal errorTimeIn As Date, ByVal titleIn As String, ByVal suggestionIn As String, ByVal excTypeIn As String, ByVal codeIn As Integer)
    MyBase.New(messageIn)
    Initialize()
    Me.ErrorDate = errorDateIn
    Me.ErrorTime = errorTimeIn
    Me.ExceptionCode = codeIn
    Title = titleIn
    Suggestion = suggestionIn
    ExceptionType = excTypeIn
    GenerateId()
  End Sub

  Private Sub Initialize()
    Me.mExceptionType = ""
    Me.mExceptionFunction = ""
    Me.mExceptionModule = ""
    Me.mExceptionMisc = ""
    Me.mExceptionCode = 0
    Me.mErrorDate = Today
    Me.mErrorTime = Now
    Me.mDisplaySuggestion = ""
    Me.mDisplayTitle = ""
    Me.mExceptionIdentifier = ""
    Me.mSystemName = ""
    Me.mSystemVersion = ""
    Me.mUserId = 0
    Me.mUserName = ""
  End Sub
  Public Property ErrorDate() As Date
    Get
      Return mErrorDate.ToShortDateString
    End Get
    Set(ByVal Value As Date)
      mErrorDate = Value
    End Set
  End Property
  Public Property ErrorTime() As String
    Get
      Return mErrorTime.ToShortTimeString
    End Get
    Set(ByVal Value As String)
      mErrorTime = Value
    End Set
  End Property
  Public Property ExceptionCode() As Integer
    Get
      Return Me.mExceptionCode
    End Get
    Set(ByVal Value As Integer)
      Me.mExceptionCode = Value
    End Set
  End Property
  Public Property Title() As String
    Get
      Return mDisplayTitle
    End Get
    Set(ByVal Value As String)
      mDisplayTitle = Value
    End Set
  End Property
  Public Property Suggestion() As String
    Get
      Return mDisplaySuggestion
    End Get
    Set(ByVal Value As String)
      mDisplaySuggestion = Value
    End Set
  End Property
  Public Property ExceptionType() As String
    Get
      Return mExceptionType
    End Get
    Set(ByVal Value As String)
      mExceptionType = Value
    End Set
  End Property
  Public Property ExeptionModule() As String
    Get
      Return mExceptionModule
    End Get
    Set(ByVal Value As String)
      mExceptionModule = Value
    End Set
  End Property
  Public Property ExceptionFunction() As String
    Get
      Return mExceptionFunction
    End Get
    Set(ByVal Value As String)
      mExceptionFunction = Value
    End Set
  End Property
  Public Property ExceptionMiscInfo() As String
    Get
      Return mExceptionMisc
    End Get
    Set(ByVal Value As String)
      mExceptionMisc = Value
    End Set
  End Property
  Public Property SystemName() As String
    Get
      Return Me.mSystemName
    End Get
    Set(ByVal Value As String)
      Me.mSystemName = Value
    End Set
  End Property
  Public Property SystemVersion() As String
    Get
      Return Me.mSystemVersion
    End Get
    Set(ByVal Value As String)
      Me.mSystemVersion = Value
    End Set
  End Property
  Public Property UserId() As Integer
    Get
      Return Me.mUserId
    End Get
    Set(ByVal Value As Integer)
      Me.mUserId = Value
    End Set
  End Property
  Public Property UserName() As String
    Get
      Return Me.mUserName
    End Get
    Set(ByVal Value As String)
      Me.mUserName = Value
    End Set
  End Property
  Public ReadOnly Property ExceptionId() As String
    Get
      Return mExceptionIdentifier
    End Get
  End Property
  Public Function LogException(Optional ByVal toFile As Boolean = False) As Boolean
    'Throw New Exception("Need to create the Database Insertion Routines for LogException.LogException")
    'Return excDb.InsertException(Me)
    If toFile Then
      Dim apppath As String
      apppath = Environment.CurrentDirectory
      Dim oWrite As System.IO.StreamWriter

      If Not IO.File.Exists(apppath & "\log\results.txt") Then
        System.IO.File.CreateText(apppath & "\log\results.txt")
      End If
      oWrite = New System.IO.StreamWriter(apppath & "\log\results.txt", True)
      oWrite.WriteLine(Me.Message)
      oWrite.Flush()
      oWrite.Close()
    End If
  End Function
  Private Sub GenerateId()
    Dim randomNumber As Integer
    Randomize() ' Initialize random-number generator.
    randomNumber = CInt(Int((1000 * Rnd()) + 1)) ' Generate random value between 1 and 1000.
    Me.mExceptionIdentifier = randomNumber.ToString & Today.ToShortDateString & Now.ToShortTimeString
  End Sub
  Public Function Print() As String
    Dim printStr As String = ""
    printStr += BuildLine(95) & vbCrLf & vbCrLf
    printStr += RepeatChar("*", 50) & "   Exception Report     " & RepeatChar("*", 50) & vbCrLf
    printStr += BuildLine(95) & vbCrLf & vbCrLf

    printStr += "Exception Id: " & vbTab & Me.ExceptionId & vbCrLf
    printStr += "Exception Type: " & vbTab & Me.ExceptionType & vbCrLf
    printStr += "Exception Date: " & vbTab & Me.ErrorDate & vbCrLf
    printStr += "Exception Time: " & vbTab & Me.ErrorTime & vbCrLf
    printStr += "Error Code: " & vbTab & Me.ExceptionCode & vbCrLf
    printStr += "Module: " & vbTab & Me.ExeptionModule & vbCrLf
    printStr += "Function: " & vbTab & Me.ExceptionFunction & vbCrLf
    printStr += BuildLine(95) & vbCrLf & vbCrLf

    If Me.Title > "" Then
      printStr += "Title: " & Me.Title & vbCrLf & vbCrLf
    End If

    If Me.Message > "" Then
      printStr += "Message: " & vbCrLf & Me.Message & vbCrLf & vbCrLf
      printStr += BuildLine(95) & vbCrLf & vbCrLf
    End If

    If Me.Suggestion > "" Then
      printStr += "Suggestion: " & vbCrLf & Me.Suggestion & vbCrLf & vbCrLf
      printStr += BuildLine(95) & vbCrLf & vbCrLf
    End If

    If Not Me.InnerException Is Nothing Then
      If Me.InnerException.Message > "" Then
        printStr += "Inner: " & vbCrLf & Me.InnerException.Message & vbCrLf & vbCrLf
      End If
      printStr += BuildLine(95) & vbCrLf & vbCrLf
    End If

    If Me.StackTrace > "" Then
      printStr += "Stack: " & vbCrLf & Me.StackTrace & vbCrLf & vbCrLf
      printStr += BuildLine(95) & vbCrLf & vbCrLf
    End If

    printStr += vbCrLf & vbCrLf & vbCrLf & BuildLine(95) & vbCrLf & vbCrLf
    printStr += RepeatChar("*", 48) & " End of CRM Exception Report " & RepeatChar("*", 48) & vbCrLf
    printStr += BuildLine(95)

    Return printStr
  End Function
  Private Function BuildLine(ByVal sizeIn As Integer) As String
    Dim counter As Integer = 0
    Dim theLine As String = ""
    While counter < sizeIn
      theLine += "_"
      counter += 1
    End While
    Return theLine
  End Function
  Private Function RepeatChar(ByVal strIn As String, ByVal repeatNum As Integer) As String
    Dim counter As Integer = 0
    Dim theLine As String = ""
    While counter < repeatNum
      theLine += strIn
      counter += 1
    End While
    Return theLine
  End Function
End Class
