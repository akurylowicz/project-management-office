Imports System.Data

Public Class UtilValidate
    Private Const cClassName As String = "UtilValidate"
    Private cClassAuthor As String = "John Vancil"
    Private Const cClassVersion As String = "1.0"

    Private mValidationKey As Integer
    Private mErrorCode As Integer
    Private mErrorDescription As String
    Private mSuggestedSolution As String
    Private mModuleId As Integer
    Private mProcess As String
    Private mErrorDate As Date
    Private mErrorTime As String
    Private mErrorOtherInfo As String

    Protected mIsNew As Boolean
    Protected mNeedsSave As Boolean
    Protected mLoadedFromDb As Boolean

#Region "Constructors"
    ''' <summary>Basic contractor. Intitializes the object and prepares it to receive 
    ''' data.</summary>
    Public Sub New()
        Initialize()
    End Sub
    ''' <summary>
    ''' Robust constructor. Allows the passing of all pertinent information at one
    ''' time.
    ''' </summary>
    Public Sub New(ByVal errorCodeIn As Integer, ByVal errorDescrip As String, ByVal suggestedSolution As String, ByVal moduleId As Integer, ByVal process As String, ByVal otherInfo As String)
        Initialize()
        mErrorCode = errorCodeIn
        mErrorDescription = errorDescrip
        mSuggestedSolution = suggestedSolution
        mModuleId = moduleId
        mProcess = process
        mErrorOtherInfo = otherInfo
    End Sub
    Protected Overridable Sub Initialize()
        mValidationKey = 0
        mErrorCode = 0
        mErrorDescription = ""
        mSuggestedSolution = ""
        mModuleId = 0
        mProcess = ""
        mErrorDate = Today.ToShortDateString
        mErrorTime = Now.ToShortTimeString
        mErrorOtherInfo = ""
        Me.mIsNew = True
        Me.mNeedsSave = True
        Me.mLoadedFromDb = False
    End Sub
#End Region
#Region "Class Information Functionality"
    Public ReadOnly Property ClassName() As String
        Get
            Return cClassName
        End Get
    End Property
    Public ReadOnly Property ClassVersion() As String
        Get
            Return cClassVersion
        End Get

    End Property
    Public ReadOnly Property ClassAuthor() As String
        Get
            Return cClassAuthor
        End Get

    End Property
    Public Function GetXml() As Object
        GetXml = ""
    End Function
    ''' <summary>
    ''' Overrides the ToString method and returns a string which contains the
    ''' concatenation of the Error code, Error Date, Error Time and Error Description if
    ''' the Error code is greater than zero (0). Otherwise returns a string with the object
    ''' name and a message that there have been no identified errors.
    ''' </summary>
    ''' <returns>String</returns>
    Public Overrides Function ToString() As String
        If Me.mErrorCode > 0 Then
            Return Me.mErrorCode & " " & Me.mErrorDate & " - " & Me.mErrorTime & " " & Me.mErrorDescription
        Else
            Return "CrmValidate - No Error Code found"
        End If

    End Function
#End Region
#Region "UtilValidate Functionality"

    Public Property ValidationKey() As Integer
        Get
            Return Me.mValidationKey
        End Get
        Set(ByVal Value As Integer)
            Me.mValidationKey = Value
        End Set
    End Property
    Public Property ErrorCode() As Integer
        Get
            Return mErrorCode
        End Get
        Set(ByVal Value As Integer)
            mErrorCode = Value
        End Set
    End Property
    Public Property ErrorDate() As Date
        Get
            Return mErrorDate
        End Get
        Set(ByVal Value As Date)
            mErrorDate = Value.ToShortDateString
        End Set
    End Property
    Public Property ErrorDescription() As String
        Get
            Return mErrorDescription
        End Get
        Set(ByVal Value As String)
            Me.mErrorDescription = Value
        End Set
    End Property
    Public Property ErrorOtherInfo() As String
        Get
            Return mErrorOtherInfo
        End Get
        Set(ByVal Value As String)
            Me.mErrorOtherInfo = Value
        End Set
    End Property
    Public Property ErrorTime() As Date
        Get
            Return mErrorTime
        End Get
        Set(ByVal Value As Date)
            mErrorTime = Value.ToShortTimeString
        End Set
    End Property
    Public ReadOnly Property ErrorString() As String
        Get
            Return Me.mErrorCode & " " & Me.mErrorDate & " - " & Me.mErrorTime & " " & Me.mErrorDescription
        End Get
    End Property
    Public ReadOnly Property FormattedErrorString() As String
        Get
            Dim formattedErrorStr As String = "Error Encountered:" & vbCrLf
            formattedErrorStr = formattedErrorStr & "*************************" & vbCrLf
            formattedErrorStr = formattedErrorStr & "Error Code" & Me.mErrorCode & vbCrLf
            formattedErrorStr = formattedErrorStr & "Error Date" & Me.mErrorDate & vbCrLf
            formattedErrorStr = formattedErrorStr & "Error Time" & Me.mErrorTime & vbCrLf
            formattedErrorStr = formattedErrorStr & "Error Description" & Me.mErrorDescription & vbCrLf
            Return formattedErrorStr
        End Get
    End Property
    Public Property ModuleId() As Integer
        Get
            Return mModuleId
        End Get
        Set(ByVal Value As Integer)
            mModuleId = Value
        End Set
    End Property
    Public Property Process() As String
        Get
            Return mProcess
        End Get
        Set(ByVal Value As String)
            mProcess = Value
        End Set
    End Property
    Public Property SuggestedSolution() As String
        Get
            Return mSuggestedSolution
        End Get
        Set(ByVal Value As String)
            Me.mSuggestedSolution = Value
        End Set
    End Property
    Protected Sub LoadDataFromDataset(ByVal ds As DataSet)
        Me.mValidationKey = ds.Tables(0).Rows(0).Item("validationKey")
        Me.mErrorCode = ds.Tables(0).Rows(0).Item("errorCode")
        Me.mModuleId = ds.Tables(0).Rows(0).Item("moduleId")
        Me.mErrorDate = ds.Tables(0).Rows(0).Item("errorDate")

        If ds.Tables(0).Rows(0).Item("txnTime") Is DBNull.Value Then
            Me.mErrorTime = ""
        Else
            Me.mErrorTime = ds.Tables(0).Rows(0).Item("txnTime")
        End If

        If ds.Tables(0).Rows(0).Item("errorDescription") Is DBNull.Value Then
            Me.mErrorDescription = ""
        Else
            Me.mErrorDescription = ds.Tables(0).Rows(0).Item("errorDescription")
        End If

        If ds.Tables(0).Rows(0).Item("suggestedSolution") Is DBNull.Value Then
            Me.mSuggestedSolution = ""
        Else
            Me.mSuggestedSolution = ds.Tables(0).Rows(0).Item("suggestedSolution")
        End If

        If ds.Tables(0).Rows(0).Item("otherInfo") Is DBNull.Value Then
            Me.mErrorOtherInfo = ""
        Else
            Me.mErrorOtherInfo = ds.Tables(0).Rows(0).Item("txnUserId")
        End If

    End Sub
    Public Function LogValidationError() As Boolean
        If Me.Validate.Count = 0 Then
            Return Me.Persist()
        Else
            Return False
        End If
    End Function
    Public Function DeleteValidationError(Optional ByVal validationKeyIn As Integer = 0) As Boolean
        If validationKeyIn > 0 Then
            Me.mValidationKey = validationKeyIn
            Me.Delete()
        Else
            If Me.mValidationKey > 0 Then
                Return Me.Delete
            Else
                Return False
            End If
        End If
    End Function
#End Region
#Region "Persistance"
    Protected Overridable Function Persist(Optional ByVal currUserId As Integer = 0) As Boolean
        Dim retCode As Boolean = False

        If Me.mValidationKey = 0 Then
            Dim dbParms As New DbParmList

            dbParms.Add(New DbParm("@validationKey", Me.mValidationKey))
            dbParms.Add(New DbParm("@errorCode", Me.mErrorCode))
            dbParms.Add(New DbParm("@moduleId", Me.mModuleId))
            dbParms.Add(New DbParm("@errorDescription", Me.mErrorDescription))
            dbParms.Add(New DbParm("suggestedSolution", Me.mSuggestedSolution))
            dbParms.Add(New DbParm("@process", Me.mProcess))
            dbParms.Add(New DbParm("@errorDate", Me.mErrorDate))
            dbParms.Add(New DbParm("@errorTime", Me.mErrorTime))
            dbParms.Add(New DbParm("@otherInfo", Me.mErrorOtherInfo))

            Dim dbMgr As DALDataManager = DALDataManager.GetDatabaseObject
            Dim dbAccessor As New DbAccess

            Dim dbXfer As New DbXfer
            dbXfer.DatabaseMgr = dbMgr
            ' TODO: Set the Database Name
            'dbXfer.DatabaseName = "crmv2"
            dbXfer.ParmList = dbParms

            If dbAccessor.Insert(dbXfer, "spInsertValidation", False) Then
                Me.mValidationKey = dbAccessor.ReturnValue
                retCode = True
            End If

        Else
            retCode = False
        End If
        Return retCode
    End Function
    Public Function Delete() As Boolean
        Dim dbParms As New DbParmList
        dbParms.Add(New DbParm("@validationKey", Me.mValidationKey))

        Dim dbMgr As DALDataManager = DALDataManager.GetDatabaseObject
        Dim dbAccessor As New DbAccess

        Dim dbXfer As New DbXfer
        dbXfer.DatabaseMgr = dbMgr
        ' TODO: Set the Database Name
        'dbXfer.DatabaseName = "crmv2"

        dbXfer.ParmList = dbParms

        Return dbAccessor.Delete(dbXfer, "spDeleteValidation")

    End Function
    Public Overridable Function Validate() As UtilValidationList
        Dim valList As New UtilValidationList
        ' Validation routine here... if needed
        Return valList
    End Function
    Public Overridable Sub Load()
        ' Check to see if the txnId is populated.  If so, load it from the db.
        If Me.mValidationKey > 0 Then
            Try
                Load(Me.mValidationKey)

            Catch xSql As SqlClient.SqlException
                Dim exc As New LogException("Load Error in " & cClassName & " v." & cClassVersion & " Sql Error: " & xSql.Number & " - " & xSql.Message)
                exc.LogException()
                Throw (exc)

            Catch xBase As Exception
                Dim exc As New LogException("Load Error in " & cClassName & " v." & cClassVersion & " - " & xBase.Message)
                exc.LogException()
                Throw (exc)
            End Try

        Else
            Dim exc As New LogException("The Transaction Id must be set prior to using this version of Load.  Otherwise use one of the overloaded versions.")
            exc.LogException()
            Throw exc
        End If

    End Sub
    Public Overridable Sub Load(ByVal validationKeyIn As Integer)
        Dim dbParms As New DbParmList
        Dim ds As DataSet
        dbParms.Add(New DbParm("@validationKey", Me.mValidationKey))

        Dim dbMgr As DALDataManager = DALDataManager.GetDatabaseObject
        Dim dbAccessor As New DbAccess

        Dim dbXfer As New DbXfer
        dbXfer.DatabaseMgr = dbMgr
        'dbXfer.DatabaseName = "crmv2"

        dbXfer.ParmList = dbParms

        Try
            ds = dbAccessor.GetRecord(dbXfer, "spDeleteValidation")

            If ds.Tables(0).Rows.Count > 0 Then
                LoadDataFromDataset(ds)
            End If

            Me.mIsNew = False
            Me.mLoadedFromDb = True
            Me.mNeedsSave = False

        Catch xSql As SqlClient.SqlException
            Dim exc As New LogException("Load Error in " & cClassName & " v." & cClassVersion & " Sql Error: " & xSql.Number & " - " & xSql.Message)
            exc.LogException()
            Throw (exc)

        Catch xBase As Exception
            Dim exc As New LogException("Load Error in " & cClassName & " v." & cClassVersion & " - " & xBase.Message)
            exc.LogException()
            Throw (exc)
        End Try

    End Sub
    Public Function IsNew() As Boolean
        Return Me.mIsNew
    End Function
    Public Function NeedsSave() As Boolean
        Return Me.mNeedsSave
    End Function
    Public Function LoadedFromDatabase() As Boolean
        Return Me.mLoadedFromDb
    End Function
#End Region

End Class
