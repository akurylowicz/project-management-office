Public Class UtilSort
  ''' <summary>Helper object used to provide sort functionality for lists.</summary>
  Public Enum Direction
    Ascending
    Descending
  End Enum
  Public Enum SortDataType
    SortAsInteger = 1
    SortAsString = 2
    SortAsDate = 3
    SortAsDouble = 4
    SortAsObject = 5
  End Enum
  Public Sub SortAnyList(ByVal List As Object, ByVal propName As String)
    Dim swapMade As Boolean = True
    Dim counter As Integer = 0
    Dim colObject As Object

    While swapMade
      swapMade = False
      counter = 0
      While counter < List.Count - 1
        If CStr(CallByName(List.Item(counter), propName, CallType.Get)).ToLower > CStr(CallByName(List.Item(counter + 1), propName, CallType.Get)).ToLower Then
          colObject = List.Item(counter)
          List.Remove(counter)
          List.Add(colObject)
          swapMade = True
        End If
        counter += 1
      End While
    End While
  End Sub
  Public Sub SortAnyListNative(ByVal List As Object, ByVal propName As String)
    Dim swapMade As Boolean = True
    Dim counter As Integer = 0
    Dim colObject As Object

    While swapMade
      swapMade = False
      counter = 0
      While counter < List.Count - 1
        If CallByName(List.Item(counter), propName, CallType.Get) > CallByName(List.Item(counter + 1), propName, CallType.Get) Then
          colObject = List.Item(counter)
          List.Remove(counter)
          List.Add(colObject)
          swapMade = True
        End If
        counter += 1
      End While
    End While
  End Sub
  Public Sub SortAnyList(ByVal List As Object, ByVal propName As String, ByVal propType As String)
    Dim swapMade As Boolean = True
    Dim counter As Integer = 0
    Dim colObject As Object

    If propType.ToLower = "integer" Then
      While swapMade
        swapMade = False
        counter = 0
        While counter < List.Count - 1
          If CInt(CallByName(List.Item(counter), propName, CallType.Get)) > CInt(CallByName(List.Item(counter + 1), propName, CallType.Get)) Then
            colObject = List.Item(counter)
            List.Remove(counter)
            List.Add(colObject)
            swapMade = True
          End If
          counter += 1
        End While
      End While
    Else
      If propType.ToLower = "double" Then
        While swapMade
          swapMade = False
          counter = 0
          While counter < List.Count - 1
            If CDbl(CallByName(List.Item(counter), propName, CallType.Get)) > CDbl(CallByName(List.Item(counter + 1), propName, CallType.Get)) Then
              colObject = List.Item(counter)
              List.Remove(counter)
              List.Add(colObject)
              swapMade = True
            End If
            counter += 1
          End While
        End While
      Else
        If propType.ToLower = "date" Then
          While swapMade
            swapMade = False
            counter = 0
            While counter < List.Count - 1
              If CDate(CallByName(List.Item(counter), propName, CallType.Get)) > CDate(CallByName(List.Item(counter + 1), propName, CallType.Get)) Then
                colObject = List.Item(counter)
                List.Remove(counter)
                List.Add(colObject)
                swapMade = True
              End If
              counter += 1
            End While
          End While
        Else
          While swapMade
            swapMade = False
            counter = 0
            While counter < List.Count - 1
              If CStr(CallByName(List.Item(counter), propName, CallType.Get)).ToLower > CStr(CallByName(List.Item(counter + 1), propName, CallType.Get)).ToLower Then
                colObject = List.Item(counter)
                List.Remove(counter)
                List.Add(colObject)
                swapMade = True
              End If
              counter += 1
            End While
          End While
        End If
      End If
    End If
  End Sub
  Public Sub SortAnyList(ByVal List As Object, ByVal propName As String, ByVal sortDirection As Direction)
    Dim swapMade As Boolean = True
    Dim counter As Integer = 0
    Dim colObject As Object

    Select Case sortDirection
      Case Direction.Ascending
        While swapMade
          swapMade = False
          counter = 0
          While counter < List.Count - 1
            If CStr(CallByName(List.Item(counter), propName, CallType.Get)).ToLower > CStr(CallByName(List.Item(counter + 1), propName, CallType.Get)).ToLower Then
              colObject = List.Item(counter)
              List.Remove(counter)
              List.Add(colObject)
              swapMade = True
            End If
            counter += 1
          End While
        End While

      Case Direction.Descending
        While swapMade
          swapMade = False
          counter = 0
          While counter < List.Count - 1
            If CStr(CallByName(List.Item(counter), propName, CallType.Get)).ToLower < CStr(CallByName(List.Item(counter + 1), propName, CallType.Get)).ToLower Then
              colObject = List.Item(counter)
              List.Remove(counter)
              List.Add(colObject)
              swapMade = True
            End If
            counter += 1
          End While
        End While
    End Select
  End Sub
  Public Sub SortAnyList(ByVal List As Object, ByVal propName As String, ByVal sortDirection As Direction, ByVal dataType As SortDataType)
    Dim swapMade As Boolean = True
    Dim counter As Integer = 0
    Dim colObject As Object

    Select Case sortDirection
      Case Direction.Ascending
        While swapMade
          swapMade = False
          counter = 0
          While counter < List.Count - 1
            Select Case dataType
              Case SortDataType.SortAsString
                If CStr(CallByName(List.Item(counter), propName, CallType.Get)).ToLower > CStr(CallByName(List.Item(counter + 1), propName, CallType.Get)).ToLower Then
                  colObject = List.Item(counter)
                  List.Remove(counter)
                  List.Add(colObject)
                  swapMade = True
                End If

              Case SortDataType.SortAsInteger
                If CInt(CallByName(List.Item(counter), propName, CallType.Get)) > CInt(CallByName(List.Item(counter + 1), propName, CallType.Get)) Then
                  colObject = List.Item(counter)
                  List.Remove(counter)
                  List.Add(colObject)
                  swapMade = True
                End If

              Case SortDataType.SortAsDouble
                If CDbl(CallByName(List.Item(counter), propName, CallType.Get)) > CDbl(CallByName(List.Item(counter + 1), propName, CallType.Get)) Then
                  colObject = List.Item(counter)
                  List.Remove(counter)
                  List.Add(colObject)
                  swapMade = True
                End If

              Case SortDataType.SortAsDate
                If CDate(CallByName(List.Item(counter), propName, CallType.Get)) > CDate(CallByName(List.Item(counter + 1), propName, CallType.Get)) Then
                  colObject = List.Item(counter)
                  List.Remove(counter)
                  List.Add(colObject)
                  swapMade = True
                End If

              Case SortDataType.SortAsObject
                If CallByName(List.Item(counter), propName, CallType.Get) > CallByName(List.Item(counter + 1), propName, CallType.Get) Then
                  colObject = List.Item(counter)
                  List.Remove(counter)
                  List.Add(colObject)
                  swapMade = True
                End If

              Case Else
                If CStr(CallByName(List.Item(counter), propName, CallType.Get)).ToLower > CStr(CallByName(List.Item(counter + 1), propName, CallType.Get)).ToLower Then
                  colObject = List.Item(counter)
                  List.Remove(counter)
                  List.Add(colObject)
                  swapMade = True
                End If

            End Select

            counter += 1
          End While
        End While

      Case Direction.Descending
        While swapMade
          swapMade = False
          counter = 0
          While counter < List.Count - 1
            Select Case dataType
              Case SortDataType.SortAsString
                If CStr(CallByName(List.Item(counter), propName, CallType.Get)).ToLower < CStr(CallByName(List.Item(counter + 1), propName, CallType.Get)).ToLower Then
                  colObject = List.Item(counter)
                  List.Remove(counter)
                  List.Add(colObject)
                  swapMade = True
                End If

              Case SortDataType.SortAsInteger
                If CInt(CallByName(List.Item(counter), propName, CallType.Get)) < CInt(CallByName(List.Item(counter + 1), propName, CallType.Get)) Then
                  colObject = List.Item(counter)
                  List.Remove(counter)
                  List.Add(colObject)
                  swapMade = True
                End If

              Case SortDataType.SortAsDouble
                If CDbl(CallByName(List.Item(counter), propName, CallType.Get)) < CDbl(CallByName(List.Item(counter + 1), propName, CallType.Get)) Then
                  colObject = List.Item(counter)
                  List.Remove(counter)
                  List.Add(colObject)
                  swapMade = True
                End If

              Case SortDataType.SortAsDate
                If CDate(CallByName(List.Item(counter), propName, CallType.Get)) < CDate(CallByName(List.Item(counter + 1), propName, CallType.Get)) Then
                  colObject = List.Item(counter)
                  List.Remove(counter)
                  List.Add(colObject)
                  swapMade = True
                End If

              Case SortDataType.SortAsObject
                If CallByName(List.Item(counter), propName, CallType.Get) < CallByName(List.Item(counter + 1), propName, CallType.Get) Then
                  colObject = List.Item(counter)
                  List.Remove(counter)
                  List.Add(colObject)
                  swapMade = True
                End If

              Case Else
                If CStr(CallByName(List.Item(counter), propName, CallType.Get)).ToLower < CStr(CallByName(List.Item(counter + 1), propName, CallType.Get)).ToLower Then
                  colObject = List.Item(counter)
                  List.Remove(counter)
                  List.Add(colObject)
                  swapMade = True
                End If

            End Select
            counter += 1
          End While
        End While
    End Select
  End Sub
End Class

