Public Class UtilImage
  Public Function GetThumbnailFromImage(ByVal pathToImage As String, Optional ByVal requestedWidth As Integer = 0, Optional ByVal requestedHeight As Integer = 0) As System.Drawing.Image
    Dim objImage As System.Drawing.Image
    Dim objThumbnail As System.Drawing.Image

    Dim strFilename As String
    Dim shtWidth, shtHeight As Short

    ' Keep MZ-Tools from complaining... obviated code.
    If requestedHeight = 0 Then
      requestedHeight = 0
    End If

    strFilename = pathToImage
    ' Retrieve file, or error.gif if not available
    Try
      objImage = Drawing.Image.FromFile(strFilename)
    Catch ex As Exception
      Throw ex
      'objImage = objImage.FromFile(strServerPath & "error.gif")
    End Try

    ' Retrieve width from query string
    If requestedWidth > 0 Then
      shtWidth = requestedWidth
    Else
      shtWidth = 100
    End If

    ' Work out a proportionate height from width
    shtHeight = objImage.Height / (objImage.Width / shtWidth)

    ' Create thumbnail
    objThumbnail = objImage.GetThumbnailImage(shtWidth, shtHeight, Nothing, System.IntPtr.Zero)

    Return objThumbnail

  End Function
End Class
