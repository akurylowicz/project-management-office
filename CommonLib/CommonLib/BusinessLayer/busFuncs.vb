Public Module busFuncs
  Public Function CheckForNull(ByVal dataValue As Object) As String
    If Not dataValue Is DBNull.Value Then
      Return CStr(dataValue).ToString
    Else
      Return ""
    End If
  End Function
  Public Function CheckForNull(ByVal valueIn As Object, ByVal typeIn As String) As Object
    Select Case typeIn
      Case "String"
        If Not valueIn Is DBNull.Value Then
          Return CStr(valueIn).ToString
        Else
          Return ""
        End If

      Case "Integer"
        If Not valueIn Is DBNull.Value Then
          Return CInt(valueIn)
        Else
          Return 0
        End If

      Case "Double"
        If Not valueIn Is DBNull.Value Then
          Return CDbl(valueIn)
        Else
          Return 0.0
        End If

      Case "Date"
        If Not valueIn Is DBNull.Value Then
          Return CDate(valueIn)
        Else
          Return Nothing
        End If

      Case "Boolean"
        If Not valueIn Is DBNull.Value Then
          Return CBool(valueIn)
        Else
          Return False
        End If

      Case "Object"
        If Not valueIn Is DBNull.Value Then
          Return CObj(valueIn)
        Else
          Return Nothing
        End If

      Case Else
        Return typeIn

    End Select
  End Function
  Public Function MixedCase(ByVal stringIn As String) As String
    Dim holdString As String = ""
    Dim retString As String = ""
    Dim counter As Integer = 0
    Dim holdChar As String = ""
    Dim prevCharWasSpace As Boolean = False

    holdString = stringIn.ToLower

    While counter < holdString.Length
      If Not IsNumeric(holdString.Chars(counter)) Then
        If (prevCharWasSpace Or counter = 0) And holdString.Chars(counter) <> " " Then
          holdChar = holdString.Chars(counter)
          retString = retString & holdChar.ToUpper
          holdChar = ""
        Else
          retString = retString & holdString.Chars(counter)
        End If
      Else
        holdChar = holdString.Chars(counter)
        retString = retString & holdString.Chars(counter)
      End If

      If holdString.Chars(counter) = " " Then
        prevCharWasSpace = True
      Else
        prevCharWasSpace = False
      End If
      counter += 1
    End While

    Return retString

  End Function
  Public Function StripAlpha(ByRef stripIn As String) As String
    Dim start As Integer
    Dim returnStr As String = ""

    start = 1
    While start <= Len(stripIn)
      If IsNumeric(Mid(stripIn, start, 1)) Then
        returnStr = returnStr & Mid(stripIn, start, 1)
      End If
      start = start + 1
    End While

    Return returnStr

  End Function
  Public Function BreakString(ByVal stringIn As String, ByVal breakPoint As Integer) As String
    Dim retStr As String = ""
    Dim strInLength As Integer = stringIn.Length
    If strInLength <= breakPoint Then
      retStr = stringIn
    Else
      Dim startPos As Integer = 0
      While startPos <= strInLength
        retStr += stringIn.Substring(startPos, breakPoint)
        retStr += vbCrLf
        startPos = (startPos + breakPoint) + 1
        If strInLength - breakPoint <= startPos Then
          retStr += stringIn.Substring(startPos, (strInLength - startPos))
          startPos = strInLength + 1
        End If
      End While
    End If
    Return retStr
  End Function
  Public Function IsValidEmail(ByVal strIn As String) As Boolean
    ' Return true if strIn is in valid e-mail format.
    Dim retVal As Boolean
    retVal = System.Text.RegularExpressions.Regex.IsMatch(strIn, ("^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$"))
    If retVal = False Then
      retVal = System.Text.RegularExpressions.Regex.IsMatch(strIn, ("^[a-zA-Z][\w\.-]*[a-zA-Z0-9]@[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]\.[a-zA-Z][a-zA-Z\.]*[a-zA-Z]$"))
    End If
    Return retVal
    'Return System.Text.RegularExpressions.Regex.IsMatch(strIn, ("^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$"))
  End Function
  Public Function IsValidWebSite(ByVal strIn As String) As Boolean
    ' Return true if strIn is in valid web site format.
    Dim isValid As Boolean = False
    If System.Text.RegularExpressions.Regex.IsMatch(strIn, ("http://([\w-]\.)+[\w-](/[\w- ./?%=]*)?")) Or System.Text.RegularExpressions.Regex.IsMatch(strIn, ("([\w-]\.)+[\w-](/[\w- ./?%=]*)?")) Then
      isValid = True
    End If
    Return isValid
  End Function
  Public Function IsValidPhoneNumber(ByVal strIn As String) As Boolean
    Dim retValue As Boolean = False
    If IsNumeric(strIn) Then
      If strIn.Length = 7 Or strIn.Length = 10 Then
        retValue = True
      End If
    Else
      ' Allows phone number of the format: NPA = [2-9][0-8][0-9] Nxx = [2-9][0-9][0-9] Station = [0-9][0-9][0-9][0-9]
      'retValue = System.Text.RegularExpressions.Regex.IsMatch(strIn, ("((\(\d{3}\) ?)|(\d{3}-))?\d{3}-\d{4}"))
      retValue = System.Text.RegularExpressions.Regex.IsMatch(strIn, ("^[01]?[- .]?(\([2-9]\d{2}\)|[2-9]\d{2})[- .]?\d{3}[- .]?\d{4}$"))
    End If
    Return retValue
  End Function
  Public Function IsValidTaxId(ByVal strIn As String) As Boolean
    ' Return true if strIn is in valid tax id format.
    Return System.Text.RegularExpressions.Regex.IsMatch(strIn, ("^\d{2}-\d{7}$"))
  End Function
  Public Function IsValidSsn(ByVal strIn As String) As Boolean
    ' Return true if strIn is in valid SSN format.
    Return System.Text.RegularExpressions.Regex.IsMatch(strIn, ("^\d{3}-\d{2}-\d{4}$"))
  End Function
  Public Function IsValidZipCode(ByVal strIn As String) As Boolean
    ' Return true if strIn is in valid SSN format.
    Return System.Text.RegularExpressions.Regex.IsMatch(strIn, ("\d{5}(-\d{4})?"))
  End Function
  Public Function IsValidFloat(ByVal strIn As String) As Boolean
    Return System.Text.RegularExpressions.Regex.IsMatch(strIn, ("[-+]?[0-9]*\.?[0-9]+"))
  End Function
  Public Function GetMonthNumberAsString(ByVal dateIn As Date) As String
    Dim theMonth As String = ""
    If dateIn.Month < 10 Then
      theMonth += "0"
    End If
    theMonth += dateIn.Month.ToString
    Return theMonth
  End Function
  Public Function GetDayNumberAsString(ByVal dateIn As Date) As String
    Dim theDay As String = ""
    If dateIn.Day < 10 Then
      theDay += "0"
    End If
    theDay += dateIn.Day.ToString
    Return theDay
  End Function
  Public Function IsValidDate(ByVal dateIn As Date) As Boolean
    Dim validDate As Boolean = False

    If IsDate(dateIn) Then
      If dateIn > CDate("4/12/1961") Then
        validDate = True
      End If
    End If

    Return validDate
  End Function
  Public Enum Direction
    Ascending
    Descending
  End Enum
  Public Sub SortAnyList(ByVal List As Object, ByVal propName As String)
    Dim swapMade As Boolean = True
    Dim counter As Integer = 0
    'Dim colCount As Integer = List.Count
    Dim colObject As Object

    While swapMade
      swapMade = False
      counter = 0
      While counter < List.Count - 1
        If CStr(CallByName(List.Item(counter), propName, CallType.Get)).ToLower > CStr(CallByName(List.Item(counter + 1), propName, CallType.Get)).ToLower Then
          colObject = List.Item(counter)
          List.Remove(counter)
          List.Add(colObject)
          swapMade = True
        End If
        counter += 1
      End While
    End While
  End Sub
  Public Sub SortAnyList(ByVal List As Object, ByVal propName As String, ByVal sortDirection As Direction)
    Dim swapMade As Boolean = True
    Dim counter As Integer = 0
    'Dim colCount As Integer = List.Count
    Dim colObject As Object

    Select Case sortDirection
      Case Direction.Ascending
        While swapMade
          swapMade = False
          counter = 0
          While counter < List.Count - 1
            If CStr(CallByName(List.Item(counter), propName, CallType.Get)).ToLower > CStr(CallByName(List.Item(counter + 1), propName, CallType.Get)).ToLower Then
              colObject = List.Item(counter)
              List.Remove(counter)
              List.Add(colObject)
              swapMade = True
            End If
            counter += 1
          End While
        End While

      Case Direction.Descending
        While swapMade
          swapMade = False
          counter = 0
          While counter < List.Count - 1
            If CStr(CallByName(List.Item(counter), propName, CallType.Get)).ToLower < CStr(CallByName(List.Item(counter + 1), propName, CallType.Get)).ToLower Then
              colObject = List.Item(counter)
              List.Remove(counter)
              List.Add(colObject)
              swapMade = True
            End If
            counter += 1
          End While
        End While
    End Select
  End Sub
  Public Sub SortAnyList(ByVal List As Object, ByVal propName As String, ByVal sortDirection As Direction, ByVal sortIsNumeric As Boolean)
    Dim swapMade As Boolean = True
    Dim counter As Integer = 0
    'Dim colCount As Integer = List.Count
    Dim colObject As Object

    Select Case sortDirection
      Case Direction.Ascending
        While swapMade
          swapMade = False
          counter = 0
          While counter < List.Count - 1
            If sortIsNumeric Then
              If CallByName(List.Item(counter), propName, CallType.Get) > CallByName(List.Item(counter + 1), propName, CallType.Get) Then
                colObject = List.Item(counter)
                List.Remove(counter)
                List.Add(colObject)
                swapMade = True
              End If
            Else
              If CStr(CallByName(List.Item(counter), propName, CallType.Get)).ToLower > CStr(CallByName(List.Item(counter + 1), propName, CallType.Get)).ToLower Then
                colObject = List.Item(counter)
                List.Remove(counter)
                List.Add(colObject)
                swapMade = True
              End If
            End If
            counter += 1
          End While
        End While

      Case Direction.Descending
        While swapMade
          swapMade = False
          counter = 0
          While counter < List.Count - 1
            If sortIsNumeric Then
              If CallByName(List.Item(counter), propName, CallType.Get) < CallByName(List.Item(counter + 1), propName, CallType.Get) Then
                colObject = List.Item(counter)
                List.Remove(counter)
                List.Add(colObject)
                swapMade = True
              End If
            Else
              If CStr(CallByName(List.Item(counter), propName, CallType.Get)).ToLower < CStr(CallByName(List.Item(counter + 1), propName, CallType.Get)).ToLower Then
                colObject = List.Item(counter)
                List.Remove(counter)
                List.Add(colObject)
                swapMade = True
              End If
            End If
            counter += 1
          End While
        End While
    End Select
  End Sub
  Public Function WeekStartDate(ByVal dateIn As Date) As Date
    If IsValidDate(dateIn) Then
      Select Case CDate(dateIn).DayOfWeek
        Case DayOfWeek.Sunday
          Return dateIn

        Case DayOfWeek.Monday
          Return DateAdd(DateInterval.Day, -1, dateIn)

        Case DayOfWeek.Tuesday
          Return DateAdd(DateInterval.Day, -2, dateIn)

        Case DayOfWeek.Wednesday
          Return DateAdd(DateInterval.Day, -3, dateIn)

        Case DayOfWeek.Thursday
          Return DateAdd(DateInterval.Day, -4, dateIn)

        Case DayOfWeek.Friday
          Return DateAdd(DateInterval.Day, -5, dateIn)

        Case DayOfWeek.Saturday
          Return DateAdd(DateInterval.Day, -6, dateIn)

      End Select
    Else
      Throw New Exception("Cannot get the WeekStartDate without a valid Start Date. CommonLib.busFuncs.WeekStartDate(dateIn as date)")
    End If
  End Function
  Public Function WeekEndDate(ByVal dateIn) As Date
    If IsValidDate(dateIn) Then
      Select Case CDate(dateIn).DayOfWeek
        Case DayOfWeek.Saturday
          Return dateIn

        Case DayOfWeek.Friday
          Return DateAdd(DateInterval.Day, +1, dateIn)

        Case DayOfWeek.Thursday
          Return DateAdd(DateInterval.Day, +2, dateIn)

        Case DayOfWeek.Wednesday
          Return DateAdd(DateInterval.Day, +3, dateIn)

        Case DayOfWeek.Tuesday
          Return DateAdd(DateInterval.Day, +4, dateIn)

        Case DayOfWeek.Monday
          Return DateAdd(DateInterval.Day, +5, dateIn)

        Case DayOfWeek.Sunday
          Return DateAdd(DateInterval.Day, +6, dateIn)

      End Select
    Else
      Throw New Exception("Cannot get the WeekEndDate without a valid End Date. CommonLib.busFuncs.WeekStartDate(dateIn as date)")
    End If

  End Function
End Module
