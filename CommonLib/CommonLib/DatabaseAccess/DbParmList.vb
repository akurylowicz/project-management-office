Public Class DbParmList
  Inherits System.Collections.CollectionBase

  Public Sub Add(ByVal parmIn As DbParm)
    List.Add(parmIn)
  End Sub

  Public Sub Remove(ByVal index As Integer)
    If index > Count - 1 Or index < 0 Then
      Dim qacException As New Exception("The index passed to the Remove method does not exist in the Collection")
      Throw qacException
    Else
      List.RemoveAt(index)
    End If
  End Sub
  Public ReadOnly Property Item(ByVal index As Integer) As DbParm
    Get
      If index > Count - 1 Or index < 0 Then
        Dim qacException As New Exception("The index passed to the Item method does not exist in the Collection")
        Throw qacException
      Else
        Return CType(List.Item(index), DbParm)
      End If
    End Get
  End Property
  Public Function GetParmByName(ByVal parmNameIn As Integer) As DbParm
    Dim tmpParm As DbParm
    Dim retVal As DbParm = Nothing
    For Each tmpParm In Me.List
      If tmpParm.ParmName = parmNameIn Then
        retVal = tmpParm
        Exit For
      End If
    Next
    Return retVal
  End Function

End Class
