Public Class DbXfer
  Private Const cClassName As String = "DbXfer"
  Private cClassAuthor As String = "John Vancil"
  Private Const cClassVersion As String = "1.0"

  Private mDbMgr As DALDataManager
  Private mDatabaseName As String
  Private mDatabaseId As Integer
  Private mParmList As DbParmList

#Region "Class Information"
  Public ReadOnly Property ClassName() As String
    Get
      Return cClassName
    End Get
  End Property
  Public ReadOnly Property ClassAuthor() As String
    Get
      Return cClassAuthor
    End Get
  End Property
  Public ReadOnly Property ClassVersion() As String
    Get
      Return cClassVersion
    End Get
  End Property
  Public Function GetXml() As Object
    GetXml = Nothing
  End Function
  Public Overrides Function ToString() As String
    If (mDatabaseName > "" Or mDatabaseId > 0) Then
      If mParmList.Count > 0 Then
        Dim theString As String
        theString = cClassName & " v." & cClassVersion & " - " & Me.mDatabaseId & " " & Me.mDatabaseName & vbCrLf
        Dim theParm As DbParm
        For Each theParm In mParmList
          theString = theString & "Param: " & theParm.ParmName & " - " & theParm.ParmValue & vbCrLf
        Next
        ToString = theString
      Else
        ToString = cClassName & " v." & cClassVersion & " - " & Me.mDatabaseId & " " & Me.mDatabaseName
      End If
    Else
      ToString = "DbXfer"
    End If
  End Function
#End Region

  Public Sub New()
    Initialize()
  End Sub
  Public Sub New(ByVal dbNameIn As String, ByVal parmListIn As DbParmList, ByVal dbMgrIn As DALDataManager)
    Initialize()
    Me.mDatabaseName = dbNameIn
    Me.mDatabaseId = GetDbId()
    Me.mParmList = parmListIn
    Me.mDbMgr = dbMgrIn
  End Sub
  Public Sub New(ByVal dbIdIn As Integer, ByVal parmListIn As DbParmList, ByVal dbMgrIn As DALDataManager)
    Initialize()
    Me.mDatabaseId = dbIdIn
    Me.mDatabaseName = GetDbName()
    Me.mParmList = parmListIn
    Me.mDbMgr = dbMgrIn
  End Sub

  Private Sub Initialize()
    Me.mDatabaseName = ""
    Me.mParmList = Nothing
  End Sub

  Public Property DatabaseName() As String
		Get
			If Not Me.DatabaseMgr.DirectSetConnection Then
				If Me.mDatabaseName = "" Then
					If Me.mDatabaseId > 0 Then
						Me.mDatabaseName = GetDbName()
					End If
				End If
			End If
			Return Me.mDatabaseName
		End Get
    Set(ByVal Value As String)
      Me.mDatabaseName = Value
    End Set
  End Property
  Public Property DatabaseId() As Integer
		Get
			If Not Me.DatabaseMgr.DirectSetConnection Then
				If Me.mDatabaseId = 0 Then
					If Me.mDatabaseName > "" Then
						Me.mDatabaseId = GetDbId()
					End If
				End If
			End If
			Return Me.mDatabaseId
		End Get
    Set(ByVal Value As Integer)
      Me.mDatabaseId = Value
    End Set
  End Property
  Public Property ParmList() As DbParmList
    Get
      Return Me.mParmList
    End Get
    Set(ByVal Value As DbParmList)
      Me.mParmList = Value
    End Set
  End Property
  Public Property DatabaseMgr() As DALDataManager
    Get
      Return Me.mDbMgr
    End Get
    Set(ByVal Value As DALDataManager)
      Me.mDbMgr = Value
    End Set
  End Property

  Private Function GetDbId() As Integer
    Return mDbMgr.GetDatabaseId(Me.mDatabaseName)
  End Function
  Private Function GetDbName() As String
    Return mDbMgr.GetDatabaseName(Me.mDatabaseId)
  End Function

End Class
