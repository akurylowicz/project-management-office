Public Class DbParm
  Private Const cClassName As String = "DbParm"
  Private Const cClassAuthor As String = "John Vancil"
  Private Const cClassVersion As String = "1.0"

  Private mParmName As String
  Private mParmValue As Object
  Private mParmType As Integer
  Private mParmCanBeNull As Boolean

  Enum ParameterType
    IsInteger = 1
    IsString = 2
    IsDate = 3
    IsLong = 4
    IsBit = 5
    IsDouble = 6
    IsObject = 7
  End Enum

#Region "Class Information"
  Public ReadOnly Property ClassName() As String
    Get
      Return cClassName
    End Get
  End Property
  Public ReadOnly Property ClassAuthor() As String
    Get
      Return cClassAuthor
    End Get
  End Property
  Public ReadOnly Property ClassVersion() As String
    Get
      Return cClassVersion
    End Get
  End Property
  Public Function GetXml() As Object
    GetXml = Nothing
  End Function
  Public Overrides Function ToString() As String
    ToString = "DbParm"
  End Function
#End Region

  Public Sub New()
    Initialize()
  End Sub
  Public Sub New(ByVal nameIn As String, ByVal valueIn As Object)
    Initialize()
    ParmName = nameIn
    ParmValue = valueIn
  End Sub
  Public Sub New(ByVal nameIn As String, ByVal valueIn As Object, ByVal typeIn As Integer)
    Initialize()
    ParmName = nameIn
    ParmValue = valueIn
    ParmType = typeIn
  End Sub
  Public Sub New(ByVal nameIn As String, ByVal valueIn As Object, ByVal typeIn As Integer, ByVal canBeNull As Boolean)
    Initialize()
    ParmName = nameIn
    ParmValue = valueIn
    ParmType = typeIn
    ParmCanBeNull = canBeNull
  End Sub

  Private Sub Initialize()
    Me.mParmCanBeNull = False
    Me.mParmName = ""
    Me.mParmValue = Nothing
    Me.mParmType = ParameterType.IsObject
  End Sub

  Public Property ParmName() As String
    Get
      Return Me.mParmName
    End Get
    Set(ByVal Value As String)
      Me.mParmName = Value
    End Set
  End Property
  Public Property ParmValue() As Object
    Get
      Return Me.mParmValue
    End Get
    Set(ByVal Value As Object)
      Me.mParmValue = Value
    End Set
  End Property
  Public Property ParmType() As Integer
    Get
      Return Me.mParmType
    End Get
    Set(ByVal Value As Integer)
      Me.mParmType = Value
    End Set
  End Property
  Public Property ParmCanBeNull() As Boolean
    Get
      Return Me.mParmCanBeNull
    End Get
    Set(ByVal Value As Boolean)
      Me.mParmCanBeNull = Value
    End Set
  End Property
End Class
