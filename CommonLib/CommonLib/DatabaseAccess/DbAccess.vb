Imports System
Imports System.Configuration
Imports System.Data
Imports System.Data.SqlClient

Public Class DbAccess
  Private mDbXfer As DbXfer
  Private mDbMgr As DALDataManager
  Private mParmList As DbParmList
  Private mSprocName As String
  Private mDbName As String
  Private mDbId As Integer
  Private cn As SqlConnection
  Private mReturnValue As Object

  Private Const mRecordAlreadyExists As Integer = 2627

  Public Sub New()
    mDbXfer = Nothing
    mDbMgr = Nothing
    mParmList = Nothing
    mDbName = ""
    mDbId = 0
    mSprocName = ""
    mReturnValue = Nothing
  End Sub

  Public ReadOnly Property ReturnValue() As Object
    Get
      Return Me.mReturnValue
    End Get
  End Property
  Public ReadOnly Property Connection(ByVal dbXferIn As DbXfer) As SqlConnection
    Get
      mDbXfer = dbXferIn
      mDbMgr = mDbXfer.DatabaseMgr
      mParmList = dbXferIn.ParmList
      mDbName = dbXferIn.DatabaseName
      mDbId = dbXferIn.DatabaseId
      Return GetConnection()
    End Get
  End Property
  Private Function GetConnection() As SqlConnection
		Try
			If Me.mDbMgr.DirectSetConnection Then
				Return mDbMgr.DbDirectSetConnection
			Else
				If Me.mDbId > 0 Then
					Return mDbMgr.DbConnection(mDbId)
				Else
					Return mDbMgr.DbConnection(mDbName)
				End If
			End If

		Catch exc As DALDataException
			exc.LogException()
			Throw exc

		Catch otherExc As Exception
			Throw otherExc

		End Try

  End Function

  Public Function Insert(ByVal dbXferIn As DbXfer, ByVal sprocName As String, ByVal tryUpdateAfterFailFlag As Boolean, Optional ByVal updateSproc As String = "") As Boolean
    Dim updateAfterFailedInsert As Boolean = tryUpdateAfterFailFlag
    Dim retValue As Integer = 0
    Dim retCode As Boolean = False
    mDbXfer = dbXferIn
    mDbMgr = mDbXfer.DatabaseMgr
    mParmList = dbXferIn.ParmList
    mDbName = dbXferIn.DatabaseName
    mDbId = dbXferIn.DatabaseId
    mSprocName = sprocName

    cn = GetConnection()

    Dim dbCmd As New SqlCommand(mSprocName, cn)
    dbCmd.CommandType = CommandType.StoredProcedure

    Dim tmpParm As DbParm

    For Each tmpParm In Me.mParmList
      dbCmd.Parameters.Add(New SqlParameter(tmpParm.ParmName, tmpParm.ParmValue))
    Next

    Dim tmpSqlParm As New SqlParameter
    tmpSqlParm.ParameterName = "@RETURN_VALUE"
    tmpSqlParm.Value = 0
    tmpSqlParm.Direction = ParameterDirection.Output
    dbCmd.Parameters.Add(tmpSqlParm)

    Try
      cn.Open()
      retValue = dbCmd.ExecuteNonQuery()

      If retValue > 0 Then
        Me.mReturnValue = dbCmd.Parameters.Item("@RETURN_VALUE").Value
        retCode = True
      Else
        Dim insertExc As New DALDataException("Insert failed.  Returned value was: " & retCode)
        Throw insertExc
      End If

    Catch xSql As SqlClient.SqlException
      ' If there is a sql error that indicates that this would be a duplicate record and if the updateAfterFailedInsert
      ' flag is set to true, then try an update.  If the update fails, then the exception will be thrown from there.

      If xSql.Number = DbAccess.mRecordAlreadyExists And updateAfterFailedInsert And updateSproc > "" Then
        retCode = Update(dbXferIn, updateSproc)
      Else
        Dim exc As New DALDataException("Unable to insert record in Insert() in NucNucDatabase.NucDatabase. " _
        & ". Sql Error in is " & xSql.Number & " - " & xSql.Message, Today.ToShortDateString, Now.ToShortTimeString)
        exc.ExceptionCode = xSql.Number
        Throw (exc)
      End If

    Catch ex As Exception
      Throw ex

    Finally
      cn.Close()
      dbCmd = Nothing
    End Try

    Return retCode

  End Function
  Public Function Update(ByVal dbXferIn As DbXfer, ByVal sprocName As String) As Boolean
    Dim retValue As Integer = 0
    Dim retCode As Boolean = False
    mDbXfer = dbXferIn
    mDbMgr = mDbXfer.DatabaseMgr
    mParmList = dbXferIn.ParmList
    mDbName = dbXferIn.DatabaseName
    mDbId = dbXferIn.DatabaseId
    mSprocName = sprocName

    cn = GetConnection()

    Dim dbCmd As New SqlCommand(mSprocName, cn)
    dbCmd.CommandType = CommandType.StoredProcedure

    Dim tmpParm As DbParm

    For Each tmpParm In Me.mParmList
      dbCmd.Parameters.Add(New SqlParameter(tmpParm.ParmName, tmpParm.ParmValue))
    Next

    Try
      cn.Open()
      retValue = dbCmd.ExecuteNonQuery()

      If retValue > 0 Then
        retCode = True
      Else
        Dim insertExc As New DALDataException("Update failed.  Returned value was: " & retCode)
        Throw insertExc
      End If

    Catch xSql As SqlClient.SqlException
      Dim excSql As New DALDataException("Unable to update record in Update() in NucNucDatabase.NucDatabase. " _
        & ". Sql Error in is " & xSql.Number & " - " & xSql.Message, Today.ToShortDateString, Now.ToShortTimeString)
      excSql.ErrorDate = Today
      Throw excSql

    Catch xSqlType As System.Data.SqlTypes.SqlTypeException
      Dim excSqlType As New DALDataException("Unable to update record in Update() in NucNucDatabase.NucDatabase. " _
        & ". " & xSqlType.Message, Today.ToShortDateString, Now.ToShortTimeString)
      excSqlType.ErrorDate = Today
      Throw (excSqlType)

    Catch ex As Exception
      Dim nucEx As New LogException(ex.Message)
      nucEx.ExeptionModule = "DbAccess"
      nucEx.ExceptionFunction = "Update()"
      nucEx.LogException()
      Throw nucEx

    Finally
      cn.Close()
      dbCmd = Nothing
    End Try

    Return retCode

  End Function
  Public Function Delete(ByVal dbXferIn As DbXfer, ByVal sprocName As String) As Boolean
    Dim retValue As Integer = 0
    Dim retCode As Boolean = False
    mDbXfer = dbXferIn
    mDbMgr = mDbXfer.DatabaseMgr
    mParmList = dbXferIn.ParmList
    mDbName = dbXferIn.DatabaseName
    mDbId = dbXferIn.DatabaseId
    mSprocName = sprocName

    cn = GetConnection()

    Dim dbCmd As New SqlCommand(mSprocName, cn)
    dbCmd.CommandType = CommandType.StoredProcedure

    Dim tmpParm As DbParm

    For Each tmpParm In Me.mParmList
      dbCmd.Parameters.Add(New SqlParameter(tmpParm.ParmName, tmpParm.ParmValue))
    Next

    Try
      cn.Open()
      retValue = dbCmd.ExecuteNonQuery()

      If retValue > 0 Then
        retCode = True
      Else
        Dim insertExc As New DALDataException("Delete failed.  Returned value was: " & retValue)
        Throw insertExc
      End If

    Catch xSql As SqlClient.SqlException
      Dim exc As New DALDataException("Unable to delete record in Delete() in NucNucDatabase.NucDatabase. " _
      & ". Sql Error in is " & xSql.Number & " - " & xSql.Message, Today.ToShortDateString, Now.ToShortTimeString)
      Throw (exc)

    Finally
      cn.Close()
      dbCmd = Nothing
    End Try

    Return retCode

  End Function
  Public Function GetRecord(ByVal dbXferIn As DbXfer, ByVal sprocName As String, Optional ByVal datasetTableName As String = "") As DataSet
    Dim ds As New DataSet
    mDbXfer = dbXferIn
    mDbMgr = mDbXfer.DatabaseMgr
    mParmList = dbXferIn.ParmList
    mDbName = dbXferIn.DatabaseName
    mDbId = dbXferIn.DatabaseId
    mSprocName = sprocName

    cn = GetConnection()

    Dim dbCmd As New SqlDataAdapter(mSprocName, cn)
    dbCmd.SelectCommand.CommandType = CommandType.StoredProcedure

    Dim tmpParm As DbParm

    If Not Me.mParmList Is Nothing Then
      For Each tmpParm In Me.mParmList
        dbCmd.SelectCommand.Parameters.Add(New SqlParameter(tmpParm.ParmName, tmpParm.ParmValue))
      Next
    End If

    If datasetTableName = "" Then
      datasetTableName = "noTableNameProvided"
    End If

    Try
      cn.Open()
      dbCmd.Fill(ds, datasetTableName)
      mReturnValue = ds

    Catch xSql As SqlClient.SqlException
      Dim exc As New DALDataException("Unable to get record in GetRecord() in NucNucDatabase.NucDatabase. " _
      & ". Sql Error in is " & xSql.Number & " - " & xSql.Message, Today.ToShortDateString, Now.ToShortTimeString)
      exc.LogException()
      Throw (exc)

    Catch ex As Exception
      Dim exc As New DALDataException("Unable to get record in GetRecord() in NucNucDatabase.NucDatabase. " _
      & ex.Message, Today.ToShortDateString, Now.ToShortTimeString)
      exc.LogException()
      Throw (exc)

    Finally
      cn.Close()
      dbCmd = Nothing
    End Try

    Return CType(mReturnValue, DataSet)

  End Function
  Public Function GetRecord(ByVal dbXferIn As DbXfer, ByVal sprocName As String, ByVal datasetIn As DataSet, Optional ByVal datasetTableName As String = "") As DataSet
    mDbXfer = dbXferIn
    mDbMgr = mDbXfer.DatabaseMgr
    mParmList = dbXferIn.ParmList
    mDbName = dbXferIn.DatabaseName
    mDbId = dbXferIn.DatabaseId
    mSprocName = sprocName

    cn = GetConnection()

    Dim dbCmd As New SqlDataAdapter(mSprocName, cn)
    dbCmd.SelectCommand.CommandType = CommandType.StoredProcedure

    Dim tmpParm As DbParm

    If Not Me.mParmList Is Nothing Then
      For Each tmpParm In Me.mParmList
        dbCmd.SelectCommand.Parameters.Add(New SqlParameter(tmpParm.ParmName, tmpParm.ParmValue))
      Next
    End If

    If datasetTableName = "" Then
      datasetTableName = "noTableNameProvided"
    End If

    Try
      cn.Open()
      dbCmd.Fill(datasetIn, datasetTableName)
      mReturnValue = datasetIn

    Catch xSql As SqlClient.SqlException
      Dim exc As New DALDataException("Unable to get record in GetRecord() in NucNucDatabase.NucDatabase. " _
      & ". Sql Error in is " & xSql.Number & " - " & xSql.Message, Today.ToShortDateString, Now.ToShortTimeString)
      Throw (exc)

    Finally
      cn.Close()
      dbCmd = Nothing
    End Try

    Return CType(mReturnValue, DataSet)

  End Function
  Public Function ExecuteProc(ByVal dbXferIn As DbXfer, ByVal sprocName As String) As Integer
    mDbXfer = dbXferIn
    mDbMgr = mDbXfer.DatabaseMgr
    mParmList = dbXferIn.ParmList
    mDbName = dbXferIn.DatabaseName
    mDbId = dbXferIn.DatabaseId
    mSprocName = sprocName

    cn = GetConnection()

    Dim dbCmd As New SqlCommand(mSprocName, cn)
    dbCmd.CommandType = CommandType.StoredProcedure

    Dim tmpParm As DbParm

    If Not Me.mParmList Is Nothing Then
      For Each tmpParm In Me.mParmList
        dbCmd.Parameters.Add(New SqlParameter(tmpParm.ParmName, tmpParm.ParmValue))
      Next
    End If

    Try
      cn.Open()
      mReturnValue = dbCmd.ExecuteNonQuery()

    Catch xSql As SqlClient.SqlException
      Dim exc As New DALDataException("Unable to execute Stored Procedure: " & mSprocName & " in ExecuteProc() in NucNucDatabase.NucDatabase. " _
      & ". Sql Error in is " & xSql.Number & " - " & xSql.Message, Today.ToShortDateString, Now.ToShortTimeString)
      Throw (exc)

    Finally
      cn.Close()
      dbCmd = Nothing
    End Try

    Return CType(mReturnValue, Integer)

  End Function
  Public Function ExecuteSql(ByVal dbXferIn As DbXfer, ByVal sqlStatememt As String) As Object
    mDbXfer = dbXferIn
    mDbMgr = mDbXfer.DatabaseMgr
    mParmList = dbXferIn.ParmList
    mDbName = dbXferIn.DatabaseName
    mDbId = dbXferIn.DatabaseId

    cn = GetConnection()

    Dim dbCmd As New SqlCommand(sqlStatememt, cn)
    dbCmd.CommandType = CommandType.Text

    Dim tmpParm As DbParm

    If Not Me.mParmList Is Nothing Then
      For Each tmpParm In Me.mParmList
        dbCmd.Parameters.Add(New SqlParameter(tmpParm.ParmName, tmpParm.ParmValue))
      Next
    End If

    Try
      cn.Open()
      mReturnValue = dbCmd.ExecuteScalar()

    Catch xSql As SqlClient.SqlException
      Dim exc As New DALDataException("Unable to execute Stored Procedure: " & mSprocName & " in ExecuteProc() in NucNucDatabase.NucDatabase. " _
      & ". Sql Error in is " & xSql.Number & " - " & xSql.Message, Today.ToShortDateString, Now.ToShortTimeString)
      Throw (exc)

    Finally
      cn.Close()
      dbCmd = Nothing
    End Try

    Return mReturnValue

  End Function
  Public Function ExecuteSqlForDataset(ByVal dbXferIn As DbXfer, ByVal sqlStatememt As String, Optional ByVal dataSetTableName As String = "") As DataSet
    mDbXfer = dbXferIn
    mDbMgr = mDbXfer.DatabaseMgr
    mParmList = dbXferIn.ParmList
    mDbName = dbXferIn.DatabaseName
    mDbId = dbXferIn.DatabaseId
    Dim ds As New DataSet

    cn = GetConnection()

    Dim dbCmd As New SqlDataAdapter(sqlStatememt, cn)
    dbCmd.SelectCommand.CommandType = CommandType.Text

    If dataSetTableName = "" Then
      dataSetTableName = "noTableNameProvided"
    End If

    Try
      cn.Open()
      dbCmd.Fill(ds, dataSetTableName)
      mReturnValue = ds

    Catch xSql As SqlClient.SqlException
      Dim exc As New DALDataException("Unable to execute Stored Procedure: " & mSprocName & " in ExecuteProc() in NucNucDatabase.NucDatabase. " _
      & ". Sql Error in is " & xSql.Number & " - " & xSql.Message, Today.ToShortDateString, Now.ToShortTimeString)
      Throw (exc)

    Finally
      cn.Close()
      dbCmd = Nothing
    End Try

    Return mReturnValue

  End Function
End Class
