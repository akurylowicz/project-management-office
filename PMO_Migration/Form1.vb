﻿Imports pmoLib
Public Class Form1

    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        CommonLib.DALDataManager.CreateDatabaseObject("Data Source=ost-sql01;Initial Catalog=pmo_v2;User Id=sa_pmo;PWD=n56!L98")
    End Sub

    Private Sub btnSimulation_Click(sender As Object, e As EventArgs) Handles btnSimulation.Click
        Dim theMgr As New pmoManager
        Dim oldRequests As OldSurveyRequestList
        oldRequests = theMgr.GetAllSurveyRequestList
        Me.lblRequestCount.Text = "Request Count: " & oldRequests.Count
        Dim counter As Integer = 0
        Dim foundCount As Integer = 0
        Dim notFoundCount As Integer = 0
        Dim toProcess As Integer = 0
        Dim tmpRequest As OldSurveyRequest
        While counter < oldRequests.Count
            tmpRequest = oldRequests.Item(counter)
            Dim ds As DataSet
            ds = theMgr.GetChangepointProjectByOldProjectId(tmpRequest.ProjectId)
            If ds.Tables(0).Rows.Count > 0 Then
                foundCount += 1
                Dim newRequest As New CpSurveyRequest
                newRequest.InitialLoad = True
                newRequest.CompleteDate = tmpRequest.CompleteDate
                newRequest.EmailAddress = tmpRequest.EmailAddress
                newRequest.FollowUpDate = tmpRequest.FollowUpDate
                newRequest.ProjectId = ds.Tables(0).Rows(0).Item("ProjectId")
                newRequest.Customer = ds.Tables(0).Rows(0).Item("Customer_name")
                newRequest.Title = ds.Tables(0).Rows(0).Item("Project_name")
                newRequest.StartDate = ds.Tables(0).Rows(0).Item("Start_date")
                newRequest.RequestDate = tmpRequest.RequestDate
                newRequest.RequestId = tmpRequest.RequestId
                If newRequest.Validate.Count > 0 Then
                    MessageBox.Show("Validation Issue on: " & newRequest.ProjectId.ToString & " Old Project Id: " & tmpRequest.ProjectId)
                Else
                    newRequest.Persist()
                End If
            Else
                Dim theProj As New OldProject(tmpRequest.ProjectId)
                notFoundCount += 1
                Dim newRequest As New CpSurveyRequest
                newRequest.InitialLoad = True
                newRequest.CompleteDate = tmpRequest.CompleteDate
                newRequest.EmailAddress = tmpRequest.EmailAddress
                newRequest.FollowUpDate = tmpRequest.FollowUpDate
                newRequest.OldProjectId = tmpRequest.ProjectId
                newRequest.RequestDate = tmpRequest.RequestDate
                newRequest.RequestId = tmpRequest.RequestId

                If theProj Is Nothing Then
                    newRequest.Customer = "Not Found for: " & tmpRequest.ProjectId
                    newRequest.Title = "Not Found for: " & tmpRequest.ProjectId
                Else
                    newRequest.Customer = theProj.Customer
                    newRequest.Title = theProj.Title
                    newRequest.StartDate = theProj.StartDate
                End If

                If newRequest.Validate.Count > 0 Then
                    MessageBox.Show("Validation Issue on: " & newRequest.ProjectId.ToString & " Old Project Id: " & tmpRequest.ProjectId)
                Else
                    newRequest.Persist()
                End If
            End If
            counter += 1
        End While
        Me.lblFoundCount.Text = "Found Count: " & foundCount
        Me.lblNotFound.Text = "Not Found Count: " & notFoundCount
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Dim theMgr As New pmoManager
        Dim oldList As New CompletedProjectList
        oldList = theMgr.GetCompletedProjectList()
        Me.lblProjCompCount.Text = "Completed Count: " & oldList.Count
        Dim counter As Integer = 0
        Dim foundCount As Integer = 0
        Dim notFoundCount As Integer = 0
        Dim toProcess As Integer = 0
        While counter < oldList.Count
            Dim oldComp As CompletedProject = oldList.Item(counter)
            Dim ds As New DataSet
            ds = theMgr.GetChangepointProjectByOldProjectId(oldComp.ProjectId)
            If ds.Tables(0).Rows.Count > 0 Then
                Dim compProj As New CpCompletedProject
                compProj.ProjectId = ds.Tables(0).Rows(0).Item("ProjectId")
                compProj.CompletedDate = oldComp.CompletedDate
                compProj.persist()
                foundCount += 1
            Else
                notFoundCount += 1
            End If
            counter += 1
        End While
        lblCompletedFound.Text = "Completed Found: " & foundCount
        lblCompletedNotFound.Text = "Completed Not Found: " & notFoundCount
    End Sub
End Class
