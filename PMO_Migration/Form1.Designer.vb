﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.btnSimulation = New System.Windows.Forms.Button()
        Me.lblRequestCount = New System.Windows.Forms.Label()
        Me.lblFoundCount = New System.Windows.Forms.Label()
        Me.lblNotFound = New System.Windows.Forms.Label()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.lblProjCompCount = New System.Windows.Forms.Label()
        Me.lblCompletedFound = New System.Windows.Forms.Label()
        Me.lblCompletedNotFound = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(12, 23)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(121, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "PMO Migration Progress"
        '
        'btnSimulation
        '
        Me.btnSimulation.Location = New System.Drawing.Point(268, 18)
        Me.btnSimulation.Name = "btnSimulation"
        Me.btnSimulation.Size = New System.Drawing.Size(125, 23)
        Me.btnSimulation.TabIndex = 1
        Me.btnSimulation.Text = "Run Simulation"
        Me.btnSimulation.UseVisualStyleBackColor = True
        '
        'lblRequestCount
        '
        Me.lblRequestCount.AutoSize = True
        Me.lblRequestCount.Location = New System.Drawing.Point(265, 54)
        Me.lblRequestCount.Name = "lblRequestCount"
        Me.lblRequestCount.Size = New System.Drawing.Size(78, 13)
        Me.lblRequestCount.TabIndex = 2
        Me.lblRequestCount.Text = "Request Count"
        '
        'lblFoundCount
        '
        Me.lblFoundCount.AutoSize = True
        Me.lblFoundCount.Location = New System.Drawing.Point(265, 76)
        Me.lblFoundCount.Name = "lblFoundCount"
        Me.lblFoundCount.Size = New System.Drawing.Size(68, 13)
        Me.lblFoundCount.TabIndex = 3
        Me.lblFoundCount.Text = "Found Count"
        '
        'lblNotFound
        '
        Me.lblNotFound.AutoSize = True
        Me.lblNotFound.Location = New System.Drawing.Point(265, 98)
        Me.lblNotFound.Name = "lblNotFound"
        Me.lblNotFound.Size = New System.Drawing.Size(88, 13)
        Me.lblNotFound.TabIndex = 4
        Me.lblNotFound.Text = "Not Found Count"
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(458, 18)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(195, 23)
        Me.Button1.TabIndex = 5
        Me.Button1.Text = "Run Completed Simulation"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'lblProjCompCount
        '
        Me.lblProjCompCount.AutoSize = True
        Me.lblProjCompCount.Location = New System.Drawing.Point(455, 54)
        Me.lblProjCompCount.Name = "lblProjCompCount"
        Me.lblProjCompCount.Size = New System.Drawing.Size(88, 13)
        Me.lblProjCompCount.TabIndex = 6
        Me.lblProjCompCount.Text = "Completed Count"
        '
        'lblCompletedFound
        '
        Me.lblCompletedFound.AutoSize = True
        Me.lblCompletedFound.Location = New System.Drawing.Point(455, 76)
        Me.lblCompletedFound.Name = "lblCompletedFound"
        Me.lblCompletedFound.Size = New System.Drawing.Size(68, 13)
        Me.lblCompletedFound.TabIndex = 7
        Me.lblCompletedFound.Text = "Found Count"
        '
        'lblCompletedNotFound
        '
        Me.lblCompletedNotFound.AutoSize = True
        Me.lblCompletedNotFound.Location = New System.Drawing.Point(455, 98)
        Me.lblCompletedNotFound.Name = "lblCompletedNotFound"
        Me.lblCompletedNotFound.Size = New System.Drawing.Size(88, 13)
        Me.lblCompletedNotFound.TabIndex = 8
        Me.lblCompletedNotFound.Text = "Not Found Count"
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1092, 262)
        Me.Controls.Add(Me.lblCompletedNotFound)
        Me.Controls.Add(Me.lblCompletedFound)
        Me.Controls.Add(Me.lblProjCompCount)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.lblNotFound)
        Me.Controls.Add(Me.lblFoundCount)
        Me.Controls.Add(Me.lblRequestCount)
        Me.Controls.Add(Me.btnSimulation)
        Me.Controls.Add(Me.Label1)
        Me.Name = "Form1"
        Me.Text = "PMO Migration Tool"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents btnSimulation As System.Windows.Forms.Button
    Friend WithEvents lblRequestCount As System.Windows.Forms.Label
    Friend WithEvents lblFoundCount As System.Windows.Forms.Label
    Friend WithEvents lblNotFound As System.Windows.Forms.Label
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents lblProjCompCount As System.Windows.Forms.Label
    Friend WithEvents lblCompletedFound As System.Windows.Forms.Label
    Friend WithEvents lblCompletedNotFound As System.Windows.Forms.Label

End Class
