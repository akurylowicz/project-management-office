﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmApplicationError.aspx.vb" Inherits="PMO.frmApplicationError" %>

<%@ Register src="../ucControls/ucStrippedHeader.ascx" tagname="ucStrippedHeader" tagprefix="uc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>PMO Portal - Application Error</title>
    <link href=../ost.css rel=Stylesheet />
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
        <uc1:ucStrippedHeader ID="ucStrippedHeader1" runat="server" />
    
    </div>
          <table width:"100%" width="100%">
            <tr>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td align="center" valign="middle">
                <asp:Panel ID="Panel1" runat="server" CssClass="pmoErrorPanel" Width="60%">
                    <table width:"50%">
                        <tr>
                            <td align="center">
                                <asp:Label ID="lblErrorMessage" runat="server" Text="Label" 
                                CssClass="messageLabel"></asp:Label>
                            </td>
                        </tr>
                    </table>
                    </asp:Panel>
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;</td>
            </tr>
              <tr>
                <td>
                    &nbsp;</td>
            </tr>
              <tr>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td>
                    <asp:Table ID="tblDebug" runat="server">
                    </asp:Table>
                </td>
            </tr>
        </table>
        </form>
</body>
</html>
