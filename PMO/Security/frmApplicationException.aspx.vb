﻿Public Partial Class frmApplicationException
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    If Not Session Is Nothing Then
      If Not Session("LastException") Is Nothing Then
        Dim ex As Exception = Session("LastException")
        If TypeOf ex Is pmoLib.PmoException Then
          If CType(ex, pmoLib.PmoException).UserAction > "" Then
            Me.lblWhatCanYouDo.Text = CType(ex, pmoLib.PmoException).UserAction
          Else
            Me.lblWhatCanYouDo.Text = "No User action is required.  You may attempt to perform the task again by clicking the Portal Home link and starting over."
          End If

          If CType(ex, pmoLib.PmoException).UserExplanation > "" Then
            Me.lblWhatAffected.Text = CType(ex, pmoLib.PmoException).UserExplanation
          Else
            Me.lblWhatAffected.Text = "A Serious error has occurred within the application.  A notification has been sent to the System Administrator and additional information is displayed below."
          End If

          'Me.lblSupportInfo.Text = "<ul>"
          Me.lblSupportInfo.Text += "<li> Time Stamp: " & CType(ex, pmoLib.PmoException).TimeStamp
          Me.lblSupportInfo.Text += "<li> Type: " & CType(ex, pmoLib.PmoException).ExceptionType
          Me.lblSupportInfo.Text += "<li> Class: " & CType(ex, pmoLib.PmoException).ExceptionClass
          Me.lblSupportInfo.Text += "<li> Function: " & CType(ex, pmoLib.PmoException).ExceptionFunction
          Me.lblSupportInfo.Text += "<li> Message: " & CType(ex, pmoLib.PmoException).ExceptionMessage
          Me.lblSupportInfo.Text += "<li> Stack Trace: " & CType(ex, pmoLib.PmoException).ExceptionStackTrace
          'Me.lblSupportInfo.Text += "</ul>"

        Else
          Me.lblWhatCanYouDo.Text = "No User action is required.  You may attempt to perform the task again by clicking the Portal Home link and starting over."
          Me.lblWhatAffected.Text = "A Serious error has occurred within the application.  A notification has been sent to the System Administrator and additional information is displayed below."
          'Me.lblSupportInfo.Text = "<ul>"
          Me.lblSupportInfo.Text += "<li> Time Stamp: " & Now
          Me.lblSupportInfo.Text += "<li> Type: " & ex.GetType.ToString
          Me.lblSupportInfo.Text += "<li> Message: " & ex.Message.ToString
          Me.lblSupportInfo.Text += "<li> Stack Trace: " & ex.StackTrace.ToString
          'Me.lblSupportInfo.Text += "</ul>"
        End If
      Else
        Me.lblWhatCanYouDo.Text = "No User action is required.  You may attempt to perform the task again by clicking the Portal Home link and starting over."
        Me.lblWhatAffected.Text = "A Serious error has occurred within the application.  No Exception was generated. A notification has been sent to the System Administrator and additional information is displayed below."
      End If
    Else
      Me.lblWhatCanYouDo.Text = "No User action is required.  You may attempt to perform the task again by clicking the Portal Home link and starting over."
      Me.lblWhatAffected.Text = "A Serious error has occurred within the application.  The Session Object is null. A notification has been sent to the System Administrator and additional information is displayed below."
    End If
    End Sub

End Class