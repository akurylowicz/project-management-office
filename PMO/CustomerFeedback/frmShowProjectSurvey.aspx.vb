﻿Imports pmoLib

Partial Public Class frmShowProjectSurvey
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            Dim surv As New CpSurveyResult(Session("currProjectSurveyId"))
            'Dim proj As New CpProject(Session("currProjectSurveyProjectId"))
            If surv.OldProjectId = 0 Then
                Dim proj As New CpProject(surv.ProjectId)
                If Not proj Is Nothing Then
                    Session("currProjectSurveyId") = Nothing
                    If proj.ProjectSurveyCompleted Then
                        If proj.ProjectSurveyCount > 1 Then
                            Me.gvCompletedSurveys.Visible = True
                            LoadCompletedSurveys(proj)
                        Else
                            Me.gvCompletedSurveys.Visible = False
                            LoadSurvey(proj.CpProjectSurveyResultId)
                        End If
                    End If
                End If
            Else
                Dim proj As New OldProject(surv.OldProjectId)
                If Not proj Is Nothing Then
                    Session("currProjectSurveyId") = Nothing
                    If proj.ProjectSurveyCompleted Then
                        If proj.ProjectSurveyCount > 1 Then
                            Me.gvCompletedSurveys.Visible = True
                            LoadCompletedSurveys(proj)
                        Else
                            Me.gvCompletedSurveys.Visible = False
                            LoadSurvey(proj.ProjectSurveyResultId)
                        End If
                    End If
                End If

            End If

        End If
    End Sub
    Protected Overrides Function OnBubbleEvent(ByVal source As Object, ByVal args As System.EventArgs) As Boolean
        If TypeOf (args) Is MenuItemSelectedEventArgs Then
            Select Case CType(args, MenuItemSelectedEventArgs).MenuName
                Case "Main"
                    Select Case CType(args, MenuItemSelectedEventArgs).MenuItem
                        Case "SowListing"
                            Response.Redirect("~/SOW/frmSowListing.aspx")

                        Case "PmoDocuments"
                            Response.Redirect("~/Documents/frmDocumentListing.aspx")

                        Case "PmoUtilities"
                            Response.Redirect("~/PmoUtilities/frmPmoUtilities.aspx")

                        Case "PmoReporting"
                            Response.Redirect("~/Reporting/frmReports.aspx")

                        Case "PmoGatherFeedback"
                            Response.Redirect("~/CustomerFeedback/frmGatherCustomerFeedback.aspx")

                        Case "PmoCompletedSurveys"
                            Response.Redirect("~/CustomerFeedback/frmCompletedSurveys.aspx")
                    End Select
            End Select
        End If
        Return True
    End Function
    Private Sub LoadSurvey(ByVal surveyResultId As Integer)
        Me.Literal1.Text = ""
        Dim survResults As New CpSurveyResult(surveyResultId)
        If Not survResults Is Nothing Then
            Me.Literal1.Text = survResults.GetSurveyHTML
        End If
    End Sub
    Private Sub LoadCompletedSurveys(ByVal proj As CpProject)
        Me.gvCompletedSurveys.DataSource = Nothing
        Dim pmoMgr As New pmoManager
        Dim survList As CpSurveyResultList
        survList = pmoMgr.GetCompletedSurveyList(proj.Id)
        Me.gvCompletedSurveys.DataSource = survList
        Me.gvCompletedSurveys.DataBind()
    End Sub
    Private Sub LoadCompletedSurveys(ByVal proj As OldProject)
        Me.gvCompletedSurveys.DataSource = Nothing
        Dim pmoMgr As New pmoManager
        Dim survList As CpSurveyResultList
        survList = pmoMgr.GetCompletedSurveyList(proj.Id)
        Me.gvCompletedSurveys.DataSource = survList
        Me.gvCompletedSurveys.DataBind()
    End Sub

    Private Sub gvCompletedSurveys_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvCompletedSurveys.RowCommand
        Select Case e.CommandName
            Case "SurveyLink"
                Dim tmpId As Integer = CInt(Me.gvCompletedSurveys.Rows(e.CommandArgument).Cells(0).Text)
                If tmpId > 0 Then
                    LoadSurvey(tmpId)
                End If
        End Select
    End Sub
End Class