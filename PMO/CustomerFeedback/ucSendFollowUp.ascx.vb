﻿Imports Commonlib
Imports pmoLib

Partial Public Class ucSendFollowUp
  Inherits System.Web.UI.UserControl

  Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

  End Sub
  Public Sub SetFollowUpRequest(ByVal requestId As Integer)
    If requestId > 0 Then
            Dim requ As New pmoLib.CpSurveyRequest(requestId)
            Dim thePrj As New pmoLib.CpProject(requ.ProjectId)
      Session("currSurveyRequest") = requ
      Me.lblFeedbackTitle.Text = "Project Survey Request: " & thePrj.Title
    End If
  End Sub
  Protected Sub lbSendSurvey_Click(ByVal sender As Object, ByVal e As EventArgs) Handles lbSendSurvey.Click
        If Not Session("currSurveyRequest") Is Nothing Then
            Dim survReq As pmoLib.CpSurveyRequest = Session("currSurveyRequest")
            Dim theEmail As New SmtpEmail
            theEmail.ToField = survReq.EmailAddress
            theEmail.FromField = "ost-pmo@ostusa.com"

            If IsValidDate(survReq.FollowUpDate) Then
                theEmail.Subject = "OST Client Satisfaction"
            Else
                theEmail.Subject = "So, what did you think? OST Client Satisfaction Follow Up"
            End If

            Dim theClickThrough As String = "<a href=""http://www.ostusa.com/PmoSurvey?requestId=" & survReq.RequestId & """>Take the OST Project Completion Survey</a>"
            'Dim theClickThrough As String = "<a href=""http://Localhost/PmoSurvey/Default.aspx?requestId=" & survReq.RequestId & """>Take the OST Project Completion Survey</a>"
            theEmail.Body = survReq.GetFollowupRequestHtml(theClickThrough)
            theEmail.BodyIsHtml = True
            theEmail.Send()
            survReq.FollowUpDate = Today.ToShortDateString
            survReq.Persist()
            Dim theargs As New ProjectSurveyFollowUpSentEventArgs(survReq.RequestId)
            RaiseBubbleEvent(Me, theargs)
        End If

  End Sub

  Protected Sub lbRemoveSurveyRequest_Click(ByVal sender As Object, ByVal e As EventArgs) Handles lbRemoveSurveyRequest.Click
        Dim pmoMgr As New pmoManager
        Dim survReq As CpSurveyRequest = Session("currSurveyRequest")
        survReq.EmailAddress = survReq.EmailAddress & " No Survey Returned; Request Cancelled"
        survReq.CompleteDate = Today.ToShortDateString
        If survReq.Persist() Then
            pmoMgr.MarkCpProjectFeedbackComplete(survReq.ProjectId, CType(Session("pmoUser"), PmoUser).UserId, survReq.OldProjectId)
            pmoMgr.DeleteCompletedCpProject(survReq.ProjectId)
            Dim theArgs As New ProjectSurveyCreatedEventArgs(survReq.ProjectId)
            RaiseBubbleEvent(Me, theArgs)
        Else
            Session("abendIssue") = "An Error has occured while attempting to mark a Survey Request as being not needed."
            Response.Redirect("~/Security/frmIssue.aspx")
        End If
  End Sub

    Protected Sub lbAssignToSales_Click(sender As Object, e As EventArgs) Handles lbAssignToSales.Click
        Me.lblEmailLabel.Visible = True
        Me.txtEmail.Visible = True
        Me.lbSendToSales.Visible = True
        If Not Session("currSurveyRequest") Is Nothing Then
            Dim survReq As pmoLib.CpSurveyRequest = Session("currSurveyRequest")
            Me.txtEmail.Text = survReq.SalesFollowUpEmailAddress
        End If
    End Sub

    Protected Sub lbSendToSales_Click(sender As Object, e As EventArgs) Handles lbSendToSales.Click
        If Not Session("currSurveyRequest") Is Nothing Then
            Dim survReq As pmoLib.CpSurveyRequest = Session("currSurveyRequest")
            Dim theEmail As New SmtpEmail
            theEmail.ToField = Me.txtEmail.Text
            theEmail.FromField = "ost-pmo@ostusa.com"
            If survReq.SalesFollowUpEmailAddress > "" Then
                theEmail.CcField = survReq.SalesFollowUpEmailAddress
            End If

            theEmail.Subject = "OST Client Satisfaction - Needs Sales Followup"
            Dim theClickThrough As String = "<a href=""http://www.ostusa.com/PmoSurvey?requestId=" & survReq.RequestId & """>Take the OST Project Completion Survey</a>"
            'Dim theClickThrough As String = "<a href=""http://Localhost/PmoSurvey/Default.aspx?requestId=" & survReq.RequestId & """>Take the OST Project Completion Survey</a>"
            theEmail.Body = survReq.GetSalesFollowupRequestHtml(theClickThrough)
            theEmail.BodyIsHtml = True
            theEmail.Send()
            If Not IsValidDate(survReq.InitialSalesFollowUpDate) Then
                survReq.InitialSalesFollowUpDate = Today.ToShortDateString
            End If

            survReq.SalesFollowUpDate = Today.ToShortDateString

            survReq.SalesFollowUpEmailAddress = Me.txtEmail.Text
            survReq.FollowUpDate = Today.ToShortDateString
            survReq.Persist()
            Dim theargs As New ProjectSurveyFollowUpSentEventArgs(survReq.RequestId)
            RaiseBubbleEvent(Me, theargs)
        End If
    End Sub
End Class