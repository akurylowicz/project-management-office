﻿Imports pmoLib

Partial Public Class frmGatherCustomerFeedback
  Inherits System.Web.UI.Page

  Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
    If Not Context.Session Is DBNull.Value Then
      If Session.IsNewSession Then
        Dim szCookieHeader As String = Request.Headers("Cookie")
        If Not szCookieHeader Is Nothing Then
          If Not szCookieHeader Is DBNull.Value And szCookieHeader.IndexOf("ASP.NET_SessionId") >= 0 Then
            Response.Redirect("~/Security/frmTimeOut.aspx")
          End If
        End If
      End If
    End If
    If Not Page.IsPostBack Then
      Me.ucFeedback1.Visible = False
    End If
  End Sub
  Protected Overrides Function OnBubbleEvent(ByVal source As Object, ByVal args As System.EventArgs) As Boolean
    If TypeOf (args) Is MenuItemSelectedEventArgs Then
      Select Case CType(args, MenuItemSelectedEventArgs).MenuName
        Case "Main"
          Select Case CType(args, MenuItemSelectedEventArgs).MenuItem
            Case "SowListing"
              Response.Redirect("~/SOW/frmSowListing.aspx")

            Case "PmoDocuments"
              Response.Redirect("~/Documents/frmDocumentListing.aspx")

            Case "PmoUtilities"
              Response.Redirect("~/PmoUtilities/frmPmoUtilities.aspx")

            Case "PmoReporting"
              Response.Redirect("~/Reporting/frmReports.aspx")

            Case "PmoGatherFeedback"
              Response.Redirect("~/CustomerFeedback/frmGatherCustomerFeedback.aspx")

            Case "PmoCompletedSurveys"
              Response.Redirect("~/CustomerFeedback/frmCompletedSurveys.aspx")
          End Select
      End Select
    End If
    If TypeOf (args) Is CompletedProjectSelectedEventArgs Then
      Me.ucFeedback1.Visible = True
      Me.ucFeedback1.SetFeedbackProject(CType(args, CompletedProjectSelectedEventArgs).ProjectId)
    End If
    If TypeOf (args) Is ProjectSurveyCreatedEventArgs Then
      Me.ucFeedback1.Visible = False
      Me.ucSendFollowUp1.Visible = False
      Me.ucCompletedProjects1.Refresh()
      Me.ucSurveyRequests1.Refresh()
    End If
    If TypeOf (args) Is ProjectSurveyFollowUpEventArgs Then
      Me.ucSendFollowUp1.SetFollowUpRequest(CType(args, ProjectSurveyFollowUpEventArgs).RequestId)
      Me.ucSendFollowUp1.Visible = True
    End If
    If TypeOf (args) Is ProjectSurveyFollowUpSentEventArgs Then
      Me.ucSendFollowUp1.Visible = False
      Me.ucCompletedProjects1.Refresh()
      Me.ucSurveyRequests1.Refresh()
    End If
        Return True
    End Function
End Class