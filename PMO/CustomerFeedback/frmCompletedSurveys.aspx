﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmCompletedSurveys.aspx.vb" Inherits="PMO.frmCompletedSurveys" %>

<%@ Register src="../ucControls/ucHeader.ascx" tagname="ucHeader" tagprefix="uc1" %>
<%@ Register src="ucCompletedSurveyList.ascx" tagname="ucCompletedSurveyList" tagprefix="uc2" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
     <title>PMO Portal - Completed Project Surveys</title>
    <link href="../ost.css" rel="Stylesheet" />
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
      <uc1:ucHeader ID="ucHeader1" runat="server" />
    
    </div>
    <uc2:ucCompletedSurveyList ID="ucCompletedSurveyList1" runat="server" />
    </form>
</body>
</html>
