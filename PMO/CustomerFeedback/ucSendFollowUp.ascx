﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="ucSendFollowUp.ascx.vb" Inherits="PMO.ucSendFollowUp" %>
<asp:Panel ID="Panel1" runat="server" CssClass="panelSurround">
<table width="100%">
  <tr>
    <td>
<asp:Label ID="lblFeedbackTitle" runat="server" CssClass="titleLabel" 
    Width="100%">Project Survey Request Followup</asp:Label>
    </td>
  </tr>
  <tr>
    <td width="15%">
        <asp:Label ID="lblEmailLabel" runat="server" Text="Enter OST Email for Followup:" Visible="False"></asp:Label>
      </td>
  </tr>
    <tr>
        <td width="15%">
            <asp:TextBox ID="txtEmail" runat="server" MaxLength="128" Visible="False" Width="300px"></asp:TextBox>
            <asp:LinkButton ID="lbSendToSales" runat="server" CssClass="pmoLink" Visible="False">Send Request</asp:LinkButton>
        </td>
    </tr>
  <tr>
    <td>
      <asp:LinkButton ID="lbSendSurvey" runat="server" CssClass="pmoLink">Send Survey Followup Request</asp:LinkButton>
      <asp:Label ID="Label2" runat="server" Text="|"></asp:Label>
      <asp:LinkButton ID="lbRemoveSurveyRequest" runat="server" CssClass="pmoLink">Remove Survey Request</asp:LinkButton>
        <asp:Label ID="Label3" runat="server" Text="|"></asp:Label>
        <asp:LinkButton ID="lbAssignToSales" runat="server" CssClass="pmoLink">Assign Request to OST for Followup</asp:LinkButton>
    </td>
  </tr>
  <tr>
    <td>
      &nbsp;</td>
  </tr>
</table>
</asp:Panel>
