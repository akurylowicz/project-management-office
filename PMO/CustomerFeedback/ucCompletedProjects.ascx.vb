﻿Imports pmoLib
Partial Public Class ucCompletedProjects
  Inherits System.Web.UI.UserControl

  Dim mLoading As Boolean = False

  Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
    If Not Page.IsPostBack Then
      Me.mLoading = True
      LoadCompletedProjects()
      Me.mLoading = False
    End If
  End Sub
  Public Sub Refresh()
    Me.mLoading = True
    LoadCompletedProjects()
    Me.mLoading = False
  End Sub
  Private Sub LoadCompletedProjects()
    Me.gvProjectList.DataSource = Nothing
    Dim theMgr As New pmoLib.pmoManager
        Dim projList As CpCompletedProjectList = Nothing
        'projList = theMgr.GetCompletedProjectList()
        projList = theMgr.GetCompletedChangepointProjectList()
    Me.gvProjectList.DataSource = projList
    Me.gvProjectList.DataBind()
    If projList.Count > 0 Then
      Me.lblMessage.Text = ""
      Me.lblMessage.Visible = False
    Else
      Me.lblMessage.Text = "There are no Completed Projects awaiting Client Feedback."
      Me.lblMessage.Visible = True
    End If
  End Sub
  Private Sub gvProjectList_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvProjectList.RowCommand
        Select Case e.CommandName
            Case "ProjectLink"
                Dim tmpId As Guid = New Guid(Me.gvProjectList.Rows(e.CommandArgument).Cells(0).Text)
                If tmpId.ToString > "" Then
                    Dim theArgs As New CompletedProjectSelectedEventArgs(tmpId)
                    RaiseBubbleEvent(Me, theArgs)
                End If
        End Select
  End Sub
End Class