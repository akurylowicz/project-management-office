﻿Imports pmoLib
Partial Public Class ucCompletedSurveyList
  Inherits System.Web.UI.UserControl

  Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
    If Not Page.IsPostBack Then
      LoadCompletedSurveys()
    End If
  End Sub
  Private Sub LoadCompletedSurveys()
    Me.gvCompletedSurveys.DataSource = Nothing
    Dim pmoMgr As New pmoManager
        Dim survList As CpSurveyResultList
    survList = pmoMgr.GetCompletedSurveyList
    Me.gvCompletedSurveys.DataSource = survList
    Me.gvCompletedSurveys.DataBind()
  End Sub

  Private Sub gvCompletedSurveys_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvCompletedSurveys.RowCommand
        Select Case e.CommandName
            Case "SurveyLink"
                Dim tmpId As Integer = CInt(Me.gvCompletedSurveys.Rows(e.CommandArgument).Cells(0).Text)
                If tmpId > 0 Then
                    Session("currProjectSurveyId") = tmpId
                    Response.Redirect("~/CustomerFeedback/frmShowProjectSurvey.aspx")
                End If
        End Select
  End Sub
End Class