﻿Imports pmoLib
Partial Public Class frmCompletedSurveys
  Inherits System.Web.UI.Page

  Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

  End Sub
  Protected Overrides Function OnBubbleEvent(ByVal source As Object, ByVal args As System.EventArgs) As Boolean
    If TypeOf (args) Is MenuItemSelectedEventArgs Then
      Select Case CType(args, MenuItemSelectedEventArgs).MenuName
        Case "Main"
          Select Case CType(args, MenuItemSelectedEventArgs).MenuItem
            Case "SowListing"
              Response.Redirect("~/SOW/frmSowListing.aspx")

            Case "PmoDocuments"
              Response.Redirect("~/Documents/frmDocumentListing.aspx")

            Case "PmoUtilities"
              Response.Redirect("~/PmoUtilities/frmPmoUtilities.aspx")

            Case "PmoReporting"
              Response.Redirect("~/Reporting/frmReports.aspx")

            Case "PmoGatherFeedback"
              Response.Redirect("~/CustomerFeedback/frmGatherCustomerFeedback.aspx")

            Case "PmoCompletedSurveys"
              Response.Redirect("~/CustomerFeedback/frmCompletedSurveys.aspx")
                    End Select
            End Select
    End If
        Return True
    End Function
End Class