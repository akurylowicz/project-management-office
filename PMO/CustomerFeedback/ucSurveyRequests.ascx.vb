﻿Imports pmoLib
Partial Public Class ucSurveyRequests
  Inherits System.Web.UI.UserControl

  Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
    If Not Page.IsPostBack Then
      LoadSurveyList()
    End If
  End Sub
  Public Sub Refresh()
    LoadSurveyList()
  End Sub
  Private Sub LoadSurveyList()
    Me.gvSurveyList.DataSource = Nothing
    Dim theMgr As New pmoManager
        Dim theList As CpSurveyRequestList
        theList = theMgr.GetAllOpenCpSurveyRequestList()
    Me.gvSurveyList.DataSource = theList
    Me.gvSurveyList.DataBind()
  End Sub

  Private Sub gvSurveyList_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvSurveyList.RowCommand
    Select Case e.CommandName
      Case "RequestLink"
        Dim tmpReq As Integer = CInt(Me.gvSurveyList.Rows(e.CommandArgument).Cells(0).Text)
        If tmpReq > 0 Then
          Dim theArgs As New ProjectSurveyFollowUpEventArgs(tmpReq)
          RaiseBubbleEvent(Me, theArgs)
        End If
    End Select
  End Sub

End Class