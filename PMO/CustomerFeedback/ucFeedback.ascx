﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="ucFeedback.ascx.vb" Inherits="PMO.ucFeedback" %>
<asp:Panel ID="Panel1" runat="server" CssClass="panelSurround">
<table width="100%">
  <tr>
    <td>
<asp:Label ID="lblFeedbackTitle" runat="server" CssClass="titleLabel" 
    Width="100%">Project Feedback</asp:Label>
    </td>
  </tr>
  <tr>
    <td width="15%">
      <asp:Label ID="Label1" runat="server" Text="Enter Email to Survey:"></asp:Label>
    </td>
  </tr>
  <tr>
    <td>
      <asp:TextBox ID="txtEmail" runat="server" MaxLength="128" Width="300px"></asp:TextBox>
    </td>
  </tr>
  <tr>
    <td>
      <asp:LinkButton ID="lbSendSurvey" runat="server" CssClass="pmoLink">Send Survey Request</asp:LinkButton>
      &nbsp;<asp:Label ID="Label2" runat="server" Text="|"></asp:Label>
      <asp:LinkButton ID="lbNoSurveyRequest" runat="server" CssClass="pmoLink">No Survey Request Needed</asp:LinkButton>
    </td>
  </tr>
  <tr>
    <td>
      &nbsp;</td>
  </tr>
</table>
</asp:Panel>