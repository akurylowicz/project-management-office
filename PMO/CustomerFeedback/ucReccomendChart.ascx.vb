﻿Imports pmoLib
Public Class ucReccomendChart
  Inherits System.Web.UI.UserControl

  Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
    Dim pmoMgr As New pmoManager
    Dim ds As DataSet = pmoMgr.GetReccomendationScoresForChart
    Chart1.DataSource = ds
    ' set series members names for the X and Y values 
    '	Chart1.Series["Series 1"].XValueMember = "Name";
    Chart1.Series.Item(0).YValueMembers = "score"
    ' data bind to the selected data source
    Chart1.DataBind()
  End Sub

End Class