﻿Imports pmoLib
Imports CommonLib

Partial Public Class ucFeedback
  Inherits System.Web.UI.UserControl

  Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

  End Sub
    Public Sub SetFeedbackProject(ByVal projectId As Guid)
        If projectId.ToString > "" Then
            Dim theProj As New CpProject(projectId)
            Session("currSurveyProjId") = projectId
            Me.lblFeedbackTitle.Text = "Project Feedback: " & theProj.Title
        End If
    End Sub
  Protected Sub lbSendSurvey_Click(ByVal sender As Object, ByVal e As EventArgs) Handles lbSendSurvey.Click
        Dim theMgr As New pmoManager
        If Me.txtEmail.Text > "" Then
            Dim proj As New CpProject(Session("currSurveyProjId"))
            If proj.ProjectSurveyCompleted Then
                'Only one survey per project is allowed...
                Dim pmoMgr As New pmoManager
                pmoMgr.MarkCpProjectFeedbackComplete(proj.Id, CType(Session("pmoUser"), PmoUser).UserId)
                pmoMgr.DeleteCompletedCpProject(proj.Id)
                Session("currProjectSurveyId") = proj.CpProjectSurveyResultId
                Response.Redirect("~/CustomerFeedback/frmShowProjectSurvey.aspx")
            Else
                If IsValidEmail(Me.txtEmail.Text) Then
                    Dim thePmoMgr As New pmoManager
                    Dim survReq As New CpSurveyRequest
                    survReq.ProjectId = Session("currSurveyProjId")
                    survReq.EmailAddress = Me.txtEmail.Text
                    survReq.RequestDate = Today.ToShortDateString
                    survReq.OnDemandRequest = False
                    survReq.CustomerName = proj.Customer
                    survReq.Title = proj.Title
                    survReq.StartDate = proj.StartDate
                    survReq.SalesFollowUpEmailAddress = theMgr.GetCompletedProjectSalesEmail(Session("currSurveyProjId"))

                    If survReq.Persist() Then
                        theMgr.DeleteCompletedCpProject(survReq.ProjectId)
                        Dim theEmail As New SmtpEmail
                        theEmail.ToField = survReq.EmailAddress
                        If survReq.SalesFollowUpEmailAddress > "" Then
                            theEmail.CcField = survReq.SalesFollowUpEmailAddress
                        End If

                        theEmail.FromField = "ost-pmo@ostusa.com"
                        theEmail.Subject = "So, what did you think? OST Client Satisfaction Survey "
                        Dim theClickThrough As String = "<a href=""http://www.ostusa.com/PmoSurvey?requestId=" & survReq.RequestId & """>Take the OST Project Completion Survey</a>"
                        'Dim theClickThrough As String = "<a href=""http://Localhost/PmoSurvey/Default.aspx?requestId=" & survReq.RequestId & """>Take the OST Project Completion Survey</a>"
                        theEmail.Body = survReq.GetRequestHtml(theClickThrough)
                        theEmail.BodyIsHtml = True
                        theEmail.Send()
                        Dim theArgs As New ProjectSurveyCreatedEventArgs(survReq.ProjectId)
                        RaiseBubbleEvent(Me, theArgs)
                        Me.txtEmail.Text = ""
                    End If
                End If
            End If
        End If
  End Sub
  Protected Sub lbNoSurveyRequest_Click(ByVal sender As Object, ByVal e As EventArgs) Handles lbNoSurveyRequest.Click
        Dim pmoMgr As New pmoManager
        Dim survReq As New CpSurveyRequest
        survReq.ProjectId = Session("currSurveyProjId")
        survReq.EmailAddress = "No Survey Requested"
        survReq.RequestDate = Today.ToShortDateString
        survReq.CompleteDate = Today.ToShortDateString
        survReq.CustomerName = "No Customer - Since No Survey Requested"
        survReq.Title = "No Project - Since No Survey Requested"

        If survReq.Persist() Then
            pmoMgr.MarkCpProjectFeedbackComplete(survReq.ProjectId, CType(Session("pmoUser"), PmoUser).UserId, survReq.OldProjectId)
            pmoMgr.DeleteCompletedCpProject(survReq.ProjectId)
            Dim survResult As New CpSurveyResult()
            survResult.OverallComments = "No Survey Requested for this project."
            survResult.EmailAddress = "No Survey Requested"
            survResult.NoSurveyRequested = True
            survResult.RequestId = survReq.RequestId
            survResult.SubmittedDate = Today.ToShortDateString
            Dim theArgs As New ProjectSurveyCreatedEventArgs(survReq.ProjectId)
            RaiseBubbleEvent(Me, theArgs)
        Else
            Session("abendIssue") = "An Error has occured while attempting to mark a Survey Request as being not needed."
            Response.Redirect("~/Security/frmIssue.aspx")
        End If

  End Sub
End Class