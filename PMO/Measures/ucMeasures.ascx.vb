﻿Imports pmoLib
Imports CommonLib

Partial Public Class ucMeasures
  Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'Dim projMeasure As New PmoMeasures
        LoadClientSatisfactionMeasures()
        LoadYtdClientSatisfactionMeasures()
    End Sub
  Private Sub LoadClientSatisfactionMeasures()
    Dim theMgr As New pmoLib.pmoManager
    Dim reccTotal As Integer
    Dim reccAvg As Double
    Dim reccCount As Integer

    Dim ds As DataSet = theMgr.GetReccomendationScores

    Dim counter As Integer = 0
    While counter < ds.Tables(0).Rows.Count
      If ds.Tables(0).Rows(counter).Item("score") > 0 Then
        reccCount += 1
        reccTotal += CInt(ds.Tables(0).Rows(counter).Item("score"))
      End If
      counter += 1
    End While

    If reccCount > 0 Then
      reccAvg = Math.Round(reccTotal / reccCount, 2)
    Else
      reccAvg = 0
    End If

    Me.lblWouldReccomend.Text = reccAvg
    Me.lblWouldReccomendInfo.Text = "(Avg from " & reccCount & " completed surveys with a Score greater than zero.)"

    Me.lblCompletedSurveys.Text = theMgr.GetCompletedSurveyRequestCount
    Me.lblRequestedSurveys.Text = theMgr.GetOpenSurveyRequestCount

    End Sub
    Private Sub LoadYtdClientSatisfactionMeasures()
        Dim theMgr As New pmoLib.pmoManager
        Dim ds As DataSet = theMgr.GetYtdClientSatScores

        If Not ds.Tables(0).Rows(0).Item("Average") Is DBNull.Value Then
            Me.lblYtdAvg.Text = Math.Round(ds.Tables(0).Rows(0).Item("Average"), 2)
            Me.lblYTDCount.Text = ds.Tables(0).Rows(0).Item("Quantity")
            Me.lblYTDMax.Text = ds.Tables(0).Rows(0).Item("Maximum")
            Me.lblYTDMin.Text = ds.Tables(0).Rows(0).Item("Minimum")
        Else
            Me.lblMessage.Text = "There are no Client Satisfaction results for the year yet..."
        End If

    End Sub
End Class