﻿Imports System.Web.SessionState

Public Class Global_asax
  Inherits System.Web.HttpApplication

  Sub Application_Start(ByVal sender As Object, ByVal e As EventArgs)
    ' Fires when the application is started
  End Sub

  Sub Session_Start(ByVal sender As Object, ByVal e As EventArgs)
    ' Set the Session Timeout to 30 for the application...
    Session.Timeout = 120

    'Make the Database Connection in the Singleton Database Object.

    'This string connects to the old VM_TRACY database...
    'CommonLib.DALDataManager.CreateDatabaseObject("Data Source=VM_TRACY;Initial Catalog=pmo;Integrated Security=True")
    'CommonLib.DALDataManager.CreateDatabaseObject("Data Source=VM_TRACY;Initial Catalog=pmo;User Id=sa_pmo;PWD=n56!L98")

    'This string connects to the new database...
        'CommonLib.DALDataManager.CreateDatabaseObject("Data Source=ost-sql01;Initial Catalog=pmo;User Id=sa_pmo;PWD=n56!L98")
        'This string connects to the Test database...
        'CommonLib.DALDataManager.CreateDatabaseObject("Data Source=ost-sql01;Initial Catalog=pmo_v2;User Id=sa_pmo;PWD=n56!L98")

        'Current Production
        'CommonLib.DALDataManager.CreateDatabaseObject("Data Source=ost-sql01;Initial Catalog=pmo_v2;User Id=sa_pmo;PWD=n56!L98")
        CommonLib.DALDataManager.CreateDatabaseObject("Data Source=.\DEV1;Initial Catalog=pmo_v2;User Id=sa_pmo;PWD=devenv")


    'This string connects to the local database...
        'CommonLib.DALDataManager.CreateDatabaseObject("Data Source=.\DEV1;Initial Catalog=pmo_v2;Integrated Security=True")
    
    Dim currUserName As String = User.Identity.Name.Replace("OSTUSA\", "")
    'currUserName = "sinerr"
    Dim pmoMgr As New pmoLib.pmoManager
    Dim tmpUser As New pmoLib.PmoUser(pmoMgr.GetPmoUser(currUserName).Tables(0).Rows(0).Item("user_id"))
    If Not tmpUser Is Nothing Then
      Session("pmoUser") = tmpUser
    Else
      Session("appErrMessage") = "The User was not found in the Project Tracking system.  Please contact the PMO with the following information: User Name: " & currUserName
      Response.Redirect("~/Security/frmApplicationError.aspx")
    End If

    Session("currUserName") = currUserName

 
  End Sub

  Sub Application_BeginRequest(ByVal sender As Object, ByVal e As EventArgs)
    ' Fires at the beginning of each request
  End Sub

  Sub Application_AuthenticateRequest(ByVal sender As Object, ByVal e As EventArgs)
    ' Fires upon attempting to authenticate the use
  End Sub

  Sub Application_Error(ByVal sender As Object, ByVal e As EventArgs)
    ' Fires when an error occurs
    'If Not Session Is Nothing Then
    '  Dim ex As Exception = Server.GetLastError().GetBaseException
    '  Session("LastException") = ex
    '  Response.Redirect("~/Security/frmApplicationException.aspx")
    'End If
  End Sub

  Sub Session_End(ByVal sender As Object, ByVal e As EventArgs)
    ' Fires when the session ends
  End Sub

  Sub Application_End(ByVal sender As Object, ByVal e As EventArgs)
    ' Fires when the application ends
  End Sub

End Class