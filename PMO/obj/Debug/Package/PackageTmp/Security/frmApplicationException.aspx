﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmApplicationException.aspx.vb" Inherits="PMO.frmApplicationException" %>

<%@ Register src="../ucControls/ucHeader.ascx" tagname="ucHeader" tagprefix="uc1" %>
<%@ Register src="../ucControls/ucStrippedHeader.ascx" tagname="ucStrippedHeader" tagprefix="uc2" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>PMO Portal - Application Error</title>
    <link href=../ost.css rel=Stylesheet />
 </head>
<body>
    <form id="form1" runat="server">
    <div>
    
      <uc2:ucStrippedHeader ID="ucStrippedHeader1" runat="server" />
    
    </div>
    <hr />
    <table width="100%">
      <tr>
        <td width="50px">
          <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/Error.png" 
            Height="62px" Width="62px" />
        </td>
        <td></td>
        <td>
          <asp:Label ID="Label4" runat="server" CssClass="pmoErrorLabel" 
            Text="An Error Has Occurred..."></asp:Label>
        </td>
      </tr>
      <tr>
        <td>
        </td>
        <td colspan="2">
          <asp:Label ID="Label1" runat="server" CssClass="pmoErrorLabel" 
            Text="What will be affected"></asp:Label>
        </td>
      </tr>
      <tr>
        <td></td>
        <td width="5px"></td>
        <td>
          <asp:Label ID="lblWhatAffected" runat="server" CssClass="normalField"></asp:Label>
        </td>
        <td></td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td colspan="2"></td>
      </tr>
      <tr>
        <td></td>
        <td colspan="2">
          <asp:Label ID="Label2" runat="server" CssClass="pmoErrorLabel" 
            Text="What can you do from here"></asp:Label>
        </td>
      </tr>
      <tr>
        <td></td>
        <td></td>
        <td>
          <asp:Label ID="lblWhatCanYouDo" runat="server" CssClass="normalField"></asp:Label>
        </td>
        <td></td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td colspan="2"></td>
      </tr>
      <tr>
        <td>
        </td>
        <td colspan="2">
          <asp:Label ID="Label3" runat="server" CssClass="pmoErrorLabel" 
            Text="Support information"></asp:Label>
        </td>
      </tr>
      <tr>
        <td></td>
        <td></td>
        <td>
          <asp:Literal ID="lblSupportInfo" runat="server"></asp:Literal>
        </td>
        <td></td>
      </tr>
      </table>
    </form>
</body>
</html>
