﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="ucHeader.ascx.vb" Inherits="PMO.ucHeader" %>
<%@ Register src="../Menus/ucPmoMain.ascx" tagname="ucPmoMain" tagprefix="uc1" %>
<style type="text/css">
    .style1
    {
        width: 100%;
    }
</style>
<table class="style1">
    <tr>
        <td>
            <asp:ImageButton ID="imgOstLogo" runat="server" Height="50px" 
                ImageUrl="~/Images/logo revised KL4 color PMS.jpg" />
        </td>
        <td width="90%">
            <asp:Label ID="Label1" runat="server" CssClass="appTitle" 
                Text="Project Management Office (PMO) Portal - V2"></asp:Label>
        </td>
        <td>
            &nbsp;</td>
    </tr>
</table>
<uc1:ucPmoMain ID="ucPmoMain1" runat="server" />
