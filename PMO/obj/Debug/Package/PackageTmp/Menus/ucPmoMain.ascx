﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="ucPmoMain.ascx.vb" Inherits="PMO.ucPmoMain" %>
<asp:Panel ID="Panel1" runat="server" CssClass="menuPanel">
    <table width="100%">
    <tr>
        <td>
            <asp:LinkButton ID="lbHome" runat="server" CssClass="menuLink">Portal Home</asp:LinkButton>
            <asp:Label ID="Label1" runat="server" Text=" | "></asp:Label>
            <asp:LinkButton ID="lbSowListing" runat="server" CssClass="menuLink">SOW Listing</asp:LinkButton>
             <asp:Label ID="Label5" runat="server" Text=" | "></asp:Label>
            <asp:LinkButton ID="lbCustomerFeedback" runat="server" CssClass="menuLink">Client Feedback</asp:LinkButton>
            <asp:Label ID="Label6" runat="server" Text=" | "></asp:Label>
            <asp:LinkButton ID="lbSurveys" runat="server" CssClass="menuLink">Completed Surveys</asp:LinkButton>
        </td>
        <td align="right">
            <asp:Label ID="lblUser" runat="server" CssClass="pmoUserLabel" 
            Text="Logged In: "></asp:Label>
        </td>
    </tr>
</table>
</asp:Panel>
