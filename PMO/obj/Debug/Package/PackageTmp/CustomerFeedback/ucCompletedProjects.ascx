﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="ucCompletedProjects.ascx.vb" Inherits="PMO.ucCompletedProjects" %>
 <asp:Panel ID="Panel1" runat="server" CssClass="panelSurround">
<div>
<asp:Label runat="server" CssClass="titleLabel" Width="100%">Completed Projects Needing Feedback</asp:Label>
</div>
<asp:GridView ID="gvProjectList" runat="server" AutoGenerateColumns="False" 
                    CellPadding="2" ForeColor="#333333" GridLines="None" 
                    Width="100%">
                    <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                    <Columns>
                        <asp:BoundField DataField="ProjectId" HeaderText="Project Id" >
                            <HeaderStyle HorizontalAlign="Left" VerticalAlign="Bottom" Width="75px" />
                        </asp:BoundField>
                        <asp:BoundField DataField="CustomerName" HeaderText="Customer" 
                            SortExpression="CustomerName" >
                            <HeaderStyle HorizontalAlign="Left" VerticalAlign="Bottom" Width="150px" />
                        </asp:BoundField>
                        <asp:ButtonField CommandName="ProjectLink" DataTextField="ProjectDescription" 
                            HeaderText="Project Description" ShowHeader="True" 
                            Text="ProjectDescription">
                            <HeaderStyle HorizontalAlign="Left" VerticalAlign="Bottom" Width="200px" />
                        </asp:ButtonField>
                       <asp:BoundField DataField="CompletedDateForDisplay" HeaderText="Completed Date" 
                            SortExpression="CompletedDateForDisplay">
                            <HeaderStyle HorizontalAlign="Left" VerticalAlign="Bottom" Width="75px" />
                        </asp:BoundField>
                        <asp:BoundField DataField="PrimaryAccountExec" HeaderText="Account Exec" 
                            SortExpression="PrimaryAccountExec">
                            <HeaderStyle HorizontalAlign="Left" VerticalAlign="Bottom" Width="200px" />
                        </asp:BoundField>
                       
                    </Columns>
                    <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                    <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                    <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <EditRowStyle BackColor="#999999" />
                    <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                </asp:GridView>
</asp:Panel>

            <asp:Label ID="lblMessage" runat="server" CssClass="messageLabel" 
    Visible="False"></asp:Label>


            