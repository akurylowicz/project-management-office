﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="ucSurveyRequests.ascx.vb" Inherits="PMO.ucSurveyRequests" %>
<asp:Panel ID="Panel1" runat="server" CssClass="panelSurround">
<div>
<asp:Label runat="server" CssClass="titleLabel" Width="100%" ID="Label1">Pending Survey Requests</asp:Label>
</div>
 <asp:GridView ID="gvSurveyList" runat="server" AutoGenerateColumns="False" 
        Width="100%">
    <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
    <Columns>
        <asp:BoundField DataField="RequestId" HeaderText="Request Id">
        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Bottom" Width="75px"  />
        <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" />
        </asp:BoundField>
           <asp:BoundField DataField="CustomerName" HeaderText="Customer">
        <HeaderStyle HorizontalAlign="Left" VerticalAlign="Bottom" Width="150px" />
        <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
        </asp:BoundField>
        <asp:ButtonField DataTextField="ProjectName" HeaderText="Project Description" 
            Text="ProjectName" CommandName="RequestLink">
        <HeaderStyle HorizontalAlign="Left" VerticalAlign="Bottom" Width="400px" />
        <ItemStyle VerticalAlign="Top" />
        </asp:ButtonField>
             <asp:BoundField DataField="EmailAddress" HeaderText="Sent To">
        <HeaderStyle HorizontalAlign="Left" VerticalAlign="Bottom" Width="200px" />
        </asp:BoundField>
        <asp:BoundField DataField="RequestDateForDisplay" HeaderText="Requested Date">
        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Bottom" />
        <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" />
        </asp:BoundField>
        <asp:BoundField DataField="FollowUpDateForDisplay" HeaderText="Follow Up Date">
        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Bottom" />
        <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" />
        </asp:BoundField>
              <asp:BoundField DataField="InitialSalesFollowUpDateForDisplay" HeaderText="Initial Sales Follow Up Date" >
         <HeaderStyle HorizontalAlign="Center" VerticalAlign="Bottom" />
        <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" />
                </asp:BoundField>
        <asp:BoundField DataField="SalesFollowUpEmailAddress" HeaderText="Sales Email">
         <HeaderStyle HorizontalAlign="Center" VerticalAlign="Bottom" />
        <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" />
        </asp:BoundField>
            <asp:BoundField DataField="SalesFollowUpDateForDisplay" HeaderText="Sales Follow Up Date" >
         <HeaderStyle HorizontalAlign="Center" VerticalAlign="Bottom" />
        <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" />
                </asp:BoundField>
    </Columns>
    <HeaderStyle BackColor="#5D7B9D" ForeColor="White" />
    <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
</asp:GridView>
</asp:Panel>