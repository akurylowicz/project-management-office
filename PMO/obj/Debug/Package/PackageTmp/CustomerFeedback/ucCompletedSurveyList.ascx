﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="ucCompletedSurveyList.ascx.vb" Inherits="PMO.ucCompletedSurveyList" %>
<asp:GridView ID="gvCompletedSurveys" runat="server" 
  AutoGenerateColumns="False" Width="65%">
  <Columns>
    <asp:BoundField DataField="RequestId" HeaderText="Request Id">
    <HeaderStyle HorizontalAlign="Center" Width="100px" />
    <ItemStyle HorizontalAlign="Center" />
    </asp:BoundField>
    <asp:ButtonField CommandName="SurveyLink" DataTextField="ProjectTitle" 
      HeaderText="Project" Text="ProjectTitle">
    <HeaderStyle HorizontalAlign="Left" />
    </asp:ButtonField>
    <asp:BoundField DataField="ProjectCustomer" HeaderText="Customer">
    <HeaderStyle HorizontalAlign="Left" Width="200px" />
    </asp:BoundField>
    <asp:BoundField DataField="SubmittedDateForDisplay" HeaderText="Submitted">
    <HeaderStyle HorizontalAlign="Left" Width="90px" />
    </asp:BoundField>
  </Columns>
   <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                    <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <EditRowStyle BackColor="#999999" />
                    <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
</asp:GridView>
