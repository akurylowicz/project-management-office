﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmSowListing.aspx.vb" Inherits="PMO.frmSowListing" %>

<%@ Register src="../ucControls/ucHeader.ascx" tagname="ucHeader" tagprefix="uc1" %>

<%@ Register src="ucSowList.ascx" tagname="ucSowList" tagprefix="uc2" %>

<%@ Register src="ucMaintainSow.ascx" tagname="ucMaintainSow" tagprefix="uc3" %>

<%@ Register src="ucCreateSowId.ascx" tagname="ucCreateSowId" tagprefix="uc4" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
     <title>PMO Portal - SOW Listing</title>
    <link href=../ost.css rel=Stylesheet />
</head>
<body vlink="#0000ff">
    <form id="form1" runat="server">
    <div>
     <uc1:ucHeader ID="ucHeader1" runat="server" />
    </div>
    <table width:100%>
    <tr>
    <td valign="top" width="60%">
            <uc2:ucSowList ID="ucSowList1" runat="server" />
    </td>
    <td valign="top">
           <uc3:ucMaintainSow ID="ucMaintainSow1" runat="server" />
            <uc4:ucCreateSowId ID="ucCreateSowId1" runat="server" />
    </td>
    </tr>
    </table>
        </form>
</body>
</html>
