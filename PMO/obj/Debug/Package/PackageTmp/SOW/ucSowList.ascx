﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="ucSowList.ascx.vb" Inherits="PMO.ucSowList" %>
<%@ Register src="ucCreateSowId.ascx" tagname="ucCreateSowId" tagprefix="uc1" %>
<%@ Register src="ucMaintainSow.ascx" tagname="ucMaintainSow" tagprefix="uc2" %>
<%@ Register src="ucAlphaList.ascx" tagname="ucAlphaList" tagprefix="uc3" %>
<table cellpadding="0" cellspacing="0" class="style1">
    <tr>
        <td valign=top>
            <table cellpadding="0" cellspacing="0" class="style1">
            <tr>
            <td>
            <asp:Panel ID="pnlSowCreate" runat="server" CssClass="appProjectListPanel">
                <table width=100%>
                <tr>
                    <td>
                        <asp:Label ID="Label4" runat="server" CssClass="titleLabel" Text="Open SOW Identifiers"></asp:Label>
                    </td>
                    <td align="right">
                        <asp:LinkButton ID="lbOpenFilter" runat="server" CssClass="pmoLink">Open Filter...</asp:LinkButton>
                    </td>
                </tr>
            <tr>
                <td colspan="2">
                    <asp:Panel ID="pnlFilter" runat="server" Visible="False">
                        <table class="style1">
                            <tr>
                                <td>
                                    <asp:CheckBox ID="cbShowInactive" runat="server" AutoPostBack="True" CssClass="normalLabel" Text="Show Inactive" />
                                </td>
                                <td>
                                    &nbsp;</td>
                              </tr>
                            <tr>
                              <td colspan =2>
                                <uc3:ucAlphaList ID="ucAlphaList1" runat="server" />
                              </td>
                             </tr>
                        </table>
                    </asp:Panel>
                </td>
            </tr>
           </table>
           </asp:Panel>
                    </td>
                </tr>
                <tr>
                    <td valign="top">
    <asp:GridView ID="gvSowList" runat="server" AutoGenerateColumns="False" 
            CellPadding="4" ForeColor="#333333" GridLines="None">
        <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
    <Columns>
        <asp:BoundField DataField="SowKey" HeaderText="SOW Key" Visible="False" />
        <asp:ButtonField CommandName="SowId" DataTextField="sowId" 
            HeaderText="SOW Identifier" Text="sowId" >
        <HeaderStyle HorizontalAlign="Left" VerticalAlign="Bottom" Width="100px" />
        </asp:ButtonField>
        <asp:BoundField DataField="SowCustName" HeaderText="Customer" >
        <HeaderStyle HorizontalAlign="Left" VerticalAlign="Bottom" Width="250px" />
        </asp:BoundField>
        <asp:BoundField DataField="SowTitle" HeaderText="Statement of Work Title" >
        <HeaderStyle HorizontalAlign="Left" VerticalAlign="Bottom" Width="400px" />
        </asp:BoundField>
        <asp:BoundField DataField="SowPracticeName" HeaderText="Practice" />
        <asp:BoundField DataField="SowEstStartDate" DataFormatString="{0:d}" 
            HeaderText="Est Start" >
        <HeaderStyle HorizontalAlign="Left" VerticalAlign="Bottom" Width="150px" />
        </asp:BoundField>
    </Columns>
        <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
        <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
        <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
        <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
        <EditRowStyle BackColor="#999999" />
        <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
    </asp:GridView>
                    </td>
                </tr>
            </table>
        </td>
        <td valign =top>
            &nbsp;</td>
    </tr>
</table>
