﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="ucMaintainSow.ascx.vb" Inherits="PMO.ucMaintainSow" %>
<%@ Register src="ucCreateSowId.ascx" tagname="ucCreateSowId" tagprefix="uc1" %>
<asp:Panel ID="pnlSowCreate" runat="server" CssClass="appSowCreatePanel">
<link href=ost.css rel=Stylesheet />
    <table class="style1" width="100%">
        <tr>
            <td colspan="3" valign="middle">
                <asp:Panel ID="Panel1" runat="server" CssClass="appProjectListPanel">
                    <asp:Label ID="Label4" runat="server" CssClass="titleLabel" 
                        Text="Maintain SOW Identifier"></asp:Label>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="lblSowIdLabel" runat="server" CssClass="normalLabel" 
                    Text="SOW Identifier:"></asp:Label>
            </td>
            <td>
                <asp:Label ID="lbSowId" runat="server" CssClass="normalField"></asp:Label>
            </td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label1" runat="server" CssClass="normalLabel" 
                    Text="Customer Name:"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="txtCustName" runat="server" CssClass="normalField" 
                    Width="274px"></asp:TextBox>
            </td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label2" runat="server" CssClass="normalLabel" 
                    Text="Project Description:"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="txtProjectDescrip" runat="server" CssClass="normalField" 
                    Width="404px"></asp:TextBox>
            </td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td valign="top">
                <asp:Label ID="Label3" runat="server" CssClass="normalLabel" 
                    Text="Expected Start:"></asp:Label>
            </td>
            <td valign="top">
                <asp:TextBox ID="txtStartDate" runat="server" CssClass="normalField"></asp:TextBox>
                <asp:Button ID="btnShowCal" runat="server" Text="..." />
            </td>
            <td rowspan="7" valign="top">
                <asp:Calendar ID="calStart" runat="server" Height="86px" Visible="False" 
                    Width="96px"></asp:Calendar>
            </td>
        </tr>
        <tr>
            <td valign="top">
                <asp:Label ID="Label8" runat="server" CssClass="normalLabel" Text="Notes:"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="txtNotes" runat="server" CssClass="normalField" 
                    MaxLength="499" TextMode="MultiLine" Width="405px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                <asp:DropDownList ID="cmbPractice" runat="server">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>
                <asp:CheckBox ID="cbBtc" runat="server" Text="Block Time Contract" />
            </td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>
                <asp:CheckBox ID="cbSowInactive" runat="server" Text="SOW is Inactive" />
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                <asp:LinkButton ID="lbSaveChanges" runat="server" CssClass="pmoLink">Save Changes</asp:LinkButton>
                <asp:Label ID="lbClearSep2" runat="server" Text="   | "></asp:Label>
                <asp:LinkButton ID="lbDeleteSow" runat="server" CssClass="pmoLink">Delete SOW</asp:LinkButton>
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                <asp:Label ID="lblMessage" runat="server" CssClass="messageField"></asp:Label>
            </td>
        </tr>
    </table>
</asp:Panel>



