﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmGatherCustomerFeedback.aspx.vb" Inherits="PMO.frmGatherCustomerFeedback" %>

<%@ Register src="../ucControls/ucHeader.ascx" tagname="ucHeader" tagprefix="uc1" %>
<%@ Register src="ucCompletedProjects.ascx" tagname="ucCompletedProjects" tagprefix="uc2" %>

<%@ Register src="ucFeedback.ascx" tagname="ucFeedback" tagprefix="uc3" %>

<%@ Register src="ucSurveyRequests.ascx" tagname="ucSurveyRequests" tagprefix="uc4" %>

<%@ Register src="ucSendFollowUp.ascx" tagname="ucSendFollowUp" tagprefix="uc5" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
  <title>PMO Portal - Gather Client Feedback</title>
    <link href="../ost.css" rel="Stylesheet" />
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
        <uc1:ucHeader ID="ucHeader1" runat="server" />
    
    </div>
    <table class="style1">
        <tr>
            <td width="75%" valign="top">
                <uc2:ucCompletedProjects ID="ucCompletedProjects1" runat="server" />
            </td>
            <td valign="top">
              <uc3:ucFeedback ID="ucFeedback1" runat="server" />
            </td>
        </tr>
        <tr>
            <td valign="top">
              &nbsp
            </td>
            <td valign="top">
              &nbsp
            </td>
        </tr>
        <tr>
            <td valign="top">
                <uc4:ucSurveyRequests ID="ucSurveyRequests1" runat="server" />
            </td>
            <td valign="top">
              
              <uc5:ucSendFollowUp ID="ucSendFollowUp1" runat="server" Visible="False" />
              
            </td>
        </tr>
    </table>
    </form>
</body>
</html>
