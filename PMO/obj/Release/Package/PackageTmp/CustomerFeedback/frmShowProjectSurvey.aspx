﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmShowProjectSurvey.aspx.vb" Inherits="PMO.frmShowProjectSurvey" %>
<%@ Register src="../ucControls/ucHeader.ascx" tagname="ucHeader" tagprefix="uc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
     <title>PMO Portal - Show Project Survey</title>
    <link href="../ost.css" rel="Stylesheet" />
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
      <uc1:ucHeader ID="ucHeader1" runat="server" />
    
    </div>
    <table class="style1" width="100%">
      <tr>
        <td width="10%">
            &nbsp;</td>
        <td align="left">
<asp:GridView ID="gvCompletedSurveys" runat="server" 
  AutoGenerateColumns="False" Width="65%">
  <Columns>
    <asp:BoundField DataField="RequestId" HeaderText="Id">
    <HeaderStyle HorizontalAlign="Center" />
    <ItemStyle HorizontalAlign="Center" />
    </asp:BoundField>
    <asp:ButtonField CommandName="SurveyLink" DataTextField="SubmittedDateForDisplay" 
      HeaderText="Submitted" Text="SubmittedDateForDisplay">
    <HeaderStyle HorizontalAlign="Left" />
    <ItemStyle HorizontalAlign="Left" />
    </asp:ButtonField>
    <asp:BoundField DataField="EmailAddress" HeaderText="Email">
    <HeaderStyle HorizontalAlign="Left" />
    </asp:BoundField>
  </Columns>
   <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                    <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <EditRowStyle BackColor="#999999" />
                    <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
</asp:GridView>
        </td>
        <td width="10%">
            &nbsp;</td>
      </tr>
      <tr>
        <td width="10%">
            &nbsp;</td>
        <td align="center">
          <asp:Literal ID="Literal1" runat="server"></asp:Literal>
        </td>
        <td width="10%">
            &nbsp;</td>
      </tr>
      <tr>
        <td>
            &nbsp;</td>
        <td>
            &nbsp;</td>
        <td>
            &nbsp;</td>
      </tr>
    </table>
    </form>
</body>
</html>
