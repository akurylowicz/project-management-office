﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="ucCreateSowId.ascx.vb" Inherits="PMO.ucCreateSowId" %>
<style type="text/css">
    .auto-style1 {
        height: 192px;
    }
</style>
<asp:Panel ID="pnlSowCreate" runat="server" CssClass="appSowCreatePanel">
<link href=ost.css rel=Stylesheet />
    <table class="style1">
        <tr>
            <td colspan="2" valign="middle">
                <asp:Panel ID="Panel1" runat="server" CssClass="appProjectListPanel">
                    <asp:Label ID="Label4" runat="server" CssClass="titleLabel" 
                        Text="Create SOW Identifier" Width="100%"></asp:Label>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label1" runat="server" CssClass="normalLabel" 
                    Text="Customer Name:"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="txtCustName" runat="server" CssClass="normalField"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label2" runat="server" CssClass="normalLabel" 
                    Text="Project Description:"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="txtProjectDescrip" runat="server" CssClass="normalField"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label3" runat="server" CssClass="normalLabel" 
                    Text="Expected Start:"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="txtStartDate" runat="server" CssClass="normalField"></asp:TextBox>
                <asp:Button ID="btnShowCal" runat="server" Text="..." />
            </td>
        </tr>
        <tr>
            <td>
                </td>
            <td>
                <asp:Calendar ID="calStart" runat="server" Height="86px" Visible="False" 
                    Width="96px"></asp:Calendar>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label5" runat="server" CssClass="normalLabel" Text="Practice:"></asp:Label>
            </td>
            <td>
                <asp:DropDownList ID="cmbPractice" runat="server">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>
                <asp:CheckBox ID="cbBtc" runat="server" Text="Block Time Contract" />
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                <asp:LinkButton ID="lbCreateSow" runat="server" CssClass="pmoLink">Create SOW Identifier</asp:LinkButton>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="lblSowIdLabel" runat="server" CssClass="normalLabel" Text="SOW Identifier:" Visible="False"></asp:Label>
            </td>
            <td>
                <asp:Label ID="lbSowId" runat="server" CssClass="normalField" Visible="False"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                <asp:Label ID="lblMessage" runat="server" CssClass="messageField" 
                    Visible="False"></asp:Label>
            </td>
        </tr>
    </table>
</asp:Panel>


