﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmTimeOut.aspx.vb" Inherits="PMO.frmTimeOut" %>

<%@ Register src="../ucControls/ucStrippedHeader.ascx" tagname="ucStrippedHeader" tagprefix="uc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
      <title>PMO Portal - Applicatin Timeout</title>
    <link href=../ost.css rel=Stylesheet />
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
        <uc1:ucStrippedHeader ID="ucStrippedHeader1" runat="server" />
    
    </div>
    <br />
    <br />
    <asp:HyperLink ID="hlRestart" runat="server" CssClass="menuLink" 
        NavigateUrl="~/Default.aspx" Target="_self">Your PMO Portal Session has timed out.  Please click here to restart the session.</asp:HyperLink>
    </form>
</body>
</html>
