﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="ucMeasures.ascx.vb" Inherits="PMO.ucMeasures" %>
<%@ Register src="../CustomerFeedback/ucReccomendChart.ascx" tagname="ucReccomendChart" tagprefix="uc2" %>
<asp:Panel ID="pnlSurveyPanel" runat="server" CssClass="appSurveyMeasuresPanel">
<link href=ost.css rel=Stylesheet />
     <table width="100%">
        <tr>
            <td bgcolor="Silver" colspan="2" valign="middle">
                <asp:Label ID="Label16" runat="server" CssClass="titleLabel" 
                    Text="Client Satisfaction Measurements" Width="100%"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="lblMeasures2" runat="server" Text="Clients would Recommend OST:" 
                    CssClass="normalLabel"></asp:Label>
            </td>
            <td align="right">
                <asp:Label ID="lblWouldReccomend" runat="server" CssClass="normalField"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="center" colspan="2">
                <asp:Label ID="lblWouldReccomendInfo" runat="server" CssClass="normalField"></asp:Label>
            </td>
        </tr>
        <tr>
          <td align="center" colspan="2">
          <asp:Panel ID="Panel1" runat="server" BackColor="Black">
            <uc2:ucReccomendChart ID="ucReccomendChart1" runat="server" />
          </asp:Panel>
          </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="lblMeasures3" runat="server" CssClass="normalLabel" 
                  Text="Completed Surveys:"></asp:Label>
            </td>
            <td align="right">
                <asp:Label ID="lblCompletedSurveys" runat="server" CssClass="normalField"></asp:Label>
            </td>
        </tr>
        <tr>
          <td>
            <asp:Label ID="lblMeasures4" runat="server" CssClass="normalLabel" 
              Text="Requested Surveys:"></asp:Label>
          </td>
          <td align="right">
            <asp:Label ID="lblRequestedSurveys" runat="server" CssClass="normalField"></asp:Label>
          </td>
        </tr>
        <tr>
          <td colspan="2">
            &nbsp;</td>
        </tr>
    </table>
    </asp:Panel>
<asp:Panel ID="pnlMeasures" runat="server" CssClass="appMeasuresPanel">
       <table width="100%">
        <tr>
            <td bgcolor="Silver" colspan="2" valign="middle">
                <asp:Label ID="Label17" runat="server" CssClass="titleLabel" 
                    Text="Quality Measurements" Width="100%"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="lblMeasures5" runat="server" Text="Calendar YTD AVG for Reccomend OST:" 
                    CssClass="normalLabel"></asp:Label>
            </td>
            <td align="right">
                <asp:Label ID="lblYtdAvg" runat="server" CssClass="normalField"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label18" runat="server" CssClass="normalLabel" 
                    Text="Calendar YTD Surveys Received:"></asp:Label>
            </td>
            <td align="right">
                <asp:Label ID="lblYTDCount" runat="server" CssClass="normalField"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label24" runat="server" CssClass="normalLabel" 
                    Text="Calendar YTD Max Score:"></asp:Label>
            </td>
            <td align="right">
                <asp:Label ID="lblYTDMax" runat="server" CssClass="normalField"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label20" runat="server" CssClass="normalLabel" Text="Calendar YTD Min Score:"></asp:Label>
            </td>
            <td align="right">
                <asp:Label ID="lblYTDMin" runat="server" CssClass="normalField"></asp:Label>
            </td>
        </tr>
           <tr>
               <td>
                   <asp:Label ID="lblMessage" runat="server" CssClass="normalLabel"></asp:Label>
               </td>
               <td align="right">&nbsp;</td>
           </tr>
        </table>
</asp:Panel>
  

  

  

