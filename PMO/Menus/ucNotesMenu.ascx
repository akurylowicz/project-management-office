﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="ucNotesMenu.ascx.vb" Inherits="PMO.ucNotesMenu" %>
<asp:Panel ID="Panel1" runat="server" CssClass="menuPanel">
    <asp:LinkButton ID="lbNew" runat="server" CssClass="menuLink">New</asp:LinkButton>
    <asp:Label ID="Label1" runat="server" Text=" | "></asp:Label>
    <asp:LinkButton ID="lbSave" runat="server" CssClass="menuLink">Save</asp:LinkButton>
    <asp:Label ID="Label2" runat="server" Text=" | "></asp:Label>
    <asp:LinkButton ID="lbDelete" runat="server" CssClass="menuLink">Delete</asp:LinkButton>
</asp:Panel>