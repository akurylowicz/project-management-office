﻿Imports pmoLib
Partial Public Class ucPmoMain
    Inherits System.Web.UI.UserControl
    Private Const cMenuName As String = "Main"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Session("pmoUser") Is Nothing Then
            Me.lblUser.Text = "Logged In: " & CType(Session("pmoUser"), pmoLib.PmoUser).FullName
        Else
            Me.lblUser.Text = ""
        End If
    End Sub

    Protected Sub lbSowListing_Click(ByVal sender As Object, ByVal e As EventArgs) Handles lbSowListing.Click
        Dim theArgs As New MenuItemSelectedEventArgs(cMenuName, "SowListing")
        RaiseBubbleEvent(Me, theArgs)
    End Sub

    Protected Sub lbHome_Click(ByVal sender As Object, ByVal e As EventArgs) Handles lbHome.Click
        Response.Redirect("~/default.aspx")
    End Sub

    Protected Sub lbCustomerFeedback_Click(ByVal sender As Object, ByVal e As EventArgs) Handles lbCustomerFeedback.Click
        Dim theArgs As New MenuItemSelectedEventArgs(cMenuName, "PmoGatherFeedback")
        RaiseBubbleEvent(Me, theArgs)
    End Sub

    Protected Sub lbSurveys_Click(ByVal sender As Object, ByVal e As EventArgs) Handles lbSurveys.Click
        Dim theArgs As New MenuItemSelectedEventArgs(cMenuName, "PmoCompletedSurveys")
        RaiseBubbleEvent(Me, theArgs)
    End Sub
End Class