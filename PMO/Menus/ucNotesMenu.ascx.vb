﻿Imports pmoLib
Partial Public Class ucNotesMenu
  Inherits System.Web.UI.UserControl
  Private Const cMenuName As String = "ProjectNotes"

  Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

  End Sub

  Protected Sub lbNew_Click(ByVal sender As Object, ByVal e As EventArgs) Handles lbNew.Click
    Dim theArgs As New MenuItemSelectedEventArgs(cMenuName, "New")
    RaiseBubbleEvent(Me, theArgs)
  End Sub

  Protected Sub lbSave_Click(ByVal sender As Object, ByVal e As EventArgs) Handles lbSave.Click
    Dim theArgs As New MenuItemSelectedEventArgs(cMenuName, "Save")
    RaiseBubbleEvent(Me, theArgs)
  End Sub

  Protected Sub lbDelete_Click(ByVal sender As Object, ByVal e As EventArgs) Handles lbDelete.Click
    Dim theArgs As New MenuItemSelectedEventArgs(cMenuName, "Delete")
    RaiseBubbleEvent(Me, theArgs)
  End Sub
End Class