﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Default.aspx.vb" Inherits="PMO._Default" %>

<%@ Register src="ucControls/ucHeader.ascx" tagname="ucHeader" tagprefix="uc1" %>

<%@ Register src="SOW/ucCreateSowId.ascx" tagname="ucCreateSowId" tagprefix="uc2" %>

<%@ Register src="Measures/ucMeasures.ascx" tagname="ucMeasures" tagprefix="uc4" %>

<%@ Register src="SOW/ucSowList.ascx" tagname="ucSowList" tagprefix="uc5" %>

<%@ Register src="CustomerFeedback/ucReccomendChart.ascx" tagname="ucReccomendChart" tagprefix="uc8" %>

<%@ Register src="SOW/ucMaintainSow.ascx" tagname="ucMaintainSow" tagprefix="uc3" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>PMO Portal - Home</title>
    <link href=ost.css rel=Stylesheet />
</head>
    <body vlink="#0000ff">
    <form id="form1" runat="server">
    <div>
    
        <uc1:ucHeader ID="ucHeader1" runat="server" />
    
    </div>
    <table class="style1">
        <tr>
            <td valign="top" width="33%">
          
                <uc2:ucCreateSowId ID="ucCreateSowId1" runat="server" />
          
            </td>
            <td valign="top" width="33%">
                </td>
            <td valign="top" width="33%">
                <uc4:ucMeasures ID="ucMeasures1" runat="server" />
            </td>
        </tr>
    </table>
</form>
</body>
</html>
