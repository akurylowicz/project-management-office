﻿Imports pmoLib
Partial Public Class _Default
  Inherits System.Web.UI.Page

  Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
    If Not Context.Session Is DBNull.Value Then
      If Session.IsNewSession Then
        Dim szCookieHeader As String = Request.Headers("Cookie")
        If Not szCookieHeader Is Nothing Then
          If Not szCookieHeader Is DBNull.Value And szCookieHeader.IndexOf("ASP.NET_SessionId") >= 0 Then
            Response.Redirect("~/Security/frmTimeOut.aspx")
          End If
        End If
      End If
    End If
  End Sub

  Protected Overrides Function OnBubbleEvent(ByVal source As Object, ByVal args As System.EventArgs) As Boolean
    If TypeOf (args) Is MenuItemSelectedEventArgs Then
      Select Case CType(args, MenuItemSelectedEventArgs).MenuName
        Case "Main"
          Select Case CType(args, MenuItemSelectedEventArgs).MenuItem
            Case "SowListing"
              Response.Redirect("~/SOW/frmSowListing.aspx")

            Case "PmoDocuments"
              Response.Redirect("~/Documents/frmDocumentListing.aspx")

            Case "PmoUtilities"
              Response.Redirect("~/PmoUtilities/frmPmoUtilities.aspx")

            Case "PmoReporting"
              Response.Redirect("~/Reporting/frmReports.aspx")

            Case "PmoGatherFeedback"
              Response.Redirect("~/CustomerFeedback/frmGatherCustomerFeedback.aspx")

            Case "PmoCompletedSurveys"
              Response.Redirect("~/CustomerFeedback/frmCompletedSurveys.aspx")
          End Select

      End Select
        End If
        Return True
  End Function

End Class