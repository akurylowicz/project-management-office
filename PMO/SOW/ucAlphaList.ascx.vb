﻿Public Class ucAlphaList
    Inherits System.Web.UI.UserControl

  Private mSelectedValue As String = ""

  Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
    'If Not Page.IsPostBack Then
    '  Me.mSelectedValue = ""
    'End If
  End Sub
  Protected Overrides Function OnBubbleEvent(ByVal sender As Object, ByVal e As EventArgs) As Boolean
    If TypeOf (sender) Is LinkButton Then
      If CType(sender, LinkButton).CommandArgument = "All" Then
        Me.mSelectedValue = ""
      Else
        Me.mSelectedValue = CType(sender, LinkButton).CommandArgument
      End If
      Dim theEvent As New pmoLib.AlphaSelectedEventArgs(Me.mSelectedValue)
      RaiseBubbleEvent(Me, theEvent)
        End If
        Return True
  End Function
  Public ReadOnly Property SelectedValue As String
    Get
      Return Me.mSelectedValue
    End Get
  End Property
End Class