﻿Imports pmolib

Partial Public Class ucSowList
    Inherits System.Web.UI.UserControl

  Private mFilteredChar As String = ""

  Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
    If Not Page.IsPostBack Then
      LoadOpenSowList()
    End If
  End Sub
  Public Sub Refresh()
    LoadOpenSowList()
  End Sub
  Private Sub LoadOpenSowList()
    Dim theMgr As New pmoManager
    Dim theList As SowList
        If Me.pnlFilter.Visible Then
            Me.mFilteredChar = Me.ucAlphaList1.SelectedValue
        End If
        If Me.mFilteredChar = "" Then
            theList = theMgr.GetSowListing(Me.cbShowInactive.Checked)
        Else
            theList = theMgr.GetFilteredSowListing(Me.mFilteredChar, Me.cbShowInactive.Checked)
        End If
        Me.gvSowList.DataSource = Nothing
        Me.gvSowList.DataSource = theList
        Me.gvSowList.DataBind()
    End Sub
 
  Private Sub gvSowList_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvSowList.RowCommand
    Select Case e.CommandName
      Case "SowId"
        Dim tmpId As Integer = CInt(CType(Me.gvSowList.Rows(e.CommandArgument).Cells(1).Controls(0), LinkButton).Text)
        If tmpId > 0 Then
          Dim theArgs As New SowSelectedEventArgs(tmpId)
          RaiseBubbleEvent(Me, theArgs)
        End If

    End Select
  End Sub

  Protected Sub lbOpenFilter_Click(ByVal sender As Object, ByVal e As EventArgs) Handles lbOpenFilter.Click
    If Me.pnlFilter.Visible Then
      Me.lbOpenFilter.Text = "Open Filter..."
      Me.pnlFilter.Visible = False
    Else
      Me.lbOpenFilter.Text = "Close Filter..."
      Me.pnlFilter.Visible = True
    End If
  End Sub

  Protected Sub cbShowInactive_CheckedChanged(ByVal sender As Object, ByVal e As EventArgs) Handles cbShowInactive.CheckedChanged
    LoadOpenSowList()
  End Sub

    Protected Overrides Function OnBubbleEvent(ByVal source As Object, ByVal args As System.EventArgs) As Boolean
        If TypeOf (args) Is pmoLib.AlphaSelectedEventArgs Then
            Me.mFilteredChar = CType(args, pmoLib.AlphaSelectedEventArgs).SelectedValue
            LoadOpenSowList()
        End If
        Return True
    End Function
End Class