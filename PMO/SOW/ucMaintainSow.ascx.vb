﻿Imports pmoLib
Imports CommonLib
Imports System.IO

Partial Public Class ucMaintainSow
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            LoadPracticeList()
        End If
    End Sub
    Public Sub LoadSowId(ByVal sowId As Integer)
        Session("SowLoaded") = True
        Dim theMgr As New pmoManager
        Dim theSow As New Sow(theMgr.GetKeyForSow(sowId))
        ClearScreenFields()

        If Not theSow Is Nothing Then
            Me.lbSowId.Text = theSow.SowId
            Me.txtCustName.Text = theSow.SowCustName
            Me.txtProjectDescrip.Text = theSow.SowTitle
            Me.txtStartDate.Text = theSow.SowEstStartDate
            Me.cbSowInactive.Checked = theSow.Inactive
            Me.cbBtc.Checked = theSow.IsBlockTimeContract

            Me.txtNotes.Text = theSow.Notes

            Me.cmbPractice.SelectedValue = theSow.SowPracticeId

            'If theSow.ProjectId > 0 Then
            '    Dim tmpProj As New Project(theSow.ProjectId)
            'End If
        Else
            Session("appErrMessage") = "The SOW record could not be created.  Please contact the PMO with the following information; Module: ucMaintainSow.ascx; Routine: LoadSowId()"
            Response.Redirect("~/Security/frmApplicationError.aspx")
        End If
    End Sub
    Private Sub LoadPracticeList()
        Dim theMgr As New pmoManager
        Dim ds As DataSet = theMgr.GetAllPractices

        Dim tmpRow As DataRow = ds.Tables(0).NewRow
        tmpRow.Item("practiceId") = 0
        tmpRow.Item("practice") = "Please Select..."
        ds.Tables(0).Rows.InsertAt(tmpRow, 0)

        Me.cmbPractice.DataSource = ds
        Me.cmbPractice.DataValueField = "practiceId"
        Me.cmbPractice.DataTextField = "practice"
        Me.cmbPractice.DataBind()
    End Sub
    Private Sub ClearScreenFields()
        Me.txtCustName.Text = ""
        Me.txtNotes.Text = ""
        Me.txtProjectDescrip.Text = ""
        Me.txtStartDate.Text = ""
        Me.cbSowInactive.Checked = False
        Me.lblMessage.Text = ""
        Me.lbSowId.Text = ""
        Me.cbBtc.Checked = False
    End Sub

    Protected Sub lbSaveChanges_Click(ByVal sender As Object, ByVal e As EventArgs) Handles lbSaveChanges.Click
        Dim theMgr As New pmoManager
        If ScreenFieldsAreValid() Then
            Dim theSow As New Sow(CInt(theMgr.GetKeyForSow(Me.lbSowId.Text)))
            theSow.SowTitle = Me.txtProjectDescrip.Text
            theSow.SowCustName = Me.txtCustName.Text
            theSow.SowEstStartDate = Me.txtStartDate.Text
            theSow.Inactive = Me.cbSowInactive.Checked
            theSow.Notes = Me.txtNotes.Text
            theSow.IsBlockTimeContract = Me.cbBtc.Checked
            theSow.SowPracticeId = Me.cmbPractice.SelectedValue

            If theSow.Persist(Session("currUserName")) Then
                LoadSowId(theSow.SowId)
                Dim theArgs As New SowUpdatedEventArgs(theSow.SowId)
                RaiseBubbleEvent(Me, theArgs)
            Else
                Me.lblMessage.Text = "SOW Creation errored.  Please contact the PMO and provide them with the following information: Module: ucCreateSowId.ascx; Routine: CreateSowId"
            End If
            Me.lblSowIdLabel.Visible = True
            Me.lbSowId.Visible = True
            Me.lbSowId.Text = theSow.SowId
        End If

    End Sub

    Private Function ScreenFieldsAreValid() As Boolean
        Dim areValid As Boolean = True
        If Me.txtCustName.Text = "" Then
            areValid = False
            Me.lblMessage.Text += "A Customer Name is required; "
        End If
        If Me.txtProjectDescrip.Text = "" Then
            areValid = False
            Me.lblMessage.Text += "A Project Description is required; "
        End If
        If Me.txtStartDate.Text = "" Then
            areValid = False
            Me.lblMessage.Text += "An Estimated Start Date is required; "
        Else
            If Not IsValidDate(Me.txtStartDate.Text) Then
                areValid = False
                Me.lblMessage.Text += "An Estimated Start Date is required and must be a valid date; "
            End If
        End If
        Return areValid
    End Function
    Protected Sub btnShowCal_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnShowCal.Click
        Me.calStart.Visible = Not Me.calStart.Visible
    End Sub
    Private Sub calStart_SelectionChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles calStart.SelectionChanged
        Me.txtStartDate.Text = Me.calStart.SelectedDate
        Me.calStart.Visible = False
    End Sub
    Protected Sub lbDeleteSow_Click(ByVal sender As Object, ByVal e As EventArgs) Handles lbDeleteSow.Click
        Dim theMgr As New pmoManager
        Dim theSow As New Sow(CInt(theMgr.GetKeyForSow(Me.lbSowId.Text)))
        If Not theSow Is Nothing Then
            If theSow.Delete() Then
                ClearScreenFields()
                Dim theArgs As New SowUpdatedEventArgs(theSow.SowId)
                RaiseBubbleEvent(Me, theArgs)
            Else
                Me.lblMessage.Text = "SOW Deletion errored.  Please contact the PMO and provide them with the following information: Module: ucCreateSowId.ascx; Routine: CreateSowId"
            End If
        End If

    End Sub
End Class