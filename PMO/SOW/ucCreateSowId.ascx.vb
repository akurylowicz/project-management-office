﻿Imports pmoLib
Imports CommonLib
Partial Public Class ucCreateSowId
  Inherits System.Web.UI.UserControl
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            LoadPracticeList()
        End If
    End Sub
  Protected Sub btnShowCal_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnShowCal.Click
    Me.calStart.Visible = Not Me.calStart.Visible
  End Sub
  Private Sub calStart_SelectionChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles calStart.SelectionChanged
    Me.txtStartDate.Text = Me.calStart.SelectedDate
    Me.calStart.Visible = False
  End Sub
  Protected Sub lbCreateSow_Click(ByVal sender As Object, ByVal e As EventArgs) Handles lbCreateSow.Click
    CreateSowId()
  End Sub
    Private Sub LoadPracticeList()
        Dim theMgr As New pmoManager
        Dim ds As DataSet = theMgr.GetAllPractices

        Dim tmpRow As DataRow = ds.Tables(0).NewRow
        tmpRow.Item("practiceId") = 0
        tmpRow.Item("practice") = "Please Select..."
        ds.Tables(0).Rows.InsertAt(tmpRow, 0)

        Me.cmbPractice.DataSource = ds
        Me.cmbPractice.DataValueField = "practiceId"
        Me.cmbPractice.DataTextField = "practice"
        Me.cmbPractice.DataBind()
    End Sub

    Private Sub CreateSowId()
        'Get the SOW Id from the library
        If ScreenFieldsAreValid() Then
            Dim theSow As New Sow
            theSow.SowTitle = Me.txtProjectDescrip.Text
            theSow.SowCustName = Me.txtCustName.Text
            theSow.SowEstStartDate = Me.txtStartDate.Text
            theSow.IsBlockTimeContract = Me.cbBtc.Checked
            theSow.SowPracticeId = Me.cmbPractice.SelectedValue

            If theSow.Persist(Session("currUserName")) Then
                Session("currentSowId") = theSow.SowId
                Response.Redirect("~/SOW/frmSowListing.aspx")
            Else
                Me.lblMessage.Text = "SOW Creation errored.  Please contact the PMO and provide them with the following information: Module: ucCreateSowId.ascx; Routine: CreateSowId"
            End If
            Me.lblSowIdLabel.Visible = True
            Me.lbSowId.Visible = True
            Me.lbSowId.Text = theSow.SowId
        End If

    End Sub
  Private Function ScreenFieldsAreValid() As Boolean
    Dim areValid As Boolean = True
    If Me.txtCustName.Text = "" Then
      areValid = False
      Me.lblMessage.Text += "A Customer Name is required; "
    End If
    If Me.txtProjectDescrip.Text = "" Then
      areValid = False
      Me.lblMessage.Text += "A Project Description is required; "
    End If
    If Me.txtStartDate.Text = "" Then
      areValid = False
      Me.lblMessage.Text += "An Estimated Start Date is required; "
    Else
      If Not IsValidDate(Me.txtStartDate.Text) Then
        areValid = False
        Me.lblMessage.Text += "An Estimated Start Date is required and must be a valid date; "
      End If
    End If
    Return areValid
  End Function

End Class