Imports CommonLib

Public Class CpSurveyRequest
#Region "Class Constants"
    ''' Class Info Constants
    Private Const cClassName As String = "CpSurveyRequest"
    Private Const cClassAuthor As String = "SQL Object Generator"
    Private Const cClassVersion As String = "Version 2.0.a"
#End Region

#Region "Class Members"
    ''' Class Members
    Private mCompleteDate As Date
    Private mEmailAddress As String
    Private mFollowUpDate As Date
    Private mProjectId As Guid
    Private mRequestDate As Date
    Private mRequestId As Integer          ' Primary Key to table... 
    Private mIsNew As Boolean
    Private mNeedsSave As Boolean
    Private mLoadedFromDb As Boolean

    '5/19/2010 - To Support On Demand Surveys
    Private mOnDemandRequest As Boolean

    '12/27/2012 - To support new Compuware GUID
    Private mOldProjectId As Integer
    Private mInitialLoadFlag As Boolean = False

    '1/7/2013 - to upport difficulty reaching CP database from Web Server for Survey System
    Private mCustomer As String
    Private mTitle As String
    Private mStartDate As Date

    Private mEmptyGuid As Guid = New Guid("00000000-0000-0000-0000-000000000000")

    Private mSalesFollowUpEmail As String
    Private mSalesFollowUpDate As Date

    '7/17/2013
    Private mInitialSalesFollowUpDate As Date
    Private mPrimaryAccountExe As String

#End Region

#Region "Constructors"
    ''' Simple Constructor
    Public Sub New()
        Initialize()
        Me.mIsNew = True
        Me.mLoadedFromDb = False
        Me.mNeedsSave = True
    End Sub
    ''' Complex Constructor
    Public Sub New(ByVal requestIdIn As Integer)
        Initialize()
        mRequestId = requestIdIn
        Load()
    End Sub
    ''' Initialization Routines
    Private Sub Initialize()
        mCompleteDate = Nothing
        mEmailAddress = ""
        mFollowUpDate = Nothing
        mProjectId = Nothing
        mRequestDate = Nothing
        mRequestId = 0
        mIsNew = False
        mNeedsSave = False
        mLoadedFromDb = False
        Me.mOnDemandRequest = False
        Me.mOldProjectId = 0
        Me.mInitialLoadFlag = False
        Me.mCustomer = ""
        Me.mTitle = ""
        Me.mStartDate = Nothing
        Me.mSalesFollowUpEmail = ""
        Me.mSalesFollowUpDate = Nothing

        '7/17/2013
        Me.mInitialSalesFollowUpDate = Nothing
        Me.mPrimaryAccountExe = ""

    End Sub
#End Region

#Region "Member Properties"
    ''' Member Properties
    ''' 
    Public Property Customer() As String
        Get
            Return Me.mCustomer
        End Get
        Set(ByVal value As String)
            Me.mCustomer = value
        End Set
    End Property
    Public Property Title() As String
        Get
            Return Me.mTitle
        End Get
        Set(ByVal value As String)
            Me.mTitle = value
        End Set
    End Property
    Public Property StartDate() As Date
        Get
            Return Me.mStartDate
        End Get
        Set(ByVal value As Date)
            Me.mStartDate = value
        End Set
    End Property
    Public Property InitialLoad() As Boolean
        Get
            Return Me.mInitialLoadFlag
        End Get
        Set(ByVal value As Boolean)
            Me.mInitialLoadFlag = value
        End Set
    End Property
    Public Property OnDemandRequest() As Boolean
        Get
            Return Me.mOnDemandRequest
        End Get
        Set(ByVal value As Boolean)
            Me.mOnDemandRequest = value
        End Set
    End Property
    Public Property CompleteDate() As Date
        Get
            Return mCompleteDate
        End Get
        Set(ByVal Value As Date)
            mCompleteDate = Value
            Me.mNeedsSave = True
        End Set
    End Property

    Public Property EmailAddress() As String
        Get
            Return mEmailAddress
        End Get
        Set(ByVal Value As String)
            mEmailAddress = Value
            Me.mNeedsSave = True
        End Set
    End Property
    Public Property SalesFollowUpEmailAddress() As String
        Get
            Return mSalesFollowUpEmail
        End Get
        Set(ByVal Value As String)
            mSalesFollowUpEmail = Value
            Me.mNeedsSave = True
        End Set
    End Property

    Public Property FollowUpDate() As Date
        Get
            Return mFollowUpDate
        End Get
        Set(ByVal Value As Date)
            mFollowUpDate = Value
            Me.mNeedsSave = True
        End Set
    End Property
    Public Property SalesFollowUpDate() As Date
        Get
            Return mSalesFollowUpDate
        End Get
        Set(ByVal Value As Date)
            mSalesFollowUpDate = Value
            Me.mNeedsSave = True
        End Set
    End Property

    '7/17/2013
    Public Property InitialSalesFollowUpDate() As Date
        Get
            Return mInitialSalesFollowUpDate
        End Get
        Set(ByVal Value As Date)
            mInitialSalesFollowUpDate = Value
            Me.mNeedsSave = True
        End Set
    End Property
    Public Property PrimaryAccountExec() As String
        Get
            Return Me.mPrimaryAccountExe
        End Get
        Set(value As String)
            Me.mPrimaryAccountExe = value
        End Set
    End Property
    Public ReadOnly Property FollowUpDateForDisplay() As String
        Get
            Dim theDate As String = ""
            If IsValidDate(Me.mFollowUpDate) Then
                theDate = mFollowUpDate.ToShortDateString
            End If
            Return theDate
        End Get
    End Property
 
    Public ReadOnly Property SalesFollowUpDateForDisplay() As String
        Get
            Dim theDate As String = ""
            If IsValidDate(Me.mSalesFollowUpDate) Then
                theDate = mSalesFollowUpDate.ToShortDateString
            End If
            Return theDate
        End Get
    End Property
    Public ReadOnly Property InitialSalesFollowUpDateForDisplay() As String
        Get
            Dim theDate As String = ""
            If IsValidDate(Me.mInitialSalesFollowUpDate) Then
                theDate = mInitialSalesFollowUpDate.ToShortDateString
            End If
            Return theDate
        End Get
    End Property
    Public Property ProjectName() As String
        Get
            'If Me.mRequestId = 646 Then
            '    Dim stopHere As Boolean = True
            'End If
            'Dim name As String = "Project Not Found"
            'If Not Me.mProjectId.ToString = mEmptyGuid.ToString Then
            '    Dim tmpProj As New CpProject(Me.mProjectId)
            '    name = tmpProj.Title
            'Else
            '    Dim oldProj As New OldProject(Me.mOldProjectId)
            '    name = oldProj.Title
            'End If
            'Return name
            Return Me.mTitle
        End Get
        Set(ByVal Value As String)
            Me.mTitle = Value
        End Set
    End Property
    Public Property CustomerName() As String
        Get
            'Dim name As String = "Project Not Found"
            'If Not Me.mProjectId.ToString = mEmptyGuid.ToString Then
            '    Dim tmpProj As New CpProject(Me.mProjectId)
            '    name = tmpProj.Customer
            'Else
            '    Dim oldProj As New OldProject(Me.mOldProjectId)
            '    name = oldProj.Customer
            'End If
            'Return name
            Return Me.mCustomer
        End Get
        Set(ByVal Value As String)
            Me.mCustomer = Value
        End Set
    End Property
    Public Property ProjectId() As Guid
        Get
            Return mProjectId
        End Get
        Set(ByVal Value As Guid)
            mProjectId = Value
            Me.mNeedsSave = True
        End Set
    End Property
    Public Property OldProjectId() As Integer
        Get
            Return mOldProjectId
        End Get
        Set(ByVal Value As Integer)
            mOldProjectId = Value
            Me.mNeedsSave = True
        End Set
    End Property

    Public Property RequestDate() As Date
        Get
            Return mRequestDate
        End Get
        Set(ByVal Value As Date)
            mRequestDate = Value
            Me.mNeedsSave = True
        End Set
    End Property
    Public ReadOnly Property RequestDateForDisplay() As String
        Get
            Return mRequestDate.ToShortDateString
        End Get
    End Property
    Public Property RequestId() As Integer
        Get
            Return mRequestId
        End Get
        Set(ByVal Value As Integer)
            mRequestId = Value
            Me.mNeedsSave = True
        End Set
    End Property
    Public ReadOnly Property IsComplete() As Boolean
        Get
            Dim complete As Boolean = False
            If IsValidDate(Me.mCompleteDate) Then
                complete = True
            End If
            Return complete
        End Get
    End Property
    Public Function GetRequestHtml(ByVal clickThrough As String) As String
        Dim html As New Text.StringBuilder
        html.Append("<html><body>")
        html.Append("<table width=""100%""><tr><td align=""left"">")
        html.Append(GetRequestHeader())
        html.Append("</td></tr><tr><td align=""left"">")
        html.Append(GetRequestBody(clickThrough))
        html.Append("</td></tr>")
        html.Append("</table>")
        html.Append("<BR><BR>")
        html.Append(GetSignature())
        html.Append("</body></html>")
        Return html.ToString
    End Function
    Public Function GetRequestHtml(ByVal clickThrough As String, ByVal theText As String) As String
        Dim html As New Text.StringBuilder
        html.Append("<html><body>")
        html.Append("<table width=""100%""><tr><td align=""left"">")
        html.Append(GetRequestHeader())
        html.Append("</td></tr><tr><td align=""left"">")
        html.Append(GetRequestBody(clickThrough, theText))
        html.Append("</td></tr>")
        html.Append("</table>")
        html.Append("<BR><BR>")
        html.Append(GetSignature())
        html.Append("</body></html>")
        Return html.ToString
    End Function
    Private Function GetRequestHeader() As String
        Dim proj As New CpProject(Me.mProjectId)
        Dim header As New Text.StringBuilder
        header.Append("<STRONG>")
        header.Append("Project: " & proj.Title)
        header.Append("<BR><BR>")
        header.Append("</STRONG>")
        Return header.ToString
    End Function
    Private Function GetRequestBody(ByVal clickThrough As String) As String
        Dim body As New Text.StringBuilder
        body.Append("We want to make sure you're happy with the project OST just completed for you. We're committed to providing outstanding customer service, and we want to hear from the experts (you). Plus, we'd love to work with you again. So would you please take a minute to tell us what you think? Thanks again for your business. ")
        body.Append("<BR><BR>")
        body.Append(clickThrough)
        Return body.ToString
    End Function
    Private Function GetRequestBody(ByVal clickThrough As String, ByVal theText As String) As String
        Dim body As New Text.StringBuilder
        body.Append(theText)
        body.Append("<BR><BR>")
        body.Append(clickThrough)
        Return body.ToString
    End Function
    Public Function GetFollowupRequestHtml(ByVal clickThrough As String) As String
        Dim html As New Text.StringBuilder
        html.Append("<html><body>")
        html.Append("<div style=""background-color:#F0F0F0;width:90%;"">")
        html.Append("<table width=""100%""><tr><td align=""left"">")
        html.Append(GetRequestHeader())
        html.Append("</td></tr><tr><td align=""left"">")
        If IsValidDate(Me.mFollowUpDate) Then
            html.Append(GetSecondFollowupRequestBody(clickThrough))
        Else
            html.Append(GetFollowupRequestBody(clickThrough))
        End If
        html.Append("</td></tr>")
        html.Append("</table>")
        html.Append("<BR><BR>")
        html.Append(GetSignature())
        html.Append("</div>")
        html.Append("</body></html>")
        Return html.ToString
    End Function
    Private Function GetFollowupRequestBody(ByVal clickThrough As String) As String
        Dim body As New Text.StringBuilder
        body.Append("Did you have a good experience during your recent project with OST? Have suggestions on how we could've made it better? We want only satisfied customers, so we would really appreciate it if you could take 5 minutes to fill out the short survey below.  Thanks again for your business. ")
        body.Append("<BR><BR>")
        body.Append(clickThrough)
        Return body.ToString
    End Function
    Private Function GetSecondFollowupRequestBody(ByVal clickThrough As String) As String
        Dim body As New Text.StringBuilder
        'body.Append("In case you weren't convinced, we really do want to hear about your recent experience with OST. We know you're busy, but we value your opinion. So would you please take a minute to tell us what you think? Thanks again for your business (and feedback!). ")
        body.Append("We really do want to hear about your recent experience with OST. We know you're busy, but we value your opinion. So would you please take a minute to tell us what you think? Thanks again for your business (and feedback!). ")
        body.Append("<BR><BR>")
        body.Append(clickThrough)
        Return body.ToString
    End Function
    Public Function GetSalesFollowupRequestHtml(ByVal clickThrough As String) As String
        Dim html As New Text.StringBuilder
        html.Append("<html><body>")
        html.Append("<div style=""background-color:#F0F0F0;width:90%;"">")
        html.Append("<table width=""100%""><tr><td align=""left"">")
        html.Append(GetRequestHeader())
        html.Append("</td></tr><tr><td align=""left"">")
        If IsValidDate(Me.mFollowUpDate) Then
            html.Append(GetSalesFollowupRequestBody(clickThrough))
        Else
            html.Append(GetSalesFollowupRequestBody(clickThrough))
        End If
        html.Append("</td></tr>")
        html.Append("</table>")
        html.Append("<BR><BR>")
        html.Append("</div>")
        html.Append("</body></html>")
        Return html.ToString
    End Function
    Private Function GetSalesFollowupRequestBody(ByVal clickThrough As String) As String
        Dim body As New Text.StringBuilder
        body.Append("This Client Satisfaction Survey needs follow-up from the Account Executive.  To date we have sent an initial request, and at least one follow up request, and we have received no reply.  Please reach out to this client and ask for a response.  The client email address and the link to the survey are both below.  Thanks!")
        body.Append("<BR><BR>")
        body.Append("Sent to: " & Me.mEmailAddress)
        body.Append("<BR><BR>")
        body.Append(clickThrough)
        Return body.ToString
    End Function
    Private Function GetSignature() As String
        Dim body As New Text.StringBuilder
        body.Append("Thank you!")
        body.Append("<BR><b>Meredith Bronk</b>")
        body.Append("<BR>President")
        body.Append("<BR>OST (Open Systems Technologies)")
        body.Append("<BR>Email: dbehm@ostusa.com")
        Return body.ToString
    End Function
    Public Property IsNew() As Boolean
        Get
            Return mIsNew
        End Get
        Set(ByVal Value As Boolean)
            mIsNew = Value
            Me.mNeedsSave = True
        End Set
    End Property

    Public Property NeedsSave() As Boolean
        Get
            Return mNeedsSave
        End Get
        Set(ByVal Value As Boolean)
            mNeedsSave = Value
            Me.mNeedsSave = True
        End Set
    End Property

    Public Property LoadedFromDb() As Boolean
        Get
            Return mLoadedFromDb
        End Get
        Set(ByVal Value As Boolean)
            mLoadedFromDb = Value
            Me.mNeedsSave = True
        End Set
    End Property

#End Region

#Region "Persistence Routines"
    Public Function Persist() As Boolean
        Dim retCode As Boolean = True

        If Me.Validate.Count > 0 Then
            Return False
        End If

        Try
            ' TODO: Add Persistence Code

            Dim dbParms As New DbParmList

            If Me.mIsNew Then
                Me.mRequestDate = Today.ToShortDateString
            End If

            If mRequestId > 0 Then
                dbParms.Add(New DbParm("@requestId", mRequestId))
            End If

            If IsValidDate(Me.mCompleteDate) Then
                dbParms.Add(New DbParm("@completeDate", mCompleteDate))
            Else
                dbParms.Add(New DbParm("@completeDate", DBNull.Value))
            End If

            dbParms.Add(New DbParm("@emailAddress", mEmailAddress))

            If IsValidDate(Me.mFollowUpDate) Then
                dbParms.Add(New DbParm("@followUpDate", mFollowUpDate))
            Else
                dbParms.Add(New DbParm("@followUpDate", DBNull.Value))
            End If

            If Not Me.mProjectId.ToString Is Nothing Then
                dbParms.Add(New DbParm("@projectId", mProjectId))
            Else
                dbParms.Add(New DbParm("@projectId", DBNull.Value))
            End If

            If IsValidDate(Me.mRequestDate) Then
                dbParms.Add(New DbParm("@requestDate", mRequestDate))
            Else
                dbParms.Add(New DbParm("@requestDate", DBNull.Value))
            End If

            dbParms.Add(New DbParm("@onDemandRequest", Me.mOnDemandRequest))
            dbParms.Add(New DbParm("@oldProjectId", Me.mOldProjectId))

            If IsValidDate(Me.mStartDate) Then
                dbParms.Add(New DbParm("@startDate", mStartDate))
            Else
                dbParms.Add(New DbParm("@startDate", DBNull.Value))
            End If


            dbParms.Add(New DbParm("@customerName", Me.mCustomer))
            dbParms.Add(New DbParm("@projectName", Me.mTitle))

            dbParms.Add(New DbParm("@salesFollowUpEmail", Me.mSalesFollowUpEmail))

            If IsValidDate(Me.mSalesFollowUpDate) Then
                dbParms.Add(New DbParm("@salesFollowUpDate", mSalesFollowUpDate))
            Else
                dbParms.Add(New DbParm("@salesFollowUpDate", DBNull.Value))
            End If

            '7/17/2013
            If IsValidDate(Me.mInitialSalesFollowUpDate) Then
                dbParms.Add(New DbParm("@initialSalesFollowUpDate", mInitialSalesFollowUpDate))
            Else
                dbParms.Add(New DbParm("@InitialSalesFollowUpDate", DBNull.Value))
            End If

            dbParms.Add(New DbParm("@primaryAccountExec", Me.mPrimaryAccountExe))

            Dim dbMgr As DALDataManager
            dbMgr = DALDataManager.GetDatabaseObject
            Dim dbAccessor As New DbAccess

            Dim dbXfer As New DbXfer
            dbXfer.DatabaseMgr = CType(dbMgr, DALDataManager)
            'Database Name goes here...
            dbXfer.DatabaseName = "pmo"
            dbXfer.ParmList = dbParms

            If mRequestId > 0 Then
                If Me.mInitialLoadFlag Then
                    If dbAccessor.Insert(dbXfer, "spInsertCpSurveyRequest", True, "spUpdateCpSurveyRequest") Then
                        retCode = True
                        Me.mNeedsSave = False
                        Me.mIsNew = False
                    End If
                Else
                    If dbAccessor.Update(dbXfer, "spUpdateCpSurveyRequest") Then
                        retCode = True
                        Me.mNeedsSave = False
                    End If
                End If
            Else
                If dbAccessor.Insert(dbXfer, "spInsertCpSurveyRequest", True, "spUpdateCpSurveyRequest") Then
                    mRequestId = dbAccessor.ReturnValue
                    retCode = True
                    Me.mNeedsSave = False
                    Me.mIsNew = False
                End If
            End If

        Catch exc As Exception
            Dim logEx As New PmoException(exc.Message)
            logEx.ExceptionFunction = "Persist()"
            logEx.ExceptionClass = ClassName()
            logEx.ExceptionStackTrace = exc.StackTrace
            logEx.ExceptionType = exc.GetType.ToString
            logEx.Persist()
            Throw logEx

        Finally

        End Try

        Return retCode
    End Function
    Public Function Validate() As UtilValidationList
        Dim objValList As New UtilValidationList
        ' perform validation edits.  Whenever an error is found, instantiate a
        ' UtilValidation object and add it to the Validation List

        Try
            ' TODO: Add Validation Code

        Catch exc As Exception
            Dim logEx As New PmoException(exc.Message)
            logEx.ExceptionFunction = "Validate()"
            logEx.ExceptionClass = ClassName()
            logEx.ExceptionStackTrace = exc.StackTrace
            logEx.ExceptionType = exc.GetType.ToString
            logEx.Persist()
            Throw logEx

        Finally

        End Try

        Return objValList
    End Function
#End Region

#Region "Load Routines"
    ''' Load
    Private Function Load() As Boolean
        Dim retCode As Boolean = True
        Dim ds As DataSet

        ' Load the dataset... then call LoadFromDataset(ds)... please change manager object and key value as needed.
        If Me.mRequestId > 0 Then
            Dim pmoMgr As New pmoManager

            Try
                ds = pmoMgr.GetCpSurveyRequest(Me.mRequestId)
                If ds.Tables(0).Rows.Count > 0 Then
                    retCode = LoadFromDataset(ds)
                    If retCode = True Then
                        Me.mIsNew = False
                        Me.mLoadedFromDb = True
                        Me.mNeedsSave = False
                    End If
                End If

            Catch exc As Exception
                Dim logEx As New PmoException(exc.Message)
                logEx.ExceptionFunction = "Load()"
                logEx.ExceptionClass = ClassName()
                logEx.ExceptionStackTrace = exc.StackTrace
                logEx.ExceptionType = exc.GetType.ToString
                logEx.Persist()
                Throw logEx

            Finally

            End Try

        Else
            Dim logEx As New PmoException("Attempt to insantiate object without key value.")
            logEx.ExceptionFunction = "Load()"
            logEx.ExceptionClass = ClassName()
            logEx.Persist()
            Throw logEx
        End If

        Return retCode
    End Function
    ''' Load From Dataset
    Private Function LoadFromDataset(ByVal dsIn As DataSet) As Boolean
        Dim retCode As Boolean = True
        ' use the data in the dataset to load the class information
        mCompleteDate = CheckForNull(dsIn.Tables(0).Rows(0).Item("completeDate"), "Date")
        mEmailAddress = CheckForNull(dsIn.Tables(0).Rows(0).Item("emailAddress"), "String")
        mFollowUpDate = CheckForNull(dsIn.Tables(0).Rows(0).Item("followUpDate"), "Date")
        If dsIn.Tables(0).Rows(0).Item("projectId") Is DBNull.Value Then
            mProjectId = Nothing
        Else
            mProjectId = dsIn.Tables(0).Rows(0).Item("projectId")
        End If

        mRequestDate = CheckForNull(dsIn.Tables(0).Rows(0).Item("requestDate"), "Date")
        mRequestId = CheckForNull(dsIn.Tables(0).Rows(0).Item("requestId"), "Integer")
        mOnDemandRequest = CheckForNull(dsIn.Tables(0).Rows(0).Item("onDemandRequest"), "Boolean")
        mOldProjectId = CheckForNull(dsIn.Tables(0).Rows(0).Item("oldProjectId"), "Integer")

        mStartDate = CheckForNull(dsIn.Tables(0).Rows(0).Item("startDate"), "Date")
        mCustomer = CheckForNull(dsIn.Tables(0).Rows(0).Item("customerName"), "String")
        mTitle = CheckForNull(dsIn.Tables(0).Rows(0).Item("projectName"), "String")

        mSalesFollowUpEmail = CheckForNull(dsIn.Tables(0).Rows(0).Item("salesFollowUpEmail"), "String")
        mSalesFollowUpDate = CheckForNull(dsIn.Tables(0).Rows(0).Item("salesFollowUpDate"), "Date")
        '7/17/2013
        mInitialSalesFollowUpDate = CheckForNull(dsIn.Tables(0).Rows(0).Item("initialSalesFollowUpDate"), "Date")
        mPrimaryAccountExe = CheckForNull(dsIn.Tables(0).Rows(0).Item("primaryAccountExec"), "String")
        Return retCode
    End Function
#End Region

#Region "Delete Routine"
    ''' Delete
    Public Function Delete() As Boolean
        Dim retCode As Boolean = True

        Try
            If Me.mRequestId > 0 Then
                Dim dbParms As New DbParmList
                dbParms.Add(New DbParm("@requestId", mRequestId))
                Dim dbMgr As CommonLib.DALDataManager
                dbMgr = CommonLib.DALDataManager.GetDatabaseObject
                Dim dbAccessor As New DbAccess

                Dim dbXfer As New DbXfer
                dbXfer.DatabaseMgr = CType(dbMgr, CommonLib.DALDataManager)
                'Database Name goes here...
                dbXfer.DatabaseName = "pmo"
                dbXfer.ParmList = dbParms

                If dbAccessor.Delete(dbXfer, "spDeleteCpSurveyRequest") Then
                    retCode = True
                Else
                    retCode = False
                End If

            End If

        Catch exc As Exception
            Dim logEx As New PmoException(exc.Message)
            logEx.ExceptionFunction = "Load()"
            logEx.ExceptionClass = ClassName()
            logEx.ExceptionStackTrace = exc.StackTrace
            logEx.ExceptionType = exc.GetType.ToString
            logEx.Persist()
            Throw logEx

        Finally

        End Try

        Return retCode
    End Function
#End Region

#Region "Class Constant Properties"
    Public ReadOnly Property ClassName()
        Get
            Return cClassName
        End Get
    End Property

    Public ReadOnly Property ClassAuthor()
        Get
            Return cClassAuthor
        End Get
    End Property

    Public ReadOnly Property ClassVersion()
        Get
            Return cClassVersion
        End Get
    End Property

#End Region

End Class
