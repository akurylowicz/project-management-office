Imports CommonLib

Public Class PmoNotification

#Region "Class Constants"
  ''' Class Info Constants
  Private Const cClassName As String = "PmoNotification"
  Private Const cClassAuthor As String = "SQL Object Generator"
  Private Const cClassVersion As String = "Version 2.0.a"
#End Region

#Region "Class Members"
  Private mNotificationId As Integer          ' Primary Key to table... 
  Private mNotificationTypeId As Integer
  Private mUserId As Integer
  Private mIsNew As Boolean
  Private mNeedsSave As Boolean
  Private mLoadedFromDb As Boolean
#End Region

#Region "Constructors"
  ''' Simple Constructor
  Public Sub New()
    Initialize()
    Me.mIsNew = True
    Me.mLoadedFromDb = False
    Me.mNeedsSave = True
  End Sub
  ''' Complex Constructor
  Public Sub New(ByVal notificationIdIn As Integer)
    Initialize()
    mNotificationId = notificationIdIn
    Load()
  End Sub
  ''' Initialization Routines
  Private Sub Initialize()
    mNotificationId = 0
    mNotificationTypeId = 0
    mUserId = 0
    mIsNew = False
    mNeedsSave = False
    mLoadedFromDb = False
  End Sub
#End Region

#Region "Member Properties"
  ''' Member Properties
  Public Property NotificationId() As Integer
    Get
      Return mNotificationId
    End Get
    Set(ByVal Value As Integer)
      mNotificationId = Value
      Me.mNeedsSave = True
    End Set
  End Property

  Public Property NotificationTypeId() As Integer
    Get
      Return mNotificationTypeId
    End Get
    Set(ByVal Value As Integer)
      mNotificationTypeId = Value
      Me.mNeedsSave = True
    End Set
  End Property
  Public ReadOnly Property NotificationType() As String
    Get
      Dim nType As String = "Not Found"
      If Me.mNotificationTypeId > 0 Then
        Dim pmoMgr As New pmoManager
        Dim ds As DataSet = pmoMgr.GetNotificationType(Me.mNotificationTypeId)
        If ds.Tables(0).Rows.Count > 0 Then
          nType = ds.Tables(0).Rows(0).Item("notificationType")
        End If
      End If
      Return nType
    End Get
  End Property
  Public Property UserId() As Integer
    Get
      Return mUserId
    End Get
    Set(ByVal Value As Integer)
      mUserId = Value
      Me.mNeedsSave = True
    End Set
  End Property

  Public Property IsNew() As Boolean
    Get
      Return mIsNew
    End Get
    Set(ByVal Value As Boolean)
      mIsNew = Value
      Me.mNeedsSave = True
    End Set
  End Property

  Public Property NeedsSave() As Boolean
    Get
      Return mNeedsSave
    End Get
    Set(ByVal Value As Boolean)
      mNeedsSave = Value
      Me.mNeedsSave = True
    End Set
  End Property

  Public Property LoadedFromDb() As Boolean
    Get
      Return mLoadedFromDb
    End Get
    Set(ByVal Value As Boolean)
      mLoadedFromDb = Value
      Me.mNeedsSave = True
    End Set
  End Property

#End Region

#Region "Persistence Routines"
  Public Function Persist() As Boolean
    Dim retCode As Boolean = True

    If Me.Validate.Count > 0 Then
      Return False
    End If

    Try
      ' TODO: Add Persistence Code
      Dim dbParms As New DbParmList
      If mNotificationId > 0 Then
        dbParms.Add(New DbParm("@notificationId", mNotificationId))
      End If
      dbParms.Add(New DbParm("@notificationTypeId", mNotificationTypeId))
      dbParms.Add(New DbParm("@userId", mUserId))

      Dim dbMgr As DALDataManager
      dbMgr = DALDataManager.GetDatabaseObject
      Dim dbAccessor As New DbAccess

      Dim dbXfer As New DbXfer
      dbXfer.DatabaseMgr = CType(dbMgr, DALDataManager)
      'Database Name goes here...
      dbXfer.DatabaseName = "pmo"
      dbXfer.ParmList = dbParms


      If mNotificationId > 0 Then
        If dbAccessor.Update(dbXfer, "spUpdatePmoNotification") Then
          retCode = True
          Me.mNeedsSave = False
        End If
      Else
        If dbAccessor.Insert(dbXfer, "spInsertPmoNotification", True, "spUpdatePmoNotification") Then
          mNotificationId = dbAccessor.ReturnValue
          retCode = True
          Me.mNeedsSave = False
          Me.mIsNew = False
        End If
      End If


    Catch exc As Exception
      Dim logEx As New PmoException(exc.Message)
      logEx.ExceptionFunction = "Persist()"
      logEx.ExceptionClass = ClassName()
      logEx.ExceptionStackTrace = exc.StackTrace
      logEx.ExceptionType = exc.GetType.ToString
      logEx.Persist()
      Throw logEx

    Finally

    End Try

    Return retCode
  End Function
  Public Function Validate() As UtilValidationList
    Dim objValList As New UtilValidationList
    ' perform validation edits.  Whenever an error is found, instantiate a
    ' UtilValidation object and add it to the Validation List

    Try
      ' TODO: Add Validation Code

    Catch exc As Exception
      Dim logEx As New PmoException(exc.Message)
      logEx.ExceptionFunction = "Validate()"
      logEx.ExceptionClass = ClassName()
      logEx.ExceptionStackTrace = exc.StackTrace
      logEx.ExceptionType = exc.GetType.ToString
      logEx.Persist()
      Throw logEx

    Finally

    End Try

    Return objValList
  End Function
#End Region

#Region "Load Routines"
  ''' Load
  Private Function Load() As Boolean
    Dim retCode As Boolean = True
    Dim ds As DataSet

    ' Load the dataset... then call LoadFromDataset(ds)... please change manager object and key value as needed.
    If Me.mNotificationId > 0 Then
      Dim pmoMgr As New pmoManager

      Try
        ds = pmoMgr.GetNotification(Me.mNotificationId)
        If ds.Tables(0).Rows.Count > 0 Then
          retCode = LoadFromDataset(ds)
          If retCode = True Then
            Me.mIsNew = False
            Me.mLoadedFromDb = True
            Me.mNeedsSave = False
          End If
        End If

      Catch exc As Exception
        Dim logEx As New PmoException(exc.Message)
        logEx.ExceptionFunction = "Load()"
        logEx.ExceptionClass = ClassName()
        logEx.ExceptionStackTrace = exc.StackTrace
        logEx.ExceptionType = exc.GetType.ToString
        logEx.Persist()
        Throw logEx

      Finally

      End Try

    Else
      Dim logEx As New PmoException("Attempt to insantiate object without key value.")
      logEx.ExceptionFunction = "Load()"
      logEx.ExceptionClass = ClassName()
      logEx.Persist()
      Throw logEx
    End If

    Return retCode
  End Function
  ''' Load From Dataset
  Private Function LoadFromDataset(ByVal dsIn As DataSet) As Boolean
    Dim retCode As Boolean = True
    ' use the data in the dataset to load the class information
    mNotificationId = CheckForNull(dsIn.Tables(0).Rows(0).Item("notificationId"), "Integer")
    mNotificationTypeId = CheckForNull(dsIn.Tables(0).Rows(0).Item("notificationTypeId"), "Integer")
    mUserId = CheckForNull(dsIn.Tables(0).Rows(0).Item("userId"), "Integer")

    Return retCode
  End Function
#End Region

#Region "Delete Routine"
  ''' Delete
  Public Function Delete() As Boolean
    Dim retCode As Boolean = True

    Try
      If Me.mNotificationId > 0 Then
        Dim dbParms As New DbParmList
        dbParms.Add(New DbParm("@notificationId", mNotificationId))
        Dim dbMgr As CommonLib.DALDataManager
        dbMgr = CommonLib.DALDataManager.GetDatabaseObject
        Dim dbAccessor As New DbAccess

        Dim dbXfer As New DbXfer
        dbXfer.DatabaseMgr = CType(dbMgr, CommonLib.DALDataManager)
        'Database Name goes here...
        dbXfer.DatabaseName = "pmo"
        dbXfer.ParmList = dbParms

        If dbAccessor.Delete(dbXfer, "spDeletePmoNotification") Then
          retCode = True
        Else
          retCode = False
        End If

      End If

    Catch exc As Exception
      Dim logEx As New PmoException(exc.Message)
      logEx.ExceptionFunction = "Delete()"
      logEx.ExceptionClass = ClassName()
      logEx.ExceptionStackTrace = exc.StackTrace
      logEx.ExceptionType = exc.GetType.ToString
      logEx.Persist()
      Throw logEx

    Finally

    End Try

    Return retCode
  End Function
#End Region

#Region "Class Constant Properties"
  Public ReadOnly Property ClassName()
    Get
      Return cClassName
    End Get
  End Property

  Public ReadOnly Property ClassAuthor()
    Get
      Return cClassAuthor
    End Get
  End Property

  Public ReadOnly Property ClassVersion()
    Get
      Return cClassVersion
    End Get
  End Property

#End Region

End Class
