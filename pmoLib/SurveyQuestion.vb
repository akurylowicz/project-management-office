Imports CommonLib

public class SurveyQuestion

#Region "Class Constants"
  ''' Class Info Constants
  private const cClassName as string = "SurveyQuestion"
  private const cClassAuthor as string = "SQL Object Generator"
  private const cClassVersion as string = "Version 2.0.a"
#End Region

#Region "Class Members"
  ''' Class Members
  private mCreatedByDate as Date
  private mCreatedById as Integer
  private mDisplayOrder as Integer
  private mEffectiveDate as Date
  private mExpiredDate as Date
  private mQuestionId as Integer          ' Primary Key to table... 
  private mQuestionText as String
  private mUpdatedById as Integer
  private mUpdatedDate as Date
  Private mIsNew as Boolean
  Private mNeedsSave as Boolean
  Private mLoadedFromDb as Boolean
#End Region

#Region "Constructors"
  ''' Simple Constructor
Public Sub New()
  Initialize()
  Me.mIsNew = True
  Me.mLoadedFromDb = False
  Me.mNeedsSave = True
End Sub
  ''' Complex Constructor
Public Sub New(byval questionIdIn as Integer)
  Initialize()
  mQuestionId = questionIdIn
  Load()
End Sub
  ''' Initialization Routines
Private Sub Initialize()
  mCreatedByDate = nothing
  mCreatedById = 0
  mDisplayOrder = 0
  mEffectiveDate = nothing
  mExpiredDate = nothing
  mQuestionId = 0
  mQuestionText = ""
  mUpdatedById = 0
  mUpdatedDate = nothing
  mIsNew = False
  mNeedsSave = False
  mLoadedFromDb = False
End Sub
#End Region

#Region "Member Properties"
  ''' Member Properties
Public Property CreatedByDate() as Date
	Get
		return mCreatedByDate
	End Get
	Set(byval Value as Date)
		mCreatedByDate = Value
		Me.mNeedsSave = True
	End Set
End Property

Public Property CreatedById() as Integer
	Get
		return mCreatedById
	End Get
	Set(byval Value as Integer)
		mCreatedById = Value
		Me.mNeedsSave = True
	End Set
End Property

Public Property DisplayOrder() as Integer
	Get
		return mDisplayOrder
	End Get
	Set(byval Value as Integer)
		mDisplayOrder = Value
		Me.mNeedsSave = True
	End Set
End Property

Public Property EffectiveDate() as Date
	Get
		return mEffectiveDate
	End Get
	Set(byval Value as Date)
		mEffectiveDate = Value
		Me.mNeedsSave = True
	End Set
End Property

Public Property ExpiredDate() as Date
	Get
		return mExpiredDate
	End Get
	Set(byval Value as Date)
		mExpiredDate = Value
		Me.mNeedsSave = True
	End Set
End Property

Public Property QuestionId() as Integer
	Get
		return mQuestionId
	End Get
	Set(byval Value as Integer)
		mQuestionId = Value
		Me.mNeedsSave = True
	End Set
End Property

Public Property QuestionText() as String
	Get
		return mQuestionText
	End Get
	Set(byval Value as String)
		mQuestionText = Value
		Me.mNeedsSave = True
	End Set
End Property

Public Property UpdatedById() as Integer
	Get
		return mUpdatedById
	End Get
	Set(byval Value as Integer)
		mUpdatedById = Value
		Me.mNeedsSave = True
	End Set
End Property

Public Property UpdatedDate() as Date
	Get
		return mUpdatedDate
	End Get
	Set(byval Value as Date)
		mUpdatedDate = Value
		Me.mNeedsSave = True
	End Set
End Property

Public Property IsNew() as Boolean
	Get
		return mIsNew
	End Get
	Set(byval Value as Boolean)
		mIsNew = Value
		Me.mNeedsSave = True
	End Set
End Property

Public Property NeedsSave() as Boolean
	Get
		return mNeedsSave
	End Get
	Set(byval Value as Boolean)
		mNeedsSave = Value
		Me.mNeedsSave = True
	End Set
End Property

Public Property LoadedFromDb() as Boolean
	Get
		return mLoadedFromDb
	End Get
	Set(byval Value as Boolean)
		mLoadedFromDb = Value
		Me.mNeedsSave = True
	End Set
End Property

#End Region

#Region "Persistence Routines"
Public function Persist() as boolean
  dim retCode as boolean = True
 
  if me.Validate.count > 0 then
    return false
  end if
 
  try
  ' TODO: Add Persistence Code
  Dim dbParms as New DbParmList
  If mquestionId > 0 then
     dbParms.Add(New DbParm("@questionId", mquestionId))
  End If
    dbParms.Add(New DbParm("@createdByDate", mcreatedByDate))
    dbParms.Add(New DbParm("@createdById", mcreatedById))
    dbParms.Add(New DbParm("@displayOrder", mdisplayOrder))
    dbParms.Add(New DbParm("@effectiveDate", meffectiveDate))
    dbParms.Add(New DbParm("@expiredDate", mexpiredDate))
    dbParms.Add(New DbParm("@questionText", mquestionText))
    dbParms.Add(New DbParm("@updatedById", mupdatedById))
    dbParms.Add(New DbParm("@updatedDate", mupdatedDate))
 
    Dim dbMgr As DALDataManager
    dbMgr = DALDataManager.GetDatabaseObject
    Dim dbAccessor As New DbAccess
 
    Dim dbXfer As New DbXfer
    dbXfer.DatabaseMgr = CType(dbMgr, DALDataManager)
    'Database Name goes here...
    dbXfer.DatabaseName = "pmo"
    dbXfer.ParmList = dbParms

      If mQuestionId > 0 Then
        If dbAccessor.Update(dbXfer, "spUpdateSurveyQuestion") Then
          retCode = True
          Me.mNeedsSave = False
        End If
      Else
        If dbAccessor.Insert(dbXfer, "spInsertSurveyQuestion", True, "spUpdateSurveyQuestion") Then
          mQuestionId = dbAccessor.ReturnValue
          retCode = True
          Me.mNeedsSave = False
          Me.mIsNew = False
        End If
      End If


    Catch exc As Exception
      Dim logEx As New PmoException(exc.Message)
      logEx.ExceptionFunction = "Persist()"
      logEx.ExceptionClass = ClassName()
      logEx.ExceptionStackTrace = exc.StackTrace
      logEx.ExceptionType = exc.GetType.ToString
      logEx.Persist()
      Throw logEx

    Finally

    End Try
 
  return retCode
end function
Public function Validate() as UtilValidationList
  dim objValList as new UtilValidationList
  ' perform validation edits.  Whenever an error is found, instantiate a
  ' UtilValidation object and add it to the Validation List
  
  try
  ' TODO: Add Validation Code
 
  catch exc as Exception
      Dim logEx As New PmoException(exc.Message)
      logEx.ExceptionFunction = "Validate()"
      logEx.ExceptionClass = ClassName()
      logEx.ExceptionStackTrace = exc.StackTrace
      logEx.ExceptionType = exc.GetType.ToString
      logEx.Persist()
      Throw logEx
 
  finally
 
  end try
 
  return objValList
end function
#End Region

#Region "Load Routines"
  ''' Load
Private function Load() as boolean
    Dim retCode As Boolean = True
    Dim ds As DataSet
 
  ' Load the dataset... then call LoadFromDataset(ds)... please change manager object and key value as needed.
   If Me.mquestionId > 0 Then
      Dim pmoMgr As New pmoManager
  
     try
        ds = pmoMgr.GetSurveyQuestion(Me.mQuestionId)
        If ds.Tables(0).Rows.Count > 0 Then
          retCode = LoadFromDataset(ds)
          If retCode = True Then
            Me.mIsNew = False
            Me.mLoadedFromDb = True
            Me.mNeedsSave = False
          End If
        End If
 
     catch exc as Exception
        Dim logEx As New PmoException(exc.Message)
        logEx.ExceptionFunction = "Load()"
        logEx.ExceptionClass = ClassName()
        logEx.ExceptionStackTrace = exc.StackTrace
        logEx.ExceptionType = exc.GetType.ToString
        logEx.Persist()
    Throw logEx
 
     finally
 
     end try
 
  Else
      Dim logEx As New PmoException("Attempt to insantiate object without key value.")
    logEx.ExceptionFunction = "Load()"
      logEx.ExceptionClass = ClassName()
      logEx.Persist()
    Throw logEx
  End If
 
  return retCode
end function
  ''' Load From Dataset
Private function LoadFromDataset(dsIn as dataset) as boolean
  dim retCode as boolean = True
  ' use the data in the dataset to load the class information
  mCreatedByDate = CheckForNull(dsIn.tables(0).rows(0).item("createdByDate"), "Date")
  mCreatedById = CheckForNull(dsIn.tables(0).rows(0).item("createdById"), "Integer")
  mDisplayOrder = CheckForNull(dsIn.tables(0).rows(0).item("displayOrder"), "Integer")
  mEffectiveDate = CheckForNull(dsIn.tables(0).rows(0).item("effectiveDate"), "Date")
  mExpiredDate = CheckForNull(dsIn.tables(0).rows(0).item("expiredDate"), "Date")
  mQuestionId = CheckForNull(dsIn.tables(0).rows(0).item("questionId"), "Integer")
  mQuestionText = CheckForNull(dsIn.tables(0).rows(0).item("questionText"), "String")
  mUpdatedById = CheckForNull(dsIn.tables(0).rows(0).item("updatedById"), "Integer")
  mUpdatedDate = CheckForNull(dsIn.tables(0).rows(0).item("updatedDate"), "Date")
 
  return retCode
end function
#End Region

#Region "Delete Routine"
  ''' Delete
Public function Delete() as boolean
  dim retCode as boolean = True
 
  try
   If Me.mquestionId > 0 Then
  Dim dbParms as New DbParmList
  dbParms.Add(New DbParm("@questionId", mquestionId))
    Dim dbMgr As CommonLib.DALDataManager
    dbMgr = CommonLib.DALDataManager.GetDatabaseObject
    Dim dbAccessor As New DbAccess
 
    Dim dbXfer As New DbXfer
    dbXfer.DatabaseMgr = CType(dbMgr, CommonLib.DALDataManager)
    'Database Name goes here...
    dbXfer.DatabaseName = "pmo"
    dbXfer.ParmList = dbParms

    If dbAccessor.Delete(dbXfer, "spDeleteSurveyQuestion") Then
      retCode = True
    Else
      retCode = False
    End If
 
  End If
 
  catch exc as Exception
      Dim logEx As New PmoException(exc.Message)
      logEx.ExceptionFunction = "Delete()"
      logEx.ExceptionClass = ClassName()
      logEx.ExceptionStackTrace = exc.StackTrace
      logEx.ExceptionType = exc.GetType.ToString
      logEx.Persist()
    Throw logEx
 
  finally
 
  end try
 
  return retCode
end function
#End Region

#Region "Class Constant Properties"
Public Readonly Property ClassName()
	Get
		return cClassName
	End Get
End Property

Public Readonly Property ClassAuthor()
	Get
		return cClassAuthor
	End Get
End Property

Public Readonly Property ClassVersion()
	Get
		return cClassVersion
	End Get
End Property

#End Region

End Class
