﻿Imports CommonLib
Public Class Project_OLD
    Private mId As Integer
    Private mCustomer As String
    Private mTitle As String
    Private mStartDate As Date
    Private mProjectInfo As ProjectInfo
    Private mProjectNotes As ProjectNoteList

    Private mStatusDateSet As String

    Private mSow As Sow

    Private Const cSystemName As String = "pmoLib.dll"
    Private Const cClassName As String = "Project"

    Public Sub New()
        Initialize()
    End Sub
    Public Sub New(ByVal projId As Integer, Optional ByVal fastLoad As Boolean = False)
        Initialize()
        Me.mId = projId
        Load(fastLoad)
    End Sub
    Private Sub Initialize()
        Me.mId = 0
        Me.mCustomer = ""
        Me.mTitle = ""
        Me.mStartDate = Nothing
        Me.mProjectInfo = Nothing
        Me.mProjectNotes = Nothing
        Me.mSow = Nothing
        Me.mStatusDateSet = ""
    End Sub
    Public Property Id() As Integer
        Get
            Return Me.mId
        End Get
        Set(ByVal value As Integer)
            Me.mId = value
        End Set
    End Property
    Public Property Customer() As String
        Get
            Return Me.mCustomer
        End Get
        Set(ByVal value As String)
            Me.mCustomer = value
        End Set
    End Property
    Public Property Title() As String
        Get
            Return Me.mTitle
        End Get
        Set(ByVal value As String)
            Me.mTitle = value
        End Set
    End Property
    Public Property StartDate() As Date
        Get
            Return Me.mStartDate
        End Get
        Set(ByVal value As Date)
            Me.mStartDate = value
        End Set
    End Property
    Public ReadOnly Property ProjectSurveyCompleted() As Boolean
        Get
            Dim pmoMgr As New pmoManager
            Return pmoMgr.SurveyCompletedForProject(Me.mId)
        End Get
    End Property
    Public ReadOnly Property ProjectSurveyCount() As Integer
        Get
            Dim pmoMgr As New pmoManager
            Return pmoMgr.SurveyCompletedCount(Me.mId)
        End Get
    End Property
    Public ReadOnly Property ProjectSurveyResultId() As Integer
        Get
            Dim pmoMgr As New pmoManager
            Return pmoMgr.GetSurveyResultIdForProject(Me.mId)
        End Get
    End Property
    Public ReadOnly Property StartDateDisplay() As String
        Get
            If IsValidDate(Me.mStartDate) Then
                Return Me.mStartDate.ToShortDateString
            Else
                Return ""
            End If
        End Get
    End Property
    'Extended Information
    Public ReadOnly Property ExtendedInfo() As ProjectInfo
        Get
            If Me.mProjectInfo Is Nothing Then
                LoadExtendedProjectInfo()
            End If
            Return Me.mProjectInfo
        End Get
    End Property
    Public Property SowId(Optional ByVal currUser As String = "Not Set") As Integer
        Get
            If Not ExtendedInfo Is Nothing Then
                Return Me.mProjectInfo.SowId
            End If
        End Get
        Set(ByVal value As Integer)
            If Not ExtendedInfo Is Nothing Then
                Me.mProjectInfo.SowId = value
                Me.mProjectInfo.Persist()
            Else
                Dim info As New ProjectInfo
                info.ProjectId = Me.mId
                info.SowId = value
            End If
        End Set
    End Property
    Public ReadOnly Property SowIdForDisplay() As String
        Get
            Dim sowId As String = ""
            If Not ExtendedInfo Is Nothing Then
                If Me.mProjectInfo.SowId > 0 Then
                    sowId = Me.mProjectInfo.SowId.ToString
                End If
            End If
            Return sowId
        End Get
    End Property
    Public ReadOnly Property HasSowAssigned() As Boolean
        Get
            Dim hasSow As Boolean = False
            If Not ExtendedInfo Is Nothing Then
                If Me.mProjectInfo.SowId > 0 Then
                    hasSow = True
                End If
            End If
            Return hasSow
        End Get
    End Property
    Public ReadOnly Property Sow() As Sow
        Get
            If Me.mSow Is Nothing Then
                LoadSow()
            End If
            Return Me.mSow
        End Get
    End Property
    Public ReadOnly Property SowDocumentLink() As String
        Get
            If Not Sow Is Nothing Then
                Return Sow.SowFileLink
            Else
                Return ""
            End If
        End Get
    End Property
    Public Property PmOwnerId() As Integer
        Get
            If Not ExtendedInfo Is Nothing Then
                Return Me.mProjectInfo.PmAssignedId
            End If
        End Get
        Set(ByVal value As Integer)
            If Not ExtendedInfo Is Nothing Then
                Me.mProjectInfo.PmAssignedId = value
                Me.mProjectInfo.PmAssignedDate = Now
                Me.mProjectInfo.Persist()
            Else
                Dim info As New ProjectInfo
                info.ProjectId = Me.mId
                info.PmAssignedId = value
                info.PmAssignedDate = Now
                info.Persist()
            End If
        End Set
    End Property
    Public ReadOnly Property PmOwnerAssignedDate() As Date
        Get
            If Not Me.mProjectInfo Is Nothing Then
                Return Me.mProjectInfo.PmAssignedDate
            End If
        End Get
    End Property
    Public Property ManagedProject() As Boolean
        Get
            If Not ExtendedInfo Is Nothing Then
                Return Me.mProjectInfo.PmoManagedProject
            End If
        End Get
        Set(ByVal value As Boolean)
            If Not ExtendedInfo Is Nothing Then
                Me.mProjectInfo.PmoManagedProject = value
                Me.mProjectInfo.Persist()
            Else
                Dim info As New ProjectInfo
                info.ProjectId = Me.mId
                info.PmoManagedProject = value
            End If
        End Set
    End Property
    Public ReadOnly Property ManagedBy() As String
        Get
            If Not ExtendedInfo Is Nothing Then
                Return Me.mProjectInfo.ManagedByName
            Else
                Return "No PM Assigned"
            End If
        End Get
    End Property
    Public ReadOnly Property ProjectStatusCode() As Integer
        Get
            If Not ExtendedInfo Is Nothing Then
                Return Me.mProjectInfo.ProjectStatusCode
            End If
        End Get
    End Property
    Public Sub SetProjectStatusCode(ByVal statusCode As Integer, ByVal userId As Integer)
        If Not ExtendedInfo Is Nothing Then
            Me.mProjectInfo.ProjectStatusCode = statusCode
            Me.mProjectInfo.ProjectStatusAssignedById = userId
            Me.mProjectInfo.Persist()
        Else
            Dim info As New ProjectInfo
            info.ProjectId = Me.mId
            Me.mProjectInfo.ProjectStatusCode = statusCode
            Me.mProjectInfo.ProjectStatusAssignedById = userId
        End If
    End Sub
    Public ReadOnly Property ProjectStatusCodeAssignedDateAsString() As String
        Get
            Return Me.mStatusDateSet
        End Get
    End Property
    Public ReadOnly Property ProjectStatusAssignment() As String
        Get
            Dim info As String = ""
            If Not ExtendedInfo Is Nothing Then
                info += " (" & Me.mProjectInfo.ProjectStatusAssignedDate.ToShortDateString
                info += " by " & Me.mProjectInfo.ProjectStatusAssignedByName & ")"
            End If
            Return info
        End Get
    End Property
    Public ReadOnly Property CurrentStatusDescription() As String
        Get
            If Me.mProjectInfo.ProjectStatusCode > 0 Then
                Dim pmoMgr As New pmoManager
                Return pmoMgr.GetProjectStatusCode(Me.mProjectInfo.ProjectStatusCode).Tables(0).Rows(0).Item("projectStatusDescrip")
            Else
                Return "Not Set"
            End If
        End Get
    End Property
    ' Project Notes
    Public ReadOnly Property ProjectNotes() As ProjectNoteList
        Get
            Return Me.mProjectNotes
        End Get
    End Property
    Public Function AddProjectNote(ByVal noteIn As String, ByVal userIdIn As String) As Boolean
        Dim retVal As Boolean = False
        Dim theNote As New ProjectNote
        theNote.Note = noteIn
        theNote.NoteAddedBy = userIdIn
        theNote.NoteAddedDate = Now
        theNote.ProjectId = Me.mId
        retVal = theNote.Persist()
        If retVal Then
            LoadProjectNotes()
        End If
        Return retVal
    End Function
    Public Function ModifyProjectNote(ByVal noteIn As String, ByVal userIdIn As String, ByVal noteKey As Integer) As Boolean
        Dim retVal As Boolean = False
        Dim theNote As New ProjectNote(noteKey)
        If Not theNote Is Nothing Then
            theNote.Note = noteIn
            theNote.NoteAddedBy = userIdIn
            retVal = theNote.Persist()
            If retVal Then
                LoadProjectNotes()
            End If
        End If
        Return retVal
    End Function
    Public Function DeleteProjectNote(ByVal userIdIn As String, ByVal noteKey As Integer) As Boolean
        Dim retVal As Boolean = False
        Dim theNote As New ProjectNote(noteKey)
        If Not theNote Is Nothing Then
            retVal = theNote.Delete
            If retVal Then
                LoadProjectNotes()
            End If
        End If
        Return retVal
    End Function
    Public Sub RefreshNotes()
        LoadProjectNotes()
    End Sub

    Private Function Load(ByVal fastLoad As Boolean) As Boolean
        Dim retCode As Boolean = True
        Dim ds As DataSet

        ' Load the dataset... then call LoadFromDataset(ds)... please change manager object and key value as needed.
        If Me.mId > 0 Then
            Dim pmoMgr As New pmoManager

            Try
                ds = pmoMgr.GetPmoProject(Me.mId)
                If ds.Tables(0).Rows.Count > 0 Then
                    retCode = LoadFromDataset(ds)
                    If retCode Then
                        retCode = LoadExtendedProjectInfo()
                        If Not fastLoad Then
                            retCode = LoadProjectNotes()
                            retCode = LoadSow()
                        End If
                    End If
                End If

            Catch exc As Exception
                Dim pmoExc As New PmoException(exc.Message.ToString)
                pmoExc.ExceptionFunction = "Load"
                pmoExc.ExceptionClass = cClassName
                pmoExc.ExceptionStackTrace = exc.StackTrace
                pmoExc.ExceptionType = exc.GetType.ToString
                pmoExc.Persist()
                Throw pmoExc
            Finally

            End Try

        Else
            Dim exc As New Exception("Attempt to insantiate a Project object without key value.")
            Dim pmoExc As New PmoException(exc.Message)
            pmoExc.ExceptionFunction = "Load"
            pmoExc.ExceptionClass = cClassName
            pmoExc.ExceptionStackTrace = "N/A"
            pmoExc.ExceptionType = pmoExc.GetType.ToString
            pmoExc.UserExplanation = "The Project cannot be loaded because there was no identifier specified.  This is a system fault and has been reported."
            pmoExc.UserAction = "No action required by the user."
            pmoExc.Persist()
            Throw pmoExc
        End If

        Return retCode
    End Function
    Private Function LoadFromDataset(ByVal dsIn As DataSet) As Boolean
        Dim retCode As Boolean = True
        ' use the data in the dataset to load the class information
        Me.mCustomer = CheckForNull(dsIn.Tables(0).Rows(0).Item("Customer_name"), "String")
        Me.mTitle = CheckForNull(dsIn.Tables(0).Rows(0).Item("Project_name"), "String")
        Me.mStartDate = CheckForNull(dsIn.Tables(0).Rows(0).Item("Start_date"), "Date")
        Return retCode
    End Function
    Private Function LoadExtendedProjectInfo() As Boolean
        Dim retCode As Boolean = True
        Me.mProjectInfo = Nothing
        If Me.mId > 0 Then
            Me.mProjectInfo = New ProjectInfo(Me.mId)
            If IsValidDate(Me.mProjectInfo.ProjectStatusAssignedDate) Then
                Me.mStatusDateSet = Me.mProjectInfo.ProjectStatusAssignedDate.ToShortDateString
            End If
        End If
        Return retCode
    End Function
    Private Function LoadProjectNotes() As Boolean
        Dim retCode As Boolean = True
        Me.mProjectNotes = Nothing
        If Me.mId > 0 Then
            Dim theMgr As New pmoManager
            Me.mProjectNotes = theMgr.GetProjectNotesList(Me.mId)
        End If
        Return retCode
    End Function
    Private Function LoadSow() As Boolean
        Dim retCode As Boolean = False
        If SowId > 0 Then
            Dim pmoMgr As New pmoManager
            Me.mSow = New Sow(pmoMgr.GetKeyForSow(SowId))
            retCode = True
        End If
        Return retCode
    End Function
    Private ReadOnly Property ClassName() As String
        Get
            Return cClassName
        End Get
    End Property
End Class
