﻿Imports Microsoft.VisualBasic
Imports CommonLib
Imports System.Data
Imports MySql
Imports System.Data.SqlClient

Public Class pmoManager
  Private mDbMgr As DALDataManager
  Private Const ClassName As String = "pmoManager"

  Public Enum PmoNotificationType
    SetToOffTrack = 1
    SetFromOffTrack = 2
    SetToConcerned = 3
    SetFromCaution = 4
    NightlyStatusReport = 5
    WeeklyStatusReport = 6
    NightlyStatusChangeReport = 7
    WeeklyStatusChangeReport = 8
    MyStatusReport = 9
  End Enum
  Public Sub New()
    mDbMgr = DALDataManager.GetDatabaseObject

    If mDbMgr Is Nothing Then
      Dim exc As New PmoException("PMO Manager instantiated without a current DALDataManager.")
      exc.ExceptionClass = ClassName
      exc.ExceptionFunction = "New()"
      exc.ExceptionStackTrace = exc.StackTrace.ToString()
      exc.Persist()
      Throw exc
    End If
  End Sub
  Public Function GetSow(ByVal sowKeyIn As Integer, Optional ByVal dataSetTablename As String = "") As DataSet
    Dim dbParms As New DbParmList
    Dim dbAccessor As New DbAccess
    Dim dbXfer As New DbXfer
    dbParms.Add(New DbParm("@sowKey", sowKeyIn))
    dbXfer.DatabaseMgr = Me.mDbMgr
    dbXfer.DatabaseName = "pmo"
    dbXfer.ParmList = dbParms

    If dataSetTablename = "" Then
      dataSetTablename = "tblSow"
    End If

    Try
      Return dbAccessor.GetRecord(dbXfer, "spSelectSow", dataSetTablename)

    Catch exc As Exception
      Dim nucEx As New PmoException(exc.Message)
      nucEx.ExceptionFunction = "GetSow"
      nucEx.ExceptionClass = ClassName
      nucEx.UserExplanation = "An exception was thrown while attempting to get the SOW: " & sowKeyIn
      nucEx.ExceptionStackTrace = exc.StackTrace.ToString
      nucEx.Persist()
      Throw nucEx
    End Try
  End Function
  Public Function GetSowList(Optional ByVal dataSetTablename As String = "") As DataSet
    Dim dbParms As New DbParmList
    Dim dbAccessor As New DbAccess
    Dim dbXfer As New DbXfer
    dbXfer.DatabaseMgr = Me.mDbMgr
    dbXfer.DatabaseName = "pmo"
    dbXfer.ParmList = dbParms

    If dataSetTablename = "" Then
      dataSetTablename = "tblSow"
    End If

    Try
      Return dbAccessor.GetRecord(dbXfer, "spGetAllSow", dataSetTablename)

    Catch exc As Exception
      Dim nucEx As New PmoException(exc.Message)
      nucEx.ExceptionFunction = "GetSowList"
      nucEx.ExceptionClass = ClassName
      nucEx.UserExplanation = "An exception was thrown while attempting to get the SOW List "
      nucEx.ExceptionStackTrace = exc.StackTrace.ToString
      nucEx.Persist()
      Throw nucEx
    End Try
  End Function
    Public Function GetSowListing(ByVal showInactive As Boolean, Optional ByVal dataSetTablename As String = "") As SowList
        Dim theList As New SowList
        Dim dbParms As New DbParmList
        Dim dbAccessor As New DbAccess
        Dim dbXfer As New DbXfer
        dbXfer.DatabaseMgr = Me.mDbMgr
        dbXfer.DatabaseName = "pmo"
        dbXfer.ParmList = dbParms

        If dataSetTablename = "" Then
            dataSetTablename = "tblSow"
        End If

        Dim ds As DataSet

        Try
            If showInactive Then
                ds = dbAccessor.GetRecord(dbXfer, "spGetAllSow", dataSetTablename)
            Else
                ds = dbAccessor.GetRecord(dbXfer, "spGetAllActiveSow", dataSetTablename)
            End If

            Dim counter As Integer = 0
            While counter < ds.Tables(0).Rows.Count
                Dim tmpSow As New Sow(ds.Tables(0).Rows(counter).Item("sowKey"))
                theList.Add(tmpSow)
                counter += 1
            End While

            Return theList

        Catch exc As Exception
            Dim nucEx As New PmoException(exc.Message)
            nucEx.ExceptionFunction = "GetSowListing"
            nucEx.ExceptionClass = ClassName
            nucEx.UserExplanation = "An exception was thrown while attempting to get the SOW Listing "
            nucEx.ExceptionStackTrace = exc.StackTrace.ToString
            nucEx.Persist()
            Throw nucEx
        End Try
    End Function
    Public Function GetFilteredSowListing(ByVal startingChar As String, ByVal showInactive As Boolean, Optional ByVal dataSetTablename As String = "") As SowList
        Dim theList As New SowList
        Dim dbParms As New DbParmList
        Dim dbAccessor As New DbAccess
        Dim dbXfer As New DbXfer
        dbParms.Add(New DbParm("@startsWith", startingChar.ToLower))
        dbXfer.DatabaseMgr = Me.mDbMgr
        dbXfer.DatabaseName = "pmo"
        dbXfer.ParmList = dbParms

        If dataSetTablename = "" Then
            dataSetTablename = "tblSow"
        End If

        Dim ds As DataSet

        Try
            If showInactive Then
                    ds = dbAccessor.GetRecord(dbXfer, "spGetAllSow_Filtered", dataSetTablename)
            Else
                ds = dbAccessor.GetRecord(dbXfer, "spGetAllActiveSow_Filtered", dataSetTablename)
            End If

            Dim counter As Integer = 0
            While counter < ds.Tables(0).Rows.Count
                Dim tmpSow As New Sow(ds.Tables(0).Rows(counter).Item("sowKey"))
                theList.Add(tmpSow)
                counter += 1
            End While

            Return theList

        Catch exc As Exception
            Dim nucEx As New PmoException(exc.Message)
            nucEx.ExceptionFunction = "GetSowListing"
            nucEx.ExceptionClass = ClassName
            nucEx.UserExplanation = "An exception was thrown while attempting to get the SOW Listing "
            nucEx.ExceptionStackTrace = exc.StackTrace.ToString
            nucEx.Persist()
            Throw nucEx
        End Try
    End Function
  Public Function GetUnassignedSowList(ByVal projectIdIn As Integer, Optional ByVal dataSetTablename As String = "") As DataSet
    ' we pass in the projectId because we always want that in the list, along with all of the unassigned.
    Dim dbParms As New DbParmList
    Dim dbAccessor As New DbAccess
    Dim dbXfer As New DbXfer
    dbParms.Add(New DbParm("@projectId", projectIdIn))
    dbXfer.DatabaseMgr = Me.mDbMgr
    dbXfer.DatabaseName = "pmo"
    dbXfer.ParmList = dbParms

    If dataSetTablename = "" Then
      dataSetTablename = "tblSow"
    End If

    Try
      Return dbAccessor.GetRecord(dbXfer, "spGetAllUnassignedSow", dataSetTablename)

    Catch exc As Exception
      Dim nucEx As New PmoException(exc.Message)
      nucEx.ExceptionFunction = "GetUnassignedSowList"
      nucEx.ExceptionClass = ClassName
      nucEx.UserExplanation = "An exception was thrown while attempting to get the Unassigned SOW List "
      nucEx.ExceptionStackTrace = exc.StackTrace.ToString
      nucEx.Persist()
      Throw nucEx
    End Try
  End Function
  Public Function GetSowWithoutDocumentList(Optional ByVal dataSetTablename As String = "") As SowList
    ' we pass in the projectId because we always want that in the list, along with all of the unassigned.
    Dim theList As New SowList
    Dim dbParms As New DbParmList
    Dim dbAccessor As New DbAccess
    Dim dbXfer As New DbXfer
    dbXfer.DatabaseMgr = Me.mDbMgr
    dbXfer.DatabaseName = "pmo"
    dbXfer.ParmList = dbParms

    If dataSetTablename = "" Then
      dataSetTablename = "tblSow"
    End If

    Try
      Dim ds As DataSet = dbAccessor.GetRecord(dbXfer, "spGetAllSowWithoutDocument", dataSetTablename)

      Dim counter As Integer = 0
      While counter < ds.Tables(0).Rows.Count
        Dim tmpSow As New Sow(ds.Tables(0).Rows(counter).Item("sowKey"))
        theList.Add(tmpSow)
        counter += 1
      End While

      Return theList
    Catch exc As Exception
      Dim nucEx As New PmoException(exc.Message)
      nucEx.ExceptionFunction = "GetSowWithoutDocumentList"
      nucEx.ExceptionClass = ClassName
      nucEx.UserExplanation = "An exception was thrown while attempting to get the SOW without Document List "
      nucEx.ExceptionStackTrace = exc.StackTrace.ToString
      nucEx.Persist()
      Throw nucEx
    End Try
  End Function
  Public Function EstablishNewSow() As Integer
    Dim dbParms As New DbParmList
    Dim dbAccessor As New DbAccess
    Dim dbXfer As New DbXfer
    Dim nextSowId As Integer = 0
    dbXfer.DatabaseMgr = Me.mDbMgr
    dbXfer.DatabaseName = "pmo"
    dbXfer.ParmList = dbParms

    Try

      Dim ds As DataSet = dbAccessor.GetRecord(dbXfer, "spGetMaxSowId")

      If ds.Tables(0).Rows.Count > 0 Then
        nextSowId = ds.Tables(0).Rows(0).Item("maxSowId")
      End If

      If nextSowId > 0 Then
        nextSowId += 1
      End If

      CreateSowIdInDatabase(nextSowId)

      Return nextSowId

    Catch exc As Exception
      Dim nucEx As New PmoException(exc.Message)
      nucEx.ExceptionFunction = "GetSow"
      nucEx.ExceptionClass = ClassName
      nucEx.UserExplanation = "An exception was thrown while attempting to esablish a new SOW "
      nucEx.ExceptionStackTrace = exc.StackTrace.ToString
      nucEx.Persist()
      Throw nucEx
    End Try
  End Function
  Private Function CreateSowIdInDatabase(ByVal sowIdIn As Integer) As Boolean
    Dim dbParms As New DbParmList
    Dim dbAccessor As New DbAccess
    Dim dbXfer As New DbXfer
    dbParms.Add(New DbParm("@sowId", sowIdIn))
    dbXfer.DatabaseMgr = Me.mDbMgr
    dbXfer.DatabaseName = "pmo"
    dbXfer.ParmList = dbParms

    Try
      Return dbAccessor.ExecuteProc(dbXfer, "spInsertSowId")

    Catch exc As Exception
      Dim nucEx As New PmoException(exc.Message)
      nucEx.ExceptionFunction = "GetSow"
      nucEx.ExceptionClass = ClassName
      nucEx.UserExplanation = "An exception was thrown while attempting to create a SOW ID in the DB: " & sowIdIn
      nucEx.ExceptionStackTrace = exc.StackTrace.ToString
      nucEx.Persist()
      Throw nucEx
    End Try
  End Function
  Public Function GetKeyForSow(ByVal sowIdIn As Integer) As Integer
    Dim dbParms As New DbParmList
    Dim dbAccessor As New DbAccess
    Dim dbXfer As New DbXfer
    dbParms.Add(New DbParm("@sowIdIn", sowIdIn))
    dbXfer.DatabaseMgr = Me.mDbMgr
    dbXfer.DatabaseName = "pmo"
    dbXfer.ParmList = dbParms
    Try
      Dim ds As DataSet = dbAccessor.GetRecord(dbXfer, "spGetKeyForSowId", "")

      If ds.Tables(0).Rows.Count > 0 Then
        Return CInt(ds.Tables(0).Rows(0).Item(0))
      Else
        Throw New Exception("The GetKeyForSow function did not find a SOW for Id: " & sowIdIn)
      End If

    Catch exc As Exception
      Dim nucEx As New PmoException(exc.Message)
      nucEx.ExceptionFunction = "GetKeyForSow"
      nucEx.ExceptionClass = ClassName
      nucEx.UserExplanation = "An exception was thrown while attempting to get the Key value for SOW ID: " & sowIdIn
      nucEx.ExceptionStackTrace = exc.StackTrace.ToString
      nucEx.Persist()
      Throw nucEx
    End Try
  End Function
  Public Function UserHasActiveSow(ByVal userId As Integer) As Boolean
    Dim retVal As Boolean = False
    Dim dbParms As New DbParmList
    Dim dbAccessor As New DbAccess
    Dim dbXfer As New DbXfer
    dbParms.Add(New DbParm("@userId", userId))
    dbXfer.DatabaseMgr = Me.mDbMgr
    dbXfer.DatabaseName = "pmo"
    dbXfer.ParmList = dbParms

    Try
      Dim ds As DataSet = dbAccessor.GetRecord(dbXfer, "spGetSowIdListForUser")

      If ds.Tables(0).Rows.Count > 0 Then
        retVal = True
      End If

      Return retVal

    Catch exc As Exception
      Dim nucEx As New PmoException(exc.Message)
      nucEx.ExceptionFunction = "UserHasActiveSow"
      nucEx.ExceptionClass = ClassName
      nucEx.UserExplanation = "An exception was thrown while attempting to get the active SOW List associatrted with user id: " & userId
      nucEx.ExceptionStackTrace = exc.StackTrace.ToString
      nucEx.Persist()
      Throw nucEx
    End Try
    End Function
    Public Function GetAllPractices(Optional ByVal datasetTableName As String = "") As DataSet
        Dim dbParms As New DbParmList
        Dim dbAccessor As New DbAccess
        Dim dbXfer As New DbXfer
        dbXfer.DatabaseMgr = Me.mDbMgr
        dbXfer.DatabaseName = "pmo"
        dbXfer.ParmList = dbParms

        If dataSetTablename = "" Then
            datasetTableName = "tblPractice"
        End If

        Try
            Return dbAccessor.GetRecord(dbXfer, "spGetAllPractice", datasetTableName)

        Catch exc As Exception
            Dim nucEx As New PmoException(exc.Message)
            nucEx.ExceptionFunction = "GetPractices"
            nucEx.ExceptionClass = ClassName
            nucEx.UserExplanation = "An exception was thrown while attempting to get the Practice List "
            nucEx.ExceptionStackTrace = exc.StackTrace.ToString
            nucEx.Persist()
            Throw nucEx
        End Try
    End Function
    Public Function GetPracticeName(practiceId As Integer, Optional ByVal datasetTableName As String = "") As String
        Dim thePractice As String = ""
        Dim dbParms As New DbParmList
        Dim dbAccessor As New DbAccess
        Dim dbXfer As New DbXfer
        dbParms.Add(New DbParm("@practiceId", practiceId))
        dbXfer.DatabaseMgr = Me.mDbMgr
        dbXfer.DatabaseName = "pmo"
        dbXfer.ParmList = dbParms

        Try
            Dim ds As DataSet = dbAccessor.GetRecord(dbXfer, "spSelectPractice", "tblPractice")
            If ds.Tables(0).Rows.Count > 0 Then
                thePractice = CheckForNull(ds.Tables(0).Rows(0).Item("practice"), "String")
            End If
            Return thePractice

        Catch exc As Exception
            Dim nucEx As New PmoException(exc.Message)
            nucEx.ExceptionFunction = "GetPracticeName"
            nucEx.ExceptionClass = ClassName
            nucEx.UserExplanation = "An exception was thrown while attempting to get the Practice Name "
            nucEx.ExceptionStackTrace = exc.StackTrace.ToString
            nucEx.Persist()
            Throw nucEx
        End Try
    End Function
#Region "Projects"
  Public Function GetPmoProject(ByVal projectId As Integer) As DataSet
    Dim myConn As Data.MySqlClient.MySqlConnection = GetMySqlConnector()
    Dim strSql As String = "select *,Customer_name from projects "
    strSql += "inner join Customers on projects.Customer_id = customers.Customer_Id where "
    strSql += "Project_id =" & projectId
    Dim da As New MySql.Data.MySqlClient.MySqlDataAdapter(strSql, myConn)
    Dim ds As New DataSet
    da.Fill(ds, "projects")
    Return (ds)
    End Function
    Public Function GetChangepointProject(ByVal projectId As Guid) As DataSet
        Dim myConn As SqlClient.SqlConnection = GetChangepointSqlConnector()
        Dim strSql As String = "select Project.ProjectId,Project.CustomerId, Project.ProjectStatus, Project.Name as Project_name, Customer.Name as Customer_name, ActualStart as Start_date "
        strSql += "from Project Inner join Customer on Project.CustomerId = Customer.CustomerId where Project.ProjectId = '" & projectId.ToString & "'"
        'strSql += "from Project Inner join Customer on Project.CustomerId = Customer.CustomerId where (ProjectStatus = 'C' or ProjectStatus = 'I') and Project.ProjectId = '" & projectId.ToString & "'"
        Dim da As New SqlClient.SqlDataAdapter(strSql, myConn)
        Dim ds As New DataSet
        da.Fill(ds, "projects")
        Return (ds)
    End Function
    Public Function GetChangepointProject(ByVal projectId As Integer) As DataSet
        Dim myConn As SqlClient.SqlConnection = GetChangepointSqlConnector()
        Dim strSql As String = "select Project.ProjectId,Project.CustomerId, Project.ProjectStatus, Project.Name as Project_name, Customer.Name as Customer_name, ActualStart as Start_date "
        strSql += "from Project Inner join Customer on Project.CustomerId = Customer.CustomerId where Project.ProjectId = '" & projectId.ToString & "'"
        'strSql += "from Project Inner join Customer on Project.CustomerId = Customer.CustomerId where ProjectStatus = 'C' and Project.ProjectId = '" & projectId.ToString & "'"
        Dim da As New SqlClient.SqlDataAdapter(strSql, myConn)
        Dim ds As New DataSet
        da.Fill(ds, "projects")
        Return (ds)
    End Function
    Public Function GetClosedChangepointProjects() As DataSet
        Dim myConn As SqlClient.SqlConnection = GetChangepointSqlConnector()
        Dim strSql As String = "select Project.ProjectId,Project.CustomerId, Project.ProjectStatus, Project.Name, Customer.Name "
        strSql += "from Project Inner join Customer on Project.CustomerId = Customer.CustomerId where ProjectStatus = 'C'"
        Dim da As New SqlClient.SqlDataAdapter(strSql, myConn)
        Dim ds As New DataSet
        da.Fill(ds, "projects")
        Return (ds)
    End Function
  Public Function GetOpenProjects() As DataSet
    Dim myConn As Data.MySqlClient.MySqlConnection = GetMySqlConnector()
    Dim strSql As String = "select *,Customer_name from projects "
    strSql += "inner join Customers on projects.Customer_id = customers.Customer_Id where "
    strSql += "(Project_phase_id = 1 or Project_phase_id = 2 or Project_phase_id = 5 or Project_phase_id = 6 or Project_phase_id = 8) "
    strSql += "order by customers.Customer_name, Date_added"

    Dim da As New MySql.Data.MySqlClient.MySqlDataAdapter(strSql, myConn)
    Dim ds As New DataSet
    da.Fill(ds, "projects")
    Return (ds)
  End Function
  'Public Function GetOpenProjectsList() As ProjectList
  '  Dim ds As DataSet = GetOpenProjects()
  '  Dim theList As New ProjectList
  '  Dim counter As Integer = 0
  '  While counter < ds.Tables(0).Rows.Count
  '    Dim tmpProj As New Project
  '    tmpProj.Id = CheckForNull(ds.Tables(0).Rows(counter).Item("Project_id"), "Integer")
  '    tmpProj.Customer = CheckForNull(ds.Tables(0).Rows(counter).Item("Customer_name"), "String")
  '    tmpProj.Title = CheckForNull(ds.Tables(0).Rows(counter).Item("Project_name"), "String")
  '    tmpProj.StartDate = CheckForNull(ds.Tables(0).Rows(counter).Item("Start_date"), "Date")
  '    theList.Add(tmpProj)
  '    counter += 1
  '  End While
  '  Return theList
  'End Function
    'Public Function GetOpenProjectsList() As ProjectList
    '  Dim ds As DataSet = GetOpenProjects()
    '  Dim theList As New ProjectList
    '  Dim counter As Integer = 0
    '  While counter < ds.Tables(0).Rows.Count
    '    Dim tmpProj As New Project(CheckForNull(ds.Tables(0).Rows(counter).Item("Project_id")))
    '    'tmpProj.Id = CheckForNull(ds.Tables(0).Rows(counter).Item("Project_id"), "Integer")
    '    'tmpProj.Customer = CheckForNull(ds.Tables(0).Rows(counter).Item("Customer_name"), "String")
    '    'tmpProj.Title = CheckForNull(ds.Tables(0).Rows(counter).Item("Project_name"), "String")
    '    'tmpProj.StartDate = CheckForNull(ds.Tables(0).Rows(counter).Item("Start_date"), "Date")
    '    theList.Add(tmpProj)
    '    counter += 1
    '  End While
    '  Return theList
    'End Function
    'Public Function GetAllOpenProjectsList(ByVal pmUserId As Integer, ByVal salesOrScopedId As Integer) As ProjectList
    '  Dim theList As New ProjectList
    '  Dim ds As DataSet = GetOpenProjects()
    '  Dim counter As Integer = 0

    '  ' Both Pm and Sales/Scoped are specified
    '  If pmUserId > 0 And salesOrScopedId > 0 Then
    '    While counter < ds.Tables(0).Rows.Count
    '      Dim tmpProj As New Project(CheckForNull(ds.Tables(0).Rows(counter).Item("Project_id")))
    '      If tmpProj.PmOwnerId = pmUserId Then
    '        If Not tmpProj.Sow Is Nothing Then
    '          If tmpProj.Sow.SalesRepId = salesOrScopedId Or tmpProj.Sow.LeadTechId = salesOrScopedId Then
    '            theList.Add(tmpProj)
    '          End If
    '        End If
    '      End If
    '      counter += 1
    '    End While
    '  Else
    '    If pmUserId > 0 Then
    '      While counter < ds.Tables(0).Rows.Count
    '        Dim tmpProj As New Project(CheckForNull(ds.Tables(0).Rows(counter).Item("Project_id")))
    '        If tmpProj.PmOwnerId = pmUserId Then
    '          theList.Add(tmpProj)
    '        End If
    '        counter += 1
    '      End While
    '    Else
    '      If salesOrScopedId > 0 Then
    '        While counter < ds.Tables(0).Rows.Count
    '          Dim tmpProj As New Project(CheckForNull(ds.Tables(0).Rows(counter).Item("Project_id")))
    '          If Not tmpProj.Sow Is Nothing Then
    '            If tmpProj.Sow.SalesRepId = salesOrScopedId Or tmpProj.Sow.LeadTechId = salesOrScopedId Then
    '              theList.Add(tmpProj)
    '            End If
    '          End If
    '          counter += 1
    '        End While
    '      Else
    '        While counter < ds.Tables(0).Rows.Count
    '          Dim tmpProj As New Project(CheckForNull(ds.Tables(0).Rows(counter).Item("Project_id")))
    '          theList.Add(tmpProj)
    '          counter += 1
    '        End While
    '      End If
    '    End If
    '  End If
    '  Return theList
    'End Function
  
  Public Function GetMySqlConnector() As MySql.Data.MySqlClient.MySqlConnection
    'Return New Data.MySqlClient.MySqlConnection("server=ost-proj; user id=ost_qry; password=0stQry1; database=ost_svcmgr; pooling=false;")
        'Return New Data.MySqlClient.MySqlConnection("server=ost-mysql1; user id=ost_qry; password=0stQry1; database=ost_svcmgr; pooling=false;")
        Return New Data.MySqlClient.MySqlConnection("server=127.0.0.1; user id=ost_svcmgr; password=ost_svcmgr; database=ost_svcmgr; pooling=false;")
    End Function
    Public Function GetChangepointSqlConnector() As SqlClient.SqlConnection
        'Return New Data.MySqlClient.MySqlConnection("server=ost-proj; user id=ost_qry; password=0stQry1; database=ost_svcmgr; pooling=false;")
        ' Test...
        Return New SqlClient.SqlConnection("server=192.168.129.134; user id=osttestuser; password=Pn4chpt4; database=Changepoint; pooling=false;")
        ' Prod
        'Return New SqlClient.SqlConnection("server=192.168.128.130; user id=ostproduser; password=Cp3npTSy; database=Changepoint; pooling=false;")
    End Function
#End Region
#Region "Users"
    Public Enum UserListType
        All = 1
        Tech = 2
        Sales = 3
        ProjectManager = 4
    End Enum
    Public Function GetEnabledUsers(Optional ByRef filterValue As UserListType = UserListType.All) As DataSet
        Dim myConn As Data.MySqlClient.MySqlConnection = GetMySqlConnector()
        Dim yesString As String = "Y"
        Dim strSql As String = ""
        strSql += "select users.user_id, ua.username as user_name, concat(ua.lname,', ',ua.fname) as list_name, concat(ua.fname,' ',ua.lname) as full_name, ua.fname, ua.lname, "
        strSql += "ua.email1 as email_addr,users.enabled,users.admin_flag,users.tech_flag,users.sales_flag "
        strSql += "from(users) left join ost_security.user_accounts ua on users.user_id = ua.user_id "
        strSql += "where users.enabled = 'Y' order by ua.lname, ua.fname"

        Select Case filterValue
            Case UserListType.Sales
                strSql += " and users.tech_flag = 'Y'"
            Case UserListType.Tech
                strSql += " and users.sales_flag = 'Y'"
            Case UserListType.ProjectManager
                'strSql += " and users.pm_flag = 'Y'"
                strSql += " and users.tech_flag = 'Y'"
        End Select

        Dim da As New MySql.Data.MySqlClient.MySqlDataAdapter(strSql, myConn)
        Dim ds As New DataSet
        da.Fill(ds, "users")
        Return (ds)
    End Function
    Public Function GetEnabledUsersList(Optional ByRef filterValue As UserListType = UserListType.All) As PmoUserList
        Dim ds As DataSet = GetEnabledUsers(filterValue)
        Dim theList As New PmoUserList
        Dim counter As Integer = 0
        While counter < ds.Tables(0).Rows.Count
            Dim tmpUser As New PmoUser(ds.Tables(0).Rows(counter).Item("user_id"))
            theList.Add(tmpUser)
        End While
        Return theList
    End Function
    Public Function GetPmoUser(ByVal userId As Integer) As DataSet
        Dim myConn As Data.MySqlClient.MySqlConnection = GetMySqlConnector()
        Dim strSql As String = ""
        strSql += "select users.user_id, ua.username as user_name, ua.fname, ua.lname, "
        strSql += "ua.email1 as email_addr,users.enabled,users.admin_flag,users.tech_flag,users.sales_flag "
        strSql += "from(users) left join ost_security.user_accounts ua on users.user_id = ua.user_id "
        strSql += "where users.user_id = " & userId

        Dim da As New MySql.Data.MySqlClient.MySqlDataAdapter(strSql, myConn)
        Dim ds As New DataSet
        da.Fill(ds, "users")
        Return (ds)
    End Function
    Public Function GetPmoUser(ByVal userName As String) As DataSet
        Dim myConn As Data.MySqlClient.MySqlConnection = GetMySqlConnector()
        Dim strSql As String = ""
        strSql += "select users.user_id, ua.username as user_name, ua.fname, ua.lname, "
        strSql += "ua.email1 as email_addr,users.enabled,users.admin_flag,users.tech_flag,users.sales_flag "
        strSql += "from(users) left join ost_security.user_accounts ua on users.user_id = ua.user_id "
        strSql += "where ua.username = '" & userName & "'"

        Dim da As New MySql.Data.MySqlClient.MySqlDataAdapter(strSql, myConn)
        Dim ds As New DataSet
        da.Fill(ds, "users")
        Return (ds)
    End Function
    Public Function GetManagingUsersList() As PmoUserList
        Dim ds As DataSet
        Dim dbParms As New DbParmList
        Dim dbAccessor As New DbAccess
        Dim dbXfer As New DbXfer
        dbXfer.DatabaseMgr = Me.mDbMgr
        dbXfer.DatabaseName = "pmo"
        dbXfer.ParmList = dbParms

        Try
            ds = dbAccessor.GetRecord(dbXfer, "spGetManagingUsers")

            Dim theList As New PmoUserList
            Dim counter As Integer = 0
            While counter < ds.Tables(0).Rows.Count
                If ds.Tables(0).Rows(counter).Item("pmAssignedId") > 0 Then
                    Dim tmpUser As New PmoUser(ds.Tables(0).Rows(counter).Item("pmAssignedId"))
                    theList.Add(tmpUser)
                End If
                counter += 1
            End While

            SortAnyList(theList, "ListName", Direction.Ascending)

            Return theList

        Catch exc As Exception
            Dim nucEx As New PmoException(exc.Message)
            nucEx.ExceptionFunction = "GetManagingUserList"
            nucEx.ExceptionClass = ClassName
            nucEx.UserExplanation = "An exception was thrown while attempting to get the managing users List "
            nucEx.ExceptionStackTrace = exc.StackTrace.ToString
            nucEx.Persist()
            Throw nucEx
        End Try

    End Function
    Public Function UserHasManagedProjects(ByVal userId As Integer) As Boolean
        Dim manages As Boolean = False
        Dim theList As PmoUserList = GetManagingUsersList()
        For Each user As PmoUser In theList
            If user.UserId = userId Then
                manages = True
                Exit For
            End If
        Next
        Return manages
    End Function
    Public Function GetSoldOrScopedByUsersList() As PmoUserList
        Dim ds As DataSet
        Dim dbParms As New DbParmList
        Dim dbAccessor As New DbAccess
        Dim dbXfer As New DbXfer
        dbXfer.DatabaseMgr = Me.mDbMgr
        dbXfer.DatabaseName = "pmo"
        dbXfer.ParmList = dbParms

        Try
            ds = dbAccessor.GetRecord(dbXfer, "spGetSoldOrScopedByUsers")

            Dim theList As New PmoUserList
            Dim counter As Integer = 0
            While counter < ds.Tables(0).Rows.Count
                If ds.Tables(0).Rows(counter).Item("userId") > 0 Then
                    Dim tmpUser As New PmoUser(ds.Tables(0).Rows(counter).Item("userId"))
                    theList.Add(tmpUser, True)
                End If
                counter += 1
            End While

            SortAnyList(theList, "ListName", Direction.Ascending)

            Return theList

        Catch exc As Exception
            Dim nucEx As New PmoException(exc.Message)
            nucEx.ExceptionFunction = "SoldOrScopedByUsersList"
            nucEx.ExceptionClass = ClassName
            nucEx.UserExplanation = "An exception was thrown while attempting to get the SoldOrScopedByUsersList users List "
            nucEx.ExceptionStackTrace = exc.StackTrace.ToString
            nucEx.Persist()
            Throw nucEx
        End Try

    End Function
#End Region
#Region "PMO Documents"
    'Public Function GetPmoDocument(ByVal recordKeyIn As Integer, Optional ByVal dataSetTablename As String = "") As DataSet
    '  Dim dbParms As New DbParmList
    '  Dim dbAccessor As New DbAccess
    '  Dim dbXfer As New DbXfer
    '  dbParms.Add(New DbParm("@recordKey", recordKeyIn))
    '  dbXfer.DatabaseMgr = Me.mDbMgr
    '  dbXfer.DatabaseName = "pmo"
    '  dbXfer.ParmList = dbParms

    '  If dataSetTablename = "" Then
    '    dataSetTablename = "tblPmoDocument"
    '  End If

    '  Try
    '    Return dbAccessor.GetRecord(dbXfer, "spSelectPmoDocument", dataSetTablename)

    '  Catch exc As Exception
    '    Dim nucEx As New PmoException(exc.Message)
    '    nucEx.ExceptionFunction = "GetPmoDocument"
    '    nucEx.ExceptionClass = ClassName
    '    nucEx.UserExplanation = "An exception was thrown while attempting to get the PMO Document Record for key: " & recordKeyIn
    '    nucEx.ExceptionStackTrace = exc.StackTrace.ToString
    '    nucEx.Persist()
    '    Throw nucEx
    '  End Try
    'End Function
    'Public Function GetAllPmoDocuments(Optional ByVal dataSetTablename As String = "") As DataSet
    '  Dim dbParms As New DbParmList
    '  Dim dbAccessor As New DbAccess
    '  Dim dbXfer As New DbXfer
    '  dbXfer.DatabaseMgr = Me.mDbMgr
    '  dbXfer.DatabaseName = "pmo"
    '  dbXfer.ParmList = dbParms

    '  If dataSetTablename = "" Then
    '    dataSetTablename = "tblPmoDocument"
    '  End If

    '  Try
    '    Return dbAccessor.GetRecord(dbXfer, "spGetAllPmoDOcument", dataSetTablename)

    '  Catch exc As Exception
    '    Dim nucEx As New PmoException(exc.Message)
    '    nucEx.ExceptionFunction = "GetAllPmoDocuments"
    '    nucEx.ExceptionClass = ClassName
    '    nucEx.UserExplanation = "An exception was thrown while attempting to get all PMO Documents"
    '    nucEx.ExceptionStackTrace = exc.StackTrace.ToString
    '    nucEx.Persist()
    '    Throw nucEx
    '  End Try
    'End Function
    'Public Function GetAllPmoDocumentsList(Optional ByVal dataSetTablename As String = "") As PmoDocumentList
    '  Dim theList As New PmoDocumentList
    '  Dim dbParms As New DbParmList
    '  Dim dbAccessor As New DbAccess
    '  Dim dbXfer As New DbXfer
    '  Dim ds As DataSet
    '  dbXfer.DatabaseMgr = Me.mDbMgr
    '  dbXfer.DatabaseName = "pmo"
    '  dbXfer.ParmList = dbParms

    '  If dataSetTablename = "" Then
    '    dataSetTablename = "tblPmoDocument"
    '  End If

    '  Try
    '    ds = dbAccessor.GetRecord(dbXfer, "spGetAllPmoDocument", dataSetTablename)

    '    Dim counter As Integer = 0
    '    While counter < ds.Tables(0).Rows.Count
    '      Dim tmpFile As New PmoDocument(ds.Tables(0).Rows(counter).Item("recordKey"))
    '      theList.Add(tmpFile)
    '      counter += 1
    '    End While

    '    Return theList

    '  Catch exc As Exception
    '    Dim nucEx As New PmoException(exc.Message)
    '    nucEx.ExceptionFunction = "GetPmoDocumentList"
    '    nucEx.ExceptionClass = ClassName
    '    nucEx.UserExplanation = "An exception was thrown while attempting to get a list of all PMO Document records"
    '    nucEx.ExceptionStackTrace = exc.StackTrace.ToString
    '    nucEx.Persist()
    '    Throw nucEx
    '  End Try
    'End Function
#End Region
#Region "Customer Feedback"
    Public Function GetCompletedProjectList(Optional ByVal dataSetTablename As String = "") As CompletedProjectList
        Dim theList As New CompletedProjectList
        Dim dbParms As New DbParmList
        Dim dbAccessor As New DbAccess
        Dim dbXfer As New DbXfer
        dbXfer.DatabaseMgr = Me.mDbMgr
        dbXfer.DatabaseName = "pmo"
        dbXfer.ParmList = dbParms

        If dataSetTablename = "" Then
            dataSetTablename = "tblProjectCompleted"
        End If

        Try
            Dim ds As DataSet = dbAccessor.GetRecord(dbXfer, "spGetAllCompletedProject", dataSetTablename)

            Dim counter As Integer = 0
            While counter < ds.Tables(0).Rows.Count
                'Dim proj As New OldProject(ds.Tables(0).Rows(counter).Item("projectId"))
                Dim compProj As New CompletedProject
                compProj.CompletedDate = CheckForNull(ds.Tables(0).Rows(counter).Item("completedDate"), "Date")
                compProj.ProjectId = ds.Tables(0).Rows(counter).Item("projectId")
                'compProj.ProjectDescription = proj.Title
                'compProj.CustomerName = proj.Customer
                'compProj.ProjectManager = proj.ManagedBy
                theList.Add(compProj)
                counter += 1
            End While

            Return theList

        Catch exc As Exception
            Dim nucEx As New PmoException(exc.Message)
            nucEx.ExceptionFunction = "GetConpletedProjectList"
            nucEx.ExceptionClass = ClassName
            nucEx.UserExplanation = "An exception was thrown while attempting to get the Completed Project List "
            nucEx.ExceptionStackTrace = exc.StackTrace.ToString
            nucEx.Persist()
            Throw nucEx
        End Try
    End Function
    Public Function GetCompletedChangepointProjectList(Optional ByVal dataSetTablename As String = "") As CpCompletedProjectList
        Dim theList As New CpCompletedProjectList
        Dim dbParms As New DbParmList
        Dim dbAccessor As New DbAccess
        Dim dbXfer As New DbXfer
        dbXfer.DatabaseMgr = Me.mDbMgr
        dbXfer.DatabaseName = "pmo"
        dbXfer.ParmList = dbParms

        If dataSetTablename = "" Then
            dataSetTablename = "tblProjectCompleted"
        End If

        Try
            Dim ds As DataSet = dbAccessor.GetRecord(dbXfer, "spGetAllCompletedCpProject", dataSetTablename)

            Dim counter As Integer = 0
            While counter < ds.Tables(0).Rows.Count
                Dim proj As New CpProject(ds.Tables(0).Rows(counter).Item("projectId"))
                Dim compProj As New CpCompletedProject
                compProj.CompletedDate = CheckForNull(ds.Tables(0).Rows(counter).Item("completedDate"), "Date")
                compProj.ProjectId = proj.Id
                compProj.ProjectDescription = proj.Title
                compProj.CustomerName = proj.Customer
                compProj.PrimaryAccountExec = CheckForNull(ds.Tables(0).Rows(counter).Item("primaryAccountExec"), "String")
                compProj.PrimaryAccountExecEmail = CheckForNull(ds.Tables(0).Rows(counter).Item("primaryAccountExecEmail"), "string")
                theList.Add(compProj)
                counter += 1
            End While

            Return theList

        Catch exc As Exception
            Dim nucEx As New PmoException(exc.Message)
            nucEx.ExceptionFunction = "GetConpletedProjectList"
            nucEx.ExceptionClass = ClassName
            nucEx.UserExplanation = "An exception was thrown while attempting to get the Completed Project List "
            nucEx.ExceptionStackTrace = exc.StackTrace.ToString
            nucEx.Persist()
            Throw nucEx
        End Try
    End Function
    Public Function GetCompletedProjectSalesName(projId As Guid) As String
        Dim theEmail As String = ""
        Dim dbParms As New DbParmList
        Dim dbAccessor As New DbAccess
        Dim dbXfer As New DbXfer
        dbXfer.DatabaseMgr = Me.mDbMgr
        dbXfer.DatabaseName = "pmo"
        dbXfer.ParmList = dbParms

        Try
            Dim ds As DataSet = dbAccessor.GetRecord(dbXfer, "spSelectCpCompletedProject", "tblCpProjectCompleted")
            If ds.Tables(0).Rows.Count > 0 Then
                theEmail = CheckForNull(ds.Tables(0).Rows(0).Item("primaryAccountExec"), "String")
            End If
            Return theEmail

        Catch exc As Exception
            Dim nucEx As New PmoException(exc.Message)
            nucEx.ExceptionFunction = "GetCompletedProjectSalesEmail"
            nucEx.ExceptionClass = ClassName
            nucEx.UserExplanation = "An exception was thrown while attempting to get the Completed Project Sales Email "
            nucEx.ExceptionStackTrace = exc.StackTrace.ToString
            nucEx.Persist()
            Throw nucEx
        End Try
    End Function
    Public Function GetCompletedProjectSalesEmail(projId As Guid) As String
        Dim theEmail As String = ""
        Dim dbParms As New DbParmList
        Dim dbAccessor As New DbAccess
        Dim dbXfer As New DbXfer
        dbParms.Add(New DbParm("@projectId", projId))
        dbXfer.DatabaseMgr = Me.mDbMgr
        dbXfer.DatabaseName = "pmo"
        dbXfer.ParmList = dbParms

        Try
            Dim ds As DataSet = dbAccessor.GetRecord(dbXfer, "spSelectCpCompletedProject", "tblCpProjectCompleted")
            If ds.Tables(0).Rows.Count > 0 Then
                theEmail = CheckForNull(ds.Tables(0).Rows(0).Item("primaryAccountExecEmail"), "String")
            End If
            Return theEmail

        Catch exc As Exception
            Dim nucEx As New PmoException(exc.Message)
            nucEx.ExceptionFunction = "GetCompletedProjectSalesEmail"
            nucEx.ExceptionClass = ClassName
            nucEx.UserExplanation = "An exception was thrown while attempting to get the Completed Project Sales Email "
            nucEx.ExceptionStackTrace = exc.StackTrace.ToString
            nucEx.Persist()
            Throw nucEx
        End Try
    End Function
    Public Sub DeleteCompletedCpProject(ByVal projectId As Guid)
        Dim dbParms As New DbParmList
        Dim dbAccessor As New DbAccess
        Dim dbXfer As New DbXfer
        dbXfer.DatabaseMgr = Me.mDbMgr
        dbXfer.DatabaseName = "pmo"
        dbXfer.ParmList = dbParms

        dbParms.Add(New DbParm("@projectId", projectId))

        Try
            dbAccessor.ExecuteProc(dbXfer, "spDeleteCompletedCpProject")

        Catch exc As Exception
            Dim nucEx As New PmoException(exc.Message)
            nucEx.ExceptionFunction = "GetSowList"
            nucEx.ExceptionClass = ClassName
            nucEx.UserExplanation = "An exception was thrown while attempting to Delete a Completed Project: " & projectId.ToString
            nucEx.ExceptionStackTrace = exc.StackTrace.ToString
            nucEx.Persist()
            Throw nucEx
        End Try
    End Sub
    Public Sub DeleteCompletedProject(ByVal projectId As Integer)
        Dim dbParms As New DbParmList
        Dim dbAccessor As New DbAccess
        Dim dbXfer As New DbXfer
        dbXfer.DatabaseMgr = Me.mDbMgr
        dbXfer.DatabaseName = "pmo"
        dbXfer.ParmList = dbParms

        dbParms.Add(New DbParm("@projectId", projectId))

        Try
            dbAccessor.ExecuteProc(dbXfer, "spDeleteCompletedProject")

        Catch exc As Exception
            Dim nucEx As New PmoException(exc.Message)
            nucEx.ExceptionFunction = "GetSowList"
            nucEx.ExceptionClass = ClassName
            nucEx.UserExplanation = "An exception was thrown while attempting to Delete a Completed Project: " & projectId
            nucEx.ExceptionStackTrace = exc.StackTrace.ToString
            nucEx.Persist()
            Throw nucEx
        End Try
    End Sub
#End Region
#Region "Survey"
  Public Function GetSurveyRequest(ByVal requestId As Integer, Optional ByVal dataSetTablename As String = "") As DataSet
    Dim dbParms As New DbParmList
    Dim dbAccessor As New DbAccess
    Dim dbXfer As New DbXfer
    dbParms.Add(New DbParm("@requestId", requestId))
    dbXfer.DatabaseMgr = Me.mDbMgr
    dbXfer.DatabaseName = "pmo"
    dbXfer.ParmList = dbParms

    If dataSetTablename = "" Then
      dataSetTablename = "tblSow"
    End If

    Try
      Return dbAccessor.GetRecord(dbXfer, "spSelectSurveyRequest", dataSetTablename)

    Catch exc As Exception
      Dim nucEx As New PmoException(exc.Message)
      nucEx.ExceptionFunction = "GetSurveyRequest"
      nucEx.ExceptionClass = ClassName
      nucEx.UserExplanation = "An exception was thrown while attempting to get Survey Request for ID: " & requestId
      nucEx.ExceptionStackTrace = exc.StackTrace.ToString
      nucEx.Persist()
      Throw nucEx
    End Try
    End Function
    Public Function GetCpSurveyRequest(ByVal requestId As Integer, Optional ByVal dataSetTablename As String = "") As DataSet
        Dim dbParms As New DbParmList
        Dim dbAccessor As New DbAccess
        Dim dbXfer As New DbXfer
        dbParms.Add(New DbParm("@requestId", requestId))
        dbXfer.DatabaseMgr = Me.mDbMgr
        dbXfer.DatabaseName = "pmo"
        dbXfer.ParmList = dbParms

        If dataSetTablename = "" Then
            dataSetTablename = "tblCpSurveyRequest"
        End If

        Try
            Return dbAccessor.GetRecord(dbXfer, "spSelectCpSurveyRequest", dataSetTablename)

        Catch exc As Exception
            Dim nucEx As New PmoException(exc.Message)
            nucEx.ExceptionFunction = "GetCpSurveyRequest"
            nucEx.ExceptionClass = ClassName
            nucEx.UserExplanation = "An exception was thrown while attempting to get Survey Request for ID: " & requestId
            nucEx.ExceptionStackTrace = exc.StackTrace.ToString
            nucEx.Persist()
            Throw nucEx
        End Try
    End Function
  Public Function GetAllSurveyRequests(ByVal email As String, Optional ByVal dataSetTablename As String = "") As DataSet
    Dim dbParms As New DbParmList
    Dim dbAccessor As New DbAccess
    Dim dbXfer As New DbXfer
    dbParms.Add(New DbParm("@emailAddress", email))
    dbXfer.DatabaseMgr = Me.mDbMgr
    dbXfer.DatabaseName = "pmo"
    dbXfer.ParmList = dbParms

    If dataSetTablename = "" Then
      dataSetTablename = "tblSow"
    End If

    Try
      Return dbAccessor.GetRecord(dbXfer, "spSelectSurveyRequestByEmail", dataSetTablename)

    Catch exc As Exception
      Dim nucEx As New PmoException(exc.Message)
      nucEx.ExceptionFunction = "GetSurveyRequest"
      nucEx.ExceptionClass = ClassName
      nucEx.UserExplanation = "An exception was thrown while attempting to get Survey Request for Email: " & email
      nucEx.ExceptionStackTrace = exc.StackTrace.ToString
      nucEx.Persist()
      Throw nucEx
    End Try
    End Function
    Public Function GetAllSurveyRequests(Optional ByVal dataSetTablename As String = "") As DataSet
        Dim dbParms As New DbParmList
        Dim dbAccessor As New DbAccess
        Dim dbXfer As New DbXfer
        dbXfer.DatabaseMgr = Me.mDbMgr
        dbXfer.DatabaseName = "pmo"
        dbXfer.ParmList = dbParms

        If dataSetTablename = "" Then
            dataSetTablename = "tblSow"
        End If

        Try
            Return dbAccessor.GetRecord(dbXfer, "spGetALlSurveyRequest", dataSetTablename)

        Catch exc As Exception
            Dim nucEx As New PmoException(exc.Message)
            nucEx.ExceptionFunction = "GetSurveyRequest"
            nucEx.ExceptionClass = ClassName
            nucEx.UserExplanation = "An exception was thrown while attempting to get All Survey Requests"
            nucEx.ExceptionStackTrace = exc.StackTrace.ToString
            nucEx.Persist()
            Throw nucEx
        End Try
    End Function
    Public Function GetAllCpSurveyRequests(ByVal email As String, Optional ByVal dataSetTablename As String = "") As DataSet
        Dim dbParms As New DbParmList
        Dim dbAccessor As New DbAccess
        Dim dbXfer As New DbXfer
        dbParms.Add(New DbParm("@emailAddress", email))
        dbXfer.DatabaseMgr = Me.mDbMgr
        dbXfer.DatabaseName = "pmo"
        dbXfer.ParmList = dbParms

        If dataSetTablename = "" Then
            dataSetTablename = "tblSow"
        End If

        Try
            Return dbAccessor.GetRecord(dbXfer, "spSelectCpSurveyRequestByEmail", dataSetTablename)

        Catch exc As Exception
            Dim nucEx As New PmoException(exc.Message)
            nucEx.ExceptionFunction = "GetCpSurveyRequests"
            nucEx.ExceptionClass = ClassName
            nucEx.UserExplanation = "An exception was thrown while attempting to get Survey Request for Email: " & email
            nucEx.ExceptionStackTrace = exc.StackTrace.ToString
            nucEx.Persist()
            Throw nucEx
        End Try
    End Function
  Public Function GetAllOpenSurveyRequests(Optional ByVal dataSetTablename As String = "") As DataSet
    Dim dbParms As New DbParmList
    Dim dbAccessor As New DbAccess
    Dim dbXfer As New DbXfer
    dbXfer.DatabaseMgr = Me.mDbMgr
    dbXfer.DatabaseName = "pmo"
    dbXfer.ParmList = dbParms

    If dataSetTablename = "" Then
      dataSetTablename = "tblSow"
    End If

    Try
      Return dbAccessor.GetRecord(dbXfer, "spGetAllOpenSurveyRequest", dataSetTablename)

    Catch exc As Exception
      Dim nucEx As New PmoException(exc.Message)
      nucEx.ExceptionFunction = "GetAllSurveyRequests"
      nucEx.ExceptionClass = ClassName
      nucEx.UserExplanation = "An exception was thrown while attempting to get All Open Survey Requests"
      nucEx.ExceptionStackTrace = exc.StackTrace.ToString
      nucEx.Persist()
      Throw nucEx
    End Try
    End Function
    Public Function GetAllOpenCpSurveyRequests(Optional ByVal dataSetTablename As String = "") As DataSet
        Dim dbParms As New DbParmList
        Dim dbAccessor As New DbAccess
        Dim dbXfer As New DbXfer
        dbXfer.DatabaseMgr = Me.mDbMgr
        dbXfer.DatabaseName = "pmo"
        dbXfer.ParmList = dbParms

        If dataSetTablename = "" Then
            dataSetTablename = "tblCpSurveyRequest"
        End If

        Try
            Return dbAccessor.GetRecord(dbXfer, "spGetAllOpenCpSurveyRequest", dataSetTablename)

        Catch exc As Exception
            Dim nucEx As New PmoException(exc.Message)
            nucEx.ExceptionFunction = "GetAllOpenCpSurveyRequests"
            nucEx.ExceptionClass = ClassName
            nucEx.UserExplanation = "An exception was thrown while attempting to get All Open CP Survey Requests"
            nucEx.ExceptionStackTrace = exc.StackTrace.ToString
            nucEx.Persist()
            Throw nucEx
        End Try
    End Function
    Public Function GetAllSurveyRequestList(Optional ByVal dataSetTablename As String = "") As OldSurveyRequestList
        Dim theList As New OldSurveyRequestList
        Dim ds As DataSet = GetAllSurveyRequests()

        Dim counter As Integer = 0
        While counter < ds.Tables(0).Rows.Count
            Dim tmpReq As New OldSurveyRequest(ds.Tables(0).Rows(counter).Item("requestId"))
            theList.Add(tmpReq)
            counter += 1
        End While

        Return theList

    End Function
    Public Function GetAllCpSurveyRequestList(ByVal email As String, Optional ByVal dataSetTablename As String = "") As CpSurveyRequestList
        Dim theList As New CpSurveyRequestList
        Dim ds As DataSet = GetAllCpSurveyRequests(email)

        Dim counter As Integer = 0
        While counter < ds.Tables(0).Rows.Count
            Dim tmpReq As New CpSurveyRequest(ds.Tables(0).Rows(counter).Item("requestId"))
            theList.Add(tmpReq)
            counter += 1
        End While

        Return theList

    End Function
    'Public Function GetAllOpenSurveyRequestList(Optional ByVal dataSetTablename As String = "") As SurveyRequestList
    '  Dim theList As New SurveyRequestList
    '      Dim ds As DataSet = GetAllOpenSurveyRequests()

    '  Dim counter As Integer = 0
    '  While counter < ds.Tables(0).Rows.Count
    '          Dim tmpReq As New SurveyRequest(ds.Tables(0).Rows(counter).Item("requestId"))
    '    theList.Add(tmpReq)
    '    counter += 1
    '  End While

    '  Return theList

    '  End Function
    Public Function GetAllOpenCpSurveyRequestList(Optional ByVal dataSetTablename As String = "") As CpSurveyRequestList
        Dim theList As New CpSurveyRequestList
        Dim ds As DataSet = GetAllOpenCpSurveyRequests()

        Dim counter As Integer = 0
        While counter < ds.Tables(0).Rows.Count
            Dim tmpReq As New CpSurveyRequest(ds.Tables(0).Rows(counter).Item("requestId"))
            theList.Add(tmpReq)
            counter += 1
        End While

        Return theList

    End Function
  Public Function GetAllProjectPositions() As DataSet
    Dim dbParms As New DbParmList
    Dim dbAccessor As New DbAccess
    Dim dbXfer As New DbXfer
    dbXfer.DatabaseMgr = Me.mDbMgr
    dbXfer.DatabaseName = "pmo"
    dbXfer.ParmList = dbParms

    Try
      Return dbAccessor.GetRecord(dbXfer, "spGetAllProjectPositions", "")


    Catch exc As Exception
      Dim nucEx As New PmoException(exc.Message)
      nucEx.ExceptionFunction = "GetAllProjectPositions"
      nucEx.ExceptionClass = ClassName
      nucEx.UserExplanation = "An exception was thrown while attempting to get all Project Positions "
      nucEx.ExceptionStackTrace = exc.StackTrace.ToString
      nucEx.Persist()
      Throw nucEx
    End Try
  End Function
  Public Function GetProjectPosition(ByVal positionId As Integer) As DataSet
    Dim dbParms As New DbParmList
    Dim dbAccessor As New DbAccess
    Dim dbXfer As New DbXfer
    dbParms.Add(New DbParm("@projectPositionId", positionId))
    dbXfer.DatabaseMgr = Me.mDbMgr
    dbXfer.DatabaseName = "pmo"
    dbXfer.ParmList = dbParms

    Try
      Return dbAccessor.GetRecord(dbXfer, "spSelectProjectPosition", "")


    Catch exc As Exception
      Dim nucEx As New PmoException(exc.Message)
      nucEx.ExceptionFunction = "GetAllProjectPositions"
      nucEx.ExceptionClass = ClassName
      nucEx.UserExplanation = "An exception was thrown while attempting to get all Project Positions "
      nucEx.ExceptionStackTrace = exc.StackTrace.ToString
      nucEx.Persist()
      Throw nucEx
    End Try
  End Function
  Public Function GetProjectPositionDescription(ByVal positionId As Integer) As String
    Dim ds As DataSet = GetProjectPosition(positionId)
    Dim pos As String = ""
    If ds.Tables(0).Rows.Count > 0 Then
      pos = ds.Tables(0).Rows(0).Item("projectPosition")
    Else
      pos = "Not Found"
    End If
    Return pos
  End Function
#End Region
#Region "Survey Questions"
  Public Function GetSurveyQuestion(ByVal questionId As Integer, Optional ByVal dataSetTablename As String = "") As DataSet
    Dim dbParms As New DbParmList
    Dim dbAccessor As New DbAccess
    Dim dbXfer As New DbXfer
    dbParms.Add(New DbParm("@questionId", questionId))
    dbXfer.DatabaseMgr = Me.mDbMgr
    dbXfer.DatabaseName = "pmo"
    dbXfer.ParmList = dbParms

    If dataSetTablename = "" Then
      dataSetTablename = "tblSow"
    End If

    Try
      Return dbAccessor.GetRecord(dbXfer, "spSelectSurveyQuestion", dataSetTablename)

    Catch exc As Exception
      Dim nucEx As New PmoException(exc.Message)
      nucEx.ExceptionFunction = "GetSurveyQuestion"
      nucEx.ExceptionClass = ClassName
      nucEx.UserExplanation = "An exception was thrown while attempting to get Survey Question for ID: " & questionId
      nucEx.ExceptionStackTrace = exc.StackTrace.ToString
      nucEx.Persist()
      Throw nucEx
    End Try
  End Function
  Public Function GetActiveQuestions(Optional ByVal dataSetTablename As String = "") As SurveyQuestionList
    Dim theList As New SurveyQuestionList
    Dim dbParms As New DbParmList
    Dim dbAccessor As New DbAccess
    Dim dbXfer As New DbXfer
    dbXfer.DatabaseMgr = Me.mDbMgr
    dbXfer.DatabaseName = "pmo"
    dbXfer.ParmList = dbParms

    If dataSetTablename = "" Then
      dataSetTablename = "tblSow"
    End If

    Try
      Dim ds As DataSet = dbAccessor.GetRecord(dbXfer, "spGetActiveSurveyQuestions", dataSetTablename)

      Dim counter As Integer = 0
      While counter < ds.Tables(0).Rows.Count
        Dim tmpQ As New SurveyQuestion(ds.Tables(0).Rows(counter).Item("questionId"))
        theList.Add(tmpQ)
        counter += 1
      End While

      Return theList

    Catch exc As Exception
      Dim nucEx As New PmoException(exc.Message)
      nucEx.ExceptionFunction = "GetSurveyQuestion"
      nucEx.ExceptionClass = ClassName
      nucEx.UserExplanation = "An exception was thrown while attempting to get the list of active Survey Questions"
      nucEx.ExceptionStackTrace = exc.StackTrace.ToString
      nucEx.Persist()
      Throw nucEx
    End Try
  End Function
#End Region
#Region "CP Survey Results"
    'The following are used to address the old projects from the Project Tracking system.  Do not remove!!! JDV
    Public Function SurveyCompletedForProject(ByVal projectId As Integer) As Boolean
        Dim completed As Boolean = False
        Dim dbParms As New DbParmList
        Dim dbAccessor As New DbAccess
        Dim dbXfer As New DbXfer
        dbParms.Add(New DbParm("@projectId", projectId))
        dbXfer.DatabaseMgr = Me.mDbMgr
        dbXfer.DatabaseName = "pmo"
        dbXfer.ParmList = dbParms

        Try
            Dim ds As DataSet = dbAccessor.GetRecord(dbXfer, "spSelectSurveyRequestForProject", "")

            If ds.Tables(0).Rows.Count > 0 Then
                Dim counter As Integer = 0
                While counter < ds.Tables(0).Rows.Count
                    Dim survResult As New CpSurveyResult(ds.Tables(0).Rows(counter).Item("requestId"))
                    If survResult.OldProjectId > 0 Then
                        completed = True
                        Exit While
                    End If
                    counter += 1
                End While
            End If

            Return completed

        Catch exc As Exception
            Dim nucEx As New PmoException(exc.Message)
            nucEx.ExceptionFunction = "SurveyCompletedForProject"
            nucEx.ExceptionClass = ClassName
            nucEx.UserExplanation = "An exception was thrown while attempting to see if a Survey had been completed for Project ID: " & projectId
            nucEx.ExceptionStackTrace = exc.StackTrace.ToString
            nucEx.Persist()
            Throw nucEx
        End Try
    End Function
    Public Function SurveyCompletedCount(ByVal projectId As Integer) As Integer
        Dim dbParms As New DbParmList
        Dim dbAccessor As New DbAccess
        Dim dbXfer As New DbXfer
        dbParms.Add(New DbParm("@projectId", projectId))
        dbXfer.DatabaseMgr = Me.mDbMgr
        dbXfer.DatabaseName = "pmo"
        dbXfer.ParmList = dbParms

        Try
            Dim ds As DataSet = dbAccessor.GetRecord(dbXfer, "spSelectSurveyRequestForProject", "")

            Dim completeCount As Integer = 0
            If ds.Tables(0).Rows.Count > 0 Then
                Dim counter As Integer = 0
                While counter < ds.Tables(0).Rows.Count
                    Dim survResult As New CpSurveyResult(ds.Tables(0).Rows(counter).Item("requestId"))
                    If survResult.OldProjectId > 0 Then
                        completeCount += 1
                    End If
                    counter += 1
                End While
            End If

            Return completeCount

        Catch exc As Exception
            Dim nucEx As New PmoException(exc.Message)
            nucEx.ExceptionFunction = "SurveyCompletedCount"
            nucEx.ExceptionClass = ClassName
            nucEx.UserExplanation = "An exception was thrown while attempting to see how many Surveys have been completed for Project ID: " & projectId
            nucEx.ExceptionStackTrace = exc.StackTrace.ToString
            nucEx.Persist()
            Throw nucEx
        End Try
    End Function
    Public Function SurveyCompletedForCpProject(ByVal projectId As Guid) As Boolean
        Dim completed As Boolean = False
        Dim dbParms As New DbParmList
        Dim dbAccessor As New DbAccess
        Dim dbXfer As New DbXfer
        dbParms.Add(New DbParm("@projectId", projectId))
        dbXfer.DatabaseMgr = Me.mDbMgr
        dbXfer.DatabaseName = "pmo"
        dbXfer.ParmList = dbParms

        Try
            Dim ds As DataSet = dbAccessor.GetRecord(dbXfer, "spSelectSurveyRequestForCpProject", "")

            If ds.Tables(0).Rows.Count > 0 Then
                Dim counter As Integer = 0
                While counter < ds.Tables(0).Rows.Count
                    Dim survResult As New CpSurveyResult(ds.Tables(0).Rows(counter).Item("requestId"))
                    If Not survResult Is Nothing Then
                        completed = True
                        Exit While
                    End If
                    counter += 1
                End While
            End If

            Return completed

        Catch exc As Exception
            Dim nucEx As New PmoException(exc.Message)
            nucEx.ExceptionFunction = "SurveyCompletedForProject"
            nucEx.ExceptionClass = ClassName
            nucEx.UserExplanation = "An exception was thrown while attempting to see if a Survey had been completed for Project ID: " & projectId.ToString
            nucEx.ExceptionStackTrace = exc.StackTrace.ToString
            nucEx.Persist()
            Throw nucEx
        End Try
    End Function
    Public Function CpSurveyCompletedCount(ByVal projectId As Guid) As Integer
        Dim dbParms As New DbParmList
        Dim dbAccessor As New DbAccess
        Dim dbXfer As New DbXfer
        dbParms.Add(New DbParm("@projectId", projectId))
        dbXfer.DatabaseMgr = Me.mDbMgr
        dbXfer.DatabaseName = "pmo"
        dbXfer.ParmList = dbParms

        Try
            Dim ds As DataSet = dbAccessor.GetRecord(dbXfer, "spSelectSurveyRequestForCpProject", "")

            Dim completeCount As Integer = 0
            If ds.Tables(0).Rows.Count > 0 Then
                Dim counter As Integer = 0
                While counter < ds.Tables(0).Rows.Count
                    Dim survResult As New CpSurveyResult(ds.Tables(0).Rows(counter).Item("requestId"))
                    If Not survResult Is Nothing Then
                        completeCount += 1
                    End If
                    counter += 1
                End While
            End If

            Return completeCount

        Catch exc As Exception
            Dim nucEx As New PmoException(exc.Message)
            nucEx.ExceptionFunction = "SurveyCompletedCount"
            nucEx.ExceptionClass = ClassName
            nucEx.UserExplanation = "An exception was thrown while attempting to see how many Surveys have been completed for Project ID: " & projectId.ToString
            nucEx.ExceptionStackTrace = exc.StackTrace.ToString
            nucEx.Persist()
            Throw nucEx
        End Try
    End Function
    Public Function GetSurveyResultIdForCpProject(ByVal projectId As Guid) As Integer
        Dim resultId As Integer = 0
        Dim dbParms As New DbParmList
        Dim dbAccessor As New DbAccess
        Dim dbXfer As New DbXfer
        dbParms.Add(New DbParm("@projectId", projectId))
        dbXfer.DatabaseMgr = Me.mDbMgr
        dbXfer.DatabaseName = "pmo"
        dbXfer.ParmList = dbParms

        Try
            Dim ds As DataSet = dbAccessor.GetRecord(dbXfer, "spSelectSurveyRequestForCpProject", "")

            If ds.Tables(0).Rows.Count > 0 Then
                resultId = ds.Tables(0).Rows(0).Item("requestId")
            End If

            Return resultId

        Catch exc As Exception
            Dim nucEx As New PmoException(exc.Message)
            nucEx.ExceptionFunction = "GetSurveyResultIdForProject"
            nucEx.ExceptionClass = ClassName
            nucEx.UserExplanation = "An exception was thrown while attempting to get the Survey result id for Project ID: " & projectId.ToString
            nucEx.ExceptionStackTrace = exc.StackTrace.ToString
            nucEx.Persist()
            Throw nucEx
        End Try
    End Function
#End Region
#Region "Survey Result"
    Public Function MarkCpProjectFeedbackComplete(ByVal projectId As Guid, ByVal userId As Integer, Optional oldProjid As Integer = 0) As Boolean
        Dim dbParms As New DbParmList
        Dim dbAccessor As New DbAccess
        Dim dbXfer As New DbXfer
        dbParms.Add(New DbParm("@projectId", projectId))
        dbParms.Add(New DbParm("@capturedById", userId))
        dbParms.Add(New DbParm("@capturedDate", Today.ToShortDateString))
        dbParms.Add(New DbParm("@oldProjectId", oldProjid))
        dbXfer.DatabaseMgr = Me.mDbMgr
        dbXfer.DatabaseName = "pmo"
        dbXfer.ParmList = dbParms

        Try
            Dim count As Integer = dbAccessor.ExecuteProc(dbXfer, "spInsertCpProjectFeedback")

            If count > 0 Then
                Return True
            Else
                Return False
            End If

        Catch exc As Exception
            Dim nucEx As New PmoException(exc.Message)
            nucEx.ExceptionFunction = "MarkCpProjectFeedbackComplete"
            nucEx.ExceptionClass = ClassName
            nucEx.UserExplanation = "An exception was thrown while attempting to mark Project Feedback as completed for Project: " & projectId.ToString
            nucEx.ExceptionStackTrace = exc.StackTrace.ToString
            nucEx.Persist()
            Throw nucEx
        End Try
    End Function
    Public Function MarkProjectFeedbackComplete(ByVal projectId As Integer, ByVal userId As Integer) As Boolean
        Dim dbParms As New DbParmList
        Dim dbAccessor As New DbAccess
        Dim dbXfer As New DbXfer
        dbParms.Add(New DbParm("@projectId", projectId))
        dbParms.Add(New DbParm("@capturedById", userId))
        dbParms.Add(New DbParm("@capturedDate", Today.ToShortDateString))
        dbXfer.DatabaseMgr = Me.mDbMgr
        dbXfer.DatabaseName = "pmo"
        dbXfer.ParmList = dbParms

        Try
            Dim count As Integer = dbAccessor.ExecuteProc(dbXfer, "spInsertProjectFeedback")

            If count > 0 Then
                Return True
            Else
                Return False
            End If

        Catch exc As Exception
            Dim nucEx As New PmoException(exc.Message)
            nucEx.ExceptionFunction = "MarkProjectFeedbackComplete"
            nucEx.ExceptionClass = ClassName
            nucEx.UserExplanation = "An exception was thrown while attempting to mark Project Feedback as completed for Project: " & projectId
            nucEx.ExceptionStackTrace = exc.StackTrace.ToString
            nucEx.Persist()
            Throw nucEx
        End Try
    End Function
    Public Function GetSurveyResult(ByVal requestId As Integer, Optional ByVal dataSetTablename As String = "") As DataSet
        Dim dbParms As New DbParmList
        Dim dbAccessor As New DbAccess
        Dim dbXfer As New DbXfer
        dbParms.Add(New DbParm("@requestId", requestId))
        dbXfer.DatabaseMgr = Me.mDbMgr
        dbXfer.DatabaseName = "pmo"
        dbXfer.ParmList = dbParms

        If dataSetTablename = "" Then
            dataSetTablename = "tblSurveyResult"
        End If

        Try
            Return dbAccessor.GetRecord(dbXfer, "spSelectSurveyResult", dataSetTablename)

        Catch exc As Exception
            Dim nucEx As New PmoException(exc.Message)
            nucEx.ExceptionFunction = "GetSurveyResult"
            nucEx.ExceptionClass = ClassName
            nucEx.UserExplanation = "An exception was thrown while attempting to get Survey Result for Request ID: " & requestId
            nucEx.ExceptionStackTrace = exc.StackTrace.ToString
            nucEx.Persist()
            Throw nucEx
        End Try
    End Function
    
    Public Function GetSurveyResultScore(ByVal resultKey As Integer, Optional ByVal dataSetTablename As String = "") As DataSet
        Dim dbParms As New DbParmList
        Dim dbAccessor As New DbAccess
        Dim dbXfer As New DbXfer
        dbParms.Add(New DbParm("@resultKey", resultKey))
        dbXfer.DatabaseMgr = Me.mDbMgr
        dbXfer.DatabaseName = "pmo"
        dbXfer.ParmList = dbParms

        If dataSetTablename = "" Then
            dataSetTablename = "tblSurveyResultScore"
        End If

        Try
            Return dbAccessor.GetRecord(dbXfer, "spSelectSurveyResultScore", dataSetTablename)

        Catch exc As Exception
            Dim nucEx As New PmoException(exc.Message)
            nucEx.ExceptionFunction = "GetSurveyResultScore"
            nucEx.ExceptionClass = ClassName
            nucEx.UserExplanation = "An exception was thrown while attempting to get the Survey Result Score for Result ID: " & resultKey
            nucEx.ExceptionStackTrace = exc.StackTrace.ToString
            nucEx.Persist()
            Throw nucEx
        End Try
    End Function
    Public Function GetSurveyResultComment(ByVal resultKey As Integer, Optional ByVal dataSetTablename As String = "") As DataSet
        Dim dbParms As New DbParmList
        Dim dbAccessor As New DbAccess
        Dim dbXfer As New DbXfer
        dbParms.Add(New DbParm("@resultKey", resultKey))
        dbXfer.DatabaseMgr = Me.mDbMgr
        dbXfer.DatabaseName = "pmo"
        dbXfer.ParmList = dbParms

        If dataSetTablename = "" Then
            dataSetTablename = "tblSurveyResultComment"
        End If

        Try
            Return dbAccessor.GetRecord(dbXfer, "spSelectSurveyResultComment", dataSetTablename)

        Catch exc As Exception
            Dim nucEx As New PmoException(exc.Message)
            nucEx.ExceptionFunction = "GetSurveyResultComment"
            nucEx.ExceptionClass = ClassName
            nucEx.UserExplanation = "An exception was thrown while attempting to get the Survey Result Comment for Result ID: " & resultKey
            nucEx.ExceptionStackTrace = exc.StackTrace.ToString
            nucEx.Persist()
            Throw nucEx
        End Try
    End Function
    Public Function GetSurveyResultScores(ByVal requestId As Integer, Optional ByVal dataSetTablename As String = "") As SurveyResultScoreList
        Dim theList As New SurveyResultScoreList
        Dim dbParms As New DbParmList
        Dim dbAccessor As New DbAccess
        Dim dbXfer As New DbXfer
        dbParms.Add(New DbParm("@requestId", requestId))
        dbXfer.DatabaseMgr = Me.mDbMgr
        dbXfer.DatabaseName = "pmo"
        dbXfer.ParmList = dbParms

        If dataSetTablename = "" Then
            dataSetTablename = "tblSurveyResultScore"
        End If

        Try
            Dim ds As DataSet = dbAccessor.GetRecord(dbXfer, "spGetAllSurveyResultScores", dataSetTablename)

            Dim counter As Integer = 0
            While counter < ds.Tables(0).Rows.Count
                Dim result As New SurveyResultScore(ds.Tables(0).Rows(counter).Item("resultKey"))
                theList.Add(result)
                counter += 1
            End While

            Return theList

        Catch exc As Exception
            Dim nucEx As New PmoException(exc.Message)
            nucEx.ExceptionFunction = "GetSurveyResultScores"
            nucEx.ExceptionClass = ClassName
            nucEx.UserExplanation = "An exception was thrown while attempting to get the Survey Result Scores for Request ID: " & requestId
            nucEx.ExceptionStackTrace = exc.StackTrace.ToString
            nucEx.Persist()
            Throw nucEx
        End Try
    End Function
    'Public Function SurveyCompletedForProject(ByVal projectId As Integer) As Boolean
    '    Dim completed As Boolean = False
    '    Dim dbParms As New DbParmList
    '    Dim dbAccessor As New DbAccess
    '    Dim dbXfer As New DbXfer
    '    dbParms.Add(New DbParm("@projectId", projectId))
    '    dbXfer.DatabaseMgr = Me.mDbMgr
    '    dbXfer.DatabaseName = "pmo"
    '    dbXfer.ParmList = dbParms

    '    Try
    '        Dim ds As DataSet = dbAccessor.GetRecord(dbXfer, "spSelectSurveyRequestForProject", "")

    '        If ds.Tables(0).Rows.Count > 0 Then
    '            Dim counter As Integer = 0
    '            While counter < ds.Tables(0).Rows.Count
    '                Dim survResult As New CpSurveyResult(ds.Tables(0).Rows(counter).Item("requestId"))
    '                If survResult.ProjectId > 0 Then
    '                    completed = True
    '                    Exit While
    '                End If
    '                counter += 1
    '            End While
    '        End If

    '        Return completed

    '    Catch exc As Exception
    '        Dim nucEx As New PmoException(exc.Message)
    '        nucEx.ExceptionFunction = "SurveyCompletedForProject"
    '        nucEx.ExceptionClass = ClassName
    '        nucEx.UserExplanation = "An exception was thrown while attempting to see if a Survey had been completed for Project ID: " & projectId
    '        nucEx.ExceptionStackTrace = exc.StackTrace.ToString
    '        nucEx.Persist()
    '        Throw nucEx
    '    End Try
    'End Function
    'Public Function SurveyCompletedCount(ByVal projectId As Integer) As Integer
    '    Dim dbParms As New DbParmList
    '    Dim dbAccessor As New DbAccess
    '    Dim dbXfer As New DbXfer
    '    dbParms.Add(New DbParm("@projectId", projectId))
    '    dbXfer.DatabaseMgr = Me.mDbMgr
    '    dbXfer.DatabaseName = "pmo"
    '    dbXfer.ParmList = dbParms

    '    Try
    '        Dim ds As DataSet = dbAccessor.GetRecord(dbXfer, "spSelectSurveyRequestForProject", "")

    '        Dim completeCount As Integer = 0
    '        If ds.Tables(0).Rows.Count > 0 Then
    '            Dim counter As Integer = 0
    '            While counter < ds.Tables(0).Rows.Count
    '                Dim survResult As New SurveyResult(ds.Tables(0).Rows(counter).Item("requestId"))
    '                If survResult.ProjectId > 0 Then
    '                    completeCount += 1
    '                End If
    '                counter += 1
    '            End While
    '        End If

    '        Return completeCount

    '    Catch exc As Exception
    '        Dim nucEx As New PmoException(exc.Message)
    '        nucEx.ExceptionFunction = "SurveyCompletedCount"
    '        nucEx.ExceptionClass = ClassName
    '        nucEx.UserExplanation = "An exception was thrown while attempting to see how many Surveys have been completed for Project ID: " & projectId
    '        nucEx.ExceptionStackTrace = exc.StackTrace.ToString
    '        nucEx.Persist()
    '        Throw nucEx
    '    End Try
    'End Function
    Public Function GetSurveyResultIdForProject(ByVal projectId As Integer) As Integer
        Dim resultId As Integer = 0
        Dim dbParms As New DbParmList
        Dim dbAccessor As New DbAccess
        Dim dbXfer As New DbXfer
        dbParms.Add(New DbParm("@projectId", projectId))
        dbXfer.DatabaseMgr = Me.mDbMgr
        dbXfer.DatabaseName = "pmo"
        dbXfer.ParmList = dbParms

        Try
            Dim ds As DataSet = dbAccessor.GetRecord(dbXfer, "spSelectSurveyRequestForProject", "")

            If ds.Tables(0).Rows.Count > 0 Then
                resultId = ds.Tables(0).Rows(0).Item("requestId")
            End If

            Return resultId

        Catch exc As Exception
            Dim nucEx As New PmoException(exc.Message)
            nucEx.ExceptionFunction = "GetSurveyResultIdForProject"
            nucEx.ExceptionClass = ClassName
            nucEx.UserExplanation = "An exception was thrown while attempting to get the Survey result id for Project ID: " & projectId
            nucEx.ExceptionStackTrace = exc.StackTrace.ToString
            nucEx.Persist()
            Throw nucEx
        End Try
    End Function
    Public Function GetCompletedSurveyList(Optional ByVal dataSetTableName As String = "") As CpSurveyResultList
        Dim theList As New CpSurveyResultList
        Dim dbParms As New DbParmList
        Dim dbAccessor As New DbAccess
        Dim dbXfer As New DbXfer
        dbXfer.DatabaseMgr = Me.mDbMgr
        dbXfer.DatabaseName = "pmo"
        dbXfer.ParmList = dbParms

        If dataSetTableName = "" Then
            dataSetTableName = "tblSurveyResult"
        End If

        Try
            Dim ds As DataSet = dbAccessor.GetRecord(dbXfer, "spGetAllSurveyResult", dataSetTableName)

            Dim counter As Integer = 0
            While counter < ds.Tables(0).Rows.Count
                If ds.Tables(0).Rows(counter).Item("requestId") = 648 Then
                    Dim stopHere As Boolean = True
                End If
                Dim result As New CpSurveyResult(ds.Tables(0).Rows(counter).Item("requestId"))
                theList.Add(result)
                counter += 1
            End While

            Return theList

        Catch exc As Exception
            Dim nucEx As New PmoException(exc.Message)
            nucEx.ExceptionFunction = "GetCompletedSurveyList"
            nucEx.ExceptionClass = ClassName
            nucEx.UserExplanation = "An exception was thrown while attempting to get the Completed Survey list"
            nucEx.ExceptionStackTrace = exc.StackTrace.ToString
            nucEx.Persist()
            Throw nucEx
        End Try

    End Function
    Public Function GetCompletedSurveyList(ByVal projectId As Integer, Optional ByVal dataSetTableName As String = "") As CpSurveyResultList
        Dim theList As New CpSurveyResultList
        Dim dbParms As New DbParmList
        Dim dbAccessor As New DbAccess
        Dim dbXfer As New DbXfer
        dbParms.Add(New DbParm("@projectId", projectId))
        dbXfer.DatabaseMgr = Me.mDbMgr
        dbXfer.DatabaseName = "pmo"
        dbXfer.ParmList = dbParms

        If dataSetTableName = "" Then
            dataSetTableName = "tblSurveyResult"
        End If

        Try
            Dim ds As DataSet = dbAccessor.GetRecord(dbXfer, "spGetAllSurveyResultForProject", dataSetTableName)

            Dim counter As Integer = 0
            While counter < ds.Tables(0).Rows.Count
                Dim result As New CpSurveyResult(ds.Tables(0).Rows(counter).Item("requestId"))
                theList.Add(result)
                counter += 1
            End While

            Return theList

        Catch exc As Exception
            Dim nucEx As New PmoException(exc.Message)
            nucEx.ExceptionFunction = "GetCompletedSurveyList"
            nucEx.ExceptionClass = ClassName
            nucEx.UserExplanation = "An exception was thrown while attempting to get the Completed Survey list"
            nucEx.ExceptionStackTrace = exc.StackTrace.ToString
            nucEx.Persist()
            Throw nucEx
        End Try

    End Function
    Public Function GetCompletedSurveyList(ByVal projectId As Guid, Optional ByVal dataSetTableName As String = "") As CpSurveyResultList
        Dim theList As New CpSurveyResultList
        Dim dbParms As New DbParmList
        Dim dbAccessor As New DbAccess
        Dim dbXfer As New DbXfer
        dbParms.Add(New DbParm("@projectId", projectId))
        dbXfer.DatabaseMgr = Me.mDbMgr
        dbXfer.DatabaseName = "pmo"
        dbXfer.ParmList = dbParms

        If dataSetTableName = "" Then
            dataSetTableName = "tblSurveyResult"
        End If

        Try
            Dim ds As DataSet = dbAccessor.GetRecord(dbXfer, "spGetAllSurveyResultForCpProject", dataSetTableName)

            Dim counter As Integer = 0
            While counter < ds.Tables(0).Rows.Count
                Dim result As New CpSurveyResult(ds.Tables(0).Rows(counter).Item("requestId"))
                theList.Add(result)
                counter += 1
            End While

            Return theList

        Catch exc As Exception
            Dim nucEx As New PmoException(exc.Message)
            nucEx.ExceptionFunction = "GetCompletedSurveyList"
            nucEx.ExceptionClass = ClassName
            nucEx.UserExplanation = "An exception was thrown while attempting to get the Completed Survey list"
            nucEx.ExceptionStackTrace = exc.StackTrace.ToString
            nucEx.Persist()
            Throw nucEx
        End Try

    End Function
    Public Function GetCompletedSurveyList(ByVal startDate As Date, ByVal endDate As Date, Optional ByVal dataSetTableName As String = "") As CpSurveyResultList
        Dim theList As New CpSurveyResultList
        Dim dbParms As New DbParmList
        Dim dbAccessor As New DbAccess
        Dim dbXfer As New DbXfer
        dbParms.Add(New DbParm("@startDate", startDate))
        dbParms.Add(New DbParm("@endDate", endDate))
        dbXfer.DatabaseMgr = Me.mDbMgr
        dbXfer.DatabaseName = "pmo"
        dbXfer.ParmList = dbParms


        If dataSetTableName = "" Then
            dataSetTableName = "tblSurveyResult"
        End If

        Try
            Dim ds As DataSet = dbAccessor.GetRecord(dbXfer, "spGetAllSurveyResultsForDateRange", dataSetTableName)

            Dim counter As Integer = 0
            While counter < ds.Tables(0).Rows.Count
                Dim result As New CpSurveyResult(ds.Tables(0).Rows(counter).Item("requestId"))
                theList.Add(result)
                counter += 1
            End While

            Return theList

        Catch exc As Exception
            Dim nucEx As New PmoException(exc.Message)
            nucEx.ExceptionFunction = "GetCompletedSurveyList"
            nucEx.ExceptionClass = ClassName
            nucEx.UserExplanation = "An exception was thrown while attempting to get the Completed Survey list for a date range of: " & startDate.ToShortDateString & " - " & endDate.ToShortDateString
            nucEx.ExceptionStackTrace = exc.StackTrace.ToString
            nucEx.Persist()
            Throw nucEx
        End Try

    End Function
#End Region
#Region "Survey Measures"
  Private Const cReccQuestionNumber As Integer = 8
  Public Function GetReccomendationScores() As DataSet
    Dim dbParms As New DbParmList
    Dim dbAccessor As New DbAccess
    Dim dbXfer As New DbXfer
    dbParms.Add(New DbParm("@questionId", cReccQuestionNumber))
    dbXfer.DatabaseMgr = Me.mDbMgr
    dbXfer.DatabaseName = "pmo"
    dbXfer.ParmList = dbParms

    Try
      Return dbAccessor.GetRecord(dbXfer, "spGetSurveyResultsForQuestionId", "")

    Catch exc As Exception
      Dim nucEx As New PmoException(exc.Message)
      nucEx.ExceptionFunction = "GetReccomendationScores"
      nucEx.ExceptionClass = ClassName
      nucEx.UserExplanation = "An exception was thrown while attempting to get All of the scores for Reccomend OST"
      nucEx.ExceptionStackTrace = exc.StackTrace.ToString
      nucEx.Persist()
      Throw nucEx
    End Try
  End Function
  Public Function GetOpenSurveyRequestCount() As Integer
    Dim theCount As Integer = 0
    Dim ds As DataSet = GetAllOpenSurveyRequests()
    If Not ds Is Nothing Then
      theCount = ds.Tables(0).Rows.Count
    End If
    Return theCount
  End Function
  Public Function GetCompletedSurveyRequestCount() As Integer
    Dim theCount As Integer = 0
        Dim theList As New CpSurveyResultList
    Dim dbParms As New DbParmList
    Dim dbAccessor As New DbAccess
    Dim dbXfer As New DbXfer
    dbXfer.DatabaseMgr = Me.mDbMgr
    dbXfer.DatabaseName = "pmo"
    dbXfer.ParmList = dbParms

    Try
      Dim ds As DataSet = dbAccessor.GetRecord(dbXfer, "spGetAllSurveyResult", "")

      If Not ds Is Nothing Then
        theCount = ds.Tables(0).Rows.Count
      End If

      Return theCount

    Catch exc As Exception
      Dim nucEx As New PmoException(exc.Message)
      nucEx.ExceptionFunction = "GetCompletedSurveyRequestCount"
      nucEx.ExceptionClass = ClassName
      nucEx.UserExplanation = "An exception was thrown while attempting to get the Completed Survey Request Count"
      nucEx.ExceptionStackTrace = exc.StackTrace.ToString
      nucEx.Persist()
      Throw nucEx
    End Try
  End Function
  Public Function GetReccomendationScoresForChart() As DataSet
    Dim dbParms As New DbParmList
    Dim dbAccessor As New DbAccess
    Dim dbXfer As New DbXfer
    dbParms.Add(New DbParm("@questionId", cReccQuestionNumber))
    dbXfer.DatabaseMgr = Me.mDbMgr
    dbXfer.DatabaseName = "pmo"
    dbXfer.ParmList = dbParms

    Try
      Return dbAccessor.GetRecord(dbXfer, "spGetSurveyResultsForChart", "")

    Catch exc As Exception
      Dim nucEx As New PmoException(exc.Message)
      nucEx.ExceptionFunction = "GetReccomendationScoresForChart"
      nucEx.ExceptionClass = ClassName
      nucEx.UserExplanation = "An exception was thrown while attempting to get All of the scores for Reccomend OST for Chart display."
      nucEx.ExceptionStackTrace = exc.StackTrace.ToString
      nucEx.Persist()
      Throw nucEx
    End Try
    End Function
    Public Function GetYtdClientSatScores() As DataSet
        Dim dbParms As New DbParmList
        Dim dbAccessor As New DbAccess
        Dim dbXfer As New DbXfer
        dbParms.Add(New DbParm("@questionId", cReccQuestionNumber))
        dbXfer.DatabaseMgr = Me.mDbMgr
        dbXfer.DatabaseName = "pmo"
        dbXfer.ParmList = dbParms

        Try
            Return dbAccessor.GetRecord(dbXfer, "spGetYtdAverageResultsForQuestionId", "")

        Catch exc As Exception
            Dim nucEx As New PmoException(exc.Message)
            nucEx.ExceptionFunction = "GetYtdClientSatScores"
            nucEx.ExceptionClass = ClassName
            nucEx.UserExplanation = "An exception was thrown while attempting to get the YTD Client Satisfaction scores for Reccomend OST"
            nucEx.ExceptionStackTrace = exc.StackTrace.ToString
            nucEx.Persist()
            Throw nucEx
        End Try
    End Function
#End Region
  Public Function GetFileExtension(ByVal fileName As String) As String
    Dim tmpFileName As String = ""
    Dim revFileName As String = ""
    Dim finalFileName As String = ""
    Dim len As Integer = fileName.Length - 1
    tmpFileName = fileName
    While tmpFileName.Substring(len, 1) <> "."
      revFileName += tmpFileName.Substring(len, 1)
      len -= 1
    End While

    Dim revLen As Integer = revFileName.Length - 1
    While revLen >= 0
      finalFileName += revFileName.Substring(revLen, 1)
      revLen -= 1
    End While
    Return finalFileName
  End Function
  Public Function StripFileExtension(ByVal fileName As String) As String
    Dim tmpFileName As String = ""
    Dim revFileName As String = ""
    Dim finalFileName As String = ""
    Dim len As Integer = fileName.Length - 1
    tmpFileName = fileName
    Dim extFound As Boolean = False
    Dim firstTime As Boolean = True
    While len > 0
      If Not extFound Then
        If tmpFileName.Substring(len, 1) = "." Then
          extFound = True
        End If
      End If
      If extFound Then
        If Not firstTime Then
          revFileName += tmpFileName.Substring(len, 1)
        Else
          firstTime = False
        End If
      End If
      len -= 1
    End While
    revFileName += tmpFileName.Substring(len, 1)

    Dim revLen As Integer = revFileName.Length - 1
    While revLen >= 0
      finalFileName += revFileName.Substring(revLen, 1)
      revLen -= 1
    End While
    Return finalFileName
  End Function
  Public Function GetFileName(ByVal pathIn As String) As String
    Dim tmpFileName As String = ""
    Dim revFileName As String = ""
    Dim finalFileName As String = ""
    Dim len As Integer = pathIn.Length - 1
    tmpFileName = pathIn
    While tmpFileName.Substring(len, 1) <> "\"
      revFileName += tmpFileName.Substring(len, 1)
      len -= 1
    End While

    Dim revLen As Integer = revFileName.Length - 1
    While revLen >= 0
      finalFileName += revFileName.Substring(revLen, 1)
      revLen -= 1
    End While
    Return finalFileName
    End Function
#Region "Migration Code"
    Public Function GetChangepointProjectByOldProjectId(ByVal projectId As Integer) As DataSet
        Dim myConn As SqlClient.SqlConnection = GetChangepointSqlConnector()
        Dim strSql As String = ""
        strSql += "select Project.UserDefinedProjectId, "
        strSql += "isnull(stuff(stuff(Project.UserDefinedProjectId,1,charindex('-',Project.UserDefinedProjectId),''),1,charindex('-',stuff(Project.UserDefinedProjectId,1,charindex('-',Project.UserDefinedProjectId),'')),''),0) * 1 as old_projid, "
        strSql += "ActualStart as Start_date, Billable as Billable_Flag, Project.ProjectStatus, ProjectStatus.Description as status_descr, Project.Name as Project_name, Customer.Name as Customer_name, "
        strSql += "Project.ProjectId, Project.CustomerId from Project left join Customer on Project.CustomerId = Customer.CustomerId "
        strSql += "left join ProjectStatus on  Project.ProjectStatus = ProjectStatus.code "
        strSql += "where project.userdefinedprojectid like 'M-%' and isnull(stuff(stuff(Project.UserDefinedProjectId,1,charindex('-',Project.UserDefinedProjectId),''),1,charindex('-',stuff(Project.UserDefinedProjectId,1,charindex('-',Project.UserDefinedProjectId),'')),''),0) * 1 = "
        strSql += projectId.ToString
        Dim da As New SqlClient.SqlDataAdapter(strSql, myConn)
        Dim ds As New DataSet
        da.Fill(ds, "projects")
        Return (ds)
    End Function
#End Region

End Class
