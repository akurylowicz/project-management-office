﻿Imports CommonLib
Public Class OldProject
    Private mId As Integer
    Private mCustomer As String
    Private mTitle As String
    Private mStartDate As Date

    Private Const cSystemName As String = "pmoLib.dll"
    Private Const cClassName As String = "Project"

    Public Sub New()
        Initialize()
    End Sub
    Public Sub New(ByVal projId As Integer, Optional ByVal fastLoad As Boolean = False)
        Initialize()
        Me.mId = projId
        Load(fastLoad)
    End Sub
    Private Sub Initialize()
        Me.mId = Nothing
        Me.mCustomer = ""
        Me.mTitle = ""
        Me.mStartDate = Nothing
    End Sub
    Public Property Id() As Integer
        Get
            Return Me.mId
        End Get
        Set(ByVal value As Integer)
            Me.mId = value
        End Set
    End Property
    Public Property Customer() As String
        Get
            Return Me.mCustomer
        End Get
        Set(ByVal value As String)
            Me.mCustomer = value
        End Set
    End Property
    Public Property Title() As String
        Get
            Return Me.mTitle
        End Get
        Set(ByVal value As String)
            Me.mTitle = value
        End Set
    End Property
    Public Property StartDate() As Date
        Get
            Return Me.mStartDate
        End Get
        Set(ByVal value As Date)
            Me.mStartDate = value
        End Set
    End Property
    Public ReadOnly Property ProjectSurveyCompleted() As Boolean
        Get
            Dim pmoMgr As New pmoManager
            Return pmoMgr.SurveyCompletedForProject(Me.mId)
        End Get
    End Property
    Public ReadOnly Property ProjectSurveyCount() As Integer
        Get
            Dim pmoMgr As New pmoManager
            Return pmoMgr.SurveyCompletedCount(Me.mId)
        End Get
    End Property
    Public ReadOnly Property ProjectSurveyResultId() As Integer
        Get
            Dim pmoMgr As New pmoManager
            Return pmoMgr.GetSurveyResultIdForProject(Me.mId)
        End Get
    End Property
    Public ReadOnly Property StartDateDisplay() As String
        Get
            If IsValidDate(Me.mStartDate) Then
                Return Me.mStartDate.ToShortDateString
            Else
                Return ""
            End If
        End Get
    End Property

    Private Function Load(ByVal fastLoad As Boolean) As Boolean
        Dim retCode As Boolean = True
        Dim ds As DataSet

        ' Load the dataset... then call LoadFromDataset(ds)... please change manager object and key value as needed.

        Dim pmoMgr As New pmoManager

        Try
            ds = pmoMgr.GetPmoProject(Me.mId)
            If ds.Tables(0).Rows.Count > 0 Then
                retCode = LoadFromDataset(ds)
            End If

        Catch exc As Exception
            Dim pmoExc As New PmoException(exc.Message.ToString)
            'Dim pmoExc As New PmoException("Blew up in Load on OldProject")
            pmoExc.ExceptionFunction = "Load"
            pmoExc.ExceptionClass = cClassName
            pmoExc.ExceptionStackTrace = exc.StackTrace
            pmoExc.ExceptionType = exc.GetType.ToString
            pmoExc.Persist()
            Throw pmoExc
        Finally

        End Try

        Return retCode
    End Function
    Private Function LoadFromDataset(ByVal dsIn As DataSet) As Boolean
        Dim retCode As Boolean = True
        ' use the data in the dataset to load the class information
        Me.mCustomer = CheckForNull(dsIn.Tables(0).Rows(0).Item("Customer_name"), "String")
        Me.mTitle = CheckForNull(dsIn.Tables(0).Rows(0).Item("Project_name"), "String")
        Me.mStartDate = CheckForNull(dsIn.Tables(0).Rows(0).Item("Start_date"), "Date")
        Return retCode
    End Function
    Private ReadOnly Property ClassName() As String
        Get
            Return cClassName
        End Get
    End Property
End Class
