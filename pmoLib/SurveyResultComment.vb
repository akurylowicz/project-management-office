Imports CommonLib

Public Class SurveyResultComment

#Region "Class Constants"
  ''' Class Info Constants
  Private Const cClassName As String = "SurveyRequestComment"
  Private Const cClassAuthor As String = "SQL Object Generator"
  Private Const cClassVersion As String = "Version 2.0.a"
#End Region

#Region "Class Members"
  ''' Class Members
  Private mComment As String
  Private mQuestionId As Integer
  Private mRequestId As Integer
  Private mResultKey As Integer          ' Primary Key to table... 
  Private mIsNew As Boolean
  Private mNeedsSave As Boolean
  Private mLoadedFromDb As Boolean
  Private mDisplayOrder As Integer
#End Region

#Region "Constructors"
  ''' Simple Constructor
  Public Sub New()
    Initialize()
    Me.mIsNew = True
    Me.mLoadedFromDb = False
    Me.mNeedsSave = True
  End Sub
  ''' Complex Constructor
  Public Sub New(ByVal resultKeyIn As Integer)
    Initialize()
    mResultKey = resultKeyIn
    Load()
  End Sub
  ''' Initialization Routines
  Private Sub Initialize()
    mComment = ""
    mQuestionId = 0
    mRequestId = 0
    mResultKey = 0
    mIsNew = False
    mNeedsSave = False
    mLoadedFromDb = False
    Me.mDisplayOrder = 0
  End Sub
#End Region

#Region "Member Properties"
  ''' Member Properties
  Public Property Comment() As String
    Get
      Return mComment
    End Get
    Set(ByVal Value As String)
      mComment = Value
      Me.mNeedsSave = True
    End Set
  End Property

  Public Property QuestionId() As Integer
    Get
      Return mQuestionId
    End Get
    Set(ByVal Value As Integer)
      mQuestionId = Value
      Me.mNeedsSave = True
    End Set
  End Property
  Public Property DisplayOrder() As Integer
    Get
      Return mDisplayOrder
    End Get
    Set(ByVal Value As Integer)
      mDisplayOrder = Value
      Me.mNeedsSave = True
    End Set
  End Property
  Public Property RequestId() As Integer
    Get
      Return mRequestId
    End Get
    Set(ByVal Value As Integer)
      mRequestId = Value
      Me.mNeedsSave = True
    End Set
  End Property

  Public Property ResultKey() As Integer
    Get
      Return mResultKey
    End Get
    Set(ByVal Value As Integer)
      mResultKey = Value
      Me.mNeedsSave = True
    End Set
  End Property

  Public Property IsNew() As Boolean
    Get
      Return mIsNew
    End Get
    Set(ByVal Value As Boolean)
      mIsNew = Value
      Me.mNeedsSave = True
    End Set
  End Property

  Public Property NeedsSave() As Boolean
    Get
      Return mNeedsSave
    End Get
    Set(ByVal Value As Boolean)
      mNeedsSave = Value
      Me.mNeedsSave = True
    End Set
  End Property

  Public Property LoadedFromDb() As Boolean
    Get
      Return mLoadedFromDb
    End Get
    Set(ByVal Value As Boolean)
      mLoadedFromDb = Value
      Me.mNeedsSave = True
    End Set
  End Property

#End Region

#Region "Persistence Routines"
  Public Function Persist() As Boolean
    Dim retCode As Boolean = True

    If Me.Validate.Count > 0 Then
      Return False
    End If

    Try
      ' TODO: Add Persistence Code
      Dim dbParms As New DbParmList

      dbParms.Add(New DbParm("@resultKey", mResultKey))
      dbParms.Add(New DbParm("@comment", mComment))
      dbParms.Add(New DbParm("@questionId", mQuestionId))
      dbParms.Add(New DbParm("@requestId", mRequestId))
      dbParms.Add(New DbParm("@displayOrder", mDisplayOrder))

      Dim dbMgr As DALDataManager
      dbMgr = DALDataManager.GetDatabaseObject
      Dim dbAccessor As New DbAccess

      Dim dbXfer As New DbXfer
      dbXfer.DatabaseMgr = CType(dbMgr, DALDataManager)
      'Database Name goes here...
      dbXfer.DatabaseName = "pmo"
      dbXfer.ParmList = dbParms

      If Not Me.mIsNew Then
        If dbAccessor.Update(dbXfer, "spUpdateSurveyResultComment") Then
          retCode = True
          Me.mNeedsSave = False
        End If
      Else
        If dbAccessor.Insert(dbXfer, "spInsertSurveyResultComment", True, "spUpdateSurveyResultComment") Then
          retCode = True
          Me.mNeedsSave = False
          Me.mIsNew = False
        End If
      End If


    Catch exc As Exception
      Dim logEx As New PmoException(exc.Message)
      logEx.ExceptionFunction = "Persist()"
      logEx.ExceptionClass = ClassName()
      logEx.ExceptionStackTrace = exc.StackTrace
      logEx.ExceptionType = exc.GetType.ToString
      logEx.Persist()
      Throw logEx

    Finally

    End Try

    Return retCode
  End Function
  Public Function Validate() As UtilValidationList
    Dim objValList As New UtilValidationList
    ' perform validation edits.  Whenever an error is found, instantiate a
    ' UtilValidation object and add it to the Validation List

    Try
      ' TODO: Add Validation Code

    Catch exc As Exception
      Dim logEx As New PmoException(exc.Message)
      logEx.ExceptionFunction = "Validate()"
      logEx.ExceptionClass = ClassName()
      logEx.ExceptionStackTrace = exc.StackTrace
      logEx.ExceptionType = exc.GetType.ToString
      logEx.Persist()
      Throw logEx
    Finally

    End Try

    Return objValList
  End Function
#End Region

#Region "Load Routines"
  ''' Load
  Private Function Load() As Boolean
    Dim retCode As Boolean = True
    Dim ds As DataSet

    ' Load the dataset... then call LoadFromDataset(ds)... please change manager object and key value as needed.
    If Me.mResultKey > 0 Then
      Dim pmoMgr As New pmoManager

      Try
        ds = pmoMgr.GetSurveyResultComment(Me.mResultKey)
        If ds.Tables(0).Rows.Count > 0 Then
          retCode = LoadFromDataset(ds)
          If retCode = True Then
            Me.mIsNew = False
            Me.mLoadedFromDb = True
            Me.mNeedsSave = False
          End If
        End If

      Catch exc As Exception
        Dim logEx As New PmoException(exc.Message)
        logEx.ExceptionFunction = "Load()"
        logEx.ExceptionClass = ClassName()
        logEx.ExceptionStackTrace = exc.StackTrace
        logEx.ExceptionType = exc.GetType.ToString
        logEx.Persist()

      Finally

      End Try

    Else
      Dim logEx As New PmoException("Attempt to insantiate object without key value.")
      logEx.ExceptionFunction = "Load()"
      logEx.ExceptionClass = ClassName()
      logEx.Persist()
      Throw logEx
    End If

    Return retCode
  End Function
  ''' Load From Dataset
  Private Function LoadFromDataset(ByVal dsIn As DataSet) As Boolean
    Dim retCode As Boolean = True
    ' use the data in the dataset to load the class information
    mComment = CheckForNull(dsIn.Tables(0).Rows(0).Item("comment"), "String")
    mQuestionId = CheckForNull(dsIn.Tables(0).Rows(0).Item("questionId"), "Integer")
    mRequestId = CheckForNull(dsIn.Tables(0).Rows(0).Item("requestId"), "Integer")
    mResultKey = CheckForNull(dsIn.Tables(0).Rows(0).Item("resultKey"), "Integer")
    mDisplayOrder = CheckForNull(dsIn.Tables(0).Rows(0).Item("displayOrder"), "Integer")

    Return retCode
  End Function
#End Region

#Region "Delete Routine"
  ''' Delete
  Public Function Delete() As Boolean
    Dim retCode As Boolean = True

    Try
      If Me.mResultKey > 0 Then
        Dim dbParms As New DbParmList
        dbParms.Add(New DbParm("@resultKey", mResultKey))
        Dim dbMgr As CommonLib.DALDataManager
        dbMgr = CommonLib.DALDataManager.GetDatabaseObject
        Dim dbAccessor As New DbAccess

        Dim dbXfer As New DbXfer
        dbXfer.DatabaseMgr = CType(dbMgr, CommonLib.DALDataManager)
        'Database Name goes here...
        dbXfer.DatabaseName = "pmo"
        dbXfer.ParmList = dbParms

        If dbAccessor.Delete(dbXfer, "spDeleteSurveyResultComment") Then
          retCode = True
        Else
          retCode = False
        End If

      End If

    Catch exc As Exception
      Dim logEx As New PmoException(exc.Message)
      logEx.ExceptionFunction = "Delete()"
      logEx.ExceptionClass = ClassName()
      logEx.ExceptionStackTrace = exc.StackTrace
      logEx.ExceptionType = exc.GetType.ToString
      logEx.Persist()
      Throw logEx

    Finally

    End Try

    Return retCode
  End Function
#End Region

#Region "Class Constant Properties"
  Public ReadOnly Property ClassName()
    Get
      Return cClassName
    End Get
  End Property

  Public ReadOnly Property ClassAuthor()
    Get
      Return cClassAuthor
    End Get
  End Property

  Public ReadOnly Property ClassVersion()
    Get
      Return cClassVersion
    End Get
  End Property

#End Region

End Class
