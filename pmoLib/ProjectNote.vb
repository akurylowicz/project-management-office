Imports CommonLib

public class ProjectNote

#Region "Class Constants"
  ''' Class Info Constants
  private const cClassName as string = "ProjectNote"
  private const cClassAuthor as string = "SQL Object Generator"
  private const cClassVersion as string = "Version 2.0.a"
#End Region

#Region "Class Members"
  ''' Class Members
  private mNote as String
  private mNoteAddedBy as String
  private mNoteAddedDate as Date
  private mProjectId as Integer
  private mRecordKey as Integer          ' Primary Key to table... 
  Private mIsNew as Boolean
  Private mNeedsSave as Boolean
  Private mLoadedFromDb as Boolean
#End Region

#Region "Constructors"
  ''' Simple Constructor
Public Sub New()
  Initialize()
  Me.mIsNew = True
  Me.mLoadedFromDb = False
  Me.mNeedsSave = True
End Sub
  ''' Complex Constructor
Public Sub New(byval recordKeyIn as Integer)
  Initialize()
  mRecordKey = recordKeyIn
  Load()
End Sub
  ''' Initialization Routines
Private Sub Initialize()
  mNote = ""
  mNoteAddedBy = ""
  mNoteAddedDate = nothing
  mProjectId = 0
  mRecordKey = 0
  mIsNew = False
  mNeedsSave = False
  mLoadedFromDb = False
End Sub
#End Region

#Region "Member Properties"
  ''' Member Properties
Public Property Note() as String
	Get
		return mNote
	End Get
	Set(byval Value as String)
		mNote = Value
		Me.mNeedsSave = True
	End Set
End Property

Public Property NoteAddedBy() as String
	Get
		return mNoteAddedBy
	End Get
	Set(byval Value as String)
		mNoteAddedBy = Value
		Me.mNeedsSave = True
	End Set
End Property

Public Property NoteAddedDate() as Date
	Get
		return mNoteAddedDate
	End Get
	Set(byval Value as Date)
		mNoteAddedDate = Value
		Me.mNeedsSave = True
	End Set
End Property

Public Property ProjectId() as Integer
	Get
		return mProjectId
	End Get
	Set(byval Value as Integer)
		mProjectId = Value
		Me.mNeedsSave = True
	End Set
End Property

Public Property RecordKey() as Integer
	Get
		return mRecordKey
	End Get
	Set(byval Value as Integer)
		mRecordKey = Value
		Me.mNeedsSave = True
	End Set
End Property

Public Property IsNew() as Boolean
	Get
		return mIsNew
	End Get
	Set(byval Value as Boolean)
		mIsNew = Value
		Me.mNeedsSave = True
	End Set
End Property

Public Property NeedsSave() as Boolean
	Get
		return mNeedsSave
	End Get
	Set(byval Value as Boolean)
		mNeedsSave = Value
		Me.mNeedsSave = True
	End Set
End Property

Public Property LoadedFromDb() as Boolean
	Get
		return mLoadedFromDb
	End Get
	Set(byval Value as Boolean)
		mLoadedFromDb = Value
		Me.mNeedsSave = True
	End Set
End Property

#End Region

#Region "Persistence Routines"
Public function Persist() as boolean
  dim retCode as boolean = True
 
  if me.Validate.count > 0 then
    return false
  end if
 
  try
  ' TODO: Add Persistence Code
  Dim dbParms as New DbParmList
  If mrecordKey > 0 then
     dbParms.Add(New DbParm("@recordKey", mrecordKey))
  End If
    dbParms.Add(New DbParm("@note", mnote))
    dbParms.Add(New DbParm("@noteAddedBy", mnoteAddedBy))
    dbParms.Add(New DbParm("@noteAddedDate", mnoteAddedDate))
    dbParms.Add(New DbParm("@projectId", mprojectId))
 
    Dim dbMgr As DALDataManager
    dbMgr = DALDataManager.GetDatabaseObject
    Dim dbAccessor As New DbAccess
 
    Dim dbXfer As New DbXfer
    dbXfer.DatabaseMgr = CType(dbMgr, DALDataManager)
    'Database Name goes here...
    dbXfer.DatabaseName = "pmo"
    dbXfer.ParmList = dbParms

      If mRecordKey > 0 Then
        If dbAccessor.Update(dbXfer, "spUpdateProjectNote") Then
          retCode = True
          Me.mNeedsSave = False
        End If
      Else
        If dbAccessor.Insert(dbXfer, "spInsertProjectNote", True, "spUpdateProjectNote") Then
          mRecordKey = dbAccessor.ReturnValue
          retCode = True
          Me.mNeedsSave = False
          Me.mIsNew = False
        End If
      End If

    Catch exc As Exception
      Dim logEx As New PmoException(exc.Message)
      logEx.ExceptionFunction = "Persist()"
      logEx.ExceptionClass = ClassName()
      logEx.ExceptionStackTrace = exc.StackTrace
      logEx.ExceptionType = exc.GetType.ToString
      logEx.Persist()
      Throw logEx

    Finally

    End Try
 
  return retCode
end function
Public function Validate() as UtilValidationList
  dim objValList as new UtilValidationList
  ' perform validation edits.  Whenever an error is found, instantiate a
  ' UtilValidation object and add it to the Validation List
  
  try
  ' TODO: Add Validation Code
 
  catch exc as Exception
      Dim logEx As New PmoException(exc.Message)
      logEx.ExceptionFunction = "Validate()"
      logEx.ExceptionClass = ClassName()
      logEx.ExceptionStackTrace = exc.StackTrace
      logEx.ExceptionType = exc.GetType.ToString
      logEx.Persist()
      Throw logEx
 
  finally
 
  end try
 
  return objValList
end function
#End Region

#Region "Load Routines"
  ''' Load
Private function Load() as boolean
  dim retCode as boolean = True
  dim ds as dataset
 
  ' Load the dataset... then call LoadFromDataset(ds)... please change manager object and key value as needed.
   If Me.mrecordKey > 0 Then
      Dim pmoMgr As New pmoManager
  
     try
        ds = pmoMgr.GetProjectNote(Me.mRecordKey)
     If ds.Tables(0).Rows.Count > 0 Then
        retCode = LoadFromDataset(ds)
        if retCode = True then
           Me.mIsNew = False
           Me.mLoadedFromDb = True
           Me.mNeedsSave = False
        End If
     End If
 
     catch exc as Exception
        Dim logEx As New PmoException(exc.Message)
        logEx.ExceptionFunction = "Load()"
        logEx.ExceptionClass = ClassName()
        logEx.ExceptionStackTrace = exc.StackTrace
        logEx.ExceptionType = exc.GetType.ToString
        logEx.Persist()
    Throw logEx
 
     finally
 
     end try
 
  Else
      Dim logEx As New PmoException("Attempt to insantiate object without key value.")
      logEx.ExceptionFunction = "Load()"
      logEx.ExceptionClass = ClassName()
      logEx.Persist()
    Throw logEx
  End If
 
  return retCode
end function
  ''' Load From Dataset
Private function LoadFromDataset(dsIn as dataset) as boolean
  dim retCode as boolean = True
  ' use the data in the dataset to load the class information
  mNote = CheckForNull(dsIn.tables(0).rows(0).item("note"), "String")
  mNoteAddedBy = CheckForNull(dsIn.tables(0).rows(0).item("noteAddedBy"), "String")
  mNoteAddedDate = CheckForNull(dsIn.tables(0).rows(0).item("noteAddedDate"), "Date")
  mProjectId = CheckForNull(dsIn.tables(0).rows(0).item("projectId"), "Integer")
  mRecordKey = CheckForNull(dsIn.tables(0).rows(0).item("recordKey"), "Integer")
 
  return retCode
end function
#End Region

#Region "Delete Routine"
  ''' Delete
Public function Delete() as boolean
  dim retCode as boolean = True
 
  try
   If Me.mrecordKey > 0 Then
  Dim dbParms as New DbParmList
  dbParms.Add(New DbParm("@recordKey", mrecordKey))
    Dim dbMgr As CommonLib.DALDataManager
    dbMgr = CommonLib.DALDataManager.GetDatabaseObject
    Dim dbAccessor As New DbAccess
 
    Dim dbXfer As New DbXfer
    dbXfer.DatabaseMgr = CType(dbMgr, CommonLib.DALDataManager)
    'Database Name goes here...
    dbXfer.DatabaseName = "pmo"
    dbXfer.ParmList = dbParms

    If dbAccessor.Delete(dbXfer, "spDeleteProjectNote") Then
      retCode = True
    Else
      retCode = False
    End If
 
  End If
 
  catch exc as Exception
      Dim logEx As New PmoException(exc.Message)
      logEx.ExceptionFunction = "Delete()"
      logEx.ExceptionClass = ClassName()
      logEx.ExceptionStackTrace = exc.StackTrace
      logEx.ExceptionType = exc.GetType.ToString
      logEx.Persist()
    Throw logEx
 
  finally
 
  end try
 
  return retCode
end function
#End Region

#Region "Class Constant Properties"
Public Readonly Property ClassName()
	Get
		return cClassName
	End Get
End Property

Public Readonly Property ClassAuthor()
	Get
		return cClassAuthor
	End Get
End Property

Public Readonly Property ClassVersion()
	Get
		return cClassVersion
	End Get
End Property

#End Region

End Class
