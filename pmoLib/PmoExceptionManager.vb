﻿Imports Microsoft.VisualBasic
Imports CommonLib
Imports System.Data
Imports MySql

Public Class PmoExceptionManager
  Private mDbMgr As DALDataManager
  Private Const ClassName As String = "PmoExceptionManager"

  Public Sub New()
    mDbMgr = DALDataManager.GetDatabaseObject

    If mDbMgr Is Nothing Then
      Dim exc As New LogException("PMO Exception Manager instantiated without a current DALDataManager.")
      exc.ExeptionModule = ClassName
      exc.LogException()
      Throw exc
    End If
  End Sub
  Public Function GetPmoException(ByVal exceptionIdIn As Integer, Optional ByVal dataSetTablename As String = "") As DataSet
    Dim dbParms As New DbParmList
    Dim dbAccessor As New DbAccess
    Dim dbXfer As New DbXfer
    dbParms.Add(New DbParm("@exceptionId", exceptionIdIn))
    dbXfer.DatabaseMgr = Me.mDbMgr
    dbXfer.DatabaseName = "pmo"
    dbXfer.ParmList = dbParms

    If dataSetTablename = "" Then
      dataSetTablename = "tblPmoException"
    End If

    Try
      Return dbAccessor.GetRecord(dbXfer, "spSelectPmoException", dataSetTablename)

    Catch exc As Exception
      Dim nucEx As New LogException(exc.Message)
      nucEx.ExceptionFunction = "GetPmoException"
      nucEx.ExeptionModule = ClassName
      nucEx.ExceptionMiscInfo = "An exception was thrown while attempting to get the PMO Exception: " & exceptionIdIn
      nucEx.SystemName = "PMO"
      nucEx.LogException()
      Throw nucEx
    End Try
  End Function
  Public Function GetAllPmoExceptions(Optional ByVal dataSetTableName As String = "") As DataSet
    Dim dbParms As New DbParmList
    Dim dbAccessor As New DbAccess
    Dim dbXfer As New DbXfer
    dbXfer.DatabaseMgr = Me.mDbMgr
    dbXfer.DatabaseName = "pmo"
    dbXfer.ParmList = dbParms

    If dataSetTableName = "" Then
      dataSetTableName = "tblPmoException"
    End If

    Try
      Return dbAccessor.GetRecord(dbXfer, "spGetAllPmoException", dataSetTableName)

    Catch exc As Exception
      Dim nucEx As New LogException(exc.Message)
      nucEx.ExceptionFunction = "GetSowList"
      nucEx.ExeptionModule = ClassName
      nucEx.ExceptionMiscInfo = "An exception was thrown while attempting to get the PMO Exception List "
      nucEx.SystemName = "PMO"
      nucEx.LogException()
      Throw nucEx
    End Try
  End Function
End Class
