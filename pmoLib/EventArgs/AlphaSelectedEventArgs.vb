﻿Public Class AlphaSelectedEventArgs
  Inherits EventArgs
  Private mSelected As String
  Public Sub New(ByVal alphaSel As String)
    Me.mSelected = alphaSel
  End Sub
  Public ReadOnly Property SelectedValue() As String
    Get
      Return Me.mSelected
    End Get
  End Property
End Class
