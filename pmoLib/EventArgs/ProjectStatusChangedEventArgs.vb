﻿Public Class ProjectStatusChangedEventArgs
  Inherits EventArgs

  Private mNewStatusId As Integer
  Private mOldStatusId As Integer

  Public Sub New(ByVal newStatus As Integer, ByVal oldStatus As Integer)
    Me.mNewStatusId = newStatus
    Me.mOldStatusId = oldStatus
  End Sub
  Public ReadOnly Property NewStatusId() As Integer
    Get
      Return Me.mNewStatusId
    End Get
  End Property
  Public ReadOnly Property OldStatusId() As Integer
    Get
      Return Me.mOldStatusId
    End Get
  End Property
End Class
