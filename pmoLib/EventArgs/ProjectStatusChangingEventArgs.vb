﻿Public Class ProjectStatusChangingEventArgs
  Inherits EventArgs

  Private mNewStatusId As Integer

  Public Sub New(ByVal newStatus As Integer)
    Me.mNewStatusId = newStatus
  End Sub
  Public ReadOnly Property NewStatusId() As Integer
    Get
      Return Me.mNewStatusId
    End Get
  End Property
End Class

