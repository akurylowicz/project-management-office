﻿Public Class MenuItemSelectedEventArgs
  Inherits EventArgs

  Private mMenuName As String
  Private mMenuItem As String

  Public Sub New(ByVal menuName As String, ByVal menuItem As String)
    Me.mMenuName = menuName
    Me.mMenuItem = menuItem
  End Sub
  Public ReadOnly Property MenuName() As String
    Get
      Return Me.mMenuName
    End Get
  End Property
  Public ReadOnly Property MenuItem() As String
    Get
      Return Me.mMenuItem
    End Get
  End Property
End Class
