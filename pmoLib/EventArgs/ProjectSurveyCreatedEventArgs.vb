﻿Public Class ProjectSurveyCreatedEventArgs
  Inherits EventArgs

    Private mProjectId As Guid

    Public Sub New(ByVal projectId As Guid)
        Me.mProjectId = projectId
    End Sub
    Public ReadOnly Property ProjectId() As Guid
        Get
            Return Me.mProjectId
        End Get
    End Property
End Class

