﻿Public Class ProjectSurveyFollowUpSentEventArgs
  Inherits EventArgs

  Private mRequestId As Integer

  Public Sub New(ByVal requestId As Integer)
    Me.mRequestId = requestId
  End Sub
  Public ReadOnly Property RequestId() As Integer
    Get
      Return Me.mRequestId
    End Get
  End Property
End Class
