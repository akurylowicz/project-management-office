﻿Imports CommonLib

Public Class PmoUser
  Private mUserId As Integer
  Private mFirstName As String
  Private mLastName As String
  Private mEmail As String
  Private mHasSalesRole As Boolean
  Private mHasTechRole As Boolean
  Private mUserName As String

  '11/19/2011
  Private mUserEnabled As Boolean

  Private Const mClassName As String = "PmoUser"

  Public Sub New()
    Initialize()
  End Sub
  Public Sub New(ByVal userId As Integer)
    Initialize()
    Me.mUserId = userId
    Load()
  End Sub
  Public Sub New(ByVal userId As Integer, ByVal lastName As String, ByVal firstName As String)
    Initialize()
    Me.mUserId = userId
    Me.mLastName = lastName
    Me.mFirstName = firstName
  End Sub
  Private Sub Initialize()
    mUserId = 0
    mFirstName = ""
    mLastName = ""
    mEmail = ""
    mHasSalesRole = False
    mHasTechRole = False
    Me.mUserName = ""
    mUserEnabled = False
  End Sub
  Public ReadOnly Property UserId() As Integer
    Get
      Return Me.mUserId
    End Get
  End Property
  Public ReadOnly Property UserName() As String
    Get
      Return Me.mUserName
    End Get
  End Property
  Public ReadOnly Property FirstName() As String
    Get
      Return (Me.mFirstName)
    End Get
  End Property
  Public ReadOnly Property LastName() As String
    Get
      Return (Me.mLastName)
    End Get
  End Property
  Public ReadOnly Property FullName() As String
    Get
      Return (Me.mFirstName & " " & Me.mLastName)
    End Get
  End Property
  Public ReadOnly Property ListName() As String
    Get
      Dim theListName As String = ""
      If Me.mLastName > "" Then
        theListName = Me.mLastName
        If Me.mFirstName > "" Then
          theListName += ", " & Me.mFirstName
        End If
      Else
        If Me.mFirstName > "" Then
          theListName = Me.mFirstName
        End If
      End If
      Return theListName
    End Get
  End Property
  Public ReadOnly Property Email() As String
    Get
      Return (Me.mEmail)
    End Get
  End Property
  Public ReadOnly Property HasSalesRole() As Boolean
    Get
      Return Me.mHasSalesRole
    End Get
  End Property
  Public ReadOnly Property HasTechRole() As Boolean
    Get
      Return Me.mHasTechRole
    End Get
  End Property
  Public ReadOnly Property IsEnabled() As Boolean
    Get
      Return Me.mUserEnabled
    End Get
  End Property
    '  Public ReadOnly Property RegisteredNotifications() As PmoNotificationList_OLD
    '      Get
    '          Dim pmoMgr As New pmoManager
    '          Dim theList As PmoNotificationList = pmoMgr.GetNotificationListForUser(Me.mUserId)
    '          Return theList
    '      End Get
    '  End Property
    'Public ReadOnly Property HasNotification(ByVal notificationTypeId As Integer) As Boolean
    '  Get
    '    Dim hasNotif As Boolean = False

    '          For Each tmpNot As PmoNotification_OLD In RegisteredNotifications
    '              If tmpNot.NotificationTypeId = notificationTypeId Then
    '                  hasNotif = True
    '                  Exit For
    '              End If
    '          Next
    '          Return hasNotif
    '      End Get
    '  End Property
    '  Public Sub RemoveNotification(ByVal notificationTypeId As Integer)
    '      For Each tmpNot As PmoNotification_OLD In RegisteredNotifications
    '          If tmpNot.NotificationTypeId = notificationTypeId Then
    '              tmpNot.Delete()
    '          End If
    '      Next
    '  End Sub
  Private Function Load() As Boolean
    Dim retCode As Boolean = True
    Dim ds As DataSet

    ' Load the dataset... then call LoadFromDataset(ds)... please change manager object and key value as needed.
    If Me.mUserId > 0 Then
      Dim pmoMgr As New pmoManager

      Try
        ds = pmoMgr.GetPmoUser(Me.mUserId)
        If ds.Tables(0).Rows.Count > 0 Then
          retCode = LoadFromDataset(ds)
        End If

      Catch exc As Exception
        Dim logEx As New PmoException(exc.Message)
        logEx.ExceptionFunction = "Load()"
        logEx.ExceptionClass = ClassName()
        logEx.ExceptionStackTrace = exc.StackTrace
        logEx.ExceptionType = exc.GetType.ToString
        logEx.Persist()
        Throw logEx

      Finally

      End Try

    Else
      Dim exc As New Exception("Attempt to insantiate a User object without key value.")
      Dim pmoExc As New PmoException(exc.Message)
      pmoExc.ExceptionFunction = "Load"
      pmoExc.ExceptionClass = ClassName
      pmoExc.ExceptionStackTrace = "N/A"
      pmoExc.ExceptionType = pmoExc.GetType.ToString
      pmoExc.UserExplanation = "The PMO User cannot be loaded because there was no identifier specified.  This is a system fault and has been reported."
      pmoExc.UserAction = "No action required by the user."
      pmoExc.Persist()
      Throw pmoExc
    End If

    Return retCode
  End Function
  Private Function LoadFromDataset(ByVal dsIn As DataSet) As Boolean
    Dim retCode As Boolean = True
    ' use the data in the dataset to load the class information
    Me.mFirstName = CheckForNull(dsIn.Tables(0).Rows(0).Item("fname"), "String")
    Me.mLastName = CheckForNull(dsIn.Tables(0).Rows(0).Item("lname"), "String")
    Me.mEmail = CheckForNull(dsIn.Tables(0).Rows(0).Item("email_addr"), "String")

    If CheckForNull(dsIn.Tables(0).Rows(0).Item("enabled"), "String") = "Y" Then
      Me.mUserEnabled = True
    Else
      Me.mUserEnabled = False
    End If

    If CheckForNull(dsIn.Tables(0).Rows(0).Item("sales_flag"), "String") = "Y" Then
      Me.mHasSalesRole = True
    Else
      Me.mHasSalesRole = False
    End If
    If CheckForNull(dsIn.Tables(0).Rows(0).Item("tech_flag"), "String") = "Y" Then
      Me.mHasTechRole = True
    Else
      Me.mHasTechRole = False
    End If
    Me.mUserName = CheckForNull(dsIn.Tables(0).Rows(0).Item("user_name"), "String")
    Return retCode
  End Function
  Private ReadOnly Property ClassName() As String
    Get
      Return mClassName
    End Get
  End Property
End Class
