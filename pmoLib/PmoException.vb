﻿Imports CommonLib
Public Class PmoException
  Inherits ApplicationException
  Private mExceptionId As Integer
  Private mExceptionTimeStamp As Date
  Private mExceptionClass As String
  Private mExceptionFunction As String
  Private mExceptionType As String
  Private mExceptionStackTrace As String

  Private mExceptionMessage As String

  Private mUserExplanation As String
  Private mUserAction As String

  Public Sub New(ByVal messageIn As String)
    MyBase.New(messageIn)
    Initialize()
    Me.mExceptionMessage = messageIn
  End Sub
  Private Sub Initialize()
    Me.mExceptionId = 0
    Me.mExceptionTimeStamp = Now
    Me.mExceptionClass = ""
    Me.mExceptionFunction = ""
    Me.mExceptionStackTrace = ""
    Me.mExceptionMessage = ""
    Me.mExceptionType = "Not Set"
  End Sub
  Public Property UserExplanation() As String
    Get
      Return Me.mUserExplanation
    End Get
    Set(ByVal value As String)
      Me.mUserExplanation = value
    End Set
  End Property
  Public Property UserAction() As String
    Get
      Return Me.mUserAction
    End Get
    Set(ByVal value As String)
      Me.mUserAction = value
    End Set
  End Property
  Public ReadOnly Property ExceptionMessage() As String
    Get
      Return Me.mExceptionMessage
    End Get
  End Property

  Public Property ExceptionFunction() As String
    Get
      Return mExceptionFunction
    End Get
    Set(ByVal Value As String)
      mExceptionFunction = Value
    End Set
  End Property
  Public Property ExceptionStackTrace() As String
    Get
      Return mExceptionStackTrace
    End Get
    Set(ByVal Value As String)
      mExceptionStackTrace = Value
    End Set
  End Property
  Public Property ExceptionClass() As String
    Get
      Return mExceptionClass
    End Get
    Set(ByVal Value As String)
      mExceptionClass = Value
    End Set
  End Property
  Public Property ExceptionType() As String
    Get
      Return mExceptionType
    End Get
    Set(ByVal Value As String)
      mExceptionType = Value
    End Set
  End Property
  Public Property ExceptionId() As Integer
    Get
      Return mExceptionId
    End Get
    Set(ByVal Value As Integer)
      mExceptionId = Value
    End Set
  End Property
  Public ReadOnly Property TimeStamp() As Date
    Get
      Return mExceptionTimeStamp
    End Get
  End Property
#Region "Persistence Routines"
  Public Function Persist() As Boolean
    Dim retCode As Boolean = False

    If Me.Validate.Count > 0 Then
      Return False
    End If

    Try
      Dim dbParms As New DbParmList

      dbParms.Add(New DbParm("@exceptionTimeStamp", mExceptionTimeStamp))
      dbParms.Add(New DbParm("@exceptionType", mExceptionType))
      dbParms.Add(New DbParm("@exceptionMessage", Me.mExceptionMessage))
      dbParms.Add(New DbParm("@exceptionStackTrace", mExceptionStackTrace))
      dbParms.Add(New DbParm("@exceptionClass", mExceptionClass))
      dbParms.Add(New DbParm("@exceptionFunction", mExceptionFunction))

      Dim dbMgr As DALDataManager
      dbMgr = DALDataManager.GetDatabaseObject
      Dim dbAccessor As New DbAccess

      Dim dbXfer As New DbXfer
      dbXfer.DatabaseMgr = CType(dbMgr, DALDataManager)
      'Database Name goes here...
      dbXfer.DatabaseName = "pmo"
      dbXfer.ParmList = dbParms

      If dbAccessor.Insert(dbXfer, "spInsertPmoException", True, "spUpdatePmoException") Then
        retCode = True
      End If

    Catch exc As Exception
      Dim logEx As New LogException(exc.Message)
      logEx.ExceptionFunction = "Persist()"
      logEx.ExeptionModule = "PmoException"
      logEx.LogException()
      Throw logEx

    Finally

    End Try

    Return retCode
  End Function
  Public Function Validate() As UtilValidationList
    Dim objValList As New UtilValidationList
    ' perform validation edits.  Whenever an error is found, instantiate a
    ' UtilValidation object and add it to the Validation List

    Try
      ' TODO: Add Validation Code

    Catch exc As Exception
      Dim logEx As New LogException(exc.Message)
      logEx.ExceptionFunction = "Validate()"
      logEx.ExeptionModule = "PmoException"
      logEx.LogException()
      Throw logEx

    Finally

    End Try

    Return objValList
  End Function
#End Region
#Region "Load Routines"
  Private Function Load() As Boolean
    Dim retCode As Boolean = True
    Dim ds As DataSet

    ' Load the dataset... then call LoadFromDataset(ds)... please change manager object and key value as needed.
    If Me.mExceptionId > 0 Then
      Dim pmoMgr As New PmoExceptionManager

      Try
        ds = pmoMgr.GetPmoException(Me.mExceptionId)
        If ds.Tables(0).Rows.Count > 0 Then
          retCode = LoadFromDataset(ds)
        End If

      Catch exc As Exception
        Dim logEx As New LogException(exc.Message)
        logEx.ExceptionFunction = "Load()"
        logEx.ExeptionModule = "PmoException"
        logEx.LogException()
        Throw logEx

      Finally

      End Try

    Else
      Dim logEx As New LogException("Attempt to insantiate PMO Exception object without key value.")
      logEx.ExceptionFunction = "Load()"
      logEx.ExeptionModule = "PmoException"
      logEx.LogException()
      Throw logEx
    End If

    Return retCode
  End Function
  Private Function LoadFromDataset(ByVal dsIn As DataSet) As Boolean
    Dim retCode As Boolean = True
    ' use the data in the dataset to load the class information
    mExceptionId = CheckForNull(dsIn.Tables(0).Rows(0).Item("exceptionId"), "Integer")
    mExceptionType = CheckForNull(dsIn.Tables(0).Rows(0).Item("exceptionType"), "String")
    mExceptionClass = CheckForNull(dsIn.Tables(0).Rows(0).Item("exceptionClass"), "String")
    mExceptionFunction = CheckForNull(dsIn.Tables(0).Rows(0).Item("exceptionFunction"), "String")
    mExceptionMessage = CheckForNull(dsIn.Tables(0).Rows(0).Item("exceptionMessage"), "String")
    mExceptionStackTrace = CheckForNull(dsIn.Tables(0).Rows(0).Item("exceptionStackTrace"), "String")
    mExceptionTimeStamp = CheckForNull(dsIn.Tables(0).Rows(0).Item("exceptionTimeStamp"), "Date")
    
    Return retCode
  End Function
#End Region
#Region "Delete Routine"
  ''' Delete
  Public Function Delete() As Boolean
    Dim retCode As Boolean = True

    Try
      If Me.mExceptionId > 0 Then
        Dim dbParms As New DbParmList
        dbParms.Add(New DbParm("@exceptionId", mExceptionId))
        Dim dbMgr As CommonLib.DALDataManager
        dbMgr = CommonLib.DALDataManager.GetDatabaseObject
        Dim dbAccessor As New DbAccess

        Dim dbXfer As New DbXfer
        dbXfer.DatabaseMgr = CType(dbMgr, CommonLib.DALDataManager)
        'Database Name goes here...
        dbXfer.DatabaseName = "pmo"
        dbXfer.ParmList = dbParms

        If dbAccessor.Delete(dbXfer, "spDeletePmoException") Then
          retCode = True
        Else
          retCode = False
        End If

      End If

    Catch exc As Exception
      Dim logEx As New LogException(exc.Message)
      logEx.ExceptionFunction = "Delete()"
      logEx.ExeptionModule = "PmoException"
      logEx.LogException()
      Throw logEx

    Finally

    End Try

    Return retCode
  End Function
#End Region
End Class
