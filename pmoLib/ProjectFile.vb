Imports CommonLib

Public Class ProjectFile

#Region "Class Constants"
  ''' Class Info Constants
  Private Const cClassName As String = "ProjectFiles"
  Private Const cClassAuthor As String = "SQL Object Generator"
  Private Const cClassVersion As String = "Version 2.0.a"
#End Region

#Region "Class Members"
  ''' Class Members
  Private mDateAdded As Date
  Private mFileLink As String
  Private mFileName As String
  Private mProjectId As Integer
  Private mRecordKey As Integer          ' Primary Key to table... 
  Private mIsNew As Boolean
  Private mNeedsSave As Boolean
  Private mLoadedFromDb As Boolean
#End Region

#Region "Constructors"
  ''' Simple Constructor
  Public Sub New()
    Initialize()
    Me.mIsNew = True
    Me.mLoadedFromDb = False
    Me.mNeedsSave = True
  End Sub
  ''' Complex Constructor
  Public Sub New(ByVal recordKeyIn As Integer)
    Initialize()
    mRecordKey = recordKeyIn
    Load()
  End Sub
  ''' Initialization Routines
  Private Sub Initialize()
    mDateAdded = Nothing
    mFileLink = ""
    mFileName = ""
    mProjectId = 0
    mRecordKey = 0
    mIsNew = False
    mNeedsSave = False
    mLoadedFromDb = False
  End Sub
#End Region

#Region "Member Properties"
  ''' Member Properties
  Public Property DateAdded() As Date
    Get
      Return mDateAdded
    End Get
    Set(ByVal Value As Date)
      mDateAdded = Value
      Me.mNeedsSave = True
    End Set
  End Property

  Public Property FileLink() As String
    Get
      Return mFileLink
    End Get
    Set(ByVal Value As String)
      mFileLink = Value
      Me.mNeedsSave = True
    End Set
  End Property

  Public Property FileName() As String
    Get
      Return mFileName
    End Get
    Set(ByVal Value As String)
      mFileName = Value
      Me.mNeedsSave = True
    End Set
  End Property

  Public Property ProjectId() As Integer
    Get
      Return mProjectId
    End Get
    Set(ByVal Value As Integer)
      mProjectId = Value
      Me.mNeedsSave = True
    End Set
  End Property

  Public Property RecordKey() As Integer
    Get
      Return mRecordKey
    End Get
    Set(ByVal Value As Integer)
      mRecordKey = Value
      Me.mNeedsSave = True
    End Set
  End Property

  Public Property IsNew() As Boolean
    Get
      Return mIsNew
    End Get
    Set(ByVal Value As Boolean)
      mIsNew = Value
      Me.mNeedsSave = True
    End Set
  End Property

  Public Property NeedsSave() As Boolean
    Get
      Return mNeedsSave
    End Get
    Set(ByVal Value As Boolean)
      mNeedsSave = Value
      Me.mNeedsSave = True
    End Set
  End Property

  Public Property LoadedFromDb() As Boolean
    Get
      Return mLoadedFromDb
    End Get
    Set(ByVal Value As Boolean)
      mLoadedFromDb = Value
      Me.mNeedsSave = True
    End Set
  End Property

#End Region

#Region "Persistence Routines"
  Public Function Persist() As Boolean
    Dim retCode As Boolean = True

    If Me.Validate.count > 0 Then
      Return False
    End If

    Try
      ' TODO: Add Persistence Code
      Dim dbParms As New DbParmList
      If mrecordKey > 0 Then
        dbParms.Add(New DbParm("@recordKey", mrecordKey))
      End If
      dbParms.Add(New DbParm("@dateAdded", mdateAdded))
      dbParms.Add(New DbParm("@fileLink", mfileLink))
      dbParms.Add(New DbParm("@fileName", mfileName))
      dbParms.Add(New DbParm("@projectId", mprojectId))

      Dim dbMgr As DALDataManager
      dbMgr = DALDataManager.GetDatabaseObject
      Dim dbAccessor As New DbAccess

      Dim dbXfer As New DbXfer
      dbXfer.DatabaseMgr = CType(dbMgr, DALDataManager)
      'Database Name goes here...
      dbXfer.DatabaseName = "pmo"
      dbXfer.ParmList = dbParms

      If mRecordKey > 0 Then
        If dbAccessor.Update(dbXfer, "spUpdateProjectFile") Then
          retCode = True
          Me.mNeedsSave = False
        End If
      Else
        If dbAccessor.Insert(dbXfer, "spInsertProjectFile", True, "spUpdateProjectFiles") Then
          mRecordKey = dbAccessor.ReturnValue
          retCode = True
          Me.mNeedsSave = False
          Me.mIsNew = False
        End If
      End If


    Catch exc As Exception
      Dim logEx As New PmoException(exc.Message)
      logEx.ExceptionFunction = "Persist()"
      logEx.ExceptionClass = ClassName()
      logEx.ExceptionStackTrace = exc.StackTrace
      logEx.ExceptionType = exc.GetType.ToString
      logEx.Persist()
      Throw logEx

    Finally

    End Try

    Return retCode
  End Function
  Public Function Validate() As UtilValidationList
    Dim objValList As New UtilValidationList
    ' perform validation edits.  Whenever an error is found, instantiate a
    ' UtilValidation object and add it to the Validation List

    Try
      ' TODO: Add Validation Code

    Catch exc As Exception
      Dim logEx As New PmoException(exc.Message)
      logEx.ExceptionFunction = "Validate()"
      logEx.ExceptionClass = ClassName()
      logEx.ExceptionStackTrace = exc.StackTrace
      logEx.ExceptionType = exc.GetType.ToString
      logEx.Persist()
      Throw logEx

    Finally

    End Try

    Return objValList
  End Function
#End Region

#Region "Load Routines"
  ''' Load
  Private Function Load() As Boolean
    Dim retCode As Boolean = True
    Dim ds As dataset

    ' Load the dataset... then call LoadFromDataset(ds)... please change manager object and key value as needed.
    If Me.mrecordKey > 0 Then
      Dim pmoMgr As New pmoManager

      Try
        ds = pmoMgr.GetProjectFile(Me.mRecordKey)
        If ds.Tables(0).Rows.Count > 0 Then
          retCode = LoadFromDataset(ds)
          If retCode = True Then
            Me.mIsNew = False
            Me.mLoadedFromDb = True
            Me.mNeedsSave = False
          End If
        End If

      Catch exc As Exception
        Dim logEx As New PmoException(exc.Message)
        logEx.ExceptionFunction = "Load()"
        logEx.ExceptionClass = ClassName()
        logEx.ExceptionStackTrace = exc.StackTrace
        logEx.ExceptionType = exc.GetType.ToString
        logEx.Persist()
        Throw logEx

      Finally

      End Try

    Else
      Dim logEx As New PmoException("Attempt to insantiate object without key value.")
      logEx.ExceptionFunction = "Load()"
      logEx.ExceptionClass = ClassName()
      logEx.Persist()
      Throw logEx
    End If

    Return retCode
  End Function
  ''' Load From Dataset
  Private Function LoadFromDataset(ByVal dsIn As dataset) As Boolean
    Dim retCode As Boolean = True
    ' use the data in the dataset to load the class information
    mDateAdded = CheckForNull(dsIn.tables(0).rows(0).item("dateAdded"), "Date")
    mFileLink = CheckForNull(dsIn.tables(0).rows(0).item("fileLink"), "String")
    mFileName = CheckForNull(dsIn.tables(0).rows(0).item("fileName"), "String")
    mProjectId = CheckForNull(dsIn.tables(0).rows(0).item("projectId"), "Integer")
    mRecordKey = CheckForNull(dsIn.tables(0).rows(0).item("recordKey"), "Integer")

    Return retCode
  End Function
#End Region

#Region "Delete Routine"
  ''' Delete
  Public Function Delete() As Boolean
    Dim retCode As Boolean = True

    Try
      If Me.mrecordKey > 0 Then
        Dim dbParms As New DbParmList
        dbParms.Add(New DbParm("@recordKey", mrecordKey))
        Dim dbMgr As CommonLib.DALDataManager
        dbMgr = CommonLib.DALDataManager.GetDatabaseObject
        Dim dbAccessor As New DbAccess

        Dim dbXfer As New DbXfer
        dbXfer.DatabaseMgr = CType(dbMgr, CommonLib.DALDataManager)
        'Database Name goes here...
        dbXfer.DatabaseName = "pmo"
        dbXfer.ParmList = dbParms

        If dbAccessor.Delete(dbXfer, "spDeleteProjectFile") Then
          retCode = True
        Else
          retCode = False
        End If

      End If

    Catch exc As Exception
      Dim logEx As New PmoException(exc.Message)
      logEx.ExceptionFunction = "Load()"
      logEx.ExceptionClass = ClassName()
      logEx.ExceptionStackTrace = exc.StackTrace
      logEx.ExceptionType = exc.GetType.ToString
      logEx.Persist()
      Throw logEx

    Finally

    End Try

    Return retCode
  End Function
#End Region

#Region "Class Constant Properties"
  Public ReadOnly Property ClassName()
    Get
      Return cClassName
    End Get
  End Property

  Public ReadOnly Property ClassAuthor()
    Get
      Return cClassAuthor
    End Get
  End Property

  Public ReadOnly Property ClassVersion()
    Get
      Return cClassVersion
    End Get
  End Property

#End Region

End Class
