Imports CommonLib

Public Class CpSurveyResult

#Region "Class Constants"
    ''' Class Info Constants
    Private Const cClassName As String = "CpSurveyResult"
    Private Const cClassAuthor As String = "SQL Object Generator"
    Private Const cClassVersion As String = "Version 2.0.a"
#End Region

#Region "Class Members"
    Private mEmailAddress As String
    Private mOverallComments As String
    Private mProjectPositionId As Integer
    Private mRequestId As Integer
    Private mSubmittedDate As Date
    Private mIsNew As Boolean
    Private mNeedsSave As Boolean
    Private mLoadedFromDb As Boolean
    Private mNoSurveyRequested As Boolean

    Private mProjId As Guid = Nothing
    Private mProjTitle As String = ""
    Private mProjCust As String = ""
    Private mSurveyScores As SurveyResultScoreList
    Private mOldProjectId As Integer
#End Region

#Region "Constructors"
    ''' Simple Constructor
    Public Sub New()
        Initialize()
        Me.mIsNew = True
        Me.mLoadedFromDb = False
        Me.mNeedsSave = True
    End Sub
    ''' Complex Constructor
    Public Sub New(ByVal requestId As Integer)
        Initialize()
        Me.mRequestId = requestId
        Load()

    End Sub
    ''' Initialization Routines
    Private Sub Initialize()
        mEmailAddress = ""
        mOverallComments = ""
        mProjectPositionId = 0
        mRequestId = 0
        mSubmittedDate = Nothing
        mIsNew = False
        mNeedsSave = False
        mLoadedFromDb = False
        mNoSurveyRequested = False
        mOldProjectId = 0
    End Sub
#End Region

#Region "Member Properties"
    ''' Member Properties
    ''' 
    Public ReadOnly Property ProjectId()
        Get
            If Me.mOldProjectId = OldProjectId Then
                Return Me.mProjId
            Else
                Return Me.mOldProjectId
            End If
        End Get
    End Property
    Public ReadOnly Property OldProjectId()
        Get
            Return Me.mOldProjectId
        End Get
    End Property
    Public Property ProjectTitle() As String
        Get
            Return Me.mProjTitle
        End Get
        Set(ByVal value As String)
            Me.mProjTitle = value
        End Set
    End Property
    Public Property ProjectCustomer() As String
        Get
            Return Me.mProjCust
        End Get
        Set(ByVal value As String)
            Me.mProjCust = value
        End Set
    End Property
    Public Property EmailAddress() As String
        Get
            Return mEmailAddress
        End Get
        Set(ByVal Value As String)
            mEmailAddress = Value
            Me.mNeedsSave = True
        End Set
    End Property
    Public Property OverallComments() As String
        Get
            Return mOverallComments
        End Get
        Set(ByVal Value As String)
            mOverallComments = Value
            Me.mNeedsSave = True
        End Set
    End Property
    Public Property ProjectPositionId() As Integer
        Get
            Return mProjectPositionId
        End Get
        Set(ByVal Value As Integer)
            mProjectPositionId = Value
            Me.mNeedsSave = True
        End Set
    End Property
    Public Property RequestId() As Integer
        Get
            Return mRequestId
        End Get
        Set(ByVal Value As Integer)
            mRequestId = Value
            Me.mNeedsSave = True
        End Set
    End Property
    Public Property SubmittedDate() As Date
        Get
            Return mSubmittedDate
        End Get
        Set(ByVal Value As Date)
            mSubmittedDate = Value
            Me.mNeedsSave = True
        End Set
    End Property
    Public ReadOnly Property SubmittedDateForDisplay() As String
        Get
            Return mSubmittedDate.ToShortDateString
        End Get
    End Property
    Public Property IsNew() As Boolean
        Get
            Return mIsNew
        End Get
        Set(ByVal Value As Boolean)
            mIsNew = Value
            Me.mNeedsSave = True
        End Set
    End Property
    Public Property NeedsSave() As Boolean
        Get
            Return mNeedsSave
        End Get
        Set(ByVal Value As Boolean)
            mNeedsSave = Value
            Me.mNeedsSave = True
        End Set
    End Property
    Public Property LoadedFromDb() As Boolean
        Get
            Return mLoadedFromDb
        End Get
        Set(ByVal Value As Boolean)
            mLoadedFromDb = Value
            Me.mNeedsSave = True
        End Set
    End Property
    Public Property NoSurveyRequested() As Boolean
        Get
            Return Me.mNoSurveyRequested
        End Get
        Set(ByVal value As Boolean)
            Me.mNoSurveyRequested = value
        End Set
    End Property
    Public ReadOnly Property ProjectPositionDescription()
        Get
            Dim descrip As String = ""
            If Me.ProjectPositionId > 0 Then
                Dim pmoMgr As New pmoManager
                descrip = pmoMgr.GetProjectPositionDescription(Me.mProjectPositionId)
            End If
            Return descrip
        End Get
    End Property
    Public Function GetSurveyHTML() As String
        Dim html As New Text.StringBuilder
        If Me.mNoSurveyRequested = True Then
            html.Append("<html><body>")
            html.Append(GetNoSurveyHeader())
            html.Append("<BR><BR><BR><BR>")
            html.Append("No Survey was requested for this project.")
            html.Append("<BR><BR>")
            html.Append("</body></html>")
        Else
            html.Append("<html><body>")
            html.Append("<table width=""95%""><tr><td align=""Left"">")
            html.Append(GetSurveyHeader())
            html.Append("</td></tr><tr><td align=""left"">")
            html.Append(GetSurveyQuestions())
            html.Append("</td></tr>")
            html.Append("</table>")
            html.Append("<BR><BR>")
            html.Append("</body></html>")
        End If
        Return html.ToString
    End Function
    Private Function GetSurveyHeader() As String
        Dim req As New CpSurveyRequest(Me.mRequestId)

        Dim header As New Text.StringBuilder
        header.Append("<STRONG>")
        header.Append("Project: " & Me.ProjectTitle)
        header.Append("<br>")
        header.Append("Client: " & Me.ProjectCustomer)
        'If req.OldProjectId > 0 Then
        '    Dim oldProj As New OldProject(req.OldProjectId)
        '    header.Append("Project: " & oldProj.Title)
        '    header.Append("<br>")
        '    header.Append("Client: " & oldProj.Customer)
        'Else
        '    Dim proj As New CpProject(req.ProjectId)
        '    header.Append("Project: " & proj.Title)
        '    header.Append("<br>")
        '    header.Append("Client: " & proj.Customer)
        'End If

        header.Append("<br>")
        header.Append("Request: " & req.RequestDate.ToShortDateString & " - " & req.EmailAddress)
        header.Append("<br>")
        header.Append("Submitted: " & req.CompleteDate.ToShortDateString)
        header.Append("<br><br>")
        header.Append("Project Position: " & ProjectPositionDescription())
        header.Append("<br>")
        header.Append("Overall Comments: " & Me.mOverallComments)
        header.Append("</STRONG>")
        Return header.ToString
    End Function
    Private Function GetNoSurveyHeader() As String
        Dim req As New CpSurveyRequest(Me.mRequestId)
        Dim header As New Text.StringBuilder
        header.Append("<STRONG>")
        header.Append("Project: " & Me.ProjectTitle)
        header.Append("<br>")
        header.Append("Client: " & Me.ProjectCustomer)
        'If req.OldProjectId > 0 Then
        '    Dim oldProj As New OldProject(req.OldProjectId)
        '    header.Append("Project: " & oldProj.Title)
        '    header.Append("<br>")
        '    header.Append("Client: " & oldProj.Customer)
        'Else
        '    Dim proj As New CpProject(req.ProjectId)
        '    header.Append("Project: " & proj.Title)
        '    header.Append("<br>")
        '    header.Append("Client: " & proj.Customer)
        'End If
        header.Append("</STRONG>")
        Return header.ToString
    End Function
    Private Function GetSurveyQuestions() As String
        Dim theMgr As New pmoManager
        Dim source As SurveyQuestionList = theMgr.GetActiveQuestions
        Dim questions As New Text.StringBuilder
        questions.Append("<table width=""99%"">")
        questions.Append("<tr><td colspan=3><hr></td></tr>")
        questions.Append("<tr><strong><td width=""20%"" align=""left"">")
        questions.Append("Question</td><td width=""10%"" align=center>Score</td><td align=""left"">Comment</td></strong></tr>")
        questions.Append("<tr><td colspan=3><hr></td></tr>")
        For Each answer As SurveyResultScore In SurveyScores(True)
            questions.Append("<tr>")
            questions.Append("<td align=""left"" valign=""top"">" & source.GetQuestionText(answer.QuestionId) & "</td>")
            questions.Append("<td align=""center"" valign=""top"">" & answer.Score & "</td>")
            questions.Append("<td align=""left"" valign=""top"">" & answer.Comment & "</td>")
            questions.Append("</tr>")
        Next
        questions.Append("</table>")
        Return questions.ToString
    End Function
    Public Function SurveyScores(Optional ByVal refresh As Boolean = True)
        If refresh Then
            Me.mSurveyScores = Nothing
            LoadSurveyScores()
        End If
        Return Me.mSurveyScores
    End Function
#End Region

#Region "Persistence Routines"
    Public Function Persist() As Boolean
        Dim retCode As Boolean = True

        If Me.Validate.Count > 0 Then
            Return False
        End If

        Try
            ' TODO: Add Persistence Code
            Dim dbParms As New DbParmList
            dbParms.Add(New DbParm("@emailAddress", mEmailAddress))
            dbParms.Add(New DbParm("@overallComments", mOverallComments))
            dbParms.Add(New DbParm("@projectPositionId", mProjectPositionId))
            dbParms.Add(New DbParm("@requestId", mRequestId))
            If Me.mIsNew Then
                dbParms.Add(New DbParm("@submittedDate", Today.ToShortDateString))
            Else
                dbParms.Add(New DbParm("@submittedDate", mSubmittedDate))
            End If
            dbParms.Add(New DbParm("@noSurveyRequested", mNoSurveyRequested))

            Dim dbMgr As DALDataManager
            dbMgr = DALDataManager.GetDatabaseObject
            Dim dbAccessor As New DbAccess

            Dim dbXfer As New DbXfer
            dbXfer.DatabaseMgr = CType(dbMgr, DALDataManager)
            'Database Name goes here...
            dbXfer.DatabaseName = "pmo"
            dbXfer.ParmList = dbParms

            If Not Me.mIsNew Then
                If dbAccessor.Update(dbXfer, "spUpdateSurveyResult") Then
                    retCode = True
                    Me.mNeedsSave = False
                End If
            Else
                If dbAccessor.Insert(dbXfer, "spInsertSurveyResult", True, "spUpdateSurveyResult") Then
                    retCode = True
                    Me.mNeedsSave = False
                    Me.mIsNew = False
                End If
            End If


        Catch exc As Exception
            Dim logEx As New PmoException(exc.Message)
            logEx.ExceptionFunction = "Persist()"
            logEx.ExceptionClass = ClassName()
            logEx.ExceptionStackTrace = exc.StackTrace
            logEx.ExceptionType = exc.GetType.ToString
            logEx.Persist()
            Throw logEx
        Finally

        End Try

        Return retCode
    End Function
    Public Function Validate() As UtilValidationList
        Dim objValList As New UtilValidationList
        ' perform validation edits.  Whenever an error is found, instantiate a
        ' UtilValidation object and add it to the Validation List

        Try
            ' TODO: Add Validation Code

        Catch exc As Exception
            Dim logEx As New PmoException(exc.Message)
            logEx.ExceptionFunction = "Validate()"
            logEx.ExceptionClass = ClassName()
            logEx.ExceptionStackTrace = exc.StackTrace
            logEx.ExceptionType = exc.GetType.ToString
            logEx.Persist()
            Throw logEx

        Finally

        End Try

        Return objValList
    End Function
#End Region

#Region "Load Routines"
    ''' Load
    Private Function Load() As Boolean
        Dim retCode As Boolean = True
        Dim ds As DataSet

        ' Load the dataset... then call LoadFromDataset(ds)... please change manager object and key value as needed.
        If Me.mRequestId > 0 Then
            Dim pmoMgr As New pmoManager

            Try
                ds = pmoMgr.GetSurveyResult(Me.mRequestId)
                If ds.Tables(0).Rows.Count > 0 Then
                    retCode = LoadFromDataset(ds)
                    If retCode = True Then
                        Dim req As New CpSurveyRequest(Me.mRequestId)
                        Me.mProjId = req.ProjectId
                        Me.mOldProjectId = req.OldProjectId
                        Me.mProjTitle = req.Title
                        Me.mProjCust = req.Customer

                        'If req.OldProjectId = 0 Then
                        '    Dim proj As New CpProject(req.ProjectId)
                        '    Me.mProjId = proj.Id
                        '    Me.mOldProjectId = 0
                        '    Me.mProjTitle = proj.Title
                        '    Me.mProjCust = proj.Customer
                        'Else
                        '    Dim oldProj As New OldProject(req.OldProjectId)
                        '    Me.mOldProjectId = oldProj.Id
                        '    Me.mProjId = Nothing
                        '    Me.mProjTitle = oldProj.Title
                        '    Me.mProjCust = oldProj.Customer
                        'End If
                       
                        LoadSurveyScores()
                        Me.mIsNew = False
                        Me.mLoadedFromDb = True
                        Me.mNeedsSave = False
                    End If
                    End If

            Catch exc As Exception
                Dim logEx As New PmoException(exc.Message)
                logEx.ExceptionFunction = "Load()"
                logEx.ExceptionClass = ClassName()
                logEx.ExceptionStackTrace = exc.StackTrace
                logEx.ExceptionType = exc.GetType.ToString
                logEx.Persist()
                Throw logEx

            Finally

            End Try

        Else
            Dim logEx As New PmoException("Attempt to insantiate object without key value.")
            logEx.ExceptionFunction = "Load()"
            logEx.ExceptionClass = ClassName()
            logEx.Persist()
            Throw logEx
        End If

        Return retCode
    End Function
    ''' Load From Dataset
    Private Function LoadFromDataset(ByVal dsIn As DataSet) As Boolean
        Dim retCode As Boolean = True
        ' use the data in the dataset to load the class information
        mEmailAddress = CheckForNull(dsIn.Tables(0).Rows(0).Item("emailAddress"), "String")
        mOverallComments = CheckForNull(dsIn.Tables(0).Rows(0).Item("overallComments"), "String")
        mProjectPositionId = CheckForNull(dsIn.Tables(0).Rows(0).Item("projectPositionId"), "Integer")
        mRequestId = CheckForNull(dsIn.Tables(0).Rows(0).Item("requestId"), "Integer")
        mSubmittedDate = CheckForNull(dsIn.Tables(0).Rows(0).Item("submittedDate"), "Date")
        mNoSurveyRequested = CheckForNull(dsIn.Tables(0).Rows(0).Item("noSurveyRequested"), "Boolean")
        Return retCode
    End Function
    Private Sub LoadSurveyScores()
        Dim pmoMgr As New pmoManager
        Me.mSurveyScores = pmoMgr.GetSurveyResultScores(Me.mRequestId)
    End Sub
#End Region

#Region "Delete Routine"
    ''' Delete
    Public Function Delete() As Boolean
        Throw New Exception("Delete not implemented for SurveyResult class.")
        'Dim retCode As Boolean = True

        'Try
        '  If Me.mRequestId > 0 Then
        '    Dim dbParms As New DbParmList
        '    dbParms.Add(New DbParm("@requestId", mRequestId))
        '    Dim dbMgr As CommonLib.DALDataManager
        '    dbMgr = CommonLib.DALDataManager.GetDatabaseObject
        '    Dim dbAccessor As New DbAccess

        '    Dim dbXfer As New DbXfer
        '    dbXfer.DatabaseMgr = CType(dbMgr, CommonLib.DALDataManager)
        '    'Database Name goes here...
        '    dbXfer.DatabaseName = "pmo"
        '    dbXfer.ParmList = dbParms

        '    If dbAccessor.Delete(dbXfer, "spDeleteSurveyResult") Then
        '      retCode = True
        '    Else
        '      retCode = False
        '    End If

        '  End If

        'Catch exc As Exception
        '  Dim logEx As New LogException(exc.Message)
        '  logEx.ExceptionFunction = "Delete()"
        '  logEx.ExeptionModule = ClassName()
        '  logEx.LogException()
        '  Throw logEx

        'Finally

        'End Try

        'Return retCode
    End Function
#End Region

#Region "Class Constant Properties"
    Public ReadOnly Property ClassName()
        Get
            Return cClassName
        End Get
    End Property

    Public ReadOnly Property ClassAuthor()
        Get
            Return cClassAuthor
        End Get
    End Property

    Public ReadOnly Property ClassVersion()
        Get
            Return cClassVersion
        End Get
    End Property

#End Region

End Class

