Imports CommonLib

Public Class ProjectInfo

#Region "Class Constants"
  ''' Class Info Constants
  Private Const cClassName As String = "ProjectInfo"
  Private Const cClassAuthor As String = "SQL Object Generator"
  Private Const cClassVersion As String = "Version 2.0.a"
#End Region
#Region "Class Members"
  ''' Class Members
  Private mPmAssignedDate As Date
  Private mPmAssignedId As Integer
  Private mPmoManagedProject As Boolean
  Private mProjectId As Integer          ' Primary Key to table... 
  Private mProjectStatusCode As Integer
  Private mProjectStatusAssignedDate As Date
  Private mProjectStatusAssignedById As Integer
  Private mIsNew As Boolean
  Private mNeedsSave As Boolean
  Private mLoadedFromDb As Boolean

  Private mSowId As Integer
#End Region
#Region "Constructors"
  ''' Simple Constructor
  Public Sub New()
    Initialize()
    Me.mIsNew = True
    Me.mLoadedFromDb = False
    Me.mNeedsSave = True
  End Sub
  ''' Complex Constructor
  Public Sub New(ByVal projectIdIn As Integer)
    Initialize()
    mProjectId = projectIdIn
    Load()
  End Sub
  ''' Initialization Routines
  Private Sub Initialize()
    mPmAssignedDate = Nothing
    mPmAssignedId = 0
    mPmoManagedProject = False
    mProjectId = 0
    mIsNew = False
    mNeedsSave = False
    mLoadedFromDb = False
    mProjectStatusCode = 0       'Default to Not Set
    mProjectStatusAssignedDate = Nothing
    mProjectStatusAssignedById = 0
    mSowId = 0
  End Sub
#End Region
#Region "Member Properties"
  ''' Member Properties
  ''' 
  Public Property SowId() As Integer
    Get
      Return Me.mSowId
    End Get
    Set(ByVal value As Integer)
      Me.mSowId = value
    End Set
  End Property
  Public Property ProjectStatusCode() As Integer
    Get
      Return Me.mProjectStatusCode
    End Get
    Set(ByVal value As Integer)
      Me.mProjectStatusCode = value
      Me.mProjectStatusAssignedDate = Now
    End Set
  End Property
  Public Property ProjectStatusAssignedById() As Integer
    Get
      Return Me.mProjectStatusAssignedById
    End Get
    Set(ByVal value As Integer)
      Me.mProjectStatusAssignedById = value
    End Set
  End Property
  Public ReadOnly Property ProjectStatusAssignedDate() As Date
    Get
      Return Me.mProjectStatusAssignedDate
    End Get
  End Property
  Public ReadOnly Property ProjectStatusAssignedByName() As String
    Get
      Dim name As String = ""
      If Me.mProjectStatusAssignedById > 0 Then
        Dim theUser As New PmoUser(Me.mProjectStatusAssignedById)
        name = theUser.FullName
      End If
      Return name
    End Get
  End Property
  Public Property PmAssignedDate() As Date
    Get
      Return mPmAssignedDate
    End Get
    Set(ByVal Value As Date)
      mPmAssignedDate = Value
      Me.mNeedsSave = True
    End Set
  End Property

  Public Property PmAssignedId() As Integer
    Get
      Return mPmAssignedId
    End Get
    Set(ByVal Value As Integer)
      If Me.mPmAssignedId > 0 Then
        mPmAssignedId = Value
        Me.mPmAssignedDate = Now
        Me.mPmoManagedProject = True
      Else
        mPmAssignedId = Value
        Me.mPmAssignedDate = Nothing
      End If
      Me.mNeedsSave = True
    End Set
  End Property
  Public ReadOnly Property ManagedByName() As String
    Get
      Dim managedName As String = "No PM Assigned"
      If Me.mPmAssignedId > 0 Then
        Dim theUser As New PmoUser(Me.mPmAssignedId)
        If Not theUser Is Nothing Then
          managedName = theUser.ListName
        End If
      End If
      Return managedName
    End Get
  End Property
  Public Property PmoManagedProject() As Boolean
    Get
      Return mPmoManagedProject
    End Get
    Set(ByVal Value As Boolean)
      mPmoManagedProject = Value
      Me.mNeedsSave = True
    End Set
  End Property

  Public Property ProjectId() As Integer
    Get
      Return mProjectId
    End Get
    Set(ByVal Value As Integer)
      mProjectId = Value
      Me.mNeedsSave = True
    End Set
  End Property

  Public Property IsNew() As Boolean
    Get
      Return mIsNew
    End Get
    Set(ByVal Value As Boolean)
      mIsNew = Value
      Me.mNeedsSave = True
    End Set
  End Property

  Public Property NeedsSave() As Boolean
    Get
      Return mNeedsSave
    End Get
    Set(ByVal Value As Boolean)
      mNeedsSave = Value
      Me.mNeedsSave = True
    End Set
  End Property

  Public Property LoadedFromDb() As Boolean
    Get
      Return mLoadedFromDb
    End Get
    Set(ByVal Value As Boolean)
      mLoadedFromDb = Value
      Me.mNeedsSave = True
    End Set
  End Property

#End Region
#Region "Persistence Routines"

  Public Function Persist() As Boolean
    Dim retCode As Boolean = True

    If Me.Validate.Count > 0 Then
      Return False
    End If

    Try
      ' TODO: Add Persistence Code
      Dim dbParms As New DbParmList

      dbParms.Add(New DbParm("@projectId", mProjectId))
      dbParms.Add(New DbParm("@pmAssignedId", mPmAssignedId))
      If IsValidDate(Me.mPmAssignedDate) Then
        If Me.mPmAssignedId = 0 Then
          dbParms.Add(New DbParm("@pmAssignedDate", DBNull.Value))
        Else
          dbParms.Add(New DbParm("@pmAssignedDate", mPmAssignedDate))
        End If
      Else
        dbParms.Add(New DbParm("@pmAssignedDate", DBNull.Value))
      End If

      dbParms.Add(New DbParm("@projectStatusCode", mProjectStatusCode))
      dbParms.Add(New DbParm("@projectStatusAssignedById", mProjectStatusAssignedById))
      If IsValidDate(Me.mProjectStatusAssignedDate) Then
        dbParms.Add(New DbParm("@projectStatusAssignedDate", mProjectStatusAssignedDate))
      Else
        dbParms.Add(New DbParm("@projectStatusAssignedDate", DBNull.Value))
      End If

      dbParms.Add(New DbParm("@pmoManagedProject", mPmoManagedProject))
      dbParms.Add(New DbParm("@sowId", mSowId))

      Dim dbMgr As DALDataManager
      dbMgr = DALDataManager.GetDatabaseObject
      Dim dbAccessor As New DbAccess

      Dim dbXfer As New DbXfer
      dbXfer.DatabaseMgr = CType(dbMgr, DALDataManager)
      'Database Name goes here...
      dbXfer.DatabaseName = "pmo"
      dbXfer.ParmList = dbParms

      If Not Me.mIsNew Then
        If dbAccessor.Update(dbXfer, "spUpdateProjectInfo") Then
          retCode = True
          Me.mNeedsSave = False
        End If
      Else
        If dbAccessor.Insert(dbXfer, "spInsertProjectInfo", True, "spUpdateProjectInfo") Then
          retCode = True
          Me.mNeedsSave = False
          Me.mIsNew = False
        End If
      End If


    Catch exc As Exception
      Dim logEx As New PmoException(exc.Message)
      logEx.ExceptionFunction = "Persist()"
      logEx.ExceptionClass = ClassName()
      logEx.ExceptionStackTrace = exc.StackTrace
      logEx.ExceptionType = exc.GetType.ToString
      logEx.Persist()
      Throw logEx

    Finally

    End Try

    Return retCode
  End Function
  Public Function Validate() As UtilValidationList
    Dim objValList As New UtilValidationList
    ' perform validation edits.  Whenever an error is found, instantiate a
    ' UtilValidation object and add it to the Validation List

    Try
      ' TODO: Add Validation Code

    Catch exc As Exception
      Dim logEx As New PmoException(exc.Message)
      logEx.ExceptionFunction = "Validate()"
      logEx.ExceptionClass = ClassName()
      logEx.ExceptionStackTrace = exc.StackTrace
      logEx.ExceptionType = exc.GetType.ToString
      logEx.Persist()
      Throw logEx

    Finally

    End Try

    Return objValList
  End Function
#End Region
#Region "Load Routines"
  Private Function Load() As Boolean
    Dim retCode As Boolean = True
    Dim ds As DataSet

    ' Load the dataset... then call LoadFromDataset(ds)... please change manager object and key value as needed.
    If Me.mProjectId > 0 Then
      Dim pmoMgr As New pmoManager

      Try
        ds = pmoMgr.GetProjectInfo(Me.mProjectId)
        If ds.Tables(0).Rows.Count > 0 Then
          retCode = LoadFromDataset(ds)
          If retCode = True Then
            Me.mIsNew = False
            Me.mLoadedFromDb = True
            Me.mNeedsSave = False
          End If
        Else
          Me.mIsNew = True
        End If

      Catch exc As Exception
        Dim logEx As New PmoException(exc.Message)
        logEx.ExceptionFunction = "Load()"
        logEx.ExceptionClass = ClassName()
        logEx.ExceptionStackTrace = exc.StackTrace
        logEx.ExceptionType = exc.GetType.ToString
        logEx.Persist()
        Throw logEx

      Finally

      End Try

    Else
      Dim logEx As New PmoException("Attempt to insantiate object without key value.")
      logEx.ExceptionFunction = "Load()"
      logEx.ExceptionClass = ClassName()
      logEx.Persist()
      Throw logEx
    End If

    Return retCode
  End Function
  Private Function LoadFromDataset(ByVal dsIn As DataSet) As Boolean
    Dim retCode As Boolean = True
    ' use the data in the dataset to load the class information
    mPmAssignedDate = CheckForNull(dsIn.Tables(0).Rows(0).Item("pmAssignedDate"), "Date")
    mPmAssignedId = CheckForNull(dsIn.Tables(0).Rows(0).Item("pmAssignedId"), "Integer")
    mPmoManagedProject = CheckForNull(dsIn.Tables(0).Rows(0).Item("pmoManagedProject"), "Boolean")
    mProjectId = CheckForNull(dsIn.Tables(0).Rows(0).Item("projectId"), "Integer")
    mProjectStatusCode = CheckForNull(dsIn.Tables(0).Rows(0).Item("projectStatusCode"), "Integer")
    mProjectStatusAssignedById = CheckForNull(dsIn.Tables(0).Rows(0).Item("projectStatusAssignedById"), "Integer")
    mProjectStatusAssignedDate = CheckForNull(dsIn.Tables(0).Rows(0).Item("projectStatusAssignedDate"), "Date")
    mSowId = CheckForNull(dsIn.Tables(0).Rows(0).Item("sowId"), "Integer")
    Return retCode
  End Function
#End Region
#Region "Delete Routine"
  ''' Delete
  Public Function Delete() As Boolean
    Dim retCode As Boolean = True

    Try
      If Me.mProjectId > 0 Then
        Dim dbParms As New DbParmList
        dbParms.Add(New DbParm("@projectId", mProjectId))
        Dim dbMgr As CommonLib.DALDataManager
        dbMgr = CommonLib.DALDataManager.GetDatabaseObject
        Dim dbAccessor As New DbAccess

        Dim dbXfer As New DbXfer
        dbXfer.DatabaseMgr = CType(dbMgr, CommonLib.DALDataManager)
        'Database Name goes here...
        dbXfer.DatabaseName = "pmo"
        dbXfer.ParmList = dbParms

        If dbAccessor.Delete(dbXfer, "spDeleteProjectInfo") Then
          retCode = True
        Else
          retCode = False
        End If

      End If

    Catch exc As Exception
      Dim logEx As New PmoException(exc.Message)
      logEx.ExceptionFunction = "Delete()"
      logEx.ExceptionClass = ClassName()
      logEx.ExceptionStackTrace = exc.StackTrace
      logEx.ExceptionType = exc.GetType.ToString
      logEx.Persist()
      Throw logEx

    Finally

    End Try

    Return retCode
  End Function
#End Region
#Region "Class Constant Properties"
  Public ReadOnly Property ClassName()
    Get
      Return cClassName
    End Get
  End Property

  Public ReadOnly Property ClassAuthor()
    Get
      Return cClassAuthor
    End Get
  End Property

  Public ReadOnly Property ClassVersion()
    Get
      Return cClassVersion
    End Get
  End Property

#End Region

End Class
