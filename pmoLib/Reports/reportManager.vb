﻿Imports Microsoft.VisualBasic
Imports CommonLib
Imports System.Data
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports MySql

Public Class reportManager
  Private mDbMgr As DALDataManager
  Private Const ClassName As String = "reportManager"

  Public Sub New()
    mDbMgr = DALDataManager.GetDatabaseObject

    If mDbMgr Is Nothing Then
      Dim exc As New PmoException("PMO Manager instantiated without a current DALDataManager.")
      exc.ExceptionClass = ClassName
      exc.Persist()
      Throw exc
    End If
  End Sub
  Public Function GetOffTrackWebReport(Optional ByVal useReportHeader As Boolean = True) As String
    Dim pmoMgr As New pmoManager
    Dim rptTbl As New System.Text.StringBuilder
    If useReportHeader Then
      rptTbl.Append(GetReportHeader("Projects in Off Track Status"))
    End If
    Dim projList As ProjectList = pmoMgr.GetProjectListForStatus(pmoManager.ProjectStatusCode.OffTrack)

    If projList.Count > 0 Then

      rptTbl.Append("<table>")
      rptTbl.Append("<tr>")
      rptTbl.Append("<td>Project ID</td>")
      rptTbl.Append("<td>Customer</td>")
      rptTbl.Append("<td>Project Title</td>")
      rptTbl.Append("<td>Managed By</td>")
      rptTbl.Append("<td>SOW ID</td>")
      rptTbl.Append("<td>Status Assignment</td>")
      rptTbl.Append("</tr>")

      For Each proj As Project In projList
        rptTbl.Append("<tr>")
        rptTbl.Append("<td>" & proj.Id & "</td>")
        rptTbl.Append("<td>" & proj.Customer & "</td>")
        rptTbl.Append("<td>" & proj.Title & "</td>")
        rptTbl.Append("<td>" & proj.ManagedBy & "</td>")
        rptTbl.Append("<td>" & proj.SowId & "</td>")
        rptTbl.Append("<td>" & proj.ProjectStatusAssignment & "</td>")
        rptTbl.Append("</tr>")
      Next

      rptTbl.Append("</table>")
    Else
      rptTbl.Append("<br>")
      rptTbl.Append("There are no projects in OFF TRACK status at this time.")
    End If

    Return rptTbl.ToString
  End Function
  Public Function GetConcernedWebReport(Optional ByVal useReportHeader As Boolean = True) As String
    Dim pmoMgr As New pmoManager
    Dim rptTbl As New System.Text.StringBuilder
    If useReportHeader Then
      rptTbl.Append(GetReportHeader("Projects in Concerned Status"))
    End If
    Dim projList As ProjectList = pmoMgr.GetProjectListForStatus(pmoManager.ProjectStatusCode.Concerned)

    If projList.Count > 0 Then

      rptTbl.Append("<table width=100%>")
      rptTbl.Append("<tr>")
      rptTbl.Append("<td width=10% class=pmoReportHeader>Project ID</td>")
      rptTbl.Append("<td width=25% class=pmoReportHeader>Customer</td>")
      rptTbl.Append("<td width=30% class=pmoReportHeader>Project Title</td>")
      rptTbl.Append("<td width=10% class=pmoReportHeader>Managed By</td>")
      rptTbl.Append("<td width=10% class=pmoReportHeader>SOW ID</td>")
      rptTbl.Append("<td width=15% class=pmoReportHeader>Status Assignment</td>")
      rptTbl.Append("</tr>")

      For Each proj As Project In projList
        rptTbl.Append("<tr>")
        rptTbl.Append("<td class=pmoReportLine>" & proj.Id & "</td>")
        rptTbl.Append("<td class=pmoReportLine>" & proj.Customer & "</td>")
        rptTbl.Append("<td class=pmoReportLine>" & proj.Title & "</td>")
        rptTbl.Append("<td class=pmoReportLine>" & proj.ManagedBy & "</td>")
        rptTbl.Append("<td class=pmoReportLine align=center>" & proj.SowId & "</td>")
        rptTbl.Append("<td class=pmoReportLine>" & proj.ProjectStatusAssignment & "</td>")
        rptTbl.Append("</tr>")
      Next

      rptTbl.Append("</table>")
    Else
      rptTbl.Append("<br>")
      rptTbl.Append("<class=pmoReportNotification>There are no projects in CONCERNED status at this time.")
    End If

    Return rptTbl.ToString
  End Function
  Public Function GetNotSetWebReport(Optional ByVal useReportHeader As Boolean = True) As String
    Dim pmoMgr As New pmoManager
    Dim rptTbl As New System.Text.StringBuilder
    If useReportHeader Then
      rptTbl.Append(GetReportHeader("Projects Without A Status"))
    End If
    Dim projList As ProjectList = pmoMgr.GetProjectListForStatus(pmoManager.ProjectStatusCode.NotSet)
    Dim openList As ProjectList = pmoMgr.GetOpenProjectsList

    If projList.Count > 0 Then

      rptTbl.Append("<table width=100%>")
      rptTbl.Append("<tr>")
      rptTbl.Append("<td width=10% class=pmoReportHeader>Project ID</td>")
      rptTbl.Append("<td width=25% class=pmoReportHeader>Customer</td>")
      rptTbl.Append("<td width=30% class=pmoReportHeader>Project Title</td>")
      rptTbl.Append("<td width=10% class=pmoReportHeader>Managed By</td>")
      rptTbl.Append("<td width=10% class=pmoReportHeader>SOW ID</td>")
      rptTbl.Append("<td width=15% class=pmoReportHeader>Status Assignment</td>")
      rptTbl.Append("</tr>")

      Dim foundCount As Integer = 0
      For Each proj As Project In projList
        If openList.ProjectExists(proj.Id) Then
          foundCount += 1
          rptTbl.Append("<tr>")
          rptTbl.Append("<td class=pmoReportLine>" & proj.Id & "</td>")
          rptTbl.Append("<td class=pmoReportLine>" & proj.Customer & "</td>")
          rptTbl.Append("<td class=pmoReportLine>" & proj.Title & "</td>")
          rptTbl.Append("<td class=pmoReportLine>" & proj.ManagedBy & "</td>")
          rptTbl.Append("<td class=pmoReportLine align=center>" & proj.SowId & "</td>")
          rptTbl.Append("<td class=pmoReportLine>" & proj.ProjectStatusAssignment & "</td>")
          rptTbl.Append("</tr>")
        End If
      Next

      If foundCount = 0 Then
        rptTbl.Append("<class=pmoReportNotification>There are no projects in NOT SET status at this time.")
      End If
      rptTbl.Append("</table>")
      rptTbl.Append("</table>")
    Else
      rptTbl.Append("<br>")
      rptTbl.Append("<class=pmoReportNotification>There are no projects in NOT SET status at this time.")
    End If

    Return rptTbl.ToString
  End Function
  Public Function GetUnassignedSowReport(Optional ByVal useReportHeader As Boolean = True) As String
    Dim pmoMgr As New pmoManager
    Dim rptTbl As New System.Text.StringBuilder
    If useReportHeader Then
      rptTbl.Append(GetReportHeader("Unassigned Statements of Work (SOW)"))
    End If
    Dim sowList As SowList = pmoMgr.GetSowListing(False, True)

    If sowList.Count > 0 Then

      rptTbl.Append("<table width=100%>")
      rptTbl.Append("<tr>")
      rptTbl.Append("<td width=10% class=pmoReportHeader>SOW ID</td>")
      rptTbl.Append("<td width=25% class=pmoReportHeader>Customer</td>")
      rptTbl.Append("<td width=30% class=pmoReportHeader>SOW Title</td>")
      rptTbl.Append("<td width=10% class=pmoReportHeader>Sales Rep</td>")
      rptTbl.Append("<td width=10% class=pmoReportHeader>Created</td>")
      rptTbl.Append("<td width=15% class=pmoReportHeader>Approved</td>")
      rptTbl.Append("</tr>")

      For Each tmpSow As Sow In sowList
        rptTbl.Append("<tr>")
        rptTbl.Append("<td class=pmoReportLine>" & tmpSow.SowId & "</td>")
        rptTbl.Append("<td class=pmoReportLine>" & tmpSow.SowCustName & "</td>")
        rptTbl.Append("<td class=pmoReportLine>" & tmpSow.SowTitle & "</td>")
        If tmpSow.SalesRep Is Nothing Then
          rptTbl.Append("<td class=pmoReportLine>Sales Rep Not Set</td>")
        Else
          rptTbl.Append("<td class=pmoReportLine>" & tmpSow.SalesRep.FullName & "</td>")
        End If

        rptTbl.Append("<td class=pmoReportLine>" & CDate(tmpSow.CreatedDate).ToShortDateString & "</td>")
        rptTbl.Append("<td class=pmoReportLine>" & CDate(tmpSow.SowApprovedDate).ToShortDateString & "</td>")
        rptTbl.Append("</tr>")
      Next

      rptTbl.Append("</table>")
    Else
      rptTbl.Append("<br>")
      rptTbl.Append("<class=pmoReportNotification>There are no SOW Records whcih are Unassigned at this time.")
    End If

    Return rptTbl.ToString
  End Function
  Public Function GetSowWithoutDocumentReport(Optional ByVal useReportHeader As Boolean = True) As String
    Dim pmoMgr As New pmoManager
    Dim rptTbl As New System.Text.StringBuilder
    If useReportHeader Then
      rptTbl.Append(GetReportHeader("Statements of Work (SOW) Without Linked Document"))
    End If
    Dim sowList As SowList = pmoMgr.GetSowWithoutDocumentList()

    If sowList.Count > 0 Then

      rptTbl.Append("<table width=100%>")
      rptTbl.Append("<tr>")
      rptTbl.Append("<td width=10% class=pmoReportHeader>SOW ID</td>")
      rptTbl.Append("<td width=25% class=pmoReportHeader>Customer</td>")
      rptTbl.Append("<td width=30% class=pmoReportHeader>SOW Title</td>")
      rptTbl.Append("<td width=10% class=pmoReportHeader>Sales Rep</td>")
      rptTbl.Append("<td width=10% class=pmoReportHeader>Created</td>")
      rptTbl.Append("<td width=15% class=pmoReportHeader>Approved</td>")
      rptTbl.Append("</tr>")

      For Each tmpSow As Sow In sowList
        rptTbl.Append("<tr>")
        rptTbl.Append("<td class=pmoReportLine>" & tmpSow.SowId & "</td>")
        rptTbl.Append("<td class=pmoReportLine>" & tmpSow.SowCustName & "</td>")
        rptTbl.Append("<td class=pmoReportLine>" & tmpSow.SowTitle & "</td>")
        If tmpSow.SalesRep Is Nothing Then
          rptTbl.Append("<td class=pmoReportLine>Sales Rep Not Set</td>")
        Else
          rptTbl.Append("<td class=pmoReportLine>" & tmpSow.SalesRep.FullName & "</td>")
        End If

        rptTbl.Append("<td class=pmoReportLine>" & CDate(tmpSow.CreatedDate).ToShortDateString & "</td>")
        rptTbl.Append("<td class=pmoReportLine>" & CDate(tmpSow.SowApprovedDate).ToShortDateString & "</td>")
        rptTbl.Append("</tr>")
      Next

      rptTbl.Append("</table>")
    Else
      rptTbl.Append("<br>")
      rptTbl.Append("<class=pmoReportNotification>There are no SOW Records whcih are Unassigned at this time.")
    End If

    Return rptTbl.ToString
  End Function
  Private Function GetReportHeader(ByVal title As String) As String
    Dim hdrRow As New System.Text.StringBuilder
    hdrRow.Append("<table width=100%><tr><td width=15% class=pmoReportDate>" & Now.ToShortDateString & " @ " & Now.ToShortTimeString & "</td>")
    hdrRow.Append("<td width=70% align=center class=pmoReportTitle>" & title & "</td>")
    hdrRow.Append("<td width=15%>&nbsp</td>")
    hdrRow.Append("</tr><tr><td colspan = 3><hr></td>")
    hdrRow.Append("</tr></table>")
    Return hdrRow.ToString
  End Function
#Region "Nightly Reports"
  Public Function GetOffTrackNightlyReport(Optional ByVal userId As Integer = 0) As String
    Dim pmoMgr As New pmoManager
    Dim rptTbl As New System.Text.StringBuilder

    Dim projList As ProjectList = pmoMgr.GetProjectListForStatus(pmoManager.ProjectStatusCode.OffTrack)

    If projList.Count > 0 Then

      rptTbl.Append("<table>")
      rptTbl.Append("<tr><h4>")
      rptTbl.Append("<td width=10% class=pmoReportHeader>Project ID</td>")
      rptTbl.Append("<td width=25% class=pmoReportHeader>Customer</td>")
      rptTbl.Append("<td width=30% class=pmoReportHeader>Project Title</td>")
      rptTbl.Append("<td width=10% class=pmoReportHeader>Managed By</td>")
      rptTbl.Append("<td width=10% class=pmoReportHeader>Status Changed</td>")
      rptTbl.Append("<td width=15% class=pmoReportHeader></td>")
      rptTbl.Append("</h4></tr><tr><td colspan=6><hr></td></tr>")

      For Each proj As Project In projList
        If userId = 0 Then
          rptTbl.Append("<tr>")
          rptTbl.Append("<td class=pmoReportLine>" & proj.Id & "</td>")
          rptTbl.Append("<td class=pmoReportLine>" & proj.Customer & "</td>")
          rptTbl.Append("<td class=pmoReportLine>" & proj.Title & "</td>")
          rptTbl.Append("<td class=pmoReportLine>" & proj.ManagedBy & "</td>")
          rptTbl.Append("<td class=pmoReportLine>" & proj.ProjectStatusCodeAssignedDateAsString & "</td>")
          rptTbl.Append("<td class=pmoReportLine></td>")
          rptTbl.Append("</tr>")
        Else
          If proj.PmOwnerId = userId Then
            rptTbl.Append("<tr>")
            rptTbl.Append("<td class=pmoReportLine>" & proj.Id & "</td>")
            rptTbl.Append("<td class=pmoReportLine>" & proj.Customer & "</td>")
            rptTbl.Append("<td class=pmoReportLine>" & proj.Title & "</td>")
            rptTbl.Append("<td class=pmoReportLine>" & proj.ManagedBy & "</td>")
            rptTbl.Append("<td class=pmoReportLine>" & proj.ProjectStatusCodeAssignedDateAsString & "</td>")
            rptTbl.Append("<td class=pmoReportLine></td>")
            rptTbl.Append("</tr>")
          End If
        End If

      Next

      rptTbl.Append("</table>")
    Else
      'rptTbl.Append("<br>")
      rptTbl.Append("There are no projects in OFF TRACK status at this time.")
    End If

    Return rptTbl.ToString
  End Function
  Public Function GetConcernedNightlyReport(Optional ByVal userId As Integer = 0) As String
    Dim pmoMgr As New pmoManager
    Dim rptTbl As New System.Text.StringBuilder
    Dim projList As ProjectList = pmoMgr.GetProjectListForStatus(pmoManager.ProjectStatusCode.Concerned)

    If projList.Count > 0 Then
      rptTbl.Append("<table width=100%>")
      rptTbl.Append("<tr><h4>")
      rptTbl.Append("<td width=10% class=pmoReportHeader>Project ID</td>")
      rptTbl.Append("<td width=25% class=pmoReportHeader>Customer</td>")
      rptTbl.Append("<td width=30% class=pmoReportHeader>Project Title</td>")
      rptTbl.Append("<td width=10% class=pmoReportHeader>Managed By</td>")
      rptTbl.Append("<td width=10% class=pmoReportHeader>Status Changed</td>")
      rptTbl.Append("<td width=15% class=pmoReportHeader></td>")
      rptTbl.Append("</h4></tr><tr><td colspan=6><hr></td></tr>")

      For Each proj As Project In projList
        If userId = 0 Then
          rptTbl.Append("<tr>")
          rptTbl.Append("<td class=pmoReportLine>" & proj.Id & "</td>")
          rptTbl.Append("<td class=pmoReportLine>" & proj.Customer & "</td>")
          rptTbl.Append("<td class=pmoReportLine>" & proj.Title & "</td>")
          rptTbl.Append("<td class=pmoReportLine>" & proj.ManagedBy & "</td>")
          rptTbl.Append("<td class=pmoReportLine>" & proj.ProjectStatusCodeAssignedDateAsString & "</td>")
          rptTbl.Append("<td class=pmoReportLine></td>")
          rptTbl.Append("</tr>")
        Else
          If proj.PmOwnerId = userId Then
            rptTbl.Append("<tr>")
            rptTbl.Append("<td class=pmoReportLine>" & proj.Id & "</td>")
            rptTbl.Append("<td class=pmoReportLine>" & proj.Customer & "</td>")
            rptTbl.Append("<td class=pmoReportLine>" & proj.Title & "</td>")
            rptTbl.Append("<td class=pmoReportLine>" & proj.ManagedBy & "</td>")
            rptTbl.Append("<td class=pmoReportLine>" & proj.ProjectStatusCodeAssignedDateAsString & "</td>")
            rptTbl.Append("<td class=pmoReportLine></td>")
            rptTbl.Append("</tr>")
          End If
        End If
      Next

      rptTbl.Append("</table>")
    Else
      'rptTbl.Append("<br>")
      rptTbl.Append("<class=pmoReportNotification>There are no projects in CONCERNED status at this time.")
    End If

    Return rptTbl.ToString
  End Function
  Public Function GetNotSetNightlyReport(Optional ByVal userId As Integer = 0) As String
    Dim pmoMgr As New pmoManager
    Dim rptTbl As New System.Text.StringBuilder

    Dim projList As ProjectList = pmoMgr.GetProjectListForStatus(pmoManager.ProjectStatusCode.NotSet)
    Dim openList As ProjectList = pmoMgr.GetOpenProjectsList

    If projList.Count > 0 Then
      rptTbl.Append("<table width=100%>")
      rptTbl.Append("<tr><h4>")
      rptTbl.Append("<td width=10% class=pmoReportHeader>Project ID</td>")
      rptTbl.Append("<td width=25% class=pmoReportHeader>Customer</td>")
      rptTbl.Append("<td width=30% class=pmoReportHeader>Project Title</td>")
      rptTbl.Append("<td width=10% class=pmoReportHeader>Managed By</td>")
      rptTbl.Append("<td width=10% class=pmoReportHeader>Status Changed</td>")
      rptTbl.Append("<td width=15% class=pmoReportHeader></td>")
      rptTbl.Append("</h4></tr><tr><td colspan=6><hr></td></tr>")

      Dim foundCount As Integer = 0
      For Each proj As Project In projList
        If openList.ProjectExists(proj.Id) Then
          foundCount += 1
          If userId = 0 Then
            rptTbl.Append("<tr>")
            rptTbl.Append("<td class=pmoReportLine>" & proj.Id & "</td>")
            rptTbl.Append("<td class=pmoReportLine>" & proj.Customer & "</td>")
            rptTbl.Append("<td class=pmoReportLine>" & proj.Title & "</td>")
            rptTbl.Append("<td class=pmoReportLine>" & proj.ManagedBy & "</td>")
            rptTbl.Append("<td class=pmoReportLine>" & proj.ProjectStatusCodeAssignedDateAsString & "</td>")
            rptTbl.Append("<td class=pmoReportLine></td>")
            rptTbl.Append("</tr>")
          Else
            If proj.PmOwnerId = userId Then
              rptTbl.Append("<tr>")
              rptTbl.Append("<td class=pmoReportLine>" & proj.Id & "</td>")
              rptTbl.Append("<td class=pmoReportLine>" & proj.Customer & "</td>")
              rptTbl.Append("<td class=pmoReportLine>" & proj.Title & "</td>")
              rptTbl.Append("<td class=pmoReportLine>" & proj.ManagedBy & "</td>")
              rptTbl.Append("<td class=pmoReportLine>" & proj.ProjectStatusCodeAssignedDateAsString & "</td>")
              rptTbl.Append("<td class=pmoReportLine></td>")
              rptTbl.Append("</tr>")
            End If
          End If
        End If
      Next
      If foundCount = 0 Then
        rptTbl.Append("<class=pmoReportNotification>There are no projects in NOT SET status at this time.")
      End If
      rptTbl.Append("</table>")
    Else
      'rptTbl.Append("<br>")
      rptTbl.Append("<class=pmoReportNotification>There are no projects in NOT SET status at this time.")
    End If

    Return rptTbl.ToString
  End Function
  Public Function GetStatusDeltaNightlyReport(Optional ByVal useReportHeader As Boolean = False) As String
    Dim pmoMgr As New pmoManager
    Dim rptTbl As New System.Text.StringBuilder
    Dim projList As ProjectList = pmoMgr.GetProjectListForChangedStatus(Today.ToShortDateString, Today.ToShortDateString)

    If useReportHeader Then
      rptTbl.Append(GetReportHeader("Status Changes - Today"))
    End If

    If projList.Count > 0 Then
      rptTbl.Append("<table width=100%>")
      rptTbl.Append("<tr><h4>")
      rptTbl.Append("<td width=10% class=pmoReportHeader>Project ID</td>")
      rptTbl.Append("<td width=25% class=pmoReportHeader>Customer</td>")
      rptTbl.Append("<td width=30% class=pmoReportHeader>Project Title</td>")
      rptTbl.Append("<td width=10% class=pmoReportHeader>Managed By</td>")
      rptTbl.Append("<td width=10% class=pmoReportHeader>New Status</td>")
      rptTbl.Append("<td width=15% class=pmoReportHeader>Status Changed</td>")
      rptTbl.Append("</h4></tr><tr><td colspan=6><hr></td></tr>")

      For Each proj As Project In projList
        rptTbl.Append("<tr>")
        rptTbl.Append("<td class=pmoReportLine>" & proj.Id & "</td>")
        rptTbl.Append("<td class=pmoReportLine>" & proj.Customer & "</td>")
        rptTbl.Append("<td class=pmoReportLine>" & proj.Title & "</td>")
        rptTbl.Append("<td class=pmoReportLine>" & proj.ManagedBy & "</td>")
        rptTbl.Append("<td class=pmoReportLine align=center>" & proj.CurrentStatusDescription & "</td>")
        rptTbl.Append("<td class=pmoReportLine>" & proj.ProjectStatusCodeAssignedDateAsString & "</td>")
        rptTbl.Append("</tr>")
      Next

      rptTbl.Append("</table>")
    Else
      'rptTbl.Append("<br>")
      rptTbl.Append("<class=pmoReportNotification>There are no projects which had their Status changed on:" & Today.ToShortDateString)
    End If

    Return rptTbl.ToString
  End Function
#End Region
#Region "Weekly Reports"
  Public Function GetStatusDeltaWeeklyReport(Optional ByVal useReportHeader As Boolean = False) As String
    Dim pmoMgr As New pmoManager
    Dim rptTbl As New System.Text.StringBuilder
    Dim weekStart As Date = WeekStartDate(Today)
    Dim weekEnd As Date = WeekEndDate(Today)

    If useReportHeader Then
      rptTbl.Append(GetReportHeader("Status Changes - This Week"))
    End If

    Dim projList As ProjectList = pmoMgr.GetProjectListForChangedStatus(weekStart, weekEnd)

    If projList.Count > 0 Then
      rptTbl.Append("<table width=100%>")
      rptTbl.Append("<tr><h4>")
      rptTbl.Append("<td width=10% class=pmoReportHeader>Project ID</td>")
      rptTbl.Append("<td width=25% class=pmoReportHeader>Customer</td>")
      rptTbl.Append("<td width=30% class=pmoReportHeader>Project Title</td>")
      rptTbl.Append("<td width=10% class=pmoReportHeader>Managed By</td>")
      rptTbl.Append("<td width=10% class=pmoReportHeader>New Status</td>")
      rptTbl.Append("<td width=15% class=pmoReportHeader></td>")
      rptTbl.Append("</h4></tr><tr><td colspan=6><hr></td></tr>")

      For Each proj As Project In projList
        rptTbl.Append("<tr>")
        rptTbl.Append("<td class=pmoReportLine>" & proj.Id & "</td>")
        rptTbl.Append("<td class=pmoReportLine>" & proj.Customer & "</td>")
        rptTbl.Append("<td class=pmoReportLine>" & proj.Title & "</td>")
        rptTbl.Append("<td class=pmoReportLine>" & proj.ManagedBy & "</td>")
        rptTbl.Append("<td class=pmoReportLine align=center>" & proj.CurrentStatusDescription & "</td>")
        rptTbl.Append("<td class=pmoReportLine></td>")
        rptTbl.Append("</tr>")
      Next

      rptTbl.Append("</table>")
    Else
      'rptTbl.Append("<br>")
      rptTbl.Append("<class=pmoReportNotification>There are no projects which had their Status changed during the week of:" & weekStart.ToShortDateString & " through " & weekEnd.ToShortDateString)
    End If

    Return rptTbl.ToString
  End Function
#End Region
End Class
