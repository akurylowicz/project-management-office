Imports CommonLib

Public Class Sow

#Region "Class Constants"
  ''' Class Info Constants
  Private Const cClassName As String = "Sow"
  Private Const cClassAuthor As String = "SQL Object Generator"
  Private Const cClassVersion As String = "Version 2.0.a"
#End Region

#Region "Class Members"
  ''' Class Members
  Private mCreatedBy As String
  Private mCreatedDate As Date
  Private mUpdatedBy As String
  Private mUpdatedDate As Date
  Private mSowCustName As String
  Private mSowEstStartDate As Date
  Private mSowFileLink As String
  Private mSowId As Integer
  Private mSowKey As Integer          ' Primary Key to table... 
  Private mSowTitle As String
  Private mIsNew As Boolean
  Private mNeedsSave As Boolean
  Private mLoadedFromDb As Boolean
  Private mSowInactive As Boolean
  Private mSowSalesRepId As Integer
  Private mSowLeadTechId As Integer
  Private mSowApprovedByPmoDate As Date
  Private mSowNotes As String

  Private mSalesRep As PmoUser
  Private mLeadTech As PmoUser
    Private mProjectId As Integer

  'Added code to support SOW documents as binary.
  Private mSowFileContent() As Byte
  Private mSowFileExtension As String
    Private mSowFileName As String

    'Added to support Blockl Time Contract identification
    Private mIsBtc As Boolean

    '10/22/2013 - Add Practice
    Private mSowPracticeId As Integer

#End Region

#Region "Constructors"
  ''' Simple Constructor
  Public Sub New()
    Initialize()
    Me.mIsNew = True
    Me.mLoadedFromDb = False
    Me.mNeedsSave = True
  End Sub
  ''' Complex Constructor
  Public Sub New(ByVal sowKeyIn As Integer)
    Initialize()
    mSowKey = sowKeyIn
    Load()
  End Sub
  ''' Initialization Routines
  Private Sub Initialize()
    mCreatedBy = ""
    mCreatedDate = Nothing
    mUpdatedBy = ""
    mUpdatedDate = Nothing
    mSowCustName = ""
    mSowEstStartDate = Nothing
    mSowFileLink = ""
    mSowId = 0
    mSowKey = 0
    mSowTitle = ""
    mIsNew = False
    mNeedsSave = False
    mLoadedFromDb = False
    Me.mSowInactive = False
    mSowSalesRepId = 0
    mSowLeadTechId = 0
    mSowApprovedByPmoDate = Nothing
    mSowNotes = ""
        Me.mProjectId = 0
    Me.mSowFileContent = Nothing
    Me.mSowFileExtension = ""
        Me.mSowFileName = ""
        Me.mIsBtc = False
        Me.mSowPracticeId = 0
  End Sub
#End Region

#Region "Member Properties"
  ''' Member Properties
  ''' 
  Public Property SowFileName() As String
    Get
      Return mSowFileName
    End Get
    Set(ByVal Value As String)
      mSowFileName = Value
      Me.mNeedsSave = True
    End Set
  End Property
  Public Property SowFileExtension() As String
    Get
      Return mSowFileExtension
    End Get
    Set(ByVal Value As String)
      mSowFileExtension = Value
      Me.mNeedsSave = True
    End Set
  End Property
  Public Property SowFileContent() As Byte()
    Get
      Return mSowFileContent
    End Get
    Set(ByVal Value As Byte())
      Me.mSowFileContent = Value
      Me.mNeedsSave = True
    End Set
  End Property
    'Public Property ProjectId() As Integer
    '  Get
    '    Return Me.mProjectId
    '  End Get
    '  Set(ByVal value As Integer)
    '    Me.mProjectId = value
    '  End Set
    'End Property
  Public Property SalesRepId() As Integer
    Get
      Return Me.mSowSalesRepId
    End Get
    Set(ByVal value As Integer)
      Me.mSowSalesRepId = value
      If Me.mSowSalesRepId > 0 Then
        Me.mSalesRep = New PmoUser(Me.mSowSalesRepId)
      Else
        Me.mSalesRep = Nothing
      End If
    End Set
  End Property
  Public Property LeadTechId() As Integer
    Get
      Return Me.mSowLeadTechId
    End Get
    Set(ByVal value As Integer)
      Me.mSowLeadTechId = value
      If Me.mSowLeadTechId > 0 Then
        Me.mLeadTech = New PmoUser(Me.mSowLeadTechId)
      Else
        Me.mLeadTech = Nothing
      End If
    End Set
  End Property
  Public ReadOnly Property SalesRep() As PmoUser
    Get
      If Me.mSalesRep Is Nothing Then
        If Me.mSowSalesRepId > 0 Then
          Me.mSalesRep = New PmoUser(Me.mSowSalesRepId)
        End If
      End If
      Return Me.mSalesRep
    End Get
  End Property
  Public ReadOnly Property LeadTech() As PmoUser
    Get
      If Me.mLeadTech Is Nothing Then
        If Me.mSowLeadTechId > 0 Then
          Me.mLeadTech = New PmoUser(Me.mSowLeadTechId)
        End If
      End If
      Return Me.mSalesRep
    End Get
  End Property
  Public Property SowApprovedDate() As Date
    Get
      Return Me.mSowApprovedByPmoDate
    End Get
    Set(ByVal value As Date)
      Me.mSowApprovedByPmoDate = value
    End Set
  End Property
  Public Property Notes() As String
    Get
      Return Me.mSowNotes
    End Get
    Set(ByVal value As String)
      Me.mSowNotes = value
    End Set
  End Property
  Public Property Inactive() As Boolean
    Get
      Return Me.mSowInactive
    End Get
    Set(ByVal value As Boolean)
      Me.mSowInactive = value
    End Set
    End Property
    Public Property IsBlockTimeContract() As Boolean
        Get
            Return Me.mIsBtc
        End Get
        Set(ByVal value As Boolean)
            Me.mIsBtc = value
        End Set
    End Property
  Public ReadOnly Property IsActive() As Boolean
    Get
      Return Not Me.mSowInactive
    End Get
  End Property
  Public ReadOnly Property CreatedBy() As String
    Get
      Return mCreatedBy
    End Get
  End Property
  Public ReadOnly Property CreatedDate() As Object
    Get
      Return mCreatedDate
    End Get
  End Property
  Public ReadOnly Property UpdatedBy() As String
    Get
      Return Me.mUpdatedBy
    End Get
  End Property
  Public ReadOnly Property UpdatedDate() As Date
    Get
      Return Me.mUpdatedDate
    End Get
  End Property
  Public Property SowCustName() As String
    Get
      Return mSowCustName
    End Get
    Set(ByVal Value As String)
      mSowCustName = Value
      Me.mNeedsSave = True
    End Set
  End Property

  Public Property SowEstStartDate() As Date
    Get
      Return mSowEstStartDate
    End Get
    Set(ByVal Value As Date)
      mSowEstStartDate = Value
      Me.mNeedsSave = True
    End Set
  End Property

  Public Property SowFileLink() As String
    Get
      Return mSowFileLink
    End Get
    Set(ByVal Value As String)
      mSowFileLink = Value
      Me.mNeedsSave = True
    End Set
  End Property

  Public Property SowId() As Integer
    Get
      Return mSowId
    End Get
    Set(ByVal Value As Integer)
      mSowId = Value
      Me.mNeedsSave = True
    End Set
  End Property
    Public Property SowPracticeId As Integer
        Get
            Return Me.mSowPracticeId
        End Get
        Set(value As Integer)
            Me.mSowPracticeId = value
        End Set
    End Property
    Public ReadOnly Property SowPracticeName As String
        Get
            Dim retVal As String = ""
            Dim theMgr As New pmoManager
            retVal = theMgr.GetPracticeName(Me.mSowPracticeId)
            Return retVal
        End Get
    End Property
  Public Property SowKey() As Integer
    Get
      Return mSowKey
    End Get
    Set(ByVal Value As Integer)
      mSowKey = Value
      Me.mNeedsSave = True
    End Set
  End Property

  Public Property SowTitle() As String
    Get
      Return mSowTitle
    End Get
    Set(ByVal Value As String)
      mSowTitle = Value
      Me.mNeedsSave = True
    End Set
  End Property

  Public Property IsNew() As Boolean
    Get
      Return mIsNew
    End Get
    Set(ByVal Value As Boolean)
      mIsNew = Value
      Me.mNeedsSave = True
    End Set
  End Property

  Public Property NeedsSave() As Boolean
    Get
      Return mNeedsSave
    End Get
    Set(ByVal Value As Boolean)
      mNeedsSave = Value
      Me.mNeedsSave = True
    End Set
  End Property

  Public Property LoadedFromDb() As Boolean
    Get
      Return mLoadedFromDb
    End Get
    Set(ByVal Value As Boolean)
      mLoadedFromDb = Value
      Me.mNeedsSave = True
    End Set
  End Property

#End Region

#Region "Persistence Routines"
    Public Function Persist(ByVal currUser As String) As Boolean
        Dim retCode As Boolean = True

        If Me.Validate.Count > 0 Then
            Return False
        End If

        Try
            Dim dbParms As New DbParmList
            If mSowKey > 0 Then
                dbParms.Add(New DbParm("@sowKey", mSowKey))
            Else
                CreateNewSowId()
                dbParms.Add(New DbParm("@sowKey", mSowKey))
                Me.mCreatedDate = Now
                Me.mCreatedBy = currUser
            End If

            dbParms.Add(New DbParm("@sowId", mSowId))
            dbParms.Add(New DbParm("@createdBy", mCreatedBy))
            If IsValidDate(Me.mCreatedDate) Then
                dbParms.Add(New DbParm("@createdDate", mCreatedDate))
            Else
                dbParms.Add(New DbParm("@createdDate", DBNull.Value))
            End If

            dbParms.Add(New DbParm("@updatedBy", currUser))
            dbParms.Add(New DbParm("@updatedDate", Now))

            dbParms.Add(New DbParm("@sowCustName", mSowCustName))
            If IsValidDate(Me.mSowEstStartDate) Then
                dbParms.Add(New DbParm("@sowEstStartDate", mSowEstStartDate))
            Else
                dbParms.Add(New DbParm("@sowEstStartDate", DBNull.Value))
            End If

            If IsValidDate(Me.mSowApprovedByPmoDate) Then
                dbParms.Add(New DbParm("@sowApprovedByPmoDate", mSowApprovedByPmoDate))
            Else
                dbParms.Add(New DbParm("@sowApprovedByPmoDate", DBNull.Value))
            End If

            dbParms.Add(New DbParm("@sowSalesRepId", mSowSalesRepId))
            dbParms.Add(New DbParm("@sowLeadTechId", mSowLeadTechId))
            dbParms.Add(New DbParm("@sowNotes", mSowNotes))
            dbParms.Add(New DbParm("@sowFileLink", mSowFileLink))
            dbParms.Add(New DbParm("@sowTitle", mSowTitle))
            dbParms.Add(New DbParm("@sowInactive", mSowInactive))
            dbParms.Add(New DbParm("@projectId", mProjectId))
            dbParms.Add(New DbParm("@sowFileName", mSowFileName))
            dbParms.Add(New DbParm("@sowFileExtension", mSowFileExtension))
            dbParms.Add(New DbParm("@sowFileContent", mSowFileContent))

            dbParms.Add(New DbParm("@isBlockTime", mIsBtc))
            dbParms.Add(New DbParm("@practiceId", mSowPracticeId))

            Dim dbMgr As DALDataManager
            dbMgr = DALDataManager.GetDatabaseObject
            Dim dbAccessor As New DbAccess

            Dim dbXfer As New DbXfer
            dbXfer.DatabaseMgr = CType(dbMgr, DALDataManager)
            'Database Name goes here...
            dbXfer.DatabaseName = "pmo"
            dbXfer.ParmList = dbParms

            If mSowKey > 0 Then
                If dbAccessor.Update(dbXfer, "spUpdateSow") Then
                    retCode = True
                    Me.mNeedsSave = False
                End If
            Else
                If dbAccessor.Insert(dbXfer, "spInsertSow", True, "spUpdateSow") Then
                    mSowKey = dbAccessor.ReturnValue
                    retCode = True
                    Me.mNeedsSave = False
                    Me.mIsNew = False
                End If
            End If


        Catch exc As Exception
            Dim logEx As New PmoException(exc.Message)
            logEx.ExceptionFunction = "Persist()"
            logEx.ExceptionClass = ClassName()
            logEx.ExceptionStackTrace = exc.StackTrace
            logEx.ExceptionType = exc.GetType.ToString
            logEx.Persist()
            Throw logEx
        Finally

        End Try

        Return retCode
    End Function
  Private Sub CreateNewSowId()
    Dim pmoMgr As New pmoManager
    Me.mSowId = pmoMgr.EstablishNewSow()
    Me.mSowKey = pmoMgr.GetKeyForSow(Me.mSowId)
  End Sub
  Public Function Validate() As UtilValidationList
    Dim objValList As New UtilValidationList
    ' perform validation edits.  Whenever an error is found, instantiate a
    ' UtilValidation object and add it to the Validation List

    Try
      ' TODO: Add Validation Code

    Catch exc As Exception
      Dim logEx As New PmoException(exc.Message)
      logEx.ExceptionFunction = "Validate()"
      logEx.ExceptionClass = ClassName()
      logEx.ExceptionStackTrace = exc.StackTrace
      logEx.ExceptionType = exc.GetType.ToString
      logEx.Persist()
      Throw logEx

    Finally

    End Try

    Return objValList
  End Function
#End Region

#Region "Load Routines"
  ''' Load
  Private Function Load() As Boolean
    Dim retCode As Boolean = True
    Dim ds As DataSet

    ' Load the dataset... then call LoadFromDataset(ds)... please change manager object and key value as needed.
    If Me.mSowKey > 0 Then
      Dim pmoMgr As New pmoManager

      Try
        ds = pmoMgr.GetSow(Me.mSowKey)
        If ds.Tables(0).Rows.Count > 0 Then
          retCode = LoadFromDataset(ds)
          'CheckAndRepairFile()
          If retCode = True Then
            Me.mIsNew = False
            Me.mLoadedFromDb = True
            Me.mNeedsSave = False
          End If
        End If

      Catch exc As Exception
        Dim logEx As New PmoException(exc.Message)
        logEx.ExceptionFunction = "Load()"
        logEx.ExceptionClass = ClassName()
        logEx.ExceptionStackTrace = exc.StackTrace
        logEx.ExceptionType = exc.GetType.ToString
        logEx.Persist()
        Throw logEx

      Finally

      End Try

    Else
      Dim logEx As New PmoException("Attempt to insantiate object without key value.")
      logEx.ExceptionFunction = "Load()"
      logEx.ExceptionClass = ClassName()
      logEx.Persist()
      Throw logEx
    End If

    Return retCode
  End Function
  Private Sub CheckAndRepairFile()
    'Dim theMgr As New pmoManager
    ''If Me.mSowInactive Then
    'If Me.SowFileLink > "" Then
    '  Dim tmpOpen As String = ""
    '  Me.mSowFileLink = Me.mSowFileLink.ToLower
    '  Try
    '    If Me.mSowKey = 108 Then
    '      Dim i As Integer = 10
    '    End If
    '    tmpOpen = Me.mSowFileLink.Replace("file:\\\\\ost-nas2\office\", "Z:\")
    '    Dim fs As New System.IO.FileStream(tmpOpen, System.IO.FileMode.Open, System.IO.FileAccess.Read)
    '    Dim fileData(fs.Length) As Byte
    '    fs.Read(fileData, 0, fs.Length)
    '    fs.Close()
    '    Me.mSowFileName = theMgr.GetFileName(Me.mSowFileLink)
    '    Me.mSowFileExtension = theMgr.GetFileExtension(Me.mSowFileName)
    '    Me.mSowFileContent = fileData
    '    Me.mSowFileLink = ""
    '    Me.Persist(149)
    '  Catch
    '    Dim theFile As String = tmpOpen
    '  End Try
    'End If
    'End If
  End Sub
    Private Function LoadFromDataset(ByVal dsIn As DataSet) As Boolean
        Dim retCode As Boolean = True
        ' use the data in the dataset to load the class information
        mCreatedBy = CheckForNull(dsIn.Tables(0).Rows(0).Item("createdBy"), "String")
        mCreatedDate = CheckForNull(dsIn.Tables(0).Rows(0).Item("createdDate"), "Date")
        mUpdatedBy = CheckForNull(dsIn.Tables(0).Rows(0).Item("updatedBy"), "String")
        mUpdatedDate = CheckForNull(dsIn.Tables(0).Rows(0).Item("updatedDate"), "Date")
        mSowCustName = CheckForNull(dsIn.Tables(0).Rows(0).Item("sowCustName"), "String")
        mSowEstStartDate = CheckForNull(dsIn.Tables(0).Rows(0).Item("sowEstStartDate"), "Date")
        mSowFileLink = CheckForNull(dsIn.Tables(0).Rows(0).Item("sowFileLink"), "String")
        mSowId = CheckForNull(dsIn.Tables(0).Rows(0).Item("sowId"), "Integer")
        mSowKey = CheckForNull(dsIn.Tables(0).Rows(0).Item("sowKey"), "Integer")
        mSowTitle = CheckForNull(dsIn.Tables(0).Rows(0).Item("sowTitle"), "String")
        mSowInactive = CheckForNull(dsIn.Tables(0).Rows(0).Item("sowInactive"), "Boolean")
        mSowSalesRepId = CheckForNull(dsIn.Tables(0).Rows(0).Item("sowSalesRepId"), "Integer")
        mSowLeadTechId = CheckForNull(dsIn.Tables(0).Rows(0).Item("sowLeadTechId"), "Integer")
        mSowNotes = CheckForNull(dsIn.Tables(0).Rows(0).Item("sowNotes"), "String")
        mSowApprovedByPmoDate = CheckForNull(dsIn.Tables(0).Rows(0).Item("sowApprovedByPmoDate"), "Date")
        'mProjectId = CheckForNull(dsIn.Tables(0).Rows(0).Item("projectId"), "Integer")
        mSowFileName = CheckForNull(dsIn.Tables(0).Rows(0).Item("sowFileName"), "String")
        mSowFileExtension = CheckForNull(dsIn.Tables(0).Rows(0).Item("sowFileExtension"), "String")
        mSowFileContent = CheckForNull(dsIn.Tables(0).Rows(0).Item("sowFileContent"), "Object")
        mIsBtc = CheckForNull(dsIn.Tables(0).Rows(0).Item("isBlockTime"), "Boolean")
        mSowPracticeId = CheckForNull(dsIn.Tables(0).Rows(0).Item("practiceId"), "Integer")
        Return retCode
    End Function
#End Region

#Region "Delete Routine"
  Public Function Delete() As Boolean
    Dim retCode As Boolean = True

    Try
      If Me.mSowKey > 0 Then
        Dim dbParms As New DbParmList
        dbParms.Add(New DbParm("@sowKey", mSowKey))
        Dim dbMgr As CommonLib.DALDataManager
        dbMgr = CommonLib.DALDataManager.GetDatabaseObject
        Dim dbAccessor As New DbAccess

        Dim dbXfer As New DbXfer
        dbXfer.DatabaseMgr = CType(dbMgr, CommonLib.DALDataManager)
        'Database Name goes here...
        dbXfer.DatabaseName = "pmo"
        dbXfer.ParmList = dbParms

        If dbAccessor.Delete(dbXfer, "spDeleteSow") Then
          retCode = True
        Else
          retCode = False
        End If

      End If

    Catch exc As Exception
      Dim logEx As New PmoException(exc.Message)
      logEx.ExceptionFunction = "Delete()"
      logEx.ExceptionClass = ClassName()
      logEx.ExceptionStackTrace = exc.StackTrace
      logEx.ExceptionType = exc.GetType.ToString
      logEx.Persist()
      Throw logEx

    Finally

    End Try

    Return retCode
  End Function
#End Region

#Region "Class Constant Properties"
  Public ReadOnly Property ClassName()
    Get
      Return cClassName
    End Get
  End Property

  Public ReadOnly Property ClassAuthor()
    Get
      Return cClassAuthor
    End Get
  End Property

  Public ReadOnly Property ClassVersion()
    Get
      Return cClassVersion
    End Get
  End Property

#End Region

End Class
