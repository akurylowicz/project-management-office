Imports CommonLib

Public Class ProjectStatusHistory

#Region "Class Constants"
  ''' Class Info Constants
  Private Const cClassName As String = "ProjectStatusHistory"
  Private Const cClassAuthor As String = "SQL Object Generator"
  Private Const cClassVersion As String = "Version 2.0.a"
#End Region

#Region "Class Members"
  ''' Class Members
  Private mProjId As Integer
  Private mRecordKey As Integer          ' Primary Key to table... 
  Private mStatusChangeById As Integer
  Private mStatusChangeDate As Date
  Private mStatusChangeFromId As Integer
  Private mStatusChangeNote As String
  Private mStatusChangeToId As Integer
  Private mIsNew As Boolean
  Private mNeedsSave As Boolean
  Private mLoadedFromDb As Boolean
#End Region

#Region "Constructors"
  ''' Simple Constructor
  Public Sub New()
    Initialize()
    Me.mIsNew = True
    Me.mLoadedFromDb = False
    Me.mNeedsSave = True
  End Sub
  ''' Complex Constructor
  Public Sub New(ByVal recordKeyIn As Integer)
    Initialize()
    mRecordKey = recordKeyIn
    Load()
  End Sub
  ''' Initialization Routines
  Private Sub Initialize()
    mProjId = 0
    mRecordKey = 0
    mStatusChangeById = 0
    mStatusChangeDate = Nothing
    mStatusChangeFromId = 0
    mStatusChangeNote = ""
    mStatusChangeToId = 0
    mIsNew = False
    mNeedsSave = False
    mLoadedFromDb = False
  End Sub
#End Region

#Region "Member Properties"
  ''' Member Properties
  Public Property ProjId() As Integer
    Get
      Return mProjId
    End Get
    Set(ByVal Value As Integer)
      mProjId = Value
      Me.mNeedsSave = True
    End Set
  End Property

  Public Property RecordKey() As Integer
    Get
      Return mRecordKey
    End Get
    Set(ByVal Value As Integer)
      mRecordKey = Value
      Me.mNeedsSave = True
    End Set
  End Property

  Public Property StatusChangeById() As Integer
    Get
      Return mStatusChangeById
    End Get
    Set(ByVal Value As Integer)
      mStatusChangeById = Value
      Me.mNeedsSave = True
    End Set
  End Property

  Public Property StatusChangeDate() As Date
    Get
      Return mStatusChangeDate
    End Get
    Set(ByVal Value As Date)
      mStatusChangeDate = Value
      Me.mNeedsSave = True
    End Set
  End Property

  Public Property StatusChangeFromId() As Integer
    Get
      Return mStatusChangeFromId
    End Get
    Set(ByVal Value As Integer)
      mStatusChangeFromId = Value
      Me.mNeedsSave = True
    End Set
  End Property

  Public Property StatusChangeNote() As String
    Get
      Return mStatusChangeNote
    End Get
    Set(ByVal Value As String)
      mStatusChangeNote = Value
      Me.mNeedsSave = True
    End Set
  End Property

  Public Property StatusChangeToId() As Integer
    Get
      Return mStatusChangeToId
    End Get
    Set(ByVal Value As Integer)
      mStatusChangeToId = Value
      Me.mNeedsSave = True
    End Set
  End Property

  Public Property IsNew() As Boolean
    Get
      Return mIsNew
    End Get
    Set(ByVal Value As Boolean)
      mIsNew = Value
      Me.mNeedsSave = True
    End Set
  End Property

  Public Property NeedsSave() As Boolean
    Get
      Return mNeedsSave
    End Get
    Set(ByVal Value As Boolean)
      mNeedsSave = Value
      Me.mNeedsSave = True
    End Set
  End Property

  Public Property LoadedFromDb() As Boolean
    Get
      Return mLoadedFromDb
    End Get
    Set(ByVal Value As Boolean)
      mLoadedFromDb = Value
      Me.mNeedsSave = True
    End Set
  End Property

#End Region

#Region "Persistence Routines"
  Public Function Persist() As Boolean
    Dim retCode As Boolean = True

    If Me.Validate.Count > 0 Then
      Return False
    End If

    Try
      ' TODO: Add Persistence Code
      Dim dbParms As New DbParmList
      If mRecordKey > 0 Then
        dbParms.Add(New DbParm("@recordKey", mRecordKey))
      End If
      dbParms.Add(New DbParm("@projId", mProjId))
      dbParms.Add(New DbParm("@statusChangeById", mStatusChangeById))
      dbParms.Add(New DbParm("@statusChangeDate", mStatusChangeDate))
      dbParms.Add(New DbParm("@statusChangeFromId", mStatusChangeFromId))
      dbParms.Add(New DbParm("@statusChangeNote", mStatusChangeNote))
      dbParms.Add(New DbParm("@statusChangeToId", mStatusChangeToId))

      Dim dbMgr As DALDataManager
      dbMgr = DALDataManager.GetDatabaseObject
      Dim dbAccessor As New DbAccess

      Dim dbXfer As New DbXfer
      dbXfer.DatabaseMgr = CType(dbMgr, DALDataManager)
      'Database Name goes here...
      dbXfer.DatabaseName = "pmo"
      dbXfer.ParmList = dbParms

      If mRecordKey > 0 Then
        If dbAccessor.Update(dbXfer, "spUpdateProjectStatusHistory") Then
          retCode = True
          Me.mNeedsSave = False
        End If
      Else
        If dbAccessor.Insert(dbXfer, "spInsertProjectStatusHistory", True, "spUpdateProjectStatusHistory") Then
          mRecordKey = dbAccessor.ReturnValue
          retCode = True
          Me.mNeedsSave = False
          Me.mIsNew = False
        End If
      End If

    Catch exc As Exception
      Dim logEx As New PmoException(exc.Message)
      logEx.ExceptionFunction = "Persist()"
      logEx.ExceptionClass = ClassName()
      logEx.ExceptionStackTrace = exc.StackTrace
      logEx.ExceptionType = exc.GetType.ToString
      logEx.Persist()
      Throw logEx

    Finally

    End Try

    Return retCode
  End Function
  Public Function Validate() As UtilValidationList
    Dim objValList As New UtilValidationList
    ' perform validation edits.  Whenever an error is found, instantiate a
    ' UtilValidation object and add it to the Validation List

    Try
      ' TODO: Add Validation Code

    Catch exc As Exception
      Dim logEx As New PmoException(exc.Message)
      logEx.ExceptionFunction = "Validate()"
      logEx.ExceptionClass = ClassName()
      logEx.ExceptionStackTrace = exc.StackTrace
      logEx.ExceptionType = exc.GetType.ToString
      logEx.Persist()
      Throw logEx

    Finally

    End Try

    Return objValList
  End Function
#End Region

#Region "Load Routines"
  Private Function Load() As Boolean
    Dim retCode As Boolean = True
    Dim ds As DataSet

    ' Load the dataset... then call LoadFromDataset(ds)... please change manager object and key value as needed.
    If Me.mRecordKey > 0 Then
      Dim pmoMgr As New pmoManager

      Try
        ds = pmoMgr.GetProjectStatusHistory(Me.mRecordKey)
        If ds.Tables(0).Rows.Count > 0 Then
          retCode = LoadFromDataset(ds)
          If retCode = True Then
            Me.mIsNew = False
            Me.mLoadedFromDb = True
            Me.mNeedsSave = False
          End If
        End If

      Catch exc As Exception
        Dim logEx As New PmoException(exc.Message)
        logEx.ExceptionFunction = "Load()"
        logEx.ExceptionClass = ClassName()
        logEx.ExceptionStackTrace = exc.StackTrace
        logEx.ExceptionType = exc.GetType.ToString
        logEx.Persist()
        Throw logEx

      Finally

      End Try

    Else
      Dim logEx As New PmoException("Attempt to insantiate object without key value.")
      logEx.ExceptionFunction = "Load()"
      logEx.ExceptionClass = ClassName()
      logEx.Persist()
      Throw logEx
    End If

    Return retCode
  End Function
  ''' Load From Dataset
  Private Function LoadFromDataset(ByVal dsIn As DataSet) As Boolean
    Dim retCode As Boolean = True
    ' use the data in the dataset to load the class information
    mProjId = CheckForNull(dsIn.Tables(0).Rows(0).Item("projId"), "Integer")
    mRecordKey = CheckForNull(dsIn.Tables(0).Rows(0).Item("recordKey"), "Integer")
    mStatusChangeById = CheckForNull(dsIn.Tables(0).Rows(0).Item("statusChangeById"), "Integer")
    mStatusChangeDate = CheckForNull(dsIn.Tables(0).Rows(0).Item("statusChangeDate"), "Date")
    mStatusChangeFromId = CheckForNull(dsIn.Tables(0).Rows(0).Item("statusChangeFromId"), "Integer")
    mStatusChangeNote = CheckForNull(dsIn.Tables(0).Rows(0).Item("statusChangeNote"), "String")
    mStatusChangeToId = CheckForNull(dsIn.Tables(0).Rows(0).Item("statusChangeToId"), "Integer")

    Return retCode
  End Function
#End Region

#Region "Delete Routine"
  ''' Delete
  Public Function Delete() As Boolean
    Dim retCode As Boolean = True

    Try
      If Me.mRecordKey > 0 Then
        Dim dbParms As New DbParmList
        dbParms.Add(New DbParm("@recordKey", mRecordKey))
        Dim dbMgr As CommonLib.DALDataManager
        dbMgr = CommonLib.DALDataManager.GetDatabaseObject
        Dim dbAccessor As New DbAccess

        Dim dbXfer As New DbXfer
        dbXfer.DatabaseMgr = CType(dbMgr, CommonLib.DALDataManager)
        'Database Name goes here...
        dbXfer.DatabaseName = "pmo"
        dbXfer.ParmList = dbParms

        If dbAccessor.Delete(dbXfer, "spDeleteProjectStatusHistory") Then
          retCode = True
        Else
          retCode = False
        End If

      End If

    Catch exc As Exception
      Dim logEx As New PmoException(exc.Message)
      logEx.ExceptionFunction = "Delete()"
      logEx.ExceptionClass = ClassName()
      logEx.ExceptionStackTrace = exc.StackTrace
      logEx.ExceptionType = exc.GetType.ToString
      logEx.Persist()
      Throw logEx

    Finally

    End Try

    Return retCode
  End Function
#End Region

#Region "Class Constant Properties"
  Public ReadOnly Property ClassName()
    Get
      Return cClassName
    End Get
  End Property

  Public ReadOnly Property ClassAuthor()
    Get
      Return cClassAuthor
    End Get
  End Property

  Public ReadOnly Property ClassVersion()
    Get
      Return cClassVersion
    End Get
  End Property

#End Region

End Class
