Imports CommonLib

'public class PmoDocument

'#Region "Class Constants"
'  ''' Class Info Constants
'  private const cClassName as string = "PmoDocument"
'  private const cClassAuthor as string = "SQL Object Generator"
'  private const cClassVersion as string = "Version 2.0.a"
'#End Region

'#Region "Class Members"
'  ''' Class Members
'  private mDateAdded as Date
'  private mFileLink as String
'  private mFileName as String
'  private mRecordKey as Integer          ' Primary Key to table... 
'  Private mIsNew as Boolean
'  Private mNeedsSave as Boolean
'  Private mLoadedFromDb as Boolean
'#End Region

'#Region "Constructors"
'  ''' Simple Constructor
'Public Sub New()
'  Initialize()
'  Me.mIsNew = True
'  Me.mLoadedFromDb = False
'  Me.mNeedsSave = True
'End Sub
'  ''' Complex Constructor
'Public Sub New(byval recordKeyIn as Integer)
'  Initialize()
'  mRecordKey = recordKeyIn
'  Load()
'End Sub
'  ''' Initialization Routines
'Private Sub Initialize()
'  mDateAdded = nothing
'  mFileLink = ""
'  mFileName = ""
'  mRecordKey = 0
'  mIsNew = False
'  mNeedsSave = False
'  mLoadedFromDb = False
'End Sub
'#End Region

'#Region "Member Properties"
'  ''' Member Properties
'Public Property DateAdded() as Date
'	Get
'		return mDateAdded
'	End Get
'	Set(byval Value as Date)
'		mDateAdded = Value
'		Me.mNeedsSave = True
'	End Set
'End Property

'Public Property FileLink() as String
'	Get
'		return mFileLink
'	End Get
'	Set(byval Value as String)
'		mFileLink = Value
'		Me.mNeedsSave = True
'	End Set
'End Property

'Public Property FileName() as String
'	Get
'		return mFileName
'	End Get
'	Set(byval Value as String)
'		mFileName = Value
'		Me.mNeedsSave = True
'	End Set
'End Property

'Public Property RecordKey() as Integer
'	Get
'		return mRecordKey
'	End Get
'	Set(byval Value as Integer)
'		mRecordKey = Value
'		Me.mNeedsSave = True
'	End Set
'End Property

'Public Property IsNew() as Boolean
'	Get
'		return mIsNew
'	End Get
'	Set(byval Value as Boolean)
'		mIsNew = Value
'		Me.mNeedsSave = True
'	End Set
'End Property

'Public Property NeedsSave() as Boolean
'	Get
'		return mNeedsSave
'	End Get
'	Set(byval Value as Boolean)
'		mNeedsSave = Value
'		Me.mNeedsSave = True
'	End Set
'End Property

'Public Property LoadedFromDb() as Boolean
'	Get
'		return mLoadedFromDb
'	End Get
'	Set(byval Value as Boolean)
'		mLoadedFromDb = Value
'		Me.mNeedsSave = True
'	End Set
'End Property

'#End Region

'#Region "Persistence Routines"
'Public function Persist() as boolean
'  dim retCode as boolean = True

'  if me.Validate.count > 0 then
'    return false
'  end if

'  try
'  ' TODO: Add Persistence Code
'  Dim dbParms as New DbParmList
'  If mrecordKey > 0 then
'     dbParms.Add(New DbParm("@recordKey", mrecordKey))
'  End If
'    dbParms.Add(New DbParm("@dateAdded", mdateAdded))
'    dbParms.Add(New DbParm("@fileLink", mfileLink))
'    dbParms.Add(New DbParm("@fileName", mfileName))

'    Dim dbMgr As DALDataManager
'    dbMgr = DALDataManager.GetDatabaseObject
'    Dim dbAccessor As New DbAccess

'    Dim dbXfer As New DbXfer
'    dbXfer.DatabaseMgr = CType(dbMgr, DALDataManager)
'    'Database Name goes here...
'    dbXfer.DatabaseName = "pmo"
'    dbXfer.ParmList = dbParms

'      If mRecordKey > 0 Then
'        If dbAccessor.Update(dbXfer, "spUpdatePmoDocument") Then
'          retCode = True
'          Me.mNeedsSave = False
'        End If
'      Else
'        If dbAccessor.Insert(dbXfer, "spInsertPmoDocument", True, "spUpdatePmoDocument") Then
'          mRecordKey = dbAccessor.ReturnValue
'          retCode = True
'          Me.mNeedsSave = False
'          Me.mIsNew = False
'        End If
'      End If


'    Catch exc As Exception
'      Dim logEx As New PmoException(exc.Message)
'      logEx.ExceptionFunction = "Persist()"
'      logEx.ExceptionClass = ClassName()
'      logEx.ExceptionStackTrace = exc.StackTrace
'      logEx.ExceptionType = exc.GetType.ToString
'      logEx.Persist()
'      Throw logEx
'    Finally

'    End Try

'  return retCode
'end function
'Public function Validate() as UtilValidationList
'  dim objValList as new UtilValidationList
'  ' perform validation edits.  Whenever an error is found, instantiate a
'  ' UtilValidation object and add it to the Validation List

'  try
'  ' TODO: Add Validation Code

'  catch exc as Exception
'      Dim logEx As New PmoException(exc.Message)
'      logEx.ExceptionFunction = "Validate()"
'      logEx.ExceptionClass = ClassName()
'      logEx.ExceptionStackTrace = exc.StackTrace
'      logEx.ExceptionType = exc.GetType.ToString
'      logEx.Persist()
'      Throw logEx

'  finally

'  end try

'  return objValList
'end function
'#End Region

'#Region "Load Routines"
'  ''' Load
'Private function Load() as boolean
'  dim retCode as boolean = True
'  dim ds as dataset

'  ' Load the dataset... then call LoadFromDataset(ds)... please change manager object and key value as needed.
'   If Me.mrecordKey > 0 Then
'     Dim pmoMgr as New pmoManager

'     try
'        ds = pmoMgr.GetPmoDocument(Me.mRecordKey)
'     If ds.Tables(0).Rows.Count > 0 Then
'        retCode = LoadFromDataset(ds)
'        if retCode = True then
'           Me.mIsNew = False
'           Me.mLoadedFromDb = True
'           Me.mNeedsSave = False
'        End If
'     End If

'     catch exc as Exception
'    Dim logEx As New LogException(exc.Message)
'    logEx.ExceptionFunction = "Load()"
'    logEx.ExeptionModule = ClassName()
'    logEx.LogException()
'    Throw logEx

'     finally

'     end try

'  Else
'    Dim logEx As New LogException("Attempt to insantiate object without key value.")
'    logEx.ExceptionFunction = "Load()"
'    logEx.ExeptionModule = ClassName()
'    logEx.LogException()
'    Throw logEx
'  End If

'  return retCode
'end function
'  ''' Load From Dataset
'Private function LoadFromDataset(dsIn as dataset) as boolean
'  dim retCode as boolean = True
'  ' use the data in the dataset to load the class information
'  mDateAdded = CheckForNull(dsIn.tables(0).rows(0).item("dateAdded"), "Date")
'  mFileLink = CheckForNull(dsIn.tables(0).rows(0).item("fileLink"), "String")
'  mFileName = CheckForNull(dsIn.tables(0).rows(0).item("fileName"), "String")
'  mRecordKey = CheckForNull(dsIn.tables(0).rows(0).item("recordKey"), "Integer")

'  return retCode
'end function
'#End Region

'#Region "Delete Routine"
'  ''' Delete
'Public function Delete() as boolean
'  dim retCode as boolean = True

'  try
'   If Me.mrecordKey > 0 Then
'  Dim dbParms as New DbParmList
'  dbParms.Add(New DbParm("@recordKey", mrecordKey))
'    Dim dbMgr As CommonLib.DALDataManager
'    dbMgr = CommonLib.DALDataManager.GetDatabaseObject
'    Dim dbAccessor As New DbAccess

'    Dim dbXfer As New DbXfer
'    dbXfer.DatabaseMgr = CType(dbMgr, CommonLib.DALDataManager)
'    'Database Name goes here...
'    dbXfer.DatabaseName = "pmo"
'    dbXfer.ParmList = dbParms

'    If dbAccessor.Delete(dbXfer, "spDeletePmoDocument") Then
'      retCode = True
'    Else
'      retCode = False
'    End If

'  End If

'  catch exc as Exception
'    Dim logEx As New LogException(exc.Message)
'    logEx.ExceptionFunction = "Delete()"
'    logEx.ExeptionModule = ClassName()
'    logEx.LogException()
'    Throw logEx

'  finally

'  end try

'  return retCode
'end function
'#End Region

'#Region "Class Constant Properties"
'Public Readonly Property ClassName()
'	Get
'		return cClassName
'	End Get
'End Property

'Public Readonly Property ClassAuthor()
'	Get
'		return cClassAuthor
'	End Get
'End Property

'Public Readonly Property ClassVersion()
'	Get
'		return cClassVersion
'	End Get
'End Property

'#End Region

'End Class
