﻿Public Class OldSurveyRequestList
    Inherits System.Collections.CollectionBase

    Public Enum SortDirection
        Ascending
        Descending
    End Enum

    Public Sub Add(ByVal aMember As OldSurveyRequest)
        If Not ItemExists(aMember) Then
            List.Add(aMember)
        End If
    End Sub

    Public Sub Remove(ByVal index As Integer)
        If index > Count - 1 Or index < 0 Then
            Dim qacException As New Exception("The index passed to the Remove method does not exist in the Collection")
            Throw qacException
        Else
            List.RemoveAt(index)
        End If
    End Sub

    Public ReadOnly Property Item(ByVal index As Integer) As OldSurveyRequest
        Get
            If index > Count - 1 Or index < 0 Then
                Dim qacException As New Exception("The index passed to the Item method does not exist in the Collection")
                Throw qacException
            Else
                Return CType(List.Item(index), OldSurveyRequest)
            End If
        End Get
    End Property

    Private Function ItemExists(ByVal aMember As OldSurveyRequest) As Boolean
        Dim retCode As Boolean = False
        '
        'TODO: Put in the item exists check code
        '
        Return retCode
    End Function

    Public Sub Sort(ByVal sortMember As String, ByVal sortDir As SortDirection)
        '
        'TODO: Put in the sort code
        '
    End Sub
End Class
