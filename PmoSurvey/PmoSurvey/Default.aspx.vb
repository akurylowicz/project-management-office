﻿Imports pmoLib
Partial Public Class _Default
  Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Context.Session Is DBNull.Value Then
            If Session.IsNewSession Then
                Dim szCookieHeader As String = Request.Headers("Cookie")
                If Not szCookieHeader Is Nothing Then
                    If Not szCookieHeader Is DBNull.Value And szCookieHeader.IndexOf("ASP.NET_SessionId") >= 0 Then
                        Response.Redirect("~/Security/frmTimeOut.aspx")
                    End If
                End If
            End If
        End If
        If Request.QueryString("requestId") > "" Then
            Dim survReq As New CpSurveyRequest(CInt(Request.QueryString("requestId")))

            If Not survReq Is Nothing Then
                If survReq.IsComplete Then
                    Session("currSurveyRequestID") = survReq.RequestId
                    Response.Redirect("~/Survey/frmShowSurvey.aspx")
                Else
                    Session("currSurveyRequestID") = survReq.RequestId
                    Response.Redirect("~/Survey/frmSurvey.aspx")
                End If
            End If
        End If
    End Sub
End Class