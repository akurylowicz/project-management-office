﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="ucThanksMessage.ascx.vb" Inherits="PmoSurvey.ucThanksMessage" %>
<%@ Register src="../Measures/ucRecommend.ascx" tagname="ucRecommend" tagprefix="uc1" %>
<style type="text/css">
    .style1
    {
        width: 100%;
    }
</style>
<table class="style1">
    <tr>
        <td width="10%">
            &nbsp;</td>
        <td align="center">
            <asp:Label ID="Label2" runat="server" Text="Thanks for sharing!" 
              CssClass="thankYouLabel"></asp:Label>
        </td>
        <td width="10%">
            &nbsp;</td>
    </tr>
    <tr>
        <td>
            &nbsp;</td>
        <td>
            &nbsp;</td>
        <td>
            &nbsp;</td>
    </tr>
    <tr>
        <td>
            &nbsp;</td>
        <td align="center">
            <asp:Label ID="Label1" runat="server" CssClass="thankYouLabel"></asp:Label>
        </td>
        <td>
            &nbsp;</td>
    </tr>
    <tr>
        <td>
            &nbsp;</td>
        <td>
            &nbsp;</td>
        <td>
            &nbsp;</td>
    </tr>
    <tr>
        <td>
            &nbsp;</td>
        <td>
            <uc1:ucRecommend ID="ucRecommend1" runat="server" />
        </td>
        <td>
            &nbsp;</td>
    </tr>
    <tr>
        <td>
            &nbsp;</td>
        <td align="center">
            <asp:Button ID="btnMoreSurveys" runat="server" Text="Button" Visible="False" />
        </td>
        <td>
            &nbsp;</td>
    </tr>
    <tr>
        <td>
            &nbsp;</td>
        <td>
            &nbsp;</td>
        <td>
            &nbsp;</td>
    </tr>
    <tr>
        <td>
            &nbsp;</td>
        <td align="center">
            <asp:HyperLink ID="HyperLink1" runat="server" 
                NavigateUrl="http://www.ostusa.com" CssClass="pmoLink">OST Web Page</asp:HyperLink>
        </td>
        <td>
            &nbsp;</td>
    </tr>
</table>
