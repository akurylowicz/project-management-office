﻿Imports pmoLib
Partial Public Class ucThanksMessage
  Inherits System.Web.UI.UserControl

  Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
    If Session("currSurveyRequestEmail") > "" Then
      Dim theMgr As New pmoLib.pmoManager
      Dim ds As DataSet = theMgr.GetAllSurveyRequests(Session("currSurveyRequestEmail"))
      If ds.Tables(0).Rows.Count > 0 Then
        Me.btnMoreSurveys.Text = "You have " & ds.Tables(0).Rows.Count & " more OST Survey Requests.  Please click to go to the list..."
        Me.btnMoreSurveys.Visible = True
      Else
        Me.btnMoreSurveys.Visible = False
        Session("currSurveyRequestEmail") = Nothing
      End If
    End If
    Me.Label1.Text = "Thank you for taking the time to provide OST with feedback.  We like to know what our clients think of us, especially whether or not they would recommend OST.  Below is a representation of our scores over time on these client surveys with respect to our clients being willing to recommend us.  Thank you for your business!"
  End Sub

  Protected Sub btnMoreSurveys_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnMoreSurveys.Click
    Response.Redirect("~/Survey/frmSurveyList.aspx")
  End Sub
End Class