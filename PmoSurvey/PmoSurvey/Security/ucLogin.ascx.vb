﻿Public Partial Class ucLogin
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub

  Protected Sub btnTakeSurvey_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnTakeSurvey.Click
        If Me.txtEmail.Text > "" Then
            Dim theMgr As New pmoLib.pmoManager
            Dim ds As DataSet = theMgr.GetAllCpSurveyRequests(Me.txtEmail.Text)
            If ds.Tables(0).Rows.Count > 0 Then
                If ds.Tables(0).Rows.Count = 1 Then
                    Session("currSurveyRequestID") = ds.Tables(0).Rows(0).Item("requestId")
                    Response.Redirect("~/Survey/frmSurvey.aspx")
                Else
                    Session("currSurveyRequestEmail") = Me.txtEmail.Text
                    Response.Redirect("~/Survey/frmSurveyList.aspx")
                End If
            Else
                Me.lblMessage.Text = "Survey request for this Email Address was not found.  Please try again.  If you need further help, please contact OST at (616) 574-3500."
            End If
        Else
            Me.lblMessage.Text = "An Email Address is required in order to access the Survey.  Please try again.  If you need further help, please contact OST at (616) 574-3500."
        End If
  End Sub
End Class