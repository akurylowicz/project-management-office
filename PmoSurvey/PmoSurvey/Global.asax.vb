﻿Imports System.Web.SessionState

Public Class Global_asax
    Inherits System.Web.HttpApplication

    Sub Application_Start(ByVal sender As Object, ByVal e As EventArgs)
        ' Fires when the application is started
    End Sub

    Sub Session_Start(ByVal sender As Object, ByVal e As EventArgs)

    'This string connects to the old VM_TRACY database - Windows security...
    'CommonLib.DALDataManager.CreateDatabaseObject("Data Source=VM_TRACY;Initial Catalog=pmo;Integrated Security=True")

    'This string connects to the new VM_TRACY database - SQL Security...
    'CommonLib.DALDataManager.CreateDatabaseObject("Data Source=VM_TRACY;Initial Catalog=pmo;User Id=sa_pmo;PWD=n56!L98")

    'This string connects to the DEV database...
    'CommonLib.DALDataManager.CreateDatabaseObject("Data Source=ost-sql01;Initial Catalog=pmo_dev;User Id=sa_pmo;PWD=n56!L98")
    'Session("isDebug") = True

    'This string connects to the PROD database...
        'CommonLib.DALDataManager.CreateDatabaseObject("Data Source=ost-sql01;Initial Catalog=pmo;User Id=sa_pmo;PWD=n56!L98")
        CommonLib.DALDataManager.CreateDatabaseObject("Data Source=ost-sql01;Initial Catalog=pmo_v2;User Id=sa_pmo;PWD=n56!L98")
        Session("ConnStringHold") = "Data Source=ost-sql01;Initial Catalog=pmo_v2;User Id=sa_pmo;PWD=n56!L98"
        Session("isDebug") = False

    'This string connects to the local database...
    'CommonLib.DALDataManager.CreateDatabaseObject("Data Source=OST-JOHNV\JVANCIL_DEV;Initial Catalog=pmo;Integrated Security=True")
    End Sub

    Sub Application_BeginRequest(ByVal sender As Object, ByVal e As EventArgs)
        ' Fires at the beginning of each request
    End Sub

    Sub Application_AuthenticateRequest(ByVal sender As Object, ByVal e As EventArgs)
        ' Fires upon attempting to authenticate the use
    End Sub

    Sub Application_Error(ByVal sender As Object, ByVal e As EventArgs)
        ' Fires when an error occurs
    End Sub

    Sub Session_End(ByVal sender As Object, ByVal e As EventArgs)
        ' Fires when the session ends
    End Sub

    Sub Application_End(ByVal sender As Object, ByVal e As EventArgs)
        ' Fires when the application ends
    End Sub

End Class