﻿Imports pmoLib
Public Class Validate
  Inherits System.Web.UI.Page

  Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
    Dim requestId As Integer = 1
        Try
            Dim result As New CpSurveyResult(requestId)
            If Not result Is Nothing Then
                If result.EmailAddress > "" Then
                    'Dim theProj As New CpProject(result.ProjectId)
                    'If Not theProj Is Nothing Then
                    Me.Literal1.Text = "Validated"
                    'Else
                    'Me.Literal1.Text = "Invalid - Project could not be instantiated - Likely MySql issue."
                    'End If
                Else
                    Me.Literal1.Text = "Invalid - No Email Address - Likely SQL Server issue"
                End If
            Else
                Me.Literal1.Text = "Invalid - Database Call Failed"
            End If
        Catch ex As Exception
            Me.Literal1.Text = "Invalid - SurveyResult(1) instantiation threw an exception."
        End Try
  End Sub

End Class