﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="ucSurveyHeader.ascx.vb" Inherits="PmoSurvey.ucSurveyHeader" %>
<asp:Panel ID="pnlSurround" runat="server" Width="100%" BackColor="#9DA116">
<table width="100%">
    <tr>
        <td width="25%" align="left">
            <asp:Label ID="Label2" runat="server" CssClass="whiteLabel" Text="Project:"></asp:Label>
        </td>
        <td align="left">
            <asp:Label ID="lblId" runat="server" CssClass="whiteField"></asp:Label>
        </td>
    </tr>
    <tr>
        <td align="left">
            <asp:Label ID="Label3" runat="server" CssClass="whiteLabel" Text="Customer:"></asp:Label>
        </td>
        <td align="left">
            <asp:Label ID="lblCustomer" runat="server" CssClass="whiteField"></asp:Label>
        </td>
    </tr>
    <tr>
        <td align="left">
            <asp:Label ID="Label4" runat="server" CssClass="whiteLabel" 
                Text="Survey Requested:"></asp:Label>
        </td>
        <td align="left">
            <asp:Label ID="lblSurveyRequested" runat="server" CssClass="whiteField"></asp:Label>
        </td>
    </tr>
</table>
</asp:Panel>