﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Default.aspx.vb" Inherits="PmoSurvey._Default" %>

<%@ Register src="ucControls/ucHeader.ascx" tagname="ucHeader" tagprefix="uc1" %>
<%@ Register src="Security/ucLogin.ascx" tagname="ucLogin" tagprefix="uc2" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
     <title>OST Client Satisfaction Survey</title>
    <link href=ost.css rel=Stylesheet />
    <script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-17261996-2']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:Panel runat="server" ID="centerPanel" CssClass="centeringPanel">
    <uc1:ucHeader ID="ucHeader1" runat="server" />
   
  <table width="100%">
  <tr>
  <td width = 3%>&nbsp</td>
  <td>&nbsp</td>
  <td width = 3%>&nbsp</td>
  </tr>
  <tr>
    <td>&nbsp</td>
    <td><uc2:ucLogin ID="ucLogin1" runat="server" /></td>
    <td>&nbsp</td>
  </tr></table>
  </asp:Panel>
  </form>
  
</body>
</html>
