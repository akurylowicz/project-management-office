﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmTimeOut.aspx.vb" Inherits="PmoSurvey.frmTimeOut" %>

<%@ Register src="../ucControls/ucHeader.ascx" tagname="ucHeader" tagprefix="uc3" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>PMO Portal - Application Error</title>
    <link href="../ost.css" rel="stylesheet" />
    <style type="text/css">

.menuLink
{
	font-family: Tahoma;
	font-size: small;
	color: #666666;
	text-decoration: none;
}
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
        <uc3:ucHeader ID="ucHeader1" runat="server" />
    
    </div>
        <p>
    <asp:HyperLink ID="hlRestart" runat="server" CssClass="menuLink" 
        NavigateUrl="~/Default.aspx" Target="_self">Your Survey Session has timed out.  Please click here to restart the session.</asp:HyperLink>
        </p>
    </form>
</body>
</html>
