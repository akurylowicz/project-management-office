﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="ucLogin.ascx.vb" Inherits="PmoSurvey.ucLogin" %>

<asp:Panel ID="Panel1" runat="server" CssClass="panelSurround">
<table width="100%">
    <tr>
        <td width="50%">
            &nbsp;</td>
        <td>
            &nbsp;</td>
    </tr>
    <tr>
        <td align="right">
            <asp:Label ID="Label1" runat="server" CssClass="normalLabel" 
                Text="Please enter the Email Address  from the Survey Invitation:"></asp:Label>
        </td>
        <td>
            <asp:TextBox ID="txtEmail" runat="server" CssClass="normalField" 
                MaxLength="128" Width="300px"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td>
            &nbsp;</td>
        <td>
            <asp:Button ID="btnTakeSurvey" runat="server" CssClass="normalLabel" 
              Text="Take the Survey..." />
        </td>
    </tr>
    <tr>
        <td>
            &nbsp;</td>
        <td>
            <asp:Label ID="lblMessage" runat="server" CssClass="messageLabel"></asp:Label>
        </td>
    </tr>
    <tr>
        <td>
            &nbsp;</td>
        <td >
            &nbsp;</td>
    </tr>
</table>
</asp:Panel>
