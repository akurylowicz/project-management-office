﻿Public Partial Class ucReccomend
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
    LoadReccomendationMeasure()
  End Sub
  Private Sub LoadReccomendationMeasure()
    Dim theMgr As New pmoLib.pmoManager
    Dim reccTotal As Integer
    Dim reccAvg As Double
    Dim reccCount As Integer

    Dim ds As DataSet = theMgr.GetReccomendationScores

    Dim counter As Integer = 0
    While counter < ds.Tables(0).Rows.Count
            If ds.Tables(0).Rows(counter).Item("score") > 0 Then
                reccCount += 1
                reccTotal += CInt(ds.Tables(0).Rows(counter).Item("score"))
            End If
      counter += 1
    End While

    If reccCount > 0 Then
      reccAvg = Math.Round(reccTotal / reccCount, 2)
    Else
      reccAvg = 0
    End If

    Me.lblResult.Text = "Current average score for all surveys with regard to Recommend OST is: " & reccAvg & ". "
    Me.lblResult2.Text = "This score is an average of the results from " & reccCount & " surveys, and is based upon a 10 point scale, with 10 being the higher selection."

  End Sub

End Class