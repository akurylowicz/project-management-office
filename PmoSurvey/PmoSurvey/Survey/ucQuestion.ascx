﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="ucQuestion.ascx.vb" Inherits="PmoSurvey.ucQuestion" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<asp:Panel ID="pnlSurround" runat="server" Width="100%">
<table width="100%">
    <tr>
        <td width="20%">
            <asp:Label ID="lblQuestion" runat="server" CssClass="normalLabel"></asp:Label>
        </td>
        <td align="right" width="10%">
            <asp:Label ID="Label2" runat="server" Text="(Negative)" CssClass="normalLabel"></asp:Label>
        </td>
        <td align ="center" width="30%">
            <asp:TextBox ID="txtScore" runat="server" ></asp:TextBox>
            <asp:SliderExtender ID="txtScore_SliderExtender" runat="server" Length="250" 
                Maximum="10" Minimum="1" TargetControlID="txtScore" 
              BoundControlID="lblScore" RaiseChangeOnlyOnMouseUp="False">
            </asp:SliderExtender>
        </td>
        <td align="left" width="10%">
            <asp:Label ID="Label3" runat="server" Text="(Positive)" CssClass="normalLabel"></asp:Label>
        </td>
        <td width="5%" align="center">
            <asp:Label ID="lblScore" runat="server" CssClass="normalLabel" Text=""></asp:Label>
        </td>
        <td align="right" width="25%">
          <asp:CheckBox ID="cbNotApplicable" runat="server" CssClass="normalField" 
            Text="Not Applicable / No Answer" />
        </td>
    </tr>
    <tr>
        <td align="right" valign="top">
            <asp:Label ID="lblComment" runat="server" Enabled="False" Font-Italic="False" 
              CssClass="normalLabel">Comment</asp:Label>
        </td>
        <td colspan="5">
            <asp:TextBox ID="txtComment" runat="server" MaxLength="500" 
                TextMode="MultiLine" Width="99%" CssClass="normalField"></asp:TextBox>
        </td>
    </tr>
</table>
<hr />
</asp:Panel>
