﻿Imports pmoLib
Imports CommonLib

Partial Public Class ucGatherSurveyData
  Inherits System.Web.UI.UserControl

  Private Const cSurveyUserMask As Integer = 99999

  Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
    If Not Page.IsPostBack Then
      Session("JSEnabled") = True
      LoadProjectRoles()
    End If

    If Session("JSEnabled") = True Then
      LoadQuestions()
      Me.lbNoSlider.Visible = True
    Else
      LoadQuestions_NoJavascript()
      Me.lbNoSlider.Visible = False
    End If

  End Sub
  Private Sub LoadProjectRoles()
    Dim theMgr As New pmoManager
    Dim ds As DataSet = theMgr.GetAllProjectPositions

    Dim tmpRow As DataRow = ds.Tables(0).NewRow
    tmpRow.Item("projectPositionId") = 0
    tmpRow.Item("projectPosition") = "Please Select..."
    ds.Tables(0).Rows.InsertAt(tmpRow, 0)

    Me.cmbProjectRole.DataSource = ds
    Me.cmbProjectRole.DataValueField = "projectPositionId"
    Me.cmbProjectRole.DataTextField = "projectPosition"
    Me.cmbProjectRole.DataBind()

  End Sub
  Private Sub SubmitSurvey()
        If ScreenFieldsAreValid() Then
            Dim requestId As Integer = Session("currSurveyRequestID")

            Dim request As New CpSurveyRequest(requestId)
            Dim result As New CpSurveyResult
            result.EmailAddress = request.EmailAddress
            result.ProjectPositionId = Me.cmbProjectRole.SelectedValue
            result.OverallComments = Me.txtProjectComments.Text
            result.RequestId = request.RequestId
            result.ProjectTitle = request.ProjectName
            result.ProjectCustomer = request.CustomerName

            If result.Persist() Then
                request.CompleteDate = Today.ToShortDateString
                request.Persist()

                For Each ctrl As Control In Me.surveyQuestions.Controls
                    If TypeOf ctrl Is ucQuestion Then
                        Dim tmpCtrl As ucQuestion = ctrl
                        Dim survResult As New SurveyResultScore
                        survResult.QuestionId = tmpCtrl.QuestionId
                        survResult.RequestId = requestId
                        survResult.DisplayOrder = tmpCtrl.DisplayOrder
                        survResult.Score = tmpCtrl.QuestionScore

                        If survResult.Persist() Then
                            If tmpCtrl.QuestionComment > "" Then
                                Dim survComment As New SurveyResultComment
                                survComment.QuestionId = tmpCtrl.QuestionId
                                survComment.RequestId = requestId
                                survComment.DisplayOrder = tmpCtrl.DisplayOrder
                                survComment.Comment = tmpCtrl.QuestionComment
                                survComment.ResultKey = survResult.ResultKey
                                survComment.Persist()
                            End If
                        Else
                            Session("abendIssue") = "An Error has occured while saving the Survey Results Questions."
                            Response.Redirect("~/Security/frmIssue.aspx")
                        End If
                        'Me.lblValues.Text += tmpCtrl.QuestionId & " - " & tmpCtrl.QuestionScore & " - " & tmpCtrl.QuestionComment
                    End If
                Next
            Else
                Session("abendIssue") = "An Error has occured while saving the Survey Results Header."
                Response.Redirect("~/Security/frmIssue.aspx")
            End If

            'If this is an On Demand Survey, we do not want to update the table... only for final surveys.
            If request.OnDemandRequest = False Then
                Dim pmoMgr As New pmoManager
                pmoMgr.MarkCpProjectFeedbackComplete(request.ProjectId, cSurveyUserMask)
            End If

            'If we are in debug mode and using the pmo_dev then do not send the email..
            If Session("isDebug") = False Then
                Dim theEmail As New SmtpEmail
                theEmail.ToField = "ost-pmo@ostusa.com"
                theEmail.FromField = request.EmailAddress
                theEmail.Subject = "OST Client Satisfaction Survey"
                theEmail.Body = result.GetSurveyHTML
                theEmail.BodyIsHtml = True
                theEmail.Send()

                Session("currSurveyRequestID") = Nothing
                Session("currSurveyRequestEmail") = request.EmailAddress
            End If

            Response.Redirect("~/Final/frmThanks.aspx")
        End If
  End Sub
  Private Function ScreenFieldsAreValid() As Boolean
    Dim valid As Boolean = True
    Dim errMsg As String = ""

    If Me.cmbProjectRole.SelectedIndex = -1 Then
      valid = False
      errMsg += "A Project Role must be selected.  If you are unsure, please select 'Other'."
    Else
      If Me.cmbProjectRole.SelectedValue = 0 Then
        valid = False
        errMsg += "A Project Role must be selected.  If you are unsure, please select 'Other'."
      End If
    End If

    If Not valid Then
      Me.lblMessage.Text = errMsg
      Me.lblMessage.Visible = True
    Else
      Me.lblMessage.Text = ""
      Me.lblMessage.Visible = False
    End If

    Return valid
  End Function
  Private Sub LoadQuestions()
    Me.btnSubmitSurvey.Attributes.Add("onclick", "javascript:return confirm('Please confirm that you wish to Submit this Survey?')")
    Me.lblRequest.Text = "With regards to the above project, please provide your opinion (and any applicable comments) to the following:"

    Dim theMgr As New pmoManager
    Dim theList As SurveyQuestionList = theMgr.GetActiveQuestions

    If theList.Count > 0 Then
      Dim currQ As Integer = 1
      For Each q As SurveyQuestion In theList
        Dim tmpControl1 As UserControl = LoadControl("ucQuestion.ascx")
        CType(tmpControl1, ucQuestion).SetQuestionValue(q.QuestionId)
        CType(tmpControl1, ucQuestion).SetBackground(Drawing.ColorTranslator.FromHtml("#EEEEEE"))
        'If currQ Mod 2 <> 0 Then
        '  CType(tmpControl1, ucQuestion).SetBackground()
        'Else
        '  CType(tmpControl1, ucQuestion).SetBackground(Drawing.Color.DarkKhaki)
        'End If

        Me.surveyQuestions.Controls.Add(tmpControl1)
      Next
    End If
  End Sub
  Private Sub LoadQuestions_NoJavascript()
    'Me.btnSubmitSurvey.Attributes.Add("onclick", "javascript:return confirm('Please confirm that you wish to Submit this Survey?')")
    Me.lblRequest.Text = "With regards to the above project, please provide your opinion (and any applicable comments) to the following:"

    Dim theMgr As New pmoManager
    Dim theList As SurveyQuestionList = theMgr.GetActiveQuestions

    If theList.Count > 0 Then
      Dim currQ As Integer = 1
      For Each q As SurveyQuestion In theList
        Dim tmpControl1 As UserControl = LoadControl("ucQuestion_NoJavaScript.ascx")
        CType(tmpControl1, ucQuestion_NoJavaScript).SetQuestionValue(q.QuestionId)
        CType(tmpControl1, ucQuestion_NoJavaScript).SetBackground(Drawing.ColorTranslator.FromHtml("#EEEEEE"))
        'If currQ Mod 2 <> 0 Then
        '  CType(tmpControl1, ucQuestion).SetBackground()
        'Else
        '  CType(tmpControl1, ucQuestion).SetBackground(Drawing.Color.DarkKhaki)
        'End If

        Me.surveyQuestions.Controls.Add(tmpControl1)
      Next
    End If
  End Sub
  Protected Sub btnSubmitSurvey_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSubmitSurvey.Click
    SubmitSurvey()
  End Sub

  Protected Sub lbNoSlider_Click(ByVal sender As Object, ByVal e As EventArgs) Handles lbNoSlider.Click
    Session("JSEnabled") = False
    Me.surveyQuestions.Controls.Clear()
    LoadQuestions_NoJavascript()
    Me.lbNoSlider.Visible = False
  End Sub
End Class