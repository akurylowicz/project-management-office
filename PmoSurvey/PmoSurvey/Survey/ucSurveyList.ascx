﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="ucSurveyList.ascx.vb" Inherits="PmoSurvey.ucSurveyList" %>
<asp:Label ID="lblListName" runat="server" CssClass="normalLabel"></asp:Label>
<br />
<asp:GridView ID="gvSurveyList" runat="server" AutoGenerateColumns="False">
    <RowStyle BackColor="#F7F6F3" ForeColor="#333333" CssClass="normalLabel" />
    <Columns>
        <asp:BoundField DataField="RequestId" HeaderText="Request Id">
        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Bottom" />
        <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" />
        </asp:BoundField>
        <asp:ButtonField DataTextField="ProjectName" HeaderText="Project Description" 
            Text="ProjectName" CommandName="RequestLink">
        <HeaderStyle HorizontalAlign="Left" VerticalAlign="Bottom" Width="400px" />
        <ItemStyle VerticalAlign="Top" />
        </asp:ButtonField>
        <asp:BoundField DataField="CustomerName" HeaderText="Customer">
        <HeaderStyle HorizontalAlign="Left" VerticalAlign="Bottom" Width="200px" />
        <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
        </asp:BoundField>
        <asp:BoundField DataField="RequestDateForDisplay" HeaderText="Requested Date">
        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Bottom" />
        <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" />
        </asp:BoundField>
        <asp:BoundField DataField="FollowUpDateForDisplay" HeaderText="Follow Up Date">
        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Bottom" />
        <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" />
        </asp:BoundField>
    </Columns>
    <HeaderStyle BackColor="#5D7B9D" ForeColor="White" CssClass="whiteLabel" />
    <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
</asp:GridView>

