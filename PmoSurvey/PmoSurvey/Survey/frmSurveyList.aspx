﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmSurveyList.aspx.vb" Inherits="PmoSurvey.frmSurveyList" %>

<%@ Register src="../ucControls/ucHeader.ascx" tagname="ucHeader" tagprefix="uc1" %>

<%@ Register src="ucSurveyList.ascx" tagname="ucSurveyList" tagprefix="uc2" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>So, what did you think? OST Client Satisfaction Survey</title>
    <link href=../ost.css rel=Stylesheet />
    <script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-17261996-2']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:Panel runat="server" ID="centerPanel" CssClass="centeringPanel">
    
        <uc1:ucHeader ID="ucHeader1" runat="server" />
    
    <br />
    <uc2:ucSurveyList ID="ucSurveyList1" runat="server" />
    </asp:panel>
    </form>
</body>
</html>
