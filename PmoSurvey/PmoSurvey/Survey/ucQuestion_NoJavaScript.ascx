﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="ucQuestion_NoJavaScript.ascx.vb" Inherits="PmoSurvey.ucQuestion_NoJavaScript" %>
<asp:Panel ID="pnlSurround" runat="server" Width="100%">
<table width="100%">
    <tr>
        <td width="20%">
            <asp:Label ID="lblQuestion" runat="server" CssClass="normalLabel"></asp:Label>
        </td>
        <td align="right" width="10%">
            &nbsp;</td>
        <td align ="center" width="30%">
            <asp:DropDownList ID="cmbScore" runat="server">
              <asp:ListItem>1 (Most Negative)</asp:ListItem>
              <asp:ListItem>2</asp:ListItem>
              <asp:ListItem>3</asp:ListItem>
              <asp:ListItem>4</asp:ListItem>
              <asp:ListItem Value="5">5 - Neutral</asp:ListItem>
              <asp:ListItem>6</asp:ListItem>
              <asp:ListItem>7</asp:ListItem>
              <asp:ListItem>8</asp:ListItem>
              <asp:ListItem>9</asp:ListItem>
              <asp:ListItem Value="10">10 - Most Positive</asp:ListItem>
            </asp:DropDownList>
        </td>
        <td align="left" width="10%">
            &nbsp;</td>
        <td width="5%" align="center">
            &nbsp;</td>
        <td align="right" width="25%">
          <asp:CheckBox ID="cbNotApplicable" runat="server" CssClass="normalField" 
            Text="Not Applicable / No Answer" />
        </td>
    </tr>
    <tr>
        <td align="right" valign="top">
            <asp:Label ID="lblComment" runat="server" Enabled="False" Font-Italic="False" 
              CssClass="normalLabel">Comment</asp:Label>
        </td>
        <td colspan="5">
            <asp:TextBox ID="txtComment" runat="server" MaxLength="500" 
                TextMode="MultiLine" Width="99%" CssClass="normalField"></asp:TextBox>
        </td>
    </tr>
</table>
<hr />
</asp:Panel>