﻿Imports pmoLib
Partial Public Class ucQuestion_NoJavaScript
  Inherits System.Web.UI.UserControl

  Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    If Not Page.IsPostBack Then
      'Me.txtScore_SliderExtender.Minimum = 1
      'Me.txtScore_SliderExtender.Maximum = 10
    End If
  End Sub
  Public Sub SetQuestionValue(ByVal questionId As Integer)
    If questionId > 0 Then
      Dim question As New SurveyQuestion(questionId)
      Me.lblQuestion.Text = question.QuestionText
      Me.lblQuestion.Attributes("questionId") = question.QuestionId
      Me.lblQuestion.Attributes("displayOrder") = question.DisplayOrder
      Me.txtComment.Attributes("questionId") = question.QuestionId
      Me.cmbScore.Attributes("questionId") = question.QuestionId
    End If
  End Sub
  Public Sub SetBackground(ByVal colorOn As Drawing.Color)
    Me.pnlSurround.BackColor = colorOn
  End Sub
  Public ReadOnly Property QuestionId() As Integer
    Get
      Return Me.lblQuestion.Attributes("questionId")
    End Get
  End Property
  Public ReadOnly Property DisplayOrder() As Integer
    Get
      Return Me.lblQuestion.Attributes("displayOrder")
    End Get
  End Property
  Public ReadOnly Property QuestionScore() As Integer
    Get
      If Me.cbNotApplicable.Checked = True Then
        Return 0
      Else
        Return Me.cmbScore.SelectedValue
      End If
    End Get
  End Property
  Public ReadOnly Property QuestionComment() As String
    Get
      Return Me.txtComment.Text
    End Get
  End Property

End Class