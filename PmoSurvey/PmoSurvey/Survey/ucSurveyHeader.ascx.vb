﻿Imports pmoLib
Partial Public Class ucSurveyHeader
  Inherits System.Web.UI.UserControl

  Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
    If Not Page.IsPostBack Then
      LoadProjectInfo()
    End If
  End Sub

  Private Sub LoadProjectInfo()
        If Not Session("currSurveyRequestID") Is Nothing Then
            Dim requestId As Integer = Session("currSurveyRequestID")
            If requestId > 0 Then
                Dim tmpRequ As New CpSurveyRequest(requestId)
                If Not tmpRequ Is Nothing Then
                    'Dim tmpProj As New CpProject(tmpRequ.ProjectId)
                    'If Not tmpProj Is Nothing Then
                    Me.lblId.Text = tmpRequ.Title
                    Me.lblCustomer.Text = tmpRequ.Customer
                    Me.lblSurveyRequested.Text = tmpRequ.RequestDate.ToShortDateString & " from: " & tmpRequ.EmailAddress
                    'End If
                End If
            Else
                Session("currSurveyRequestID") = Nothing
                Response.Redirect("~/Default.aspx")
            End If
        End If
  End Sub

End Class