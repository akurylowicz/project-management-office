﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="ucGatherSurveyData.ascx.vb" Inherits="PmoSurvey.ucGatherSurveyData" %>
<%@ Register src="ucQuestion.ascx" tagname="ucQuestion" tagprefix="uc1" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
</asp:ToolkitScriptManager>
<asp:Panel ID="pnlSurround" runat="server" Width="100%" BackColor="#70A4AF">
  <table  width="100%">
     <tr>
        <td width="25%" align="left">
            <asp:Label ID="lblRequest0" runat="server" CssClass="whiteLabel">Your Role: </asp:Label>
        </td>
        <td align="left">
            <asp:DropDownList ID="cmbProjectRole" runat="server" CssClass="normalField">
            </asp:DropDownList>
            <asp:Label ID="lblMessage" runat="server" CssClass="messageLabel"></asp:Label>
        </td>
     </tr>
     <tr>
        <td valign="top" align="left">
          <asp:Label ID="lblRequest1" runat="server" CssClass="whiteLabel">Overall Comments: </asp:Label>
        </td>
        <td align="left">
            <asp:TextBox ID="txtProjectComments" runat="server" Height="65px" 
                    MaxLength="1000" TextMode="MultiLine" Width="99%" 
              CssClass="normalField"></asp:TextBox>
        </td>
     </tr>
  </table>
  </asp:Panel>
<table width="100%">
  <tr>
  <td width="2%" align="right" colspan="3">
    <asp:LinkButton ID="lbNoSlider" runat="server" CssClass="pmoLink">(Click here if you cannot see a Slider below...)</asp:LinkButton>
    </td>
  </tr>
  <tr>
  <td width="2%" align="left" colspan="3">
<asp:Label ID="lblRequest" runat="server" CssClass="normalLabel"></asp:Label>
    </td>
  </tr>
  <tr>
  <td width="2%"></td>
    <td>
      <asp:PlaceHolder ID="surveyQuestions" runat="server"></asp:PlaceHolder>
    </td>
    <td width="2%"></td>
  </tr>
<tr>
<td width="2%"></td>
<td align="center">
  <asp:Button ID="btnSubmitSurvey" runat="server" Text="Submit Survey" 
    CssClass="normalLabel" />
  </td>
<td width="2%"></td>
</tr>
</table>
