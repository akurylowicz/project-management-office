﻿Imports pmoLib
Partial Public Class ucSurveyList
  Inherits System.Web.UI.UserControl

  Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
    If Not Page.IsPostBack Then
      LoadSurveyList()
    End If
  End Sub
  Private Sub LoadSurveyList()
        Me.lblListName.Text = "Current Survey Requests for: " & Session("currSurveyRequestEmail")
        Dim theMgr As New pmoManager
        Dim theList As CpSurveyRequestList
        theList = theMgr.GetAllCpSurveyRequestList(Session("currSurveyRequestEmail"))
        Me.gvSurveyList.DataSource = theList
        Me.gvSurveyList.DataBind()
  End Sub

  Private Sub gvSurveyList_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvSurveyList.RowCommand
    Select Case e.CommandName
      Case "RequestLink"
        Dim tmpReq As Integer = CInt(Me.gvSurveyList.Rows(e.CommandArgument).Cells(0).Text)
        If tmpReq > 0 Then
          Session("currSurveyRequestID") = tmpReq
          Response.Redirect("~/Survey/frmSurvey.aspx")
        End If
    End Select
  End Sub
End Class