﻿Imports pmoLib
Partial Public Class frmShowSurvey
  Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Context.Session Is DBNull.Value Then
            If Session.IsNewSession Then
                Dim szCookieHeader As String = Request.Headers("Cookie")
                If Not szCookieHeader Is Nothing Then
                    If Not szCookieHeader Is DBNull.Value And szCookieHeader.IndexOf("ASP.NET_SessionId") >= 0 Then
                        Response.Redirect("~/Security/frmTimeOut.aspx")
                    End If
                End If
            End If
        End If
        Dim requestId As Integer = Session("currSurveyRequestID")
        Dim result As New CpSurveyResult(requestId)
        Me.Literal1.Text = result.GetSurveyHTML
    End Sub

End Class